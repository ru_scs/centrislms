// We're just using angular to get the api endpoint to use when saving/getting the annotations 
var apiUrl = angular.module("apiUrl", ["config"]);

apiUrl.controller('apiUrlCtrl', ['ENV', apiUrlCtrl]);

function apiUrlCtrl(env) {
	Annotator.apiUrl = env.apiEndpoint;
}

// The annotator object is utilized by external code (pdf.js). 
// For each page loaded loadAnnotations(pageNumber) is called on this object.
// This object also contains all the non-canvas text objects. That is, the ones inside the text bubbles.
// TODO: This object should likely not contain any type of annotation objects. It should be a collection of general 
// annotation functions. Such as loadAnnotations(pageNumber), which loads all annotations for a given page. (It already has this).
// Also isTeacher, which tells us whether the user who has the pdf open is a teacher or not. 
// As for where the text objects should go, they should likely go in a common object.
//  E.g. we should move towards a solution where all annotations are stored in one object, given that they all have enough in common.  
var Annotator = {
	isTeacher:         false,
	movingObjectIndex: -1,
	movingObject:      null,
	movingStartPoint:  {},
	// An object looks like the following: 
	// { "top": "3px", "left": "3px", "pageid" : "pageContainer1", "annotation" : "text" }
	// pageid is the ID the div that contains the page the annotation is on. The last character is the page number. 
	textObjects: [], 
	// Gets the object at the given coordinates ("3px", "4px"). Used for instance when the  user clicks and we want to know which object he clicked. 
	getAnnotationIndex: function(top, left) {
		for (var i = 0; i < this.textObjects.length; i++) {
			if (this.textObjects[i].left === left && this.textObjects[i].top === top) {
				return i;
			} 
		}

		return -1; 
	},
	// Deletes the annotation object with the given index in the array. 
	deleteAnnotation: function(index) {
		this.textObjects.splice(index, 1);
	},
	// Clears the canvas at the given page number. 
	clearCanvas: function(pageNumber) {
		var $canvas = $("canvas.overlay.page" + pageNumber);
		var context = $canvas[0].getContext("2d");
		context.clearRect(0, 0, $canvas.width(), $canvas.height());	
	}
};

$( document ).ready(function() {
	// Set up our toastr
	// Will be used to display an error when we fail to save/get annotations
	toastr.options = {
		"closeButton" : false,
		"debug" : false,
		"newestOnTop" : false,
		"progressBar" : false,
		"positionClass" : "toast-top-center",
		"preventDuplicates" : true,
		"onclick" : null,
		"showDuration" : "300",
		"hideDuration" : "1000",
		"timeOut" : "5000",
		"extendedTimeOut" : "1000",
		"showEasing" : "swing",
		"hideEasing" : "linear",
		"showMethod" : "fadeIn",
		"hideMethod" : "fadeOut" 
	};

	// Belongs in the annotation object which contains the chat bubbles. Tells us whether the user is currently entering an annotation.
	// E.g. whether an annotation bubble is open.
	var enteringAnnotation = false;
	// Tells us whether the move tool is being used. Belongs in the Annotator object. It is common to all.
	var moveToolActive = false;
	// Tells us whether the text tool is active, e.g. the annotation chat bubbles. Belongs in the object containing those annotations.
	var textToolActive = true;

	// This contains all the pen objects. E.g. all the objects drawn with the pen tool.
	// TODO: Combine this object with the other annotation objects.
	var drawing = {
		penActive: false,
		// pageContainer is the div containing the annotation on the page it's on. The last character is the number of the page.
		penObjects: [], // [ { "points" : [ {"left" : x, "top" : y } ], "pageContainer" : "pageContainer1", "color" : "#000" } ]
		// The shape that is currently being drawn.
		// Points added to this on the fly, as we move the pen. 
		shapeBeingDrawn: {}, // { "points" : [ {"left" : x, "top" : y } ], "pageContainer" : "pageContainer1" }
		canvas: null, // TODO: Remove canvas and context. I believe it's not necessary. 
		context: null,
		isDrawing: false, // Tells us whether we're currently drawing
		currentCanvas: null,
		currentContext: null,
		LEEWAY: 3, // This is some constant. When we're selecting a pen object, this tells us how far off we can be for the object to be selected.  
		selectedObjectIndex: -1, // The array index of the selected object. 
		movingObjectIndex: -1, // The array index of the moving object. 
		movingObjectPageNumber: -1, // The page number of the object that we're moving
		// The point where the user clicked when he began moving the object. 
		// We calculate the amount the object moves relative tot his  
		movingStartPoint: {}, // { "left" : "3px", "top" : "1px" } 
		// Given a point p: { "left" : "2px", "top" : "4px" }, we return the index of the object the point belongs to
		hasPoint: function(p) {
			for (var i = 0; i < this.penObjects.length; i++) {
				if (this.penObjects[i].pageContainer === p.pageContainer) {
					//Go through all the points in the object
					for (var j = 0; j < this.penObjects[i].points.length; j++) {
						if (p.left >= (this.penObjects[i].points[j].left - this.LEEWAY) && p.left <= (this.penObjects[i].points[j].left + this.LEEWAY)) {
							if (p.top >= (this.penObjects[i].points[j].top - this.LEEWAY) && p.top <= (this.penObjects[i].points[j].top + this.LEEWAY)) {
								return i;
							}
						}
					}
				}
			}
			return -1;
		}, 
		// Clears the canvas for the given pageNumber and redraws all canvas shapes
		// TODO: should be moved to the annotator object. It is common for all shapes on the canvas.  
		redrawObjects: function(pageNumber) {
			Annotator.clearCanvas(pageNumber);
			var annotations = {};
			annotations.annotations = drawing.penObjects;
			ReconstructAnnotations(annotations, "drawing", pageNumber);
			annotations.annotations = canvasText.objects;
			ReconstructAnnotations(annotations, "canvasText", pageNumber);
		},
		// Deletes the pen object that is currently selected. 
		deleteSelectedObject: function() { 
			if (drawing.selectedObjectIndex !== -1) {
				// TODO: Make a function that grabs the page number from the pageContainer ID. 
				var pageNumber = Number(drawing.penObjects[drawing.selectedObjectIndex].pageContainer.substring("pageContainer".length));
				drawing.penObjects.splice(drawing.selectedObjectIndex, 1);
				drawing.selectedObjectIndex = -1; 
				$(".delete-tool").addClass("annotator-hidden");
				drawing.redrawObjects(pageNumber);
				SaveAnnotations();
			}
		}
	};

	// TODO: Combine this object with the drawing object, if it has enough in common.
	// This object contains all the canvas text objects. That is, the text annotations which are displayed directly, and not in a chat bubble.   
	var canvasText = {
		// The pageContainer in this object as in the objects above is the ID of the div containing the annotation. 
		// The last character of it is the number of the page the annotation is on. 
		objects: [], // { "top" : 4, "left" : 4, "text" : "Annotation", "pageContainer", width: "3", height: "3", "color" : "#000" }
		isActive: false, // Tells us whether this tool is active
		enteringAnnotation: false, // Tells us whether we're currently entering an annotation
		// Given a point p: { "left" : 4, "top" : 4 }, tells us the index of the object the user clicked (if any). 
		hasPoint: function(p) {
			for (var i = 0; i < this.objects.length; i++) {
				if (p.left >= this.objects[i].left && 
					p.top >= this.objects[i].top && 
					p.left <= (this.objects[i].left + this.objects[i].width) &&
					p.top <= (this.objects[i].top + this.objects[i].height)) {
						return i; 
				}
			}

			return -1; 
		},
		movingObjectIndex: -1, // The array index of the moving object
		movingObjectPageNumber: -1, // The page number of the moving object
		// The start point from which the user started to move the object
		// { "left" : 2, "top" : 2 }, used to calculate how much the user moved the object. 
		movingStartPoint: {}, 
		// The array index of the selected object. 
		selectedObjectIndex: -1,
		// Deletes the selected canvas text object
		deleteSelectedObject: function() {
			if (canvasText.selectedObjectIndex !== -1) {
				var pageNumber = Number(canvasText.objects[canvasText.selectedObjectIndex].pageContainer.substring("pageContainer".length));
				canvasText.objects.splice(canvasText.selectedObjectIndex, 1);
				canvasText.selectedObjectIndex = -1; 
				$(".delete-tool").addClass("annotator-hidden");
				drawing.redrawObjects(pageNumber);
				SaveAnnotations();
			}
		}
	};

	// We start by hiding all the tools
	// If the user is a teacher, we will display them below. 
	$("button.tool").hide(); 
	$(".color-picker").hide(); 

	// Helper function. Used to get some parameter from the query string by name. 
	// TODO: Move this into some helper object. 
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	// Inserts an annotation bubble in the clicked location, if we're inserting bubbles. 
	$("body").on("click", ".textLayer", function(e) {
		// TODO: Refactor selection
		if (!enteringAnnotation && textToolActive && Annotator.isTeacher && drawing.selectedObjectIndex === -1 && canvasText.selectedObjectIndex === -1) {
			enteringAnnotation = true; 

			// TODO: This is common code to get the clicked point from within the textLayer. Put it in a helper function perhaps.  
			var parentOffset = $(this).parent().offset();
			// Magic number 9. I believe this was some margin above/left of the element. 
			var relX = e.pageX - (parentOffset.left + 9);
			var relY = e.pageY - (parentOffset.top + 9); 

			var $textLayer = $(this); 
			CreateTextAnnotation(relX, relY, $textLayer, null); 
		}
	}); 

	// Saves the text annotation, the one that is i the currently open bubble. 
	$("body").on("click", ".btn-save", function(event) {
		if (Annotator.isTeacher) {
			event.stopPropagation();

			var $container = $(this).closest(".annotation-container");

			var textObject = {
				"top" : $container.css("top"),
				"left" : $container.css("left"),
				"pageid" : $(this).closest(".page").attr("id"),
				"annotation" : $(this).parent().find("textarea").val()
			};

			Annotator.textObjects.push(textObject);

			// Stop displaying the text part of the annotation.
			$(".annotation-container.enter-comment").removeClass("enter-comment");
			SaveAnnotations();
			enteringAnnotation = false;
		} 
	}); 

	// Deletes the open annotation from the view.. (bubble annotation) 
	$("body").on("click", ".btn-cancel", function(event) {
		if (Annotator.isTeacher) {
			event.stopPropagation();

			var top = $(".annotation-container.enter-comment").css("top");
			var left = $(".annotation-container.enter-comment").css("left");
			var annotationIndex = Annotator.getAnnotationIndex(top, left);

			if (annotationIndex !== -1) {
				Annotator.deleteAnnotation(annotationIndex);
			}

			$(".annotation-container.enter-comment").remove();
			SaveAnnotations();
			enteringAnnotation = false;
		} 
	}); 

	// Opens the clicked annotation bubble, allows the user to change the annotation
	$("body").on("click", ".chat-bubble", function(event) {
		if (!enteringAnnotation && textToolActive) {
			enteringAnnotation = true; 
			event.stopPropagation();
			$(this).closest(".annotation-container").addClass("enter-comment");
			$(".annotation-container.enter-comment textarea").focus();
		}
	});

	// When the user uses mousedown on a chat-bubble, if the move tool is on, we want to grab that bubble and the start point to move from. 
	$("body").on("mousedown", ".chat-bubble", function(event) {
		if (moveToolActive) {
			var $container = $(this).closest(".annotation-container");
			$container.find(".annotation-bubble").addClass("annotation-selection-highlight");
			Annotator.movingObject = $container;

			var top = Annotator.movingObject.css("top");
			var left = Annotator.movingObject.css("left");

			var index = Annotator.getAnnotationIndex(top, left);
			Annotator.movingObjectIndex = index;

			var parentOffset = $(this).closest(".textLayer").parent().offset(); 

			Annotator.movingStartPoint = {
				"left" : event.pageX - (parentOffset.left + 9),
				"top" : event.pageY - (parentOffset.top + 9)  
			};
		} 
	});

	// The X button on the annotaiton chat bubbles. We want to stop displaying the given annotation text when this is clicked. 
	$("body").on("click", ".annotation-container.enter-comment .fa-times", function(event) {
		event.stopPropagation();
		$(this).closest(".annotation-container").removeClass("enter-comment");
		enteringAnnotation = false; 
	});

	// Activates the clicked tool
	// Some tools require all text on the page to become unselectable while the tool is in use.
	// TODO: Refactor 
	$(".tool").on("click", function(e) {
		e.preventDefault();
		if ($(this).hasClass("pen-tool")) {
			drawing.penActive = true; 
			$("body").addClass("unselectable");
			$("body").addClass("pen-active"); 
		} else {
			$("body").removeClass("unselectable");
			$("body").removeClass("pen-active");
			drawing.penActive = false;   
		}

		if ($(this).hasClass("delete-tool")) {
			if (drawing.selectedObjectIndex !== -1) {
				drawing.deleteSelectedObject();
			} else if (canvasText.selectedObjectIndex !== -1) {
				canvasText.deleteSelectedObject();	
			}
		} 

		if ($(this).hasClass("move-tool")) { 
			moveToolActive = true; 
			$("body").addClass("unselectable move-active");
		} else {
			moveToolActive = false;
			$("body").removeClass("unselectable move-active");
		} 

		if ($(this).hasClass("text-tool")) {
			textToolActive = true; 
		} else {
			textToolActive = false; 
		}

		if ($(this).hasClass("canvas-text-tool")) {
			canvasText.isActive = true;
		} else {
			canvasText.isActive = false;
		}

		enteringAnnotation = false;
		$(this).parent().find("button").removeClass("active");
		$(this).addClass("active");
	});
	
	$(".color-picker").on("change", function() {
		// If any canvas-text inputs are open, change the font color.
		$(".canvas-text").css("color", $(this).val());
	});
	
	$("body").on("mousedown", ".textLayer", function(e) {
		// First we attempt to select an object
		if (!canvasText.enteringAnnotation && !drawing.isDrawing && !moveToolActive && Annotator.isTeacher && !enteringAnnotation) {
			var parentOffset = $(this).parent().offset();
			 
			var p = {};
			p.left = e.pageX - (parentOffset.left + 9);
			p.top = e.pageY - (parentOffset.top + 9);
			p.pageContainer = $(this).closest(".page").attr("id");
			var objectIndex = -1;

			// TODO: Refactor. Haspoint is certainly a common method, once the drawing/canvasText object instance have been combined for instance this could be made smaller.
			if ( (objectIndex = drawing.hasPoint(p)) !== -1 ) {
				drawing.selectedObjectIndex = objectIndex;
				canvasText.selectedObjectIndex = -1;
				drawing.redrawObjects(Number(p.pageContainer.substring("pageContainer".length))); 
				$(".delete-tool").removeClass("annotator-hidden");
				return;
			} else if ( (objectIndex = canvasText.hasPoint(p)) !== -1) {
				canvasText.selectedObjectIndex = objectIndex;
				drawing.selectedObjectIndex = -1;
				drawing.redrawObjects(Number(p.pageContainer.substring("pageContainer".length)));
				$(".delete-tool").removeClass("annotator-hidden");
				return;
			} else {
				drawing.selectedObjectIndex = -1;
				canvasText.selectedObjectIndex = -1;
				drawing.redrawObjects(Number(p.pageContainer.substring("pageContainer".length)));
				$(".delete-tool").addClass("annotator-hidden");
			}
		}

		// If we're drawing 
		if (drawing.penActive && Annotator.isTeacher) {
			var newShape = { 
				"points" : [],
				"pageContainer" : $(this).closest(".page").attr("id"),
			};

			drawing.selectedObjectIndex = -1;
			$(".delete-tool").addClass("annotator-hidden");
			drawing.shapeBeingDrawn = newShape;
			var parentOffset = $(this).parent().offset();
			// TODO: Common code again for getting the clicked point, should go to a helper method on a helper object perhaps.
			var relX = e.pageX - (parentOffset.left + 9);
			var relY = e.pageY - (parentOffset.top + 9); 
			drawing.penActive = true; // User is starting to write.. 
			drawing.currentCanvas = $(this).parent().find(".canvasWrapper canvas.overlay");
			drawing.currentContext = drawing.currentCanvas[0].getContext("2d");
			drawing.currentContext.beginPath(); //Closes the prev. path if it exists
			drawing.currentContext.strokeStyle = $("input.color-picker").val() || "#000";
			drawing.shapeBeingDrawn.color = drawing.currentContext.strokeStyle;
			drawing.currentContext.moveTo(relX, relY); // Move the pen to these coords
			drawing.currentContext.fillRect(relX,relY,1,1); // Draw
			drawing.currentContext.stroke();
			drawing.isDrawing = true;
			drawing.shapeBeingDrawn.points.push({
				"left" : relX,
				"top" : relY
			});
		}

		// Find a shape to move
		if (moveToolActive) {
			// TODO: Common code for getting clicked point, put in helper method on a helper object. 
			var parentOffset = $(this).parent().offset();
			var left = e.pageX - (parentOffset.left + 9);
			var top = e.pageY - (parentOffset.top + 9);
			var p = {
				"left" : left,
				"top" : top,
				"pageContainer" : $(this).closest(".page").attr("id")
			};

			var objectIndex = -1; 
			// TODO: Refactor. Again, hasPoint for instance is ccommon, once canvasText/drawing are combined this could be made smaller. 
			if ( (objectIndex = canvasText.hasPoint(p)) !== -1) {
				canvasText.movingObjectIndex = objectIndex;
				canvasText.movingObjectPageNumber = Number($(this).closest(".page").attr("id").substring("pageContainer".length)); 
				canvasText.movingStartPoint = p; 
			} else if ( (objectIndex = drawing.hasPoint(p)) !== -1) {
				drawing.movingObjectIndex = objectIndex;
				drawing.movingObjectPageNumber = Number($(this).closest(".page").attr("id").substring("pageContainer".length));
				drawing.movingStartPoint = p;
			}
		}  
	});

	$("body").on("mousemove", ".textLayer", function(e) {
		// If the user is drawing, draw.. 
		if (drawing.isDrawing && Annotator.isTeacher) {
			// TODO: Common code for getting clicked point, put in helper method on helper object. 
			var parentOffset = $(this).parent().offset(); 
			var relX = e.pageX - (parentOffset.left + 9);
			var relY = e.pageY - (parentOffset.top + 9); 
			drawing.currentContext.lineTo(relX, relY);
			drawing.currentContext.stroke();
			drawing.shapeBeingDrawn.points.push({
				"left" : relX, 
				"top" : relY 
			});
		}

		// Move text bubble
		if (moveToolActive && Annotator.movingObjectIndex !== -1) {
			var fromLeft = Annotator.movingStartPoint.left;
			var fromTop =  Annotator.movingStartPoint.top;
			// TODO: Common code for getting clicked point, put in helper method on helper object.
			var parentOffset = $(this).parent().offset(); 
			var toLeft = e.pageX - (parentOffset.left + 9);
			var toTop = e.pageY - (parentOffset.top + 9);

			var leftDiff = toLeft - fromLeft;
			var topDiff = toTop - fromTop;

			Annotator.movingStartPoint.left += leftDiff;
			Annotator.movingStartPoint.top += topDiff;

			var newTop = parseInt(Annotator.movingObject.css("top"), 10) + topDiff;
			var newLeft = parseInt(Annotator.movingObject.css("left"), 10) + leftDiff;

			Annotator.movingObject.css("top", newTop + "px");
			Annotator.movingObject.css("left", newLeft + "px");
		} 

		// Move canvas text object TODO: Refactor. This could probably be shortened by combing the canvasText / drawing objects. 
		if (moveToolActive && canvasText.movingObjectIndex !== -1) {
			var fromLeft = canvasText.movingStartPoint.left;
			var fromTop =  canvasText.movingStartPoint.top;

			// TODO: Common code for getting clicked point, put in helper method on helper object.
			var parentOffset = $(this).parent().offset(); 
			var toLeft = e.pageX - (parentOffset.left + 9);
			var toTop = e.pageY - (parentOffset.top + 9);

			var leftDiff = toLeft - fromLeft;
			var topDiff = toTop - fromTop;

			canvasText.movingStartPoint.left += leftDiff;
			canvasText.movingStartPoint.top += topDiff;

			canvasText.objects[canvasText.movingObjectIndex].left += leftDiff;
			canvasText.objects[canvasText.movingObjectIndex].top += topDiff;
			drawing.redrawObjects(canvasText.movingObjectPageNumber);
		} else if (moveToolActive && drawing.movingObjectIndex !== -1) { // Move the pen object 
			var fromLeft = drawing.movingStartPoint.left;
			var fromTop =  drawing.movingStartPoint.top;

			// TODO: Common code for getting clicked point, put in helper method on helper object.
			var parentOffset = $(this).parent().offset(); 
			var toLeft = e.pageX - (parentOffset.left + 9);
			var toTop = e.pageY - (parentOffset.top + 9);

			var leftDiff = toLeft - fromLeft;
			var topDiff = toTop - fromTop;

			drawing.movingStartPoint.left += leftDiff;
			drawing.movingStartPoint.top += topDiff;

			for (var i = 0; i < drawing.penObjects[drawing.movingObjectIndex].points.length; i++) {
				drawing.penObjects[drawing.movingObjectIndex].points[i].left += leftDiff;
				drawing.penObjects[drawing.movingObjectIndex].points[i].top += topDiff;
			}

			drawing.redrawObjects(drawing.movingObjectPageNumber); 
		}
	});

	$("body").on("mouseup", ".textLayer", function(e) {
		if ($(e.target).hasClass("canvas-text")) {
			return; 
		}

		// If the user was drawing, we will add this object to our list of objects 
		if (drawing.penActive && drawing.isDrawing && Annotator.isTeacher) {
			drawing.penObjects.push(drawing.shapeBeingDrawn);
			drawing.isDrawing = false;
			SaveAnnotations();
		}

		// Add an input box for the canvas text, e.g. the text that is displayed directly on the canvas.
		if (canvasText.isActive && !canvasText.enteringAnnotation && drawing.selectedObjectIndex === -1 && canvasText.selectedObjectIndex === -1) {
			var $textLayer = $(this);
			// TODO: Common code for getting clicked point, put in helper method on helper object. 
			var parentOffset = $(this).parent().offset();
			var p = {};
			p.left = e.pageX - (parentOffset.left + 9);  //Magic number 9. I believe this was some margin above/left of the element.
			p.top = e.pageY - (parentOffset.top + 9);
			p.pageContainer = $(this).closest(".page").attr("id");

			var $textArea = $("<textarea/>");
			$textArea.addClass("canvas-text");
			$textArea.attr("placeholder", "Write a comment");
			$textArea.css("left", p.left);
			$textArea.css("top", p.top);
			var currentColor = $(".color-picker").val() || "#000";
			$textArea.css("color", currentColor);
			$textLayer.append($textArea);
			$textArea.focus();
			canvasText.enteringAnnotation = true; 
		} else if (canvasText.isActive) { // If we were entering an annotation and we're clicking the textLayer again, we stop entering and save it.
			var $textArea = $("textarea.canvas-text");
			var text = $textArea.val();
			var left = parseInt($textArea.css("left"));
			var top = parseInt($textArea.css("top"));

			$textArea.remove();
			canvasText.enteringAnnotation = false; 
			var $canvas = $(this).parent().find("canvas.overlay");
			var ctx = $canvas[0].getContext("2d");

			// TODO: Keep the font-size in a variable in the canvasText object
			ctx.beginPath();
			ctx.fillStyle = $("input.color-picker").val() || "#000"; 
			ctx.font = "13.3333px monospace";
			ctx.textBaseline = "top";
			var newObject = {
				"top" : top,
				"left" : left, 
				"text" : text, 
				"pageContainer" : $(this).closest(".page").attr("id"),
				"color" : ctx.fillStyle
			};

			var lineHeight = ctx.measureText("M").width * 2;
			var lines = text.split("\n");
			var widestLine = 0, tmpWidth = 0, height = 0; 

			for (var i = 0; i < lines.length; ++i) {
				ctx.fillText(lines[i], left, top);
				top += lineHeight; 
				height += lineHeight;
				tmpWidth = ctx.measureText(lines[i]).width;

				if (tmpWidth > widestLine) {
					widestLine = tmpWidth;
				}
			}

			newObject.height = height; 
			newObject.width = widestLine;
			canvasText.objects.push(newObject);

			SaveAnnotations();
		}

		// If we're in the process of moving a text-tool object, we stop moving it and save the new location 
		if (moveToolActive && Annotator.movingObjectIndex !== -1) {
			var top = Annotator.movingObject.css("top");
			var left = Annotator.movingObject.css("left");
			Annotator.textObjects[Annotator.movingObjectIndex].top = top;
			Annotator.textObjects[Annotator.movingObjectIndex].left = left; 
			Annotator.movingObjectIndex = -1;
			Annotator.movingObject = null;
			$(".annotation-selection-highlight").removeClass("annotation-selection-highlight");
			SaveAnnotations();
		}

		// If moving canvas text..
		if (moveToolActive && canvasText.movingObjectIndex !== -1) {
			var pageNr = Number(canvasText.objects[canvasText.movingObjectIndex].pageContainer.substring("pageContainer".length));
			canvasText.movingObjectIndex = -1;
			canvasText.movingObjectPageNumber = -1;
			drawing.redrawObjects(pageNr);
			SaveAnnotations();
		}

		// If moving a pen object
		if (moveToolActive && drawing.movingObjectIndex !== -1) {
			var pageNr = Number(drawing.penObjects[drawing.movingObjectIndex].pageContainer.substring("pageContainer".length));
			drawing.movingObjectIndex = -1;
			drawing.movingObjectPageNumber = -1;
			drawing.redrawObjects(pageNr);
			SaveAnnotations();
		}
	});

	// Saves all annotation.
	function SaveAnnotations() {
		if (!Annotator.isTeacher) {
			return;
		}
		
		var annotations = [];

		var textAnnotations = {
			"type" : "text",
			"annotations" : []
		};

		textAnnotations.annotations = Annotator.textObjects;

		if (textAnnotations.annotations.length > 0) {
			annotations.push(textAnnotations); 
		}

		if (drawing.penObjects.length > 0) {
			var drawingAnnotations = {
				"type" : "drawing",
				"annotations" : drawing.penObjects 
			};

			annotations.push(drawingAnnotations);
		}

		if (canvasText.objects.length > 0) {
			var canvasTextAnnotations = {
				"type" : "canvasText",
				"annotations" : canvasText.objects
			};

			annotations.push(canvasTextAnnotations);
		}

		var courseID = getParameterByName("courseID");
		var assignmentID = getParameterByName("assignmentID");
		var handinID = getParameterByName("handinID");
		var attachmentID = getParameterByName("attachmentID");
		SaveToAPI(annotations, courseID, assignmentID, handinID, attachmentID);
	}

	// Takes an array of annotations. 
	// Example: 
	// [ { "type" : "canvasText", "annotations" : canvasTextObjects (array as well) } ] 
	function SaveToAPI(annotations, courseID, assignmentID, handinID, attachmentID) {
		if (!Annotator.isTeacher) {
			return;
		}

		var authorizationToken = window.localStorage.getItem("ls.Token");
		var annotationsToSave = [];

		for (var i = 0; i < annotations.length; i++) {
			annotationsToSave.push({
				"Value" : JSON.stringify(annotations[i]),
				"Pos" : "Not used" 
			});
		}

		$.ajax({
			method: "POST",
			url: Annotator.apiUrl + "courses/" + courseID + "/assignments/" + assignmentID + "/handins/" + handinID + "/attachments/" + attachmentID + "/annotation",
			headers: { "Authorization" : "Bearer " + authorizationToken },
			data: { "": annotationsToSave }
		})
		.done(function( msg ) {
		}).fail(function() {
			toastr.error("Annotations could not be saved.", "Could not save");
		});
	}

	// Loads all annotations for a given page.
	// Called by pdf.js once a page is loaded.
	Annotator.loadAnnotations = function(pageNumber) {
		var annotations = []; 
		var authorizationToken = window.localStorage.getItem("ls.Token");
		var courseID = getParameterByName("courseID");
		var assignmentID = getParameterByName("assignmentID");
		var handinID = getParameterByName("handinID");
		var attachmentID = getParameterByName("attachmentID");

		// Add an overlay canvas
		if ($("#pageContainer" + pageNumber + " canvas.overlay").length < 1) {
			var $pdfCanvas = $("#pageContainer" + pageNumber).find('canvas');
			var $overlayCanvas = $("<canvas/>");
			$overlayCanvas.attr("width", $pdfCanvas.width());
			$overlayCanvas.attr("height", $pdfCanvas.height());
			$overlayCanvas.addClass("overlay");
			$overlayCanvas.addClass($pdfCanvas.attr("id"));
			$overlayCanvas.insertAfter($pdfCanvas);
		}

		// TODO: Perhaps only do this for the first loaded page. This gets all the annotations. Should not be necessary for each page load. 
		$.ajax({
			method: "GET",
			url: Annotator.apiUrl + "courses/" + courseID + "/assignments/" + assignmentID + "/handins/" + handinID + "/attachments/" + attachmentID + "/annotation",
			headers: { "Authorization" : "Bearer " + authorizationToken }
		}).done(function(msg) {
			Annotator.isTeacher = msg.IsTeacher;
			if (Annotator.isTeacher) {
				$("button.tool").show();
				$(".color-picker").show(); 
			}
			msg = msg.Items;

			if (!msg) {
				return;
			}

			for (var i = 0; i < msg.length; i++) {
				annotations = JSON.parse(msg[i].Value);
				ReconstructAnnotations(annotations, annotations.type, pageNumber); 
			}
		}).fail(function() {
			toastr.error("Annotations could not be loaded.", "Could not load");
		});
	}

	// Takes an array of annotations and displays them on the page 
	function ReconstructAnnotations(annotations, type, pageNumber) {
		if (type === "text") {
			Annotator.textObjects = annotations.annotations;
			for (var i = 0; i < annotations.annotations.length; i++) {
				var currentPageContainer = annotations.annotations[i].pageid;
				var currentPageNumber = Number(currentPageContainer.substring("pageContainer".length));

				if (currentPageNumber !== pageNumber) {
					continue;
				}
				
				var $textLayer = $("#" + annotations.annotations[i].pageid + " .textLayer");
				var y = annotations.annotations[i].top;
				var x = annotations.annotations[i].left; 
				CreateTextAnnotation(x, y, $textLayer, annotations.annotations[i].annotation);
			}
		} else if (type === "drawing") {
			drawing.penObjects = annotations.annotations; 

			var $overlayCanvas = $("#pageContainer" + pageNumber).find("canvas.overlay");
			drawing.currentCanvas = $overlayCanvas;
			drawing.currentContext = drawing.currentCanvas[0].getContext("2d");

			for (var i = 0; i < annotations.annotations.length; i++) {
				var currentPageContainer = annotations.annotations[i].pageContainer;
				var currentPageNumber = Number(currentPageContainer[currentPageContainer.length - 1]);

				if (pageNumber !== currentPageNumber) {
					continue;
				}

				if ( (drawing.selectedObjectIndex !== -1 && i === drawing.selectedObjectIndex) || (drawing.movingObjectIndex !== -1 && i === drawing.movingObjectIndex) ) {
					drawing.currentContext.strokeStyle = "#a8d1ff"; //"#BF2A1D";
				} else {
					drawing.currentContext.strokeStyle = annotations.annotations[i].color;
				}

				drawing.currentContext.beginPath(); 
				drawing.currentContext.moveTo(annotations.annotations[i].points[0].left, annotations.annotations[i].points[0].top); //FÃ¦rum pennan aÃ° Ã¾essum coords
				drawing.currentContext.fillRect(annotations.annotations[i].points[0].left, annotations.annotations[i].points[0].top,1,1); //Teiknum 
				drawing.currentContext.stroke();
				
				for (var j = 1; j < annotations.annotations[i].points.length; j++) {
					drawing.currentContext.lineTo(annotations.annotations[i].points[j].left, annotations.annotations[i].points[j].top);
					drawing.currentContext.stroke();
				}
				
				drawing.currentContext.strokeStyle = "#000";
			}
		} else if (type === "canvasText") {
			canvasText.objects = annotations.annotations; 
			var $overlayCanvas = $("#pageContainer" + pageNumber).find("canvas.overlay");
			var ctx = $overlayCanvas[0].getContext("2d");
			ctx.font = "13.3333px monospace";
			ctx.textBaseline = "top";
			var lineHeight = ctx.measureText("M").width * 2;

			for (var i = 0; i < annotations.annotations.length; i++) {
				var currentPageContainer = annotations.annotations[i].pageContainer;
				var currentPageNumber = Number(currentPageContainer[currentPageContainer.length - 1]);

				if (pageNumber !== currentPageNumber) {
					continue;
				}

				var top = annotations.annotations[i].top;
				var left = annotations.annotations[i].left;
				var lines = annotations.annotations[i].text.split("\n");

				if ( (canvasText.selectedObjectIndex !== -1 && i === canvasText.selectedObjectIndex) || (canvasText.movingObjectIndex !== -1 && i === canvasText.movingObjectIndex)) {
					var selectRectLeft = left - 10;
					var selectRectTop = top - 10;
					var selectRectWidth = annotations.annotations[i].width + 20;
					var selectRectHeight = annotations.annotations[i].height + 20; 

					ctx.beginPath();
					ctx.rect(selectRectLeft, selectRectTop, selectRectWidth, selectRectHeight);
					ctx.stroke();
				} 

				ctx.beginPath();
				ctx.fillStyle = annotations.annotations[i].color; 

				for (var j = 0; j < lines.length; ++j) {
					ctx.fillText(lines[j], left, top);
					top += lineHeight;
				} 

				ctx.fillStyle = "#000";
			}
		}
	}

	// TODO: Put this on whatever object holds the bubble annotations.
	// It will create a text annotation in the given text layer, with the given text.
	// If annotationText is null it will assume we're creating a new annotation.    
	function CreateTextAnnotation(x, y, $textLayer, annotationText) {
		var $annotationContainer = $("<div/>");
		$annotationContainer.addClass("annotation-container");
		if (annotationText === null) { // If entering new annotation
			$annotationContainer.addClass("enter-comment");
		}

		$annotationContainer.css("top", y);
		$annotationContainer.css("left", x);
		var $annotationBubble = $("<div/>");
		$annotationBubble.addClass("annotation-bubble");
		var $chatBubble = $("<div/>");
		$chatBubble.addClass("chat-bubble");
		$annotationBubble.append($chatBubble);
		$annotationBubble.append( $("<i/>").addClass("fa fa-times") );
		$annotationContainer.append($annotationBubble); 

		// Insert the input part
		var $annotationInput = $("<div/>");
		$annotationInput.addClass("annotation-input");

		var $textArea = $("<textarea/>");
		$textArea.attr("placeholder", "Enter comment here");
		if (annotationText !== null) {
			$textArea.val(annotationText);
		}
		if (!Annotator.isTeacher) {
			$textArea.attr("disabled", "disabled");
			$annotationInput.addClass("student");
		}

		$annotationInput.append($textArea); 

		if (Annotator.isTeacher) {
			var $btnSave = $("<button/>");
			$btnSave.addClass("btn").addClass("btn-submit").addClass("btn-save");
			$btnSave.html("Save");
			var $btnCancel = $("<button/>");
			$btnCancel.addClass("btn").addClass("btn-cancel").html("Delete");
			$annotationInput.append($btnSave);
			$annotationInput.append($btnCancel);
		}

		$annotationContainer.append($annotationInput);
		$textLayer.append($annotationContainer);

		// If creating a new annotation 
		if (annotationText === null) {
			$(".annotation-container.enter-comment textarea").focus();
		}
	}

	// Delete the selected drawing / pen object
	// TODO: Could be refactored if we combine the drawing/canvasText object.  
	$("html").keyup(function(e) {
		if (e.keyCode === 46) {
			if (drawing.selectedObjectIndex !== -1) {
				drawing.deleteSelectedObject();
			} else if (canvasText.selectedObjectIndex !== -1) {
				canvasText.deleteSelectedObject();
			}
		}
	});
});
