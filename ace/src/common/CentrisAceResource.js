"use strict";
angular.module( "aceApp").factory("CentrisAceResource", ["$http", "$location", "$q",
function CentrisAceResource($http, $location, $q) {
	return {
		// call this function before using this factory.  When the init function
		// has completed running the callback will be run if it is defined.
		init: function init() {
			return this.getConstants();
		},
		reportError: function reportError(errorMessage) {
			// todo: how to handle error reporting.
			console.log(errorMessage);
		},
		hostUrl: function hostUrl() {
			if (this.constants === undefined) {
				this.reportError("CentrisAceResource: init() must be called before");
				return "";
			}
			if (this.constants.apiEndpoint === undefined) {
				this.reportError("CentrisAceResource: constants.apiEndpoint do not exist");
				return "";
			}
			var host = this.constants.apiEndpoint + "api/api/v1/";
			return host;
		},
		mooshakHostUrl: function mooshakHostUrl() {
			if (this.constants === undefined) {
				this.reportError("CentrisAceResource: init() must be called before");
				return "";
			}
			if (this.constants.mooshakServer === undefined) {
				this.reportError("CentrisAceResource: constants.mooshakApi do not exist");
				return "";
			}
			var host = this.constants.mooshakApi;
			return host;
		},
		// returns a one level json object
		// nested json objects are not handled
		// returns undefined if the function fails
		extractJson: function extractJson(strAppconfig) {
			var obj;
			if (strAppconfig !== undefined && strAppconfig.data !== undefined) {
				var start = strAppconfig.data.indexOf("{");
				var stop = strAppconfig.data.indexOf("}");
				if (start > -1 && stop > -1) {
					var str = strAppconfig.data.substring(start, stop + 1);
					obj = JSON.parse(str);
				}
			}
			return obj;
		},
		// saves to a json accessible from CentrisAceResource.constats (this.constants).
		// values added to this object ar extracted from the file(or url) "/scripts/appconfig.js"
		// object will be undefined if extraction to the json object fails.
		// the function uses a promise object so use the .then(.... to use the function
		// The callback function can access the new constant object via the first parameter.
		// this function will always update the this.constants object.
		getConstants: function getConstants() {
			var deferred = $q.defer();
			var url = "/scripts/appconfig.js";
			// to keep this function in scope in nameless function
			var self = this;
			this.getData(url)
				.then(function(responce) {
					// we want to save the constants in this object
					self.constants = self.extractJson(responce);
					deferred.resolve(self.constants);
				},function(responce) {
					deferred.reject();
				}
			);
			return deferred.promise;
		},
		// makes a get request for the supplied url, sending the access token also to api.
		getData: function getData(url) {
			var deferred = $q.defer();
			var authorizationToken = window.localStorage.getItem("ls.Token");
			$http({
				method: "GET",
				url: url,
				headers: { Authorization: "Bearer " + authorizationToken },
			}).
			then(function(response) {
				deferred.resolve(response);
			}, function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
		},
		// extracts a value from the url
		getValueFromUrl: function getValueFromUrl(variableName) {
			var url = $location.absUrl();
			return this.extractQueryValue(url, variableName);
		},
		// helper functiono for getValueFromUrl
		extractQueryValue: function extractQueryValue(url, variableName) {
			var searchStr = variableName + "=";
			var iStart = url.indexOf(searchStr);
			if (iStart < 0) {
				return undefined;
			}
			url = url.substring(iStart + searchStr.length);
			iStart = url.indexOf("&");
			if (iStart < 0) {
				iStart = url.indexOf("#");
			}
			if (iStart > -1 ) {
				url = url.substring(0, iStart);
			}
			return url;
		},
		// returns false if unable to do a call to the mooshak server
		// returns true if a call was made to the mooshak server
		// todo: make promise instead of callback see how the function getConstants(); is implemented
		getCodeFromMooshakServer: function getCodeFromMooshakServer(contest, problem, user, successFunction, errorFunction) {
			if (contest === undefined || problem === undefined || user === undefined ||
				successFunction === undefined) {
				this.reportError("getCodeFromMooshakServer: Required parameter missing");
				return false;
			}
			this.reportError("- DIRECT CALL TO MOOSHAK SERVER, this should be removed when ace viewer has been developed - ");
			var mooshakApiUrl = this.mooshakHostUrl() + "api/api/v1/contests/" + contest + "/problems/" + problem + "/codes";
			// todo: when done testing and creating view,  remove this function client should not call the
			// mooshak server directly, calls should only be allowed via the centrisAPI
			$http({
				method:  "GET",
				url:     mooshakApiUrl,
				crossDomain: true
			})
			.then(function( response ) {
				this.reportError("running success function");
				successFunction(response);
			}, function(response) {
				if (errorFunction !== undefined) {
					this.reportError("running ERROR function");
					errorFunction(response);
				} else {
					this.reportError("Error from getCode when calling route : " + mooshakApiUrl);
				}
			});
			return true;
		},
		// this function should probably no
		// todo: make promise instead of callback see how the function init(); is implemented
		getProblemSubmissionseFromCentrisServer: function getProblemSubmissionseFromCentrisServer(contest, problem,
		courseInstanceID, successFunction, errorFunction) {
			// group submission
			// submission
			// submissions
			return this.getSubmissionFromCentrisServer("submissions", contest, problem,
			courseInstanceID, "", successFunction, errorFunction);
		},
		// todo: make promise instead of callback see how the function init(); is implemented
		getCodeFromCentrisServer: function getCodeFromCentrisServer(contest, problem, courseInstanceID,
		groupSSN, successFunction, errorFunction) {
			// group submission
			// submission
			// submissions
			return this.getSubmissionFromCentrisServer("submission", contest, problem,
			courseInstanceID, groupSSN, successFunction, errorFunction);
		},
		// a helper function for getCodeFromCentrisServer and getProblemSubmissionseFromCentrisServer
		// todo: make promise instead of callback see how the function getConstants(); is implemented
		getSubmissionFromCentrisServer: function getSubmissionFromCentrisServer(command, contest, problem, courseInstanceID,
		groupSSN, successFunction, errorFunction) {
			if (contest === undefined || problem === undefined || groupSSN === undefined ||
				courseInstanceID === undefined || successFunction === undefined) {
				this.reportError("getCodeFromCentrisServer: Required parameter missing");
				return false;
			}
			var param = {
				AssignmentName: contest,
				ProblemName:    problem,
				UserSSNList:    groupSSN
			};
			var authorizationToken = window.localStorage.getItem("ls.Token");
			var centrisApiUrl = this.hostUrl() + "courses/" + courseInstanceID +
			"/assignments/plugins/0/problems/" + command;
			this.reportError("getSubmissionFromCentrisServer : url " + centrisApiUrl);
			$http({
				method: "POST",
				url: centrisApiUrl,
				headers: { Authorization: "Bearer " + authorizationToken },
				data: param
			}).
			then(function(response) {
				var copyResponse = response;
				if ( response.data !== undefined ) {
					copyResponse.data = JSON.parse(response.data);
				}
				successFunction(copyResponse);
			}, function(response) {
				if (errorFunction !== undefined) {
					var copyResponse = response;
					if ( response.data !== undefined ) {
						copyResponse.data = JSON.parse(response.data);
					}
					this.reportError("getCodeFromCentrisServer: running ERROR function");
					errorFunction(copyResponse);
				} else {
					this.reportError("getCodeFromCentrisServer: Error from getCode when calling route : " + centrisApiUrl);
				}
			});
			return true;
		},
		// all paramenters are requred except the last one which is errorFunction.
		// todo: make promise instead of callback see how the function init(); is implemented
		saveAnnotation: function saveAnnotation(objectToSave, courseID, assignmentID, handinID, attachmentID) {
			var url = this.hostUrl() + "courses/" + courseID +
			"/assignments/" + assignmentID + "/handins/" + handinID + "/attachments/" + attachmentID + "/annotation";
			var object = {
				"Value": JSON.stringify(objectToSave),
				"Pos": "temp"
			};
			var objectList = [];
			objectList.push(object);
			return this.postData(objectList, url);
		},
		// todo: make promise instead of callback see how the function getConstants(); is implemented
		postData: function postData(objectList, url) {
			var deferred = $q.defer();
			var authorizationToken = window.localStorage.getItem("ls.Token");
			console.log("posting object");
			console.log(objectList);
			$http({
				method: "POST",
				url: url,
				headers: { Authorization: "Bearer " + authorizationToken },
				data: objectList
			}).
			then(function(response) {
				deferred.resolve(response);
			}, function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
		},
		// all paramenters are requred exept the last one which is errorFunction.
		// todo: make promise instead of callback see how the function init(); is implemented
		loadAnnotation: function loadAnnotation(courseID, assignmentID, handinID, attachmentID) {
			var url = this.hostUrl() + "courses/" + courseID + "/assignments/" +
			assignmentID + "/handins/" + handinID + "/attachments/" + attachmentID + "/annotation";
			return this.getData(url);
		}
	};
}]);