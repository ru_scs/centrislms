 "use strict";
 angular.module("aceApp").factory( "AceFactory", [function AceFactory() {
	return {
		// call this function in the onLoad function specified in the ui-ace directive
		// save a handle on the Ace editor
		init: function init(_editor) {
			this.editor = _editor;
			this.session = this.editor.getSession();
			this.renderer = this.editor.renderer;
		},
		setDefaultValues: function setDefaultValues() {
			// possible commands : http://ajaxorg.github.io/ace/#nav=api&api=editor
			this.editor.setTheme("ace/theme/monokai");
			this.editor.setReadOnly(true);
			this.renderer.setShowGutter(true);
			this.editor.setOption("highlightActiveLine", true);
			this.editor.setOption("highlightGutterLine", true);
			this.editor.setOption("fontSize", 12);
			this.editor.setOption("minLines", 3);
			this.editor.setOption("maxLines", 30);
			this.editor.renderer.setScrollMargin(10, 10);
			this.editor.$blockScrolling = Infinity;
			this.session.setUseWrapMode(true);
		},
		getExtension: function getExtension(str) {
			var index = str.lastIndexOf(".") + 1;
			if (index < 1) {
				return "";
			}
			return str.substring(index);
		},
		supportedFileType: function supportedFileType(fileName) {
			var str = this.getMode(fileName);
			if (str.length > 0)
			{
				return true;
			}
			return false;
		},
		// sets the file to be used
		setMode: function setMode(fileName) {
			var mode = this.getMode(fileName);
			this.session.setMode(mode);
		},
		// todo: check out var modelist = require("ace/ext/modelist")
		getMode: function getMode(fileName) {
			if (fileName === undefined) {
				return "";
			}
			var extension = this.getExtension(fileName).toLowerCase();
			var mode;
			switch (extension) {
				case "cpp":
				case "c":
				case "h": {
					mode = "ace/mode/c_cpp";
					break;
				}
				case "js": {
					mode = "ace/mode/javascript";
					break;
				}
				case "java":
				case "class": {
					mode = "ace/mode/java";
					break;
				}
				case "bat": {
					mode = "ace/mode/batchfile";
					break;
				}
				case "cs": {
					mode = "ace/mode/csharp";
					break;
				}
				case "css": {
					mode = "ace/mode/css";
					break;
				}
				case "html": {
					mode = "ace/mode/html";
					break;
				}
				case "json": {
					mode = "ace/mode/json";
					break;
				}
				case "less": {
					mode = "ace/mode/less";
					break;
				}
				case "lisp": {
					mode = "ace/mode/lisp";
					break;
				}
				case "md": {
					mode = "ace/mode/markdown";
					break;
				}
				case "sql": {
					mode = "ace/mode/sql";
					break;
				}
				case "tex": {
					mode = "ace/mode/tex";
					break;
				}
				case "vb": {
					mode = "ace/mode/vbscript";
					break;
				}
				case "xml": {
					mode = "ace/mode/xml";
					break;
				}
				case "py": {
					mode = "ace/mode/python";
					break;
				}
				case "sh": {
					mode = "ace/mode/sh";
					break;
				}
				case "jsp": {
					mode = "ace/mode/jsp";
					break;
				}
				case "cshtml": {
					mode = "ace/mode/razor";
					break;
				}
				case "pl": {
					mode = "ace/mode/prolog";
					break;
				}
				case "pas": {
					mode = "ace/mode/pascal";
					break;
				}
				case "scss": {
					mode = "ace/mode/scss";
					break;
				}
				case "ss": {
					mode = "ace/mode/scheme";
					break;
				}
				default: {
					mode = "";
				}
			}
			return mode;
		},
		allThemes:[
			"ambiance",
			"chaos",
			"chrome",
			"clouds",
			"clouds_midnight",
			"cobalt",
			"crimson_editor",
			"dawn",
			"dreamweaver",
			"eclipse",
			"github",
			"idle_fingers",
			"iplastic",
			"katzenmilch",
			"kr_theme",
			"kuroir",
			"merbivore",
			"merbivore_soft",
			"monokai",
			"mono_industrial",
			"pastel_on_dark",
			"solarized_dark",
			"solarized_light",
			"sqlserver",
			"terminal",
			"textmate",
			"tomorrow",
			"tomorrow_night",
			"tomorrow_night_blue",
			"tomorrow_night_bright",
			"tomorrow_night_eighties",
			"twilight",
			"vibrant_ink",
			"xcode",
		]
	};
}]);