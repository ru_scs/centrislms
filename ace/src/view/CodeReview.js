"use strict";

angular.module("aceApp").controller("CodeReview", ["$scope", "AceFactory",
	"CentrisAceResource", "$location", "$http", "toastr",
function($scope, AceFactory, CentrisAceResource, $location, $http, toastr) {
	$scope.aceCommentinit = function aceCommentinit(_editor, readonly) {
		_editor = _editor;
		_editor.setTheme("ace/theme/xcode");
		_editor.setReadOnly(readonly);
		_editor.setOption("highlightActiveLine", false);
		_editor.renderer.setShowGutter(false);
		_editor.setOption("fontSize", 12);
		_editor.setOption("minLines", 3);
		_editor.setOption("maxLines", 7);
		_editor.$blockScrolling = Infinity;
		_editor.getSession().setUseWrapMode(true);
	};
	$scope.aceCommentInputLoaded = function aceCommentInputLoaded(_editor) {
		$scope.commentEditor = _editor;
		$scope.aceCommentinit(_editor, false);
	};
	$scope.aceCommentDisplayLoaded = function aceCommentDisplayLoaded(_editor) {
		$scope.editorDisplay = _editor;
		$scope.aceCommentinit(_editor, true);
	};
	// show the input new comment and add text to it
	$scope.showInput = function showInput(show, lineNumber, leftXCoords) {
		$scope.ShowAceCommentInput = show;
		if (show === false) {
			$scope.aceCommentInput = "";
			return;
		}
		// we want to edit comments in current line if exists
		// otherwise show a empty edit box.
		$scope.aceCommentInput = $scope.getComment(lineNumber);
		if ($scope.aceCommentInput !== "") {
			$scope.CommentExistedBefore = true;
		} else {
			$scope.CommentExistedBefore = false;
		}
		$scope.positionComment(lineNumber, leftXCoords);
		// set focus to the edit
		$scope.commentEditor.focus();
	};

	$scope.getComment = function getComment(lineNumber) {
		var savedComment = $scope.comments[lineNumber];
		if (savedComment === undefined) {
			return "";
		}
		return savedComment;
	};

	$scope.showComment = function showComment(lineNumber, leftXCoords) {
		$scope.aceCommentDisplay = $scope.getComment(lineNumber);
		if ($scope.aceCommentDisplay !== "") {
			$scope.positionComment(lineNumber, leftXCoords);
			$scope.ShowAceCommentDisplay = true;
		} else {
			$scope.ShowAceCommentDisplay = false;
		}
	};

	$scope.aceClickHandler = function aceClickHandler(e) {
		var editor = e.editor;
		var pos = editor.getCursorPosition();
		if ($scope.editingComment) {
			$scope.commentRow = pos.row;
			$scope.showInput(true, pos.row, e.clientX);
			$scope.$apply(); // I seem to need these two appys this one and the one at the end of the function.
		} else {
			$scope.showInput(false);
			$scope.showComment(pos.row, e.clientX);
		}
		$scope.$apply(); // to make ng-show update in html
	};
	$scope.CommentsSaveSuccess = function CommentsSaveSuccess(response) {
		$scope.onEditingComment();
		$scope.notifySuccess("Comments successfully saved");
	};
	$scope.CommentsSaveError = function CommentsSaveError() {
		$scope.notifyError("Unable to save comments");
	};
	$scope.onBtnCommentSave = function onBtnCommentSave() {
		var keys = Object.keys($scope.comments);
		var str = "";
		keys.forEach(function(item) {
			str = str + item + ": " + $scope.comments[item] + "\n";
		});
		var handinID     = CentrisAceResource.getValueFromUrl("handinID");
		var attachmentID = CentrisAceResource.getValueFromUrl("attachmentID");
		var assignmentID = CentrisAceResource.getValueFromUrl("assignmentID");
		var courseID     = CentrisAceResource.getValueFromUrl("courseID");
		if (handinID     === undefined || handinID.length     < 1 ||
			attachmentID === undefined || attachmentID.length < 1 ||
			assignmentID === undefined || assignmentID.length < 1 ||
			courseID     === undefined || courseID.length     < 1 ) {
			console.log("Unable to save because not all url values are available");
			return;
		}
		CentrisAceResource.saveAnnotation($scope.comments, courseID, assignmentID, handinID, attachmentID)
		.then(function(response) {
			$scope.CommentsSaveSuccess(response);
		}, function() {
			$scope.CommentsSaveError();
		});
	};
	$scope.onBtnCommentUse = function onBtnCommentUse() {
		if ($scope.commentRow === undefined) {
			return; // this should not happen
		}
		if ($scope.aceCommentInput.length > 0) {
			$scope.addComment($scope.commentRow, $scope.aceCommentInput);
		}
		$scope.showInput(false);
	};
	$scope.onBtnCommentDelete = function onBtnCommentDelete() {
		if ($scope.commentRow === undefined) {
			return; // this should not happen, but if it does someone forgot to set commentRow
		}
		$scope.deleteComment($scope.commentRow);
		$scope.showInput(false);
	};
	$scope.onBtnCommentCancel = function onBtnCommentCancel() {
		$scope.showInput(false);
	};
	// run this function after the main ace code viewer has loaded
	$scope.postLoad = function postLoad() {
		// save important values from the url
		$scope.values = {};
		$scope.values.handinID       = CentrisAceResource.getValueFromUrl("handinID");
		$scope.values.attachmentID   = CentrisAceResource.getValueFromUrl("attachmentID");
		$scope.values.assignmentID   = CentrisAceResource.getValueFromUrl("assignmentID");
		$scope.values.courseID       = CentrisAceResource.getValueFromUrl("courseID");
		$scope.values.attachmentLink = $scope.makeAttachmentPath();
		// setup for comments
		$scope.showInvisible = false;
		$scope.editingComment = false;
		$scope.showHover = true;
		$scope.comments = {};
		// display the attachment with comments
		$scope.displayFile();
	};
	// checks if all values from url are valid
	// returns true if all values are valid otherwise false.
	$scope.urlValuesAreOk = function urlValuesAreOk() {
		if ($scope.values.handinID === undefined || $scope.values.handinID.length     < 1 ||
		$scope.values.attachmentID === undefined || $scope.values.attachmentID.length < 1 ||
		$scope.values.assignmentID === undefined || $scope.values.assignmentID.length < 1 ||
		$scope.values.courseID     === undefined || $scope.values.courseID.length     < 1 ) {
			return false;
		}
		return true;
	};
	$scope.displayFile = function displayFile() {
		$scope.aceStyle = "height:100px";
		$scope.file = {};
		$scope.file.name    = CentrisAceResource.getValueFromUrl("filename");
		$scope.themes       = AceFactory.allThemes;
		var theme = CentrisAceResource.getValueFromUrl("theme");
		if (theme !== undefined) {
			$scope.theme = theme;
		} else {
			$scope.theme = "monokai";
		}
		console.log("$scope.displayFile");
		var link = $scope.values.attachmentLink;
		if (link !== undefined) {
			// get attachment from centris API
			$scope.openAttachment(link);
		} else {
			// for viewing short files sent in url (ugly, I know)
			$scope.file.content = decodeURIComponent(CentrisAceResource.getValueFromUrl("content"));
			if ($scope.file.name !== undefined &&
				$scope.file.name.length > 0 &&
				$scope.file.content !== undefined) {
				AceFactory.setMode($scope.file.name);
				$scope.aceValue = $scope.file.content;
			}
		}
	};
	// returns undefined if one or more of the
	// requred values are missing from the URL.
	$scope.makeAttachmentPath = function makeAttachmentPath() {
		var handinID     = CentrisAceResource.getValueFromUrl("handinID");
		var attachmentID = CentrisAceResource.getValueFromUrl("attachmentID");
		var assignmentID = CentrisAceResource.getValueFromUrl("assignmentID");
		var courseID     = CentrisAceResource.getValueFromUrl("courseID");
		if (handinID     === undefined || handinID.length     < 1 ||
			attachmentID === undefined || attachmentID.length < 1 ||
			assignmentID === undefined || assignmentID.length < 1 ||
			courseID     === undefined || courseID.length     < 1 ) {
			return undefined;
		}
		return CentrisAceResource.hostUrl() + "courses/" + courseID +
		"/assignments/" + assignmentID +
		"/solutions/attachments/" + attachmentID;
	};
	// finds the total offset of all parent elements
	$scope.cumulativeOffset = function cumulativeOffset(element) {
		var top = 0, left = 0;
		do {
			top += element.offsetTop  || 0;
			left += element.offsetLeft || 0;
			element = element.offsetParent;
		} while (element);

		return {
			top: top,
			left: left
		};
	};
	// uses a zero based index for lineNumber
	// line 1 for user is line 0 for us
	$scope.positionComment = function positionComment(lineNumber, leftXCoords) {
		var rowHeight = AceFactory.renderer.lineHeight * lineNumber;
		var elm = document.getElementById("ace-code-display");
		var width = elm.offsetWidth;
		var heightOffset = $scope.cumulativeOffset(elm).top + 25; // 25 is height of "edit mode"  + "name"
		var topStyle = "top: " + (rowHeight + heightOffset) + "px";
		var left = 610;
		var leftStyle = "";
		if (width < left + 300) {
			left = width - 300;
			if (left < 70 ) {
				left = 70;
			}
		}
		if (leftXCoords !== undefined) {
			left = leftXCoords;
		}
		leftStyle = "left: " + left + "px";
		$scope.CommentTop = leftStyle + ";" + topStyle;
	};
	// mark which lines in the ace editor have comments associated with it.
	$scope.markCommentLines = function markCommentLines() {
		var keys = Object.keys($scope.comments);
		keys.forEach(function(item) {
			AceFactory.session.addGutterDecoration(item, "ace-gutter-decoration");
		});
	};
	// uses a zero based index for lineNumber
	// line 1 for user is line 0 for us
	$scope.addComment = function addComment(lineNumber, comment) {
		$scope.comments[lineNumber] = comment;
		AceFactory.session.addGutterDecoration(lineNumber, "ace-gutter-decoration");
	};
	$scope.deleteComment = function deleteComment(lineNumber) {
		delete $scope.comments[lineNumber];
		AceFactory.session.removeGutterDecoration(lineNumber, "ace-gutter-decoration");
	};
	$scope.onEditingComment = function onEditingComment() {
		$scope.editingComment = !$scope.editingComment;
		$scope.showInput(false);
		$scope.ShowAceCommentDisplay = false;
	};
	$scope.onShowInvisible = function onShowInvisible() {
		$scope.showInvisible = !$scope.showInvisible;
		AceFactory.renderer.setShowInvisibles($scope.showInvisible);
	};
	$scope.onShowHover = function onShowHover() {
		$scope.showHover = !$scope.showHover;
	};
	$scope.onWrap = function onWrap() {
		$scope.wrap = !$scope.wrap;
		AceFactory.session.setUseWrapMode($scope.wrap);
	};
	$scope.onThemeSelectChange = function onThemeSelectChange() {
		if ($scope.theme !== undefined) {
			AceFactory.editor.setTheme("ace/theme/" + $scope.theme);
		}
	};
	// $scope.comments
	$scope.onloadAnnotationSuccsess = function onloadAnnotationSuccsess(response) {
		$scope.comments = {}; // clear all commentsc
		console.log(response);
		var isTeacher = false;
		if (response !== undefined && response.data !== undefined &&
			response.data.Items !== undefined && response.data.Items[0] !== undefined &&
			response.data.Items[0].Value !== undefined) {
			var obj = JSON.parse(response.data.Items[0].Value);
			if (obj !== undefined) {
				if (response.data.IsTeacher === true) {
					isTeacher = true;
				}
				$scope.comments = obj;
				$scope.markCommentLines();
			}
		}
		$scope.isTeacher = isTeacher;
	};
	$scope.onloadAnnotationError = function onloadAnnotationError() {
		$scope.notifyError("Unable to load comments!");
	};
	// pass true for isError if this is a error notifycation
	$scope.helperNotify = function helperNotify(message, title, isError) {
		if (isError === undefined || isError === false) {
			toastr.success(message, title);
		} else {
			toastr.error(message, title);
		}
	};
	// wrapper function for $scope.helperNotify so we can have all notifycations
	// in one place so it will be easy to change the notifycation style
	// like using the CentrisNotify for all notifycations
	$scope.notifySuccess = function notifySuccess(message, title) {
		$scope.helperNotify(message, title);
	};
	$scope.notifyError = function notifyError(message, title) {
		$scope.helperNotify(message, title, true);
	};
	// todo: think about if this function or a part of it should be in the
	// CentrisAceResource.
	$scope.openAttachment = function openAttachment(link) {
		$scope.file.name = "";
		$scope.aceValue = "";
		$http.get(link).success( function (data, status, headers, config) {
			$scope.file.name = headers("X-Centris-Filename");
			$scope.file.content = data;
			AceFactory.editor.setOption("maxLines", Number.MAX_VALUE);
			AceFactory.setMode($scope.file.name);
			$scope.aceValue = $scope.file.content;
			$scope.onThemeSelectChange();
			// now lets get comments if there are any
			if (!$scope.urlValuesAreOk()) {
				$scope.notifyError("Unable to load because not all url values are available");
				return;
			}
			CentrisAceResource.loadAnnotation(
				$scope.values.courseID, $scope.values.assignmentID,
				$scope.values.handinID, $scope.values.attachmentID)
				.then(function(response) {
					$scope.onloadAnnotationSuccsess(response);
				},function(response) {
					$scope.onloadAnnotationError();
				});
		});
	};
	$scope.onMouseMove = function onMouseMove(e) {
		if (!$scope.showHover || $scope.editingComment) {
			return;
		}
		var pos = e.getDocumentPosition();
		$scope.showComment(pos.row, e.clientX);
		$scope.$apply();
	};
	$scope.aceLoaded = function(_editor) {
		AceFactory.init(_editor);
		AceFactory.setDefaultValues();
		_editor.on("click", $scope.aceClickHandler);
		_editor.on("mousemove", $scope.onMouseMove);
		// all $scope functions used by postLoad must be declared above (I think)
		CentrisAceResource.init()
			.then($scope.postLoad)
			.catch(function() {
				$scope.notifyError("Unable to initialize CentrisAceResource");
			});
	};

}]);