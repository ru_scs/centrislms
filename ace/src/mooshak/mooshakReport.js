"use strict";
angular.module( "aceApp").directive("mooshakReport", [function() {
	return {
		restrict: "E",
		replace: true,
		scope: {
			ngModel: "=",
			htmlreport:"@"
		},
		link: function(scope, element, attributes) {
			attributes.$observe("htmlreport", function(value) {
				var str = "";
				// only interested what is within the body element.
				var start = value.indexOf("<body>") + 6;
				var stop = value.indexOf("</body>");
				if (start > 5 && stop > -1) {
					str = value.substring(start, stop);
				}
				var newHtmlReport = "<div class='mooshak-report-class'>" + str + "</div>";
				element.empty();
				element.append(newHtmlReport);
			});
		}
	};
}]);