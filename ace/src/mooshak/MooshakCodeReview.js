"use strict";
angular.module("aceApp").controller("MooshakCodeReview", ["$scope", "AceFactory", "CentrisAceResource",
"$location", "$http", function($scope, AceFactory, CentrisAceResource, $location, $http) {
	$scope.aceLoaded = function(_editor) {
		AceFactory.init(_editor);
		AceFactory.setDefaultValues();
		if ($scope.theme === undefined) {
			$scope.theme = "monokai";
		}
	};
	$scope.showInvisible = false;
	// Show the code
	$scope.aceValue = "no code to view";
	// Get the application code path from the url
	$scope.showCode = function showCode(response) {
		$scope.aceValue = "no code to view";
		$scope.fileNames = [];
		$scope.status = response.status;
		$scope.data = response.data;
		// get the key
		$scope.user = undefined;
		var keys = Object.keys(response.data);
		if (keys.length > 0) {
			$scope.user = keys[0];
		}
		if (response.data[$scope.user] !== undefined) {
			var files = response.data[$scope.user].code;
			Object.keys(files).forEach(function(item, index, array) {
				var saveObj = {id: $scope.fileNames.length + 1, name: item, code:files[item]};
				$scope.fileNames.push(saveObj);
			});
			$scope.selectedItem = $scope.fileNames[0];
			// display the selectedItem in the ace editor
			$scope.OnFileSelectChange();
			$scope.report = response.data[$scope.user].report;
			$scope.grade = response.data[$scope.user].grade;
			$scope.gradeClass = $scope.getGradeClass($scope.grade);
			if ($scope.theme !== undefined) {
				AceFactory.editor.setTheme("ace/theme/" + $scope.theme);
			}
		}
	};
	$scope.getGradeClass = function getGradeClass(grade) {
		var gradeClass;
		if (grade <= 6) {
			gradeClass = "bg-danger";
		} else
		if (grade < 8) {
			gradeClass = "bg-warning";
		} else {
			gradeClass = "bg-success";
		}
		return gradeClass;
		// return "background:" + gradeClass + ";color:" + color;
	};
	$scope.fileNames = [{id: 1, name: "No code!", code: "No code to display"}];
	$scope.OnFileSelectChange = function OnFileSelectChange() {
		if ( $scope.selectedItem === undefined ) {
			$scope.aceValue = "No code to display";
			return;
		}
		AceFactory.setMode($scope.selectedItem.name);
		$scope.aceValue =  $scope.selectedItem.code;
	};
	$scope.OnThemeSelectChange = function OnThemeSelectChange() {
		if ($scope.theme !== undefined) {
			AceFactory.editor.setTheme("ace/theme/" + $scope.theme);
		}
	};
	$scope.onShowInvisible = function onShowInvisible(showInvisible) {
		AceFactory.renderer.setShowInvisibles(showInvisible);
	};
	$scope.init = function init() {
		$scope.contest      = CentrisAceResource.getValueFromUrl("contest");
		$scope.problem      = CentrisAceResource.getValueFromUrl("problem");
		$scope.themes = AceFactory.allThemes;
		var courseInstanceID = CentrisAceResource.getValueFromUrl("courseinstance"),
		groupSSN             = CentrisAceResource.getValueFromUrl("groupssn");
		if ( CentrisAceResource.getValueFromUrl("theme") !== undefined) {
			$scope.theme     = CentrisAceResource.getValueFromUrl("theme");
		}
		$scope.user      = CentrisAceResource.getValueFromUrl("user");
		$scope.report = "";
		CentrisAceResource.getCodeFromCentrisServer($scope.contest, $scope.problem,
		courseInstanceID, groupSSN, $scope.showCode);
	};
	CentrisAceResource.init().then($scope.init);
}]);