"use strict";

angular.module("homeApp").controller("SidebarController",
function SidebarController($scope, $rootScope, CentrisResource, AuthenticationService, CourseInstanceRoleService) {
	$scope.courses = [];
	$scope.teacherAndStudent = false;

	function getCourses() {
		CentrisResource.get("my", "courses").success(function(data) {
			$scope.courses = data;

			if ((data.Studying.length > 0) && (data.Teaching.length > 0)) {
				$scope.teacherAndStudent = true;
			}
		});
		$scope.showSidebar = true;
	}

	// When the user has successfully logged in, we reload the list
	// of courses:
	$scope.$on("loginSuccess", getCourses);

	$scope.$on("logoutSuccess", function() {
		$scope.courses = [];
		$scope.teacherAndStudent = false;
		$scope.showSidebar = false;
	});

	// Note that the user may open the application and already be logged
	// in. In that case, we simply load the list of courses immediately.
	if (AuthenticationService.isLoggedIn()) {
		getCourses();
	}

	// Open and close mobile sidebar.
	$scope.toggleVisibility = function() {
		$scope.isOpen = !$scope.isOpen;

		// Make sure that the page isn't scrollable when
		// sidebar is open on mobile devices.
		$rootScope.sidebarIsOpen = $scope.isOpen;
	};

	// Listen for a event fired by the hamburger.
	$scope.$on("Sidebar-toggle", $scope.toggleVisibility);

	// Close the sidebar on navigation.
	$(".Sidebar").on("click", ".Pill", function() {
		if ($scope.isOpen) {
			$scope.toggleVisibility();
		}
	});
});
