"use strict";

angular.module("homeApp").directive("toggleMinNav",
function toggleMinNav($rootScope) {
	return {
		restrict: "A",
		link: function(scope, ele) {
			var $window, Timer, app, updateClass;
			return app = $("#app"), $window = $(window), ele.on("click", function(e) {
				return app.hasClass("nav-min") ?
					app.removeClass("nav-min") :
					(app.addClass("nav-min"), $rootScope.$broadcast("minNav:enabled")), e.preventDefault();
			}), Timer = void 0, updateClass = function() {
				var width;
				return width = $window.width(), 768 > width ? app.removeClass("nav-min") : void 0;
			}, $window.resize(function() {
				var t;
				return clearTimeout(t), t = setTimeout(updateClass, 300);
			});
		}
	};
});
