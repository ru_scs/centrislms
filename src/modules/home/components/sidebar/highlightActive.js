"use strict";

angular.module("homeApp").directive("highlightActive",
function highlightActive($location, $timeout) {
	var highlightedClass = "is-selected";
	var parentSelector   = ".Sidebar-item";
	var openClass        = "is-open";

	function highlightPath(sidebar) {
		return function() {
			var path = $location.path();

			// Remove current highlights
			sidebar.find("." + highlightedClass).removeClass(highlightedClass);

			if (path === "" || path === "/") {
				return;
			}

			// Set current highlighted and open
			sidebar.find("[href=\"#" + $location.path() + "\"]").parent()
				.addClass(highlightedClass)
				.closest(parentSelector)
				.addClass(openClass);
		};
	}

	function link(scope, element) {
		scope.$on("$locationChangeStart", highlightPath(element));

		// Hack: Replace with a sane approach.
		// Need to wait for side bar to have all content present.
		$timeout(highlightPath(element), 2000);
	}

	return {
		restrict: "A",
		link: link
	};
});
