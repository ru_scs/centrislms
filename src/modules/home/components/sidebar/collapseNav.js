"use strict";

angular.module("homeApp").directive("collapseNav", function() {
	function link(scope, element) {
		var GROUP_IS_OPEN_ = "is-open";

		element.on("click", ".Sidebar-group", function(event) {
			event.preventDefault();

			var current = $(this);
			var group = current.parent();
			var list = current.next();
			var isOpen = group.hasClass(GROUP_IS_OPEN_);

			group.siblings()
				.removeClass(GROUP_IS_OPEN_)
				.find("ul")
				.slideUp(200);

			list.slideToggle(200);
			group.toggleClass(GROUP_IS_OPEN_, !isOpen);
		});
	}

	return {
		restrict: "A",
		link: link
	};
});
