"use strict";

angular.module("homeApp").controller("HomeController",
function ($scope, EventResource, centrisNotify, $translate) {
	$scope.events =           {};
	$scope.scheduleEvents =   [];
	$scope.schoolWorkEvents = [];
	$scope.socialEvents =     [];

	// Get events
	$scope.loadingData = true;
	EventResource.getEvents().success(function (data) {
		$scope.loadingData = false;
		$scope.events = data;
		addEvents();
	})
	.error(function (data, status, headers, config) {
		$scope.loadingData = false;
		if (status !== 401) {
			centrisNotify.error("FrontPageFeed.DashboardError");
		} else {
			$scope.notLoggedIn = true;
		}
	});

	// Returns the correct html template according to category and type
	$scope.getEventView = function (event) {
		var filename = event.Category + "." + event.Type + ".html";
		// TODO: can we ask the $templateCache if the given filename
		// exists? And if not, return a default template?
		return "home/components/home/templates/" + filename;
	};

	// Adds events to appropriate scope array
	function addEvents () {
		$scope.scheduleEvents   = $scope.events.ScheduleEvents;
		$scope.schoolWorkEvents = $scope.events.SchoolWorkEvents;
		$scope.socialEvents     = $scope.events.SocialEvents;
	}

	// Defines the steps and which DOM elements each step will attach to:
	$scope.IntroOptions = {
		steps: [{
				element: "#IntroHomeStep1"
			}, {
				element: "#IntroHomeStep2",
				position: "right"
			}, {
				element: "#IntroHomeStep3",
				position: "right"
			}, {
				element: "#IntroHomeStep4",
				position: "left"
			}, {
				element: "#IntroHomeStep5",
				position: "left"
			}, {
				element: "#IntroHomeStep6",
				position: "left"
			}, {
				element: "#IntroHomeStep7",
				position: "right"
			}, {
				element: ".Search-input",
				position: "bottom"
			}
		],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};

	// Calling the translate files for each step and putting them into an array
	var introKeys = [
		"AngularIntro.Welcome",
		"AngularIntro.MyCources",
		"AngularIntro.LeftSidebar",
		"AngularIntro.PersonalInfo",
		"AngularIntro.Notifications",
		"AngularIntro.Upcoming",
		"AngularIntro.SpinningLogo",
		"AngularIntro.SearchBar"
	];

	// Setting the translates as the text for each step
	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
		$scope.IntroOptions.steps[1].intro = translations[introKeys[1]];
		$scope.IntroOptions.steps[2].intro = translations[introKeys[2]];
		$scope.IntroOptions.steps[3].intro = translations[introKeys[3]];
		$scope.IntroOptions.steps[4].intro = translations[introKeys[4]];
		$scope.IntroOptions.steps[5].intro = translations[introKeys[5]];
		$scope.IntroOptions.steps[6].intro = translations[introKeys[6]];
		$scope.IntroOptions.steps[7].intro = translations[introKeys[7]];
	});

	// Calling the translate files for each button and putting them into an array
	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	// Setting the translates as the text for each button
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.$on("onCentrisIntro", function() {
		$scope.ShowHomeIntro();
	});
});
