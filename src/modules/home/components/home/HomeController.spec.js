"use strict";

describe("HomeController", function() {
	beforeEach(module("homeApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));

	var scope, controller, createController;

	var dashboard = {
		ScheduleEvents: [{
				ID:       0,
				Category: "Schedule",
				Type:     "Lecture",
				Room:     "V101"
			}
		],
		SchoolWorkEvents: [{
				ID:       1,
				Category: "SchoolWork",
				Type:     "Project",
				Class:    "WEPO"
			}
		],
		SocialEvents: [{
				ID:       2,
				Category: "SocialEvent",
				Type:     "ScienceTrip",
			}
		]
	};

	var mockEventResource = {
		getEvents: function () {
			return {
				success: function (fn) {
					fn(dashboard);
					return {
						error: function (errorFn) { }
					};
				}
			};
		}
	};

	var mockEventResourceError = {
		getEvents: function () {
			return {
				success: function (fn) {
					return {
						error: function (errorFn) {
							errorFn();
						}
					};
				}
			};
		}
	};

	var mockCentrisNotify = {
		error: function (msg) { }
	};

	var mockTranslate = function mockTranslate(arr) {
		return {
			then: function(fn) {

			}
		};
	};

	beforeEach(inject(function ($rootScope, $controller) {
		scope = $rootScope.$new();

		createController = function (isError) {
			var mockEvent = isError ? mockEventResourceError : mockEventResource;

			return $controller("HomeController",{
				$scope:        scope,
				EventResource: mockEvent,
				centrisNotify: mockCentrisNotify,
				$translate:    mockTranslate
			});
		};

		spyOn(mockEventResource, "getEvents").and.callThrough();
		spyOn(mockEventResourceError, "getEvents").and.callThrough();
		spyOn(mockCentrisNotify, "error");
	}));

	describe("when eventResource returns success", function () {
		beforeEach(function () {
			controller = createController(false);
		});

		it("should call EventResource for events", function () {
			expect(mockEventResource.getEvents).toHaveBeenCalled();
			expect(scope.events).toEqual(dashboard);
			expect(scope.scheduleEvents).toEqual(dashboard.ScheduleEvents);
			expect(scope.schoolWorkEvents).toEqual(dashboard.SchoolWorkEvents);
			expect(scope.socialEvents).toEqual(dashboard.SocialEvents);
		});

		it("should return the correct filename", function () {
			var ev = dashboard.ScheduleEvents[0];
			var filename = ev.Category + "." + ev.Type + ".html";
			var fullPath = "home/components/home/templates/" + filename;

			var result = scope.getEventView(ev);
			expect(result).toEqual(fullPath);
		});
	});

	describe("when eventResource returns error", function () {
		beforeEach(function () {
			controller = createController(true);
		});

		it("should log to console that getEvents failed", function () {
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("FrontPageFeed.DashboardError");
		});
	});
});
