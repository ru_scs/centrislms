"use strict";

angular.module("homeApp").factory("EventResource",
function (CentrisResource) {
	return {
		getEvents: function () {
			return CentrisResource.get(
				"my",
				"dashboard"
			);
		},
		getMockEvents: function () {
			var events = [{
					Category:    "Schedule",
					Type:        "Lecture",
					DateCreated: new Date(),
					Body: {
						Title:     "HTML5 new elements",
						Course:    "WEPO II",
						ClassRoom: "M102",
						StartTime: new Date("April 29, 2015 15:00:00"),
						EndTime:   new Date("April 29, 2015 17:00:00"),
					}
				}, {
					Category:   "SchoolWork",
					Type:       "Project",
					DateCreated: new Date(),
					Body: {
						Title:     "Flappy Bird",
						Course:    "WEPO II",
						Handin:    false,
						StartTime: new Date("May 15, 2015 9:00:00"),
						EndTime:   new Date("June 1, 2015 23:59:59")
					}
				}, {
					Category:   "SchoolWork",
					Type:       "Project",
					DateCreated: new Date(),
					Body: {
						Title:     "Malloclab",
						Course:    "STÝ",
						Handin:    true,
						StartTime: new Date("May 20, 2015 9:00:00"),
						EndTime:   new Date("June 2, 2015 23:59:59")
					}
				}, {
					Category:   "Schedule",
					Type:       "Lecture",
					DateCreated: new Date(),
					Body: {
						Title:     "Bufflab overview",
						Course:    "STÝ",
						ClassRoom: "V101",
						StartTime: new Date("April 29, 2015 17:00:00"),
						EndTime:   new Date("April 29, 2015 19:00:00"),
					}
				}, {
					Category:    "SchoolWork",
					Type:        "OnlineExam",
					DateCreated: new Date(),
					Body: {
						Title:     "Online exam 1",
						Course:    "Inngangur að tölvunarfræði",
						Handin:    false,
						StartTime: new Date(),
						EndTime:   new Date("April 30, 2015 23:00:00")
					}
				}, {
					Category:    "SchoolWork",
					Type:        "CourseRegistrationDeadline",
					DateCreated: new Date(),
					Body: {
						Title:     "Course Registration Deadline",
						Handin:    false,
						StartTime: new Date("April 1, 2015, 9:00:00"),
						EndTime:   new Date("May 10, 2015 23:59:59")
					}
				}, {
					Category:    "SchoolWork",
					Type:        "TeacherEvaluationDeadline",
					DateCreated: new Date(),
					Body: {
						Title:     "Teacher Evaluation Deadline",
						Handin:    true,
						StartTime: new Date("April 7, 2015 9:00:00"),
						EndTime:   new Date("May 2, 2015 23:59:59")
					}
				}, {
					Category:    "Social",
					Type:        "Social",
					DateCreated: new Date(),
					Body: {
						Title:        "Vísindaferð í Meniga",
						Registration: new Date("May 6, 2015 13:37:00"),
						StartTime:    new Date("May 8, 2015 17:00:00"),
						EndTime:      new Date("May 8, 2015 19:00:00")
					}
				}, {
					Category:    "SchoolWork",
					Type:        "Exam",
					DateCreated: new Date(),
					Body: {
						Title:     "Final Exam",
						Course:    "WEPO II",
						StartTime: new Date("April 20, 2015 9:00:00"),
						EndTime:   new Date("April 20, 2015 12:00:00")
					}
				}, {
					Category:    "Social",
					Type:        "Social",
					DateCreated: new Date(),
					Body: {
						Title:        "Vísindaferð í CCP",
						Handin:       true,
						Registration: new Date("April 6, 2015 13:37:00"),
						StartTime:    new Date("April 8, 2015 17:00:00"),
						EndTime:      new Date("April 8, 2015 19:00:00")
					}
				}
			];
			return events;
		},
		postEvent: function () {

		}
	};
});
