"use strict";

angular.module("homeApp", ["pascalprecht.translate", "ui.router", "angular-intro"])
.config(function ($stateProvider) {
	$stateProvider.state("home", {
		url:         "/",
		templateUrl: "home/components/home/home.html",
		controller:  "HomeController"
	});

	$stateProvider.state("comingsoon", {
		url:         "/comingsoon",
		templateUrl: "home/components/comingsoon/comingsoon.html"
	});
});
