"use strict";

angular.module("homeApp").controller("HomeFooterController",
function HomeFooterController($rootScope) {
	$rootScope.showIntro = function showIntro() {
		// We don't actually do anything here. Instead,
		// we broadcast an event, and the "active" page may or may
		// not respond to it by initiating an "intro" session,
		// guiding the user through that particular page.
		$rootScope.$broadcast("onCentrisIntro");
	};
});
