"use strict";

angular.module("homeApp").controller("TopbarController",
function TopbarController($scope, $rootScope, AuthenticationService) {
	$scope.showTopbarItems = false;
	$scope.toggleSearch = function() {
		$scope.isSearching = !$scope.isSearching;
	};
	$scope.toggleSidebar = function() {
		$rootScope.$broadcast("Sidebar-toggle");
	};
	$scope.$on("loginSuccess", function() {
		$scope.showTopbarItems = true;
	});
	$scope.$on("logoutSuccess", function() {
		$scope.showTopbarItems = false;
	});
	if (AuthenticationService.isLoggedIn()) {
		$scope.showTopbarItems = true;
	}
});
