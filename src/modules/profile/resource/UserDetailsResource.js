"use strict";

angular.module("userProfileApp").factory("UserProfileResource",
function UserDetailsResource(CentrisResource) {
	return {
		getMyDetails: function() {
			return CentrisResource.get("my/profile");
		},
		updateMyDetails: function(returnObject) {
			return CentrisResource.put("my", "profile", "", returnObject);
		},
		updateMyGeneralSettings: function(returnObject) {
			return CentrisResource.put("my", "profile/generalsettings", "", returnObject);
		},
		updateMyNotificationSettings: function(data) {
			return CentrisResource.put("my", "profile/notifications", "", data);
		},
		updateProfilePicture: function(returnObject) {
			return CentrisResource.post("my", "picture", "", returnObject);
		},
		getCourses: function getCourses() {
			return CentrisResource.get("my", "courses");
		},
		getProfilePicture: function() {
			var params = {
				width: 200,
				height: 200,
				img: true
			};

			return CentrisResource.get("my", "picture?w=:width&h=:height&base64=:img", params);
		},
		updateLanguage: function(lang) {
			var params = {
				lang: lang
			};

			moment.locale(lang);

			return CentrisResource.put("my", "profile/language/:lang", params);
		}
	};
});
