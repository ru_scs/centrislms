"use strict";

angular.module("userProfileApp").controller("UserProfileController",
function UserProfileController($translate,$scope, $state, $uibModal, UserProfileResource, centrisNotify, modalService) {
	$scope.information = {};
	$scope.courses = [];
	$scope.hasNoCourses = true;
	$scope.loadingData = true;
	// GET method for profile information
	UserProfileResource.getMyDetails().success(function(data) {
		$scope.information = data;
		UserProfileResource.getCourses().success(function (courses) {
			$scope.courses = courses;
			$scope.studentCourses = courses["Studying"];
			$scope.teacherCourses = courses["Teaching"];
			var c = courses["Studying"].length + courses["Teaching"].length;
			if (c > 0) {
				$scope.hasNoCourses = false;
			}
			$scope.loadingData = false;
		}).error(function (err) {
			$scope.loadingData = false;
			console.log("Error: ", err);
		});
	}).error(function() {
		$scope.loadingData = false;
		centrisNotify.error("ProfilePage.ErrorMsg");
	});

	// Called when the user wants to update his/her communication settings:
	$scope.updateNotifications = function updateNotifications(data) {
		UserProfileResource.updateMyNotificationSettings(data).success(function() {
			centrisNotify.success("ProfilePage.SuccessMsg");
		}).error(function() {
			centrisNotify.error("ProfilePage.ErrorMsg");
		});
	};

	// Called when the user wants to select a new profile picture:
	$scope.openImgCropper = function() {
		var modalInstance = modalService.show({}, "ImgCropperController",
		"components/images/image-cropper/img-cropper.tpl.html", "md");
		modalInstance.result.then(function(returnObject) {
			UserProfileResource.updateProfilePicture(returnObject).success(function(data) {
				centrisNotify.success("ProfilePage.SuccessMsg");
				$scope.information.HasPendingProfilePictureRequest = true;
			}).error(function(data) {
				centrisNotify.error("ProfilePage.ErrorMsg");
			});
		});
	};

	// Course assignment details tabs
	$scope.tabs = {
		list: [
			{ route: "userprofile",               active: true  },
			{ route: "userprofile.study",         active: false },
			{ route: "userprofile.notifications", active: false },
			{ route: "userprofile.courses",      active: false },
			{ route: "userprofile.teaching",      active: false }
		],
		// Changes the current route
		go: function go(route) {
			$scope.$broadcast(route);
			$state.go(route);
		},
		active: function active(route) {
			return $state.is(route);
		}
	};

	// Listening after state change event
	$scope.$on("$stateChangeSuccess", function stateChangeSuccess() {
		$scope.tabs.list.forEach(function forEachTab(tab) {
			tab.active = $state.is(tab.route);
		});
	});
	// INTRO otions
	$scope.IntroOptions = {
		steps: [{
			element: "#profilepicture",
			position: "right",
			intro: ""
		}, {
			element: "#personalinfo",
			position: "left",
			intro: ""
		}, {
			element: "#personalinfotab",
			position: "right",
			intro: ""
		}, {
			element: "#generalsettings",
			postition: "right",
			intro: ""
		}, {
			element: "#schoolinfotab",
			position: "left",
			intro: ""
		}, {
			element: "#notificationtab",
			position: "left",
			intro: ""
		}, {
			element: "#coursesprofile",
			position: "left",
			intro: ""
		}],
		showStepNumbers:    false,
		exitOnOverlayClick: true,
		exitOnEsc:          true,
	};

	var introKeys = [
		"AngIntroProfile.ProfilePicture",
		"AngIntroProfile.PersonalInfo",
		"AngIntroProfile.PersonalInfoTab",
		"AngIntroProfile.GeneralSettings",
		"AngIntroProfile.SchoolInfoTab",
		"AngIntroProfile.NotificationTab",
		"AngIntroProfile.CoursesProfile"

	];

	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
		$scope.IntroOptions.steps[1].intro = translations[introKeys[1]];
		$scope.IntroOptions.steps[2].intro = translations[introKeys[2]];
		$scope.IntroOptions.steps[3].intro = translations[introKeys[3]];
		$scope.IntroOptions.steps[4].intro = translations[introKeys[4]];
		$scope.IntroOptions.steps[5].intro = translations[introKeys[5]];
		$scope.IntroOptions.steps[6].intro = translations[introKeys[6]];
	});

	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.$on("onCentrisIntro", function(event, args) {
		// Only show this if the current route is for our page:
		if ($state.current.name === "userprofile") {
			$scope.ShowProfileIntro();
		}
	});
});
