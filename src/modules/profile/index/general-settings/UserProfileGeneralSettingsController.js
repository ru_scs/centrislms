"use strict";

angular.module("courseDetailsApp").controller("UserProfileGeneralSettingsController",
function CourseDescriptionController($scope, centrisNotify, UserProfileResource) {
	$scope.updateGeneralSettings = function updateGeneralSettings(personalGeneralSettings) {
		// New object with updated info
		var userUpdate = angular.copy(personalGeneralSettings);

		UserProfileResource.updateMyGeneralSettings(userUpdate).success(function(data) {
			centrisNotify.success("ProfilePage.SuccessMsg");
		}).error(function(data) {
			centrisNotify.error("ProfilePage.ErrorMsg");
		});
	};
});