"use strict";

angular.module("userProfileApp").directive("userprofileGeneralsettings",
	function userprofileGeneralsettings() {
		return {
			restrict: "E",
			templateUrl: "profile/index/general-settings/index.html",
			scope: {
				general: "="
			},
			link: function(scope, element, attribute) {
			},
			controller: "UserProfileGeneralSettingsController"
		};
	});