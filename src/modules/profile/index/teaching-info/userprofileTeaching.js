"use strict";

angular.module("userProfileApp").directive("userprofileTeaching",
function userprofileTeaching() {
	return {
		restrict: "E",
		scope: {
			history: "="
		},
		templateUrl: "profile/components/index/teaching-info/index.html"
	};
});