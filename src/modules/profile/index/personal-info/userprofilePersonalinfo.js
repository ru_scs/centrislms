"use strict";

angular.module("userProfileApp").directive("userprofilePersonalinfo",
function userprofilePersonalinfo() {
	return {
		restrict: "E",
		scope: {
			settings: "="
		},
		templateUrl: "profile/index/personal-info/index.html",
		link: function (scope, element, attributes) {
		},
		controller: "UserProfilePersonalController"
	};
});