"use strict";

angular.module("courseDetailsApp").controller("UserProfilePersonalController",
function CourseDescriptionController($scope, centrisNotify, UserProfileResource) {
	$scope.updatePersonal = function updatePersonal(personalInformations) {
		// New object with updated info
		var userUpdate = angular.copy(personalInformations);

		UserProfileResource.updateMyDetails(userUpdate).success(function(data) {
			centrisNotify.success("ProfilePage.SuccessMsg");
		}).error(function(data) {
			centrisNotify.error("ProfilePage.ErrorMsg");
		});
	};
});