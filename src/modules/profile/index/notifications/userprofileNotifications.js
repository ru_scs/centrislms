"use strict";

angular.module("userProfileApp").directive("userprofileNotifications",
function userprofileCommunication() {
	return {
		restrict: "E",
		scope: {
			settings: "=",
			onUpdateNotifications: "&"
		},
		templateUrl: "profile/index/notifications/index.html",
		link: function (scope, element, attributes) {
		}
	};
});
