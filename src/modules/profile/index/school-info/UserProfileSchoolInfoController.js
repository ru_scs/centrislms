"use strict";

/**
 * This controller takes care of displaying the courses a student has completed,
 * and those courses which are remaining.
 */
angular.module("userProfileApp").controller("UserProfileSchoolInfoController",
function UserProfileSchoolInfoController($scope) {

	$scope.$watch("history", function(newValue, oldValue) {
		if (newValue) {
			$scope.semesterGroups = groupBySemester(newValue.Courses);
		}
	});

	var groupBySemester = function(courseList) {
		var semesterGroups = _.groupBy(courseList, "Semester");
		// TODO: finish this!!!
		return semesterGroups;
	};
});