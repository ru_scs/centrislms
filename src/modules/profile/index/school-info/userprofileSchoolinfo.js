"use strict";

angular.module("userProfileApp").directive("userprofileSchoolinfo",
function userprofileSchoolinfo() {
	return {
		restrict: "E",
		scope: {
			history: "="
		},
		templateUrl: "profile/index/school-info/index.html",
		controller: "UserProfileSchoolInfoController",
		link: function (scope, element, attributes) {
		}
	};
});
