"use strict";

angular.module("userProfileApp").directive("notificationFrequency",
function notificationFrequency($translate) {
	return {
		restrict: "E",
		scope: {
			frequency: "=ngModel"
		},
		templateUrl: "profile/index/notification-frequency/index.html",
		link: function(scope, element, attributes) {

			scope.options = [{
				value: 1,
				caption: "notificationFrequency.Immediately"
			}, {
				value: 2,
				caption: "notificationFrequency.Every10Minutes"
			}, {
				value: 3,
				caption: "notificationFrequency.EveryHour"
			}, {
				value: 4,
				caption: "notificationFrequency.StartOfDay"
			}, {
				value: 5,
				caption: "notificationFrequency.Every2Days"
			}, {
				value: 6,
				caption: "notificationFrequency.StartOfWeek"
			}, {
				value: 7,
				caption: "notificationFrequency.Never"
			}];

			var labels = _.map(scope.options, function(item) {
				return item.caption;
			});

			$translate(labels).then(function(translated) {
				_.forEach(scope.options, function(value, key) {
					value.caption = translated[value.caption];
				});
			});
		}
	};
});