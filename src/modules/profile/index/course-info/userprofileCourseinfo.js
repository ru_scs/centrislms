"use strict";

angular.module("userProfileApp").directive("userprofileCourseinfo",
	function userprofileCourseinfo() {
		return {
			restrict: "E",
			templateUrl: "profile/index/course-info/index.html",
			scope: {
				hasNoCourses: "=",
				studentCourses: "=",
				teacherCourses: "="
			},
			link: function(scope, element, attribute) {
			}
		};
	});