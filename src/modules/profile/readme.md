# Profile

This folder contains the various modules which handle the profile of a user.
This will include:

* The personal information (name, address, phone, email etc.),
* Student information such as which major/emphasisline the student is registered to etc.,
* How notifications should be dispatched to the user (via the web, smartphone, email),
  when they should get dispatched (instantly, every 10 minutes, every hour, 
  every morning etc.) and optionally if any notifications should be disabled.
* Teaching history (if any)
* Possibly more later.

The user should also be able to issue requests to change any of the above,
i.e. those which need approval of the department (such as changes to
the major/courses/coursegroups, change of profile picture and possible more).
An issue will be created and routed to the appropriate department office..