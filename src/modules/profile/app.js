"use strict";

angular.module("userProfileApp", [])
.config(function ($stateProvider) {
	$stateProvider.state("userprofile", {
		url:         "/profile",
		templateUrl: "profile/index/index.html",
		controller:  "UserProfileController"
	});
	$stateProvider.state("userprofile.study", {
		url:         "/study"
	});
	$stateProvider.state("userprofile.notifications", {
		url:         "/notifications"
	});
	$stateProvider.state("userprofile.teaching", {
		url:         "/teaching"
	});
	$stateProvider.state("userprofile.courses", {
		url:         "/courses"
	});
});
