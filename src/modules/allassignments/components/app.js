"use strict";

angular.module("allAssignmentsApp", ["ui.router"])
.config(function ($stateProvider) {
	$stateProvider.state("myAssignments", {
		url:         "/assignments",
		templateUrl: "allassignments/components/index/index.html",
		controller:  "AllAssignmentsController"
	});
});
