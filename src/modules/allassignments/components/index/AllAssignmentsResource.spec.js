'use strict';

describe('AllAssignmentsResource', function() {

	var factory, centris;

	beforeEach(module('allAssignmentsApp', 'sharedServices'));
	beforeEach(module('allAssignmentsApp', function($provide) {
		centris = {};
		centris.get = jasmine.createSpy();
		$provide.value('CentrisResource', centris);
	}));

	beforeEach(inject(function(AllAssignmentsResource) {
		factory = AllAssignmentsResource;
		spyOn(factory, 'getAssignments').and.callThrough();
	}));

	it('getAssignments for undefined', function() {
		factory.getAssignments(undefined);
		expect(centris.get).toHaveBeenCalledWith('my/assignments', '');
	});

	it('getAssignments for defined', function() {
		factory.getAssignments(20162);
		expect(centris.get).toHaveBeenCalledWith('my/assignments', '?semester=:semester', {
			semester: 20162
		});
	});

});
