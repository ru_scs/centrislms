"use strict";

describe("Semester Assignment List", function() {

	var scope, ctrl, filter, state, translate, resource, notify;

	beforeEach(module("allAssignmentsApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockAllAssignmentsApp"));

	beforeEach(inject(function($filter) {
		filter = $filter;
	}));

	beforeEach(function() {
		state = {
			current: {
				name: "current_state"
			}
		};
	});

	beforeEach(inject(function(mockAllAssignmentsResource) {
		resource = mockAllAssignmentsResource;
	}));

	beforeEach(inject(function(mockCentrisNotify) {
		notify = mockCentrisNotify;
	}));

	beforeEach(inject(function(mockTranslate) {
		translate = mockTranslate.mockTranslate;
	}));

	describe("initalize resource success", function() {
		beforeEach(function() {
			resource.setAnswers([
				[{
					Deadline: new Date(2014, 10, 23)
				}, {
					Deadline: new Date(2014, 9, 29)
				}, {
					Deadline: new Date(2014, 11, 16)
				}, {
					Deadline: new Date(2014, 7, 5)
				}]
			]);
			resource.setConditions([
				true // Should load the assignments
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
			ctrl = $controller("AllAssignmentsController", {
				$scope: scope,
				$filter: filter,
				$state: state,
				$translate: translate,
				AllAssignmentsResource: resource,
				centrisNotify: notify
			});
		}));

		it("should have scope.assignemnts set and sorted by deadline", function() {
			expect(scope.assignments).toEqual([{
				Deadline: new Date(2014, 7, 5)
			}, {
				Deadline: new Date(2014, 9, 29)
			}, {
				Deadline: new Date(2014, 10, 23)
			}, {
				Deadline: new Date(2014, 11, 16)
			}]);
		});

		it("should have scope.altArr set to sort by deadline", function() {

		});

		it("should have scope.assignemntCount set", function() {
			expect(scope.assignmentCount).toEqual(4);
		});

		it("should have scope.loadingData to be false", function() {
			expect(scope.loadingData).toEqual(false);
		});

	});

	describe("update semester", function() {

		beforeEach(function() {
			resource.setAnswers([
				[]
			]);
			resource.setConditions([
				true
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
			ctrl = $controller("AllAssignmentsController", {
				$scope: scope,
				$filter: filter,
				$state: state,
				$translate: translate,
				AllAssignmentsResource: resource,
				centrisNotify: notify
			});
		}));

		beforeEach(function() {
			spyOn(scope, "fetchData");
		});

		it("should fetch data for the semester", function() {
			scope.semester = "value";
			scope.updateSemester();
			expect(scope.fetchData).toHaveBeenCalledWith("value");
		});

	});

	describe("initalize resource failed", function() {

		beforeEach(function() {
			resource.setAnswers([{}]);
			resource.setConditions([
				false
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
			spyOn(notify, "error");
			ctrl = $controller("AllAssignmentsController", {
				$scope: scope,
				$filter: filter,
				$state: state,
				$translate: translate,
				AllAssignmentsResource: resource,
				centrisNotify: notify
			});
		}));

		it("should have not load assignments", function() {
			expect(notify.error).toHaveBeenCalledWith("allassignments.Server_Error");
		});

	});

	describe("arrows", function() {

		beforeEach(function() {
			resource.setAnswers([
				[]
			]);
			resource.setConditions([
				true
			]);
		});
		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
			ctrl = $controller("AllAssignmentsController", {
				$scope: scope,
				$filter: filter,
				$state: state,
				$translate: translate,
				AllAssignmentsResource: resource,
				centrisNotify: notify
			});
			scope.showArrow(2);
		}));

		it("should make altArr[2] show true", function() {
			expect(scope.altArr[2].show).toBe(true);
		});

		it("should make altArr[3] show false", function() {
			expect(scope.altArr[3].show).toBe(false);
		});
	});

	describe("fetchData should set loadingData until callback", function() {

		beforeEach(function() {
			resource.setAnswers([
				[]
			]);
			resource.setConditions([
				undefined
			]);
		});
		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
			ctrl = $controller("AllAssignmentsController", {
				$scope: scope,
				$filter: filter,
				$state: state,
				$translate: translate,
				AllAssignmentsResource: resource,
				centrisNotify: notify
			});
			scope.showArrow(2);
		}));

		it("should set loadingData true", function() {
			expect(scope.loadingData).toEqual(true);
		});

	});

	describe("sort", function() {

		beforeEach(function() {
			resource.setAnswers([
				[]
			]);
			resource.setConditions([
				true // Should load the assignments
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
			ctrl = $controller("AllAssignmentsController", {
				$scope: scope,
				$filter: filter,
				$state: state,
				$translate: translate,
				AllAssignmentsResource: resource,
				centrisNotify: notify
			});
		}));

		describe("misc sort", function() {

			// Tests the order function that should sort elements by some property

			it("asc", function() {
				scope.assignmentCount = 5;
				scope.assignments = [{
					Property: 3
				}, {
					Property: 4
				}, {
					Property: 2
				}, {
					Property: 1
				}, {
					Property: 5
				}];
				scope.order("Property");
				expect(scope.assignments).toEqual([{
					Property: 1
				}, {
					Property: 2
				}, {
					Property: 3
				}, {
					Property: 4
				}, {
					Property: 5
				}]);
			});

			it("desc", function() {
				scope.assignmentCount = 3;
				scope.assignments = [{
					Property: 2
				}, {
					Property: 1
				}, {
					Property: 3
				}];
				scope.order("-Property");
				expect(scope.assignments).toEqual([{
					Property: 3
				}, {
					Property: 2
				}, {
					Property: 1
				}]);
			});

			it("equal", function() {
				scope.assignmentCount = 3;
				scope.assignments = [{
					Property: 3
				}, {
					Property: 1
				}, {
					Property: 1
				}];
				scope.order("Property");
				expect(scope.assignments).toEqual([{
					Property: 1
				}, {
					Property: 1
				}, {
					Property: 3
				}]);
			});

		});

		describe("deadline", function() {

			it("asc", function() {
				scope.assignmentCount = 5;
				scope.assignments = [{
					Deadline: new Date(2015, 10, 13)
				}, {
					Deadline: new Date(2014, 10, 16)
				}, {
					Deadline: new Date(2014, 10, 23)
				}, {
					Deadline: new Date(2014, 10, 5)
				}, {
					Deadline: new Date(2014, 10, 10)
				}];
				scope.orderByDeadline(-1);
				expect(scope.assignments).toEqual([{
					Deadline: new Date(2014, 10, 5)
				}, {
					Deadline: new Date(2014, 10, 10)
				}, {
					Deadline: new Date(2014, 10, 16)
				}, {
					Deadline: new Date(2014, 10, 23)
				}, {
					Deadline: new Date(2015, 10, 13)
				}]);
			});

			it("desc", function() {
				scope.assignmentCount = 3;
				scope.assignments = [{
					Deadline: new Date(2015, 10, 13)
				}, {
					Deadline: new Date(2014, 10, 16)
				}, {
					Deadline: new Date(2014, 10, 23)
				}];
				scope.orderByDeadline(1);
				expect(scope.assignments).toEqual([{
					Deadline: new Date(2015, 10, 13)
				}, {
					Deadline: new Date(2014, 10, 23)
				}, {
					Deadline: new Date(2014, 10, 16)
				}]);
			});

			it("equal", function() {
				scope.assignmentCount = 3;
				scope.assignments = [{
					Deadline: new Date(2014, 10, 16)
				}, {
					Deadline: new Date(2014, 10, 16)
				}, {
					Deadline: new Date(2015, 10, 13)
				}];
				scope.orderByDeadline(1);
				expect(scope.assignments).toEqual([{
					Deadline: new Date(2015, 10, 13)
				}, {
					Deadline: new Date(2014, 10, 16)
				}, {
					Deadline: new Date(2014, 10, 16)
				}]);
			});

		});

		describe("rank", function() {

			it("asc", function() {
				scope.assignmentCount = 5;
				scope.assignments = [{
					Rank: "2-2"
				}, {
					Rank: "1-3"
				}, {
					Rank: undefined
				}, {
					Rank: "3-10"
				}, {
					Rank: "200-220"
				}];
				scope.orderByRank(-1);
				expect(scope.assignments).toEqual([{
					Rank: "200-220"
				}, {
					Rank: "3-10"
				}, {
					Rank: "2-2"
				}, {
					Rank: "1-3"
				}, {
					Rank: undefined
				}]);
			});

			it("desc", function() {
				scope.assignmentCount = 4;
				scope.assignments = [{
					Rank: "13-15"
				}, {
					Rank: undefined
				}, {
					Rank: "27-32"
				}, {
					Rank: "5-7"
				}];
				scope.orderByRank(1);
				expect(scope.assignments).toEqual([{
					Rank: undefined
				}, {
					Rank: "5-7"
				}, {
					Rank: "13-15"
				}, {
					Rank: "27-32"
				}]);
			});

			it("equal & undefined", function() {

				scope.assignmentCount = 4;
				scope.assignments = [{
					Rank: "13-15"
				}, {
					Rank: undefined
				}, {
					Rank: undefined
				}, {
					Rank: "5-7"
				}];
				scope.orderByRank(1);
				expect(scope.assignments).toEqual([{
					Rank: undefined
				}, {
					Rank: undefined
				}, {
					Rank: "5-7"
				}, {
					Rank: "13-15"
				}]);
			});
			it("equal & defined", function() {
				scope.assignmentCount = 4;
				scope.assignments = [{
					Rank: "13-15"
				}, {
					Rank: "1-3"
				}, {
					Rank: "8-10"
				}, {
					Rank: "1-3"
				}];
				scope.orderByRank(1);
				expect(scope.assignments).toEqual([{
					Rank: "1-3"
				}, {
					Rank: "1-3"
				}, {
					Rank: "8-10"
				}, {
					Rank: "13-15"
				}]);
			});

		});

	});

	describe("alternate", function() {

		beforeEach(function() {
			resource.setAnswers([
				[]
			]);
			resource.setConditions([
				true // Should load the assignments
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
			ctrl = $controller("AllAssignmentsController", {
				$scope: scope,
				$filter: filter,
				$state: state,
				$translate: translate,
				AllAssignmentsResource: resource,
				centrisNotify: notify
			});
		}));

		it("should reverse arrow", function() {
			var index = 3;
			scope.altArr[index].arrow = true;
			scope.alternate(index, "property");
			expect(scope.altArr[index].arrow).toEqual(false);
		});

		it("should set arrow show", function() {
			var index = 3;
			scope.altArr[index].show = false;
			scope.alternate(index, "property");
			expect(scope.altArr[index].show).toEqual(true);
		});

		it("should clean up previously shown arrow", function() {
			var previ = 2;
			var index = 3;
			scope.altArr[previ].show = true;
			scope.alternate(index, "property");
			expect(scope.altArr[previ].show).toEqual(false);
		});

		describe("misc", function() {

			// alternate between some property

			beforeEach(function() {
				spyOn(scope, "order");
			});

			it("should aternate to reverse", function() {
				var index = 1;
				scope.altArr[index].arrow = false;
				scope.alternate(index, "property");
				expect(scope.order).toHaveBeenCalledWith("-property");
			});

			it("should aternate from reverse", function() {
				var index = 3;
				scope.altArr[index].arrow = true;
				scope.alternate(index, "property");
				expect(scope.order).toHaveBeenCalledWith("property");
			});

		});

		describe("deadline", function() {

			// alternate between some property

			beforeEach(function() {
				spyOn(scope, "orderByDeadline");
			});

			it("should aternate to reverse", function() {
				var index = 3;
				scope.altArr[index].arrow = true;
				scope.alternate(index, "Deadline");
				expect(scope.orderByDeadline).toHaveBeenCalledWith(-1);
			});

			it("should aternate from reverse", function() {
				var index = 1;
				scope.altArr[index].arrow = false;
				scope.alternate(index, "Deadline");
				expect(scope.orderByDeadline).toHaveBeenCalledWith(1);
			});

		});

		describe("rank", function() {

			// alternate between some property

			beforeEach(function() {
				spyOn(scope, "orderByRank");
			});

			it("should aternate to reverse", function() {
				var index = 3;
				scope.altArr[index].arrow = true;
				scope.alternate(index, "Rank");
				expect(scope.orderByRank).toHaveBeenCalledWith(-1);
			});

			it("should aternate from reverse", function() {
				var index = 1;
				scope.altArr[index].arrow = false;
				scope.alternate(index, "Rank");
				expect(scope.orderByRank).toHaveBeenCalledWith(1);
			});

		});

	});

	describe("onCentrisIntro", function() {

		// alternate between some property
		beforeEach(function() {
			resource.setAnswers([
				[]
			]);
			resource.setConditions([
				true // Should load the assignments
			]);
		});

		describe("myAssignments true", function() {

			beforeEach(inject(function($rootScope, $controller) {
				scope = $rootScope.$new();

				scope.ShowMyAssignmentsIntro = jasmine.createSpy().and.callThrough();
				state.current.name = "myAssignments";

				scope.$on = function MockScopeOn(name, fn) {
					fn();
				};

				ctrl = $controller("AllAssignmentsController", {
					$scope: scope,
					$filter: filter,
					$state: state,
					$translate: translate,
					AllAssignmentsResource: resource,
					centrisNotify: notify
				});
			}));

			it("should call ShowMyAssignmentsIntro", function() {
				expect(scope.ShowMyAssignmentsIntro).toHaveBeenCalled();
			});

		});

		describe("myAssignments true", function() {

			beforeEach(inject(function($rootScope, $controller) {
				scope = $rootScope.$new();

				scope.ShowMyAssignmentsIntro = jasmine.createSpy();
				state.current.name = "something entirely different";

				scope.$on = function MockScopeOn(name, fn) {
					fn();
				};

				ctrl = $controller("AllAssignmentsController", {
					$scope: scope,
					$filter: filter,
					$state: state,
					$translate: translate,
					AllAssignmentsResource: resource,
					centrisNotify: notify
				});
			}));

			it("should not call ShowMyAssignmentsIntro", function() {
				expect(scope.ShowMyAssignmentsIntro).not.toHaveBeenCalled();
			});

		});

	});

	describe("myAssignments true", function() {

		var msInADay = 24 * 3600 * 1000;

		beforeEach(function() {
			resource.setAnswers([
				[]
			]);
			resource.setConditions([
				true // Should load the assignments
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
			ctrl = $controller("AllAssignmentsController", {
				$scope: scope,
				$filter: filter,
				$state: state,
				$translate: translate,
				AllAssignmentsResource: resource,
				centrisNotify: notify
			});
		}));

		it("should return the days until assignments deadline", function() {
			scope.time = 5 * msInADay;
			var deadline = 6 * msInADay;
			var assignment = {
				Deadline: deadline
			};
			expect(scope.daysUntilDeadline(assignment)).toEqual(1);
		});

		it("should always return an Integer", function() {
			scope.time = 5 * msInADay;
			var deadline = 8 * msInADay + msInADay * 0.5;
			var assignment = {
				Deadline: deadline
			};
			expect(scope.daysUntilDeadline(assignment)).toEqual(3);
		});

		it("should work for past deadlines", function() {
			scope.time = 13 * msInADay;
			var deadline = 8 * msInADay + msInADay * 0.5;
			var assignment = {
				Deadline: deadline
			};
			expect(scope.daysUntilDeadline(assignment)).toEqual(-5);
		});

	});

});
