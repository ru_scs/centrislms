"use strict";

angular.module("mockAllAssignmentsApp", []).factory("mockAllAssignmentsResource",
	function AllAssignmentsResource() {
		var index = 0;
		var fakeanswers;
		var fakeconditions;
		return {
			setAnswers: function setAnswers(answers) {
				fakeanswers = answers;
			},
			setConditions: function setConditions(conditions) {
				fakeconditions = conditions;
			},
			getAssignments: function getAssignments(semester) {
				var condition = fakeconditions[index];
				var answer = fakeanswers[index];
				index++;
				return {
					success: function(fn) {
						if (condition === undefined) {
							return {
								error: function(fn) {
									return;
								}
							};
						}
						if (condition) {
							fn(answer);
						}
						return {
							error: function(fn) {
								if (!condition) {
									fn(answer);
								}
							}
						};
					}
				};
			}
		};
	});
