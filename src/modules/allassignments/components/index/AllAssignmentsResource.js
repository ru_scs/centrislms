"use strict";

angular.module("allAssignmentsApp").factory("AllAssignmentsResource",
	function AllAssignmentsResource(CentrisResource) {
		return {
			getAssignments: function getAssignments(semester) {
				if (semester) {
					return CentrisResource.get("my/assignments", "?semester=:semester", {
						semester: semester
					});
				} else {
					return CentrisResource.get("my/assignments", "");
				}
			}
		};
	});
