"use strict";

angular.module("allAssignmentsApp").controller("AllAssignmentsController",
	function AllAssignmentsController($scope, $filter, $state, $translate, AllAssignmentsResource, centrisNotify) {

		var orderBy = $filter("orderBy");

		// Only one sorting-arrow will be shown at a time
		$scope.altArr = [{
			arrow: false,
			show: false
		}, {
			arrow: false,
			show: false
		}, {
			arrow: false,
			show: false
		}, {
			arrow: false,
			show: false
		}, {
			arrow: false,
			show: false
		}, {
			arrow: false,
			show: true
		}];

		// Will call the API when a semester is picked from the drop down
		$scope.updateSemester = function updateSemester() {
			$scope.fetchData($scope.semester);
		};

		// Will hide arrows that are not in use
		$scope.showArrow = function showArrow(n) {
			for (var i = 0; i < $scope.altArr.length; i++) {
				$scope.altArr[i].show = false;
			}
			$scope.altArr[n].show = true;
		};

		$scope.order = function search(predicate, reverse) {
			$scope.assignments = orderBy($scope.assignments, predicate, reverse);
		};

		// Special sorting method for dates
		$scope.orderByDeadline = function orderByDeadline(order) {
			var data = $scope.assignments;
			data.sort(function(a, b) {
				var o1 = new Date(a.Deadline);
				var o2 = new Date(b.Deadline);
				if ((o2 - o1) > 0) {
					return order;
				} else if ((o2 - o1) < 0) {
					return -order;
				} else {
					return order;
				}
			});
			return data;
		};

		// Sorts according to the first number in the rank
		$scope.orderByRank = function orderByRank(order) {
			var data = $scope.assignments;
			data.sort(function(a, b) {
				if (a.Rank && !b.Rank) {
					return order;
				} else if (!a.Rank && b.Rank) {
					return -order;
				} else if (!a.Rank && !b.Rank) {
					return 0;
				}
				var sp1 = a.Rank.split("-");
				var o1 = parseFloat(sp1[0]);
				var sp2 = b.Rank.split("-");
				var o2 = parseFloat(sp2[0]);
				if (o1 > o2) {
					return order;
				} else if (o1 < o2) {
					return -order;
				} else {
					return 0;
				}
			});
			return data;
		};

		// Takes care of calling the right sorting method, either acceding or descending
		$scope.alternate = function alternate(n, type) {
			$scope.altArr[n].arrow = !$scope.altArr[n].arrow;
			if (type === "Deadline") {
				if ($scope.altArr[n].arrow) {
					$scope.assignments = $scope.orderByDeadline(1);
				} else {
					$scope.assignments = $scope.orderByDeadline(-1);
				}
			}

			if (type === "Rank") {
				if ($scope.altArr[n].arrow) {
					$scope.assignments = $scope.orderByRank(1);
				} else {
					$scope.assignments = $scope.orderByRank(-1);
				}
			} else {
				if ($scope.altArr[n].arrow) {
					$scope.order("-" + type);
				} else {
					$scope.order(type);
				}
			}
			$scope.showArrow(n);
		};

		$scope.fetchData = function fetchData(semester) {
			$scope.loadingData = true;
			AllAssignmentsResource.getAssignments(semester)
				.success(function successFetchingCourses(data) {
					$scope.assignments = data;
					$scope.assignmentCount = data.length;
					$scope.assignments = $scope.orderByDeadline(-1);
					$scope.loadingData = false;
				})
				.error(function errorFetchingCourses(error) {
					centrisNotify.error("allassignments.Server_Error");
					$scope.loadingData = false;
				});
		};

		$scope.fetchData();

		$scope.time = Date.now();

		$scope.daysUntilDeadline = function(assignment) {
			var deadline = new Date(assignment.Deadline).getTime();
			return Math.floor((deadline - $scope.time) / (24 * 3600 * 1000));
		};

		// Starting helper viewer
		// Defines the steps and calls the step id
		$scope.IntroOptions = {
			steps: [{
				element: "#content",
				position: "right",
				intro: ""
			}, {
				element: "#assignment-helper-step2",
				position: "right",
				intro: ""
			}, {
				element: "#assignment-helper-step3",
				position: "right",
				intro: ""
			}, {
				element: "#assignment-helper-step4",
				position: "right",
				intro: ""
			}],
			showStepNumbers: false,
			exitOnOverlayClick: true,
			exitOnEsc: true,
		};

		// Calling the translate files for each step and putting them into an array
		var introKeys = [
			"allassignmentsHelper.Step1",
			"allassignmentsHelper.Step2",
			"allassignmentsHelper.Step3",
			"allassignmentsHelper.Step4"
		];

		// Setting the translates as the text for each step
		$translate(introKeys).then(function whenDoneTranslating(translations) {
			$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
			$scope.IntroOptions.steps[1].intro = translations[introKeys[1]];
			$scope.IntroOptions.steps[2].intro = translations[introKeys[2]];
			$scope.IntroOptions.steps[3].intro = translations[introKeys[3]];
		});

		// Calling the translate files for each button and putting them into an array
		var introButtons = [
			"AngularIntro.Next",
			"AngularIntro.Prev",
			"AngularIntro.Skip",
			"AngularIntro.Done"
		];

		// Setting the translates as the text for each button
		$translate(introButtons).then(function whenDoneTranslating(translations) {
			$scope.IntroOptions.nextLabel = translations[introButtons[0]];
			$scope.IntroOptions.prevLabel = translations[introButtons[1]];
			$scope.IntroOptions.skipLabel = translations[introButtons[2]];
			$scope.IntroOptions.doneLabel = translations[introButtons[3]];
		});

		$scope.$on("onCentrisIntro", function(event, args) {
			// Only show this if the current route is for our page:
			if ($state.current.name === "myAssignments") {
				$scope.ShowMyAssignmentsIntro();
			}
		});

	});
