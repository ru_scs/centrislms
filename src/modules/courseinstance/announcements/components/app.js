"use strict";

angular.module("announcementsApp", ["pascalprecht.translate", "ui.router", "sharedServices", "textAngular"])
.config(function ($stateProvider) {
	$stateProvider.state("courseinstance.announcements", {
		url:         "/announcements",
		templateUrl: "courseinstance/announcements/components/index/index.html",
		controller:  "AnnouncementsController"
	});
	$stateProvider.state("courseinstance.announcements.details", {
		url:         "/:announcementID",
		templateUrl: "courseinstance/announcements/components/details/announcement-detail.html",
		controller:  "AnnouncementDetailController"
	});
});
