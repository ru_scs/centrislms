"use strict";

angular.module("mockCourseInstanceAnnouncements", []).factory("MockAnnouncementsResource",
function() {
	var mock = {
		successGetList: true,
		successGetDetails: true,
		success: true,
		hasID: true,
		data: {
			Body: "This is a detailed announcement body",
			ID: 3,
			Title: "Detailed announcement",
			PublishDate: "2015-04-30T20:36:24.823",
			AuthorName: "Dabs",
			AuthorSSN: "1203735289",
			HasDiscussionBoard: true
		},
		editedData: {
			Title: "Detailed announcement changed",
			Body: "This is a changed detailed announcement body",
			ForTheWorld: false,
			ForStudents: true,
			ForStaff: true,
			HasDiscussionBoard: true,
			PublishDate: "2015-04-30T20:36:24.823",
		},
		getAnnouncements: function() {
			var data = [{
				Body: "This is announcement",
				ID: 1,
				Title: "Announcement",
				PublishDate: null
			}, {
				Body: "This is another",
				ID: 2,
				Title: "Announcement 2",
				PublishDate: null
			}];

			return {
				success: function (fn) {
					if (mock.successGetList) {
						fn(data);
					}
					return {
						error: function(fn) {
							if (!mock.successGetList) {
								fn();
							}
						}
					};
				}
			};
		},
		getAnnouncementDetail: function() {
			return {
				success: function (fn) {
					if (!mock.hasID) {
						mock.data.ID = undefined;
						fn(mock.data);
					} else if (mock.successGetDetails) {
						fn(mock.data);
					}
					return {
						error: function(fn) {
							if (!mock.successGetDetails) {
								fn();
							}
						}
					};
				}
			};
		},

		addAnnouncement: function() {
			return {
				success: function (fn) {
					if (mock.success) {
						fn(mock.data);
					}
					return {
						error: function(fn) {
							if (!mock.success) {
								fn();
							}
						}
					};
				}
			};
		},

		editAnnouncement: function(courseInstanceID, announcementID, body) {
			return {
				success: function (fn) {
					if (mock.success) {
						fn(mock.editedData);
					}
					return {
						error: function(fn) {
							if (!mock.success) {
								fn();
							}
						}
					};
				}
			};
		},

		deleteAnnouncement: function(data) {
			return {
				success: function (fn) {
					if (mock.success) {
						fn(data);
					}
					return {
						error: function(fn) {
							if (!mock.success) {
								fn();
							}
						}
					};
				}
			};
		}
	};

	return mock;
});
