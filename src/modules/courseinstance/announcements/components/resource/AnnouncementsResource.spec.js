"use strict";

describe("Testing AnnouncementsResource", function () {
	var AnnouncementsResource, mock, mockAnnouncementObject;

	beforeEach(module("announcementsApp", "sharedServices", "courseInstanceShared"));

	beforeEach(module("announcementsApp", function($provide) {
		mock = {};
		mock.get = jasmine.createSpy();
		mock.put = jasmine.createSpy();
		mock.post = jasmine.createSpy();
		mock.remove = jasmine.createSpy();
		$provide.value("CentrisResource", mock);

		mockAnnouncementObject = {
			title: "name",
			files: [{
				file: {
					name: "http://www.url1.com"
				}
			},{
				file: {
					name: "http://www.url2.com"
				}
			}],
			isLink: true
		};
	}));

	beforeEach(inject(function(_AnnouncementsResource_) {
		AnnouncementsResource = _AnnouncementsResource_;
		spyOn(AnnouncementsResource, "getAnnouncements").and.callThrough();
		spyOn(AnnouncementsResource, "addAnnouncement").and.callThrough();
		spyOn(AnnouncementsResource, "getAnnouncementDetail").and.callThrough();
		spyOn(AnnouncementsResource, "deleteAnnouncement").and.callThrough();
		spyOn(AnnouncementsResource, "editAnnouncement").and.callThrough();
	}));

	it("should call get anouncements", function() {
		AnnouncementsResource.getAnnouncements();
		expect(AnnouncementsResource.getAnnouncements).toHaveBeenCalled();
		expect(mock.get).toHaveBeenCalled();
	});

	it("should call add anouncements", function() {
		AnnouncementsResource.addAnnouncement();
		expect(AnnouncementsResource.addAnnouncement).toHaveBeenCalled();
		expect(mock.post).toHaveBeenCalled();
	});

	it("should call get anouncementdetail", function() {
		AnnouncementsResource.getAnnouncementDetail();
		expect(AnnouncementsResource.getAnnouncementDetail).toHaveBeenCalled();
		expect(mock.get).toHaveBeenCalled();
	});

	it("should call delete anouncement", function() {
		AnnouncementsResource.deleteAnnouncement();
		expect(AnnouncementsResource.deleteAnnouncement).toHaveBeenCalled();
		expect(mock.remove).toHaveBeenCalled();
	});

	it("should call edit anouncement", function() {
		AnnouncementsResource.editAnnouncement();
		expect(AnnouncementsResource.editAnnouncement).toHaveBeenCalled();
		expect(mock.put).toHaveBeenCalled();
	});
});
