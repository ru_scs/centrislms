"use strict";

/**
 * All the function necessary to create, edit, get and delete announcements. All these function
 * talk to the Centris API and return success or error like seen f.x. in the Announcement Controller
 */
angular.module("announcementsApp").factory("AnnouncementsResource",
function (CentrisResource) {
	return {
		getAnnouncements: function getAnnouncements(courseID) {
			return CentrisResource.get("courses", ":courseID/announcements", {courseID: courseID});
		},
		addAnnouncement: function addAnnouncement(courseID, data) {
			return CentrisResource.post("courses", ":courseID/announcements", {courseID: courseID}, data);
		},
		getAnnouncementDetail: function addAnnouncement(courseID, announcementID) {
			var param = {
				courseID: courseID,
				announcementID: announcementID
			};
			return CentrisResource.get("courses", ":courseID/announcements/:announcementID", param);
		},
		deleteAnnouncement: function deleteAnnouncement(courseID, announcementID) {
			var param = {
				courseID: courseID,
				announcementID: announcementID
			};
			return CentrisResource.remove("courses", ":courseID/announcements/:announcementID", param);
		},
		editAnnouncement: function editAnnouncement(courseID, announcementID, data) {
			var param = {
				courseID: courseID,
				announcementID: announcementID
			};
			return CentrisResource.put("courses", ":courseID/announcements/:announcementID", param, data);
		}
	};
});
