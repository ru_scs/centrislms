"use strict";

describe("Testing AnnouncementDlg", function () {

	var dialog,
	mockObject = {

	};

	beforeEach(module("announcementsApp", "sharedServices", "mockConfig"));
	beforeEach(inject(function($injector) {
		dialog = $injector.get("AnnouncementDlg");
	}));

	it("can get an instance of AnnouncementDlg", function() {
		expect(dialog).toBeDefined();
	});

	it("AnnouncementDlg.show should be defined", function() {
		dialog.show(mockObject);
		expect(dialog.show).toBeDefined();
	});
});
