"use strict";

describe("AnnouncementDlgController", function() {

	beforeEach(module("announcementsApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));
	beforeEach(module("ui.bootstrap"));
	beforeEach(module("mockCourseInstanceAnnouncements"));

	var scope, rootScope, ctrl, mockModel, dateTS;

	var mockAnnouncement = {
		ID: 79596,
		Title: "Hér er fyrirsögn (breytt)",
		Body: "<p>... og við breytum body-inu.</p>",
		ForTheWorld: true,
		ForStudents: true,
		ForStaff: true,
		AuthorSSN: "1203735289",
		AuthorName: "",
		HasDiscussionBoard: true,
		PublishDate: "2015-02-27T00:50:00",
		ThreadCount: 0
	};

	var mockClose = function(model) {
		mockModel = model;
	};

	describe("AnnouncementDlgController editing announcement success", function() {
		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
			rootScope = $rootScope;

			ctrl = $controller("AnnouncementDlgController", {
				$scope: scope,
				announcement: mockAnnouncement
			});

			scope.$close = mockClose;
			mockModel = undefined;
			mockAnnouncement.PublishDate = "2015-04-30T20:06:02.623";

			spyOn(scope, "$close").and.callThrough();
		}));

		it("should define functions and variables", function() {
			// Variables:
			expect(scope.showError).toBeDefined();
			expect(scope.disableDiscussionBoard).toBeDefined();
			expect(scope.newAnnouncement).toBeDefined();
			expect(scope.minTime).toBeDefined();
			expect(scope.maxTime).toBeDefined();

			// Functions:
			expect(scope.onSave).toBeDefined();
		});

		it("should return the same announcement with a different date if save is the only action", function() {
			var objectIsInvalid = false;
			scope.onSave(objectIsInvalid);

			// Same
			expect(mockModel.Title).toEqual(mockAnnouncement.Title);
			expect(mockModel.Body).toEqual(mockAnnouncement.Body);
			expect(mockModel.ForTheWorld).toEqual(mockAnnouncement.ForTheWorld);
			expect(mockModel.ForStaff).toEqual(mockAnnouncement.ForStaff);
			expect(mockModel.ForStudents).toEqual(mockAnnouncement.ForStudents);
			expect(mockModel.HasDiscussionBoard).toEqual(mockAnnouncement.ForStudents);

			// Different
			expect(mockModel.PublishDate).not.toEqual(mockAnnouncement.PublishDate);
			expect(mockModel.PublishDate).not.toEqual(undefined);

			expect(scope.$close).toHaveBeenCalled();
		});

		it("should not close the dialog on save if the announcement object is invalid", function() {
			var objectIsInvalid = true;
			scope.onSave(objectIsInvalid);
			expect(mockModel).toEqual(undefined);
			expect(scope.$close).not.toHaveBeenCalled();
		});
	});

	describe("announcements with a future Publish date", function() {
		beforeEach(inject(function($rootScope, $controller) {
			// One thousand years enough?
			mockAnnouncement.PublishDate = "3015-04-30T20:06:02.623";
			mockAnnouncement.HasDiscussionBoard = false;

			scope = $rootScope.$new();
			rootScope = $rootScope;

			ctrl = $controller("AnnouncementDlgController", {
				$scope: scope,
				announcement: mockAnnouncement
			});

			scope.$close = mockClose;
			mockModel = undefined;

			spyOn(scope, "$close").and.callThrough();
		}));

		it("should treat announcements correctly", function() {
			var objectIsInvalid = false;
			scope.onSave(objectIsInvalid);

			// Same
			expect(mockModel.Title).toEqual(mockAnnouncement.Title);
			expect(mockModel.Body).toEqual(mockAnnouncement.Body);
			expect(mockModel.ForTheWorld).toEqual(mockAnnouncement.ForTheWorld);
			expect(mockModel.ForStaff).toEqual(mockAnnouncement.ForStaff);
			expect(mockModel.ForStudents).toEqual(mockAnnouncement.ForStudents);

			// Different
			expect(mockModel.PublishDate).not.toEqual(mockAnnouncement.PublishDate);
			expect(mockModel.PublishDate).not.toEqual(undefined);

			expect(scope.$close).toHaveBeenCalled();
		});

		it("should not close the dialog on save if the announcement object is invalid", function() {
			var objectIsInvalid = true;
			scope.onSave(objectIsInvalid);
			expect(mockModel).toEqual(undefined);
			expect(scope.$close).not.toHaveBeenCalled();
		});
	});

	describe("creating new announcements with success", function() {
		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
			rootScope = $rootScope;

			ctrl = $controller("AnnouncementDlgController", {
				$scope: scope,
				announcement: mockAnnouncement
			});

			scope.$close = mockClose;
			mockModel = undefined;
			mockAnnouncement = undefined;

			spyOn(scope, "$close").and.callThrough();
		}));

		it("should define functions and variables", function() {
			// Variables:
			expect(scope.showError).toBeDefined();
			expect(scope.disableDiscussionBoard).toBeDefined();
			expect(scope.newAnnouncement).toBeDefined();
			expect(scope.minTime).toBeDefined();
			expect(scope.maxTime).toBeDefined();

			// Functions:
			expect(scope.onSave).toBeDefined();
		});

		it("should stay the same but with a different date if save is the only thing done", function() {
			var objectIsInvalid = false;
			scope.onSave(objectIsInvalid);

			// Same
			expect(mockModel.Title).toEqual("");
			expect(mockModel.Body).toEqual("");
			expect(mockModel.ForTheWorld).toEqual(true);
			expect(mockModel.ForStaff).toEqual(true);
			expect(mockModel.ForStudents).toEqual(true);
			expect(mockModel.HasDiscussionBoard).toEqual(true);

			// Different
			expect(mockModel.PublishDate).not.toEqual("");
			expect(mockModel.PublishDate).not.toEqual(undefined);

			// Normaly, empty fields we would get invalid errors on this model
			// but that is not what we are testing here
			expect(scope.$close).toHaveBeenCalled();
		});

		it("should not close the dialog on save if the announcement object is invalid", function() {
			var objectIsInvalid = true;
			scope.onSave(objectIsInvalid);
			expect(mockModel).toEqual(undefined);
			expect(scope.$close).not.toHaveBeenCalled();
		});
	});
});
