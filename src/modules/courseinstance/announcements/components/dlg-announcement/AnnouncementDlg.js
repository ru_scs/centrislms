"use strict";

angular.module("announcementsApp").factory("AnnouncementDlg",
function AnnouncementDlg($uibModal) {
	return {
		show: function show(announcement) {
			var templateInstance = {
				controller:  "AnnouncementDlgController",
				templateUrl: "courseinstance/announcements/components/dlg-announcement/announcement-dlg.html",
				resolve: {
					announcement: function () {
						return announcement;
					}
				}
			};

			var modalInstance = $uibModal.open(templateInstance);

			return modalInstance.result;
		}
	};
});