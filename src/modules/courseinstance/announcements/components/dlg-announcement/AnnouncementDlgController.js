"use strict";

/*
 * AnnouncementDlgController controls what happens in the new/edit dialog
 * for an announcement, and can therefore handle both cases.
 * It does NOT interact with the resource, the caller should supply us
 * with the data to display (i.e. if editing), and will receive the object
 * edited by the user.
 */
angular.module("announcementsApp").controller("AnnouncementDlgController",
function AnnouncementDlgController($scope, announcement, dateTimeService) {

	// A few scope variables:
	$scope.showError              = false;
	$scope.disableDiscussionBoard = false;

	// Check if we are about to edit an announcement:
	if (announcement) {
		var parsedDate = Date.parse(announcement.PublishDate);
		var currentDate = true;
		if (announcement.HasDiscussionBoard) {
			$scope.disableDiscussionBoard = true;
		}

		// Check if the announcement meant to be displayed later
		if (Date.now() < parsedDate) {
			currentDate = false;
		}

		$scope.newAnnouncement = {
			title:              announcement.Title,
			displayDate:        "", // ???
			body:               announcement.Body,
			hasDiscussionBoard: announcement.HasDiscussionBoard,
			useCurrentDate:     currentDate,
			date:               moment(parsedDate).format(dateTimeService.dateFormat()),
			time:               moment(parsedDate).format(dateTimeService.timeFormat()),
			id:                 announcement.ID
		};
	} else {
		// Nope, adding a new announcement.
		$scope.newAnnouncement = {
			title:           "",
			displayDate:     "",
			body:            "",
			hasDiscussionBoard: true,
			useCurrentDate:  true,
			date:            "",
			time:            "",
			id:              -1
		};
	}

	// Defining variables for timepicker
	$scope.minTime = moment().hour(0).minute(30).format(dateTimeService.timeFormat());
	$scope.maxTime = moment().hour(0).minute(0).format(dateTimeService.timeFormat());

	// This function is called when the user presses the "Save" button.
	$scope.onSave = function onSave(isInvalid) {
		// Check if we should show errors:
		$scope.showError = false;
		if (isInvalid) {
			$scope.showError = true;
			return;
		}

		var date;

		if ($scope.newAnnouncement.useCurrentDate === true) {
			date = moment();
		} else {
			date = dateTimeService.mergeDateAndTime($scope.newAnnouncement.date.toString(),
				$scope.newAnnouncement.time.toString());
		}

		var model = {
			Title:              $scope.newAnnouncement.title,
			Body:               $scope.newAnnouncement.body,
			ForTheWorld:        true,
			ForStudents:        true,
			ForStaff:           true,
			HasDiscussionBoard: $scope.newAnnouncement.hasDiscussionBoard,
			PublishDate:        date
		};

		$scope.$close(model);
	};
});
