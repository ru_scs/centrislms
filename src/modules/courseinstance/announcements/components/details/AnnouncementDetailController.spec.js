"use strict";

describe("AnnouncementDetailController", function() {

	beforeEach(module("announcementsApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));
	beforeEach(module("ui.bootstrap"));
	beforeEach(module("mockCourseInstanceAnnouncements"));

	var scope, location, ctrl, mockResource, cNotify;

	var mockRoleProvider = {
		isTeacherInCourse: function(courseInstanceID) {
			return {
				then: function(fn) {
					fn(true);
				}
			};
		}
	};

	var mockDlg = {
		userPressedSave: true,
		dlgResult: undefined,
		show: function() {
			return {
				then: function(fn) {
					if (mockDlg.userPressedSave) {
						fn(mockDlg.dlgResult);
					} else {

					}
				}
			};
		}
	};

	describe("loading data success", function() {
		beforeEach(inject(function($rootScope, $controller, $location, MockAnnouncementsResource, centrisNotify) {
			scope = $rootScope.$new();
			location = $location;
			mockResource = MockAnnouncementsResource;
			cNotify = centrisNotify;
			spyOn(mockResource, "getAnnouncementDetail").and.callThrough();
			spyOn(mockResource, "editAnnouncement").and.callThrough();
			spyOn(mockResource, "deleteAnnouncement").and.callThrough();
			spyOn(mockDlg, "show").and.callThrough();
			spyOn(cNotify, "success").and.callThrough();
			spyOn(cNotify, "error").and.callThrough();
			spyOn(location, "path").and.callThrough();
			ctrl = $controller("AnnouncementDetailController", {
				$scope: scope,
				AnnouncementsResource: mockResource,
				CourseInstanceRoleService: mockRoleProvider,
				AnnouncementDlg: mockDlg
			});

			// Reset global variables
			mockDlg.userPressedSave = true;
		}));

		it("should test if functions and variables are defined", function() {
			// Variables:
			expect(scope.announcementDetail).toBeDefined();
			expect(scope.isTeacher).toBeDefined();
			expect(scope.loadingData).toBeDefined();

			// Functions:
			expect(scope.editAnnouncement).toBeDefined();
			expect(scope.deleteAnnouncement).toBeDefined();
		});

		it("should return a detailed announcement", function() {
			expect(mockResource.getAnnouncementDetail).toHaveBeenCalled();
			expect(scope.announcementDetail).toEqual({
				Body: "This is a detailed announcement body",
				ID: 3,
				Title: "Detailed announcement",
				PublishDate: "2015-04-30T20:36:24.823",
				AuthorName: "Dabs",
				AuthorSSN: "1203735289",
				HasDiscussionBoard: true
			});
		});

		// EDIT
		it("should successfully change the announcement", function() {
			scope.editAnnouncement();
			expect(mockDlg.show).toHaveBeenCalled();
			expect(mockResource.editAnnouncement).toHaveBeenCalled();
			expect(cNotify.success).toHaveBeenCalled();
			expect(scope.announcementDetail.Title).toEqual("Detailed announcement changed");
			expect(scope.announcementDetail.Body).toEqual("This is a changed detailed announcement body");
		});

		it("should NOT change the announcement if there was an error", function() {
			mockResource.success = false;
			scope.editAnnouncement();
			expect(mockDlg.show).toHaveBeenCalled();
			expect(mockResource.editAnnouncement).toHaveBeenCalled();
			expect(cNotify.error).toHaveBeenCalled();
			expect(scope.announcementDetail.Title).toEqual("Detailed announcement");
			expect(scope.announcementDetail.Body).toEqual("This is a detailed announcement body");
		});

		it("should NOT change the announcement if we pressed cancel", function() {
			mockDlg.userPressedSave = false;
			scope.editAnnouncement();
			expect(mockDlg.show).toHaveBeenCalled();
			expect(mockResource.editAnnouncement).not.toHaveBeenCalled();
			expect(cNotify.success).not.toHaveBeenCalled();
			expect(cNotify.error).not.toHaveBeenCalled();
			expect(scope.announcementDetail.Title).toEqual("Detailed announcement");
			expect(scope.announcementDetail.Body).toEqual("This is a detailed announcement body");
		});

		// DELETE
		it("should successfully delete announcements", function() {
			scope.deleteAnnouncement();
			expect(mockDlg.show).not.toHaveBeenCalled();
			expect(mockResource.deleteAnnouncement).toHaveBeenCalled();
			expect(cNotify.success).toHaveBeenCalled();
			expect(location.path).toHaveBeenCalledWith("/courses/undefined/announcements");
		});

		it("should not delete on errors", function() {
			mockResource.success = false;
			scope.deleteAnnouncement();
			expect(mockDlg.show).not.toHaveBeenCalled();
			expect(mockResource.deleteAnnouncement).toHaveBeenCalled();
			expect(cNotify.error).toHaveBeenCalled();
			expect(location.path).not.toHaveBeenCalled();
		});
	});

	describe("loading data error", function() {
		beforeEach(inject(function($rootScope, $controller, $location, MockAnnouncementsResource, centrisNotify) {
			scope = $rootScope.$new();
			location = $location;
			mockResource = MockAnnouncementsResource;
			mockResource.successGetDetails = false;
			cNotify = centrisNotify;
			spyOn(mockResource, "getAnnouncementDetail").and.callThrough();
			spyOn(mockResource, "editAnnouncement").and.callThrough();
			spyOn(mockResource, "deleteAnnouncement").and.callThrough();
			spyOn(mockDlg, "show").and.callThrough();
			spyOn(cNotify, "success").and.callThrough();
			spyOn(cNotify, "error").and.callThrough();
			spyOn(location, "path").and.callThrough();
			ctrl = $controller("AnnouncementDetailController", {
				$scope: scope,
				AnnouncementsResource: mockResource,
				CourseInstanceRoleService: mockRoleProvider,
				AnnouncementDlg: mockDlg
			});

			// Reset global variables
			mockDlg.userPressedSave = true;
		}));

		it("should test if functions and variables are defined", function() {
			// Variables:
			expect(scope.announcementDetail).toBeDefined();

			// Functions:
			expect(scope.editAnnouncement).toBeDefined();
			expect(scope.deleteAnnouncement).toBeDefined();
		});

		it("should NOT have an announcement from the resource", function() {
			expect(mockResource.getAnnouncementDetail).toHaveBeenCalled();
			expect(scope.announcementDetail).toEqual({});
		});
	});

	describe("loading with no announcement ID", function() {
		beforeEach(inject(function($rootScope, $controller, $location, MockAnnouncementsResource, centrisNotify) {
			scope = $rootScope.$new();
			location = $location;
			mockResource = MockAnnouncementsResource;
			mockResource.hasID = false;
			cNotify = centrisNotify;
			spyOn(cNotify, "error").and.callThrough();
			spyOn(location, "path").and.callThrough();
			ctrl = $controller("AnnouncementDetailController", {
				$scope: scope,
				AnnouncementsResource: mockResource,
				CourseInstanceRoleService: mockRoleProvider,
				AnnouncementDlg: mockDlg
			});
		}));

		it("should go back to the announcements page if there is no ID", function() {
			expect(cNotify.error).toHaveBeenCalled();
			expect(location.path).toHaveBeenCalledWith("/courses/undefined/announcements");
		});
	});
});
