"use strict";

angular.module("announcementsApp").controller("AnnouncementDetailController",
function AnnouncementDetailController($scope, $stateParams, AnnouncementsResource,
	centrisNotify, $location, CourseInstanceRoleService, AnnouncementDlg) {
	$scope.announcementDetail = {};
	$scope.rid = $stateParams.announcementID;

	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(isTeacher) {
		$scope.isTeacher = isTeacher;
	});

	$scope.loadingData = true;
	AnnouncementsResource.getAnnouncementDetail($stateParams.courseInstanceID, $stateParams.announcementID)
		.success(function(data, status, headers, config) {
			$scope.loadingData = false;
			// this callback will be called asynchronously
			// when the response is available
			if (!data.ID) {
				$location.path("/courses/" + $stateParams.courseInstanceID + "/announcements").replace();
				centrisNotify.error("announcements.NoAnnouncement");
			}
			$scope.announcementDetail = data;
		})
		.error(function(data, status, headers, config) {
			$scope.loadingData = false;
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			centrisNotify.error("announcements.ErrorFromServer");
		});

	$scope.editAnnouncement = function editAnnouncement() {
		AnnouncementDlg.show($scope.announcementDetail).then(function(updatedAnnouncement) {
			AnnouncementsResource.editAnnouncement($stateParams.courseInstanceID,
				$stateParams.announcementID,
				updatedAnnouncement
			)
			.success(function(data, status, headers, config) {
				$scope.announcementDetail = data;
				centrisNotify.success("announcements.AnnouncementEdited");
			})
			.error(function(data, status, headers, config) {
				centrisNotify.error("announcements.CouldNotEditAnnouncement");
			});
		});
	};

	// Note: a very (but not entirely) similar function can be
	// found in AnnouncementsController.
	$scope.deleteAnnouncement = function deleteAnnouncement() {
		AnnouncementsResource.deleteAnnouncement($stateParams.courseInstanceID, $stateParams.announcementID)
			.success(function(data, status, headers, config) {
				$location.path("/courses/" + $stateParams.courseInstanceID + "/announcements").replace();
				centrisNotify.success("announcements.AnnouncementDeleted");
			})
			.error(function(data, status, headers, config) {
				centrisNotify.error("announcements.CouldNotDeleteAnnouncement");
			});
	};
});
