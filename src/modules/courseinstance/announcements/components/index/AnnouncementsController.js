"use strict";

/**
 * AnnouncementsController displays a list of announcements, and has
 * event handlers for create/edit/delete operations.
 */
angular.module("announcementsApp").controller("AnnouncementsController",
function AnnouncementsController($scope, $stateParams, AnnouncementsResource,
	centrisNotify, dateTimeService, CourseInstanceRoleService, AnnouncementDlg) {
	$scope.courseInstanceID = $stateParams.courseInstanceID;
	$scope.announcements = [];

	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(isTeacher) {
		$scope.isTeacher = isTeacher;
	});

	$scope.loadingData = true;
	AnnouncementsResource.getAnnouncements($stateParams.courseInstanceID)
		.success(function(data, status, headers, config) {
			$scope.announcements = data;
			$scope.loadingData   = false;
		})
		.error(function(data, status, headers, config) {
			$scope.loadingData = false;
			centrisNotify.error("announcements.ErrorFromServer");
		});

	$scope.addAnnouncement = function addAnnouncement() {
		AnnouncementDlg.show().then(function(announcement) {
			AnnouncementsResource.addAnnouncement($stateParams.courseInstanceID, announcement)
				.success(function(data, status, headers, config) {
					// Here we add manually to the scope to force the site to show the new announcement
					$scope.announcements.push(data);
					centrisNotify.success("announcements.AddAnnouncementSuccess");
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					centrisNotify.error("announcements.CouldNotAddAnnouncement");
				});
		});
	};

	$scope.editAnnouncement = function editAnnouncement(announcement) {
		AnnouncementDlg.show(announcement).then(function(model) {
			AnnouncementsResource.editAnnouncement($stateParams.courseInstanceID, announcement.ID, model)
			.success(function(data, status, headers, config) {
				// Update the object in the list:
				announcement.PublishDate = data.PublishDate;
				announcement.Title       = data.Title;
				announcement.Body        = data.Body;
				centrisNotify.success("announcements.AnnouncementEdited");
			})
			.error(function(data, status, headers, config) {
				centrisNotify.error("announcements.CouldNotEditAnnouncement");
			});
		});
	};

	$scope.deleteAnnouncement = function deleteAnnouncement(announcementID) {
		AnnouncementsResource.deleteAnnouncement($stateParams.courseInstanceID, announcementID)
			.success(function(data, status, headers, config) {
				_.remove($scope.announcements, { "ID": announcementID});

				var undoParam = {
					type: "announcement-delete",
					id: {
						announcement: announcementID,
						instance:     $scope.courseInstanceID
					}
				};

				centrisNotify.successWithUndo("announcements.AnnouncementDeleted", undoParam);
			})
			.error(function(data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
				centrisNotify.error("announcements.CouldNotDeleteAnnouncement");
			});
	};

	// This event gets broadcast whenever an undo command
	// is issued. We check that it matches an operation we know how to undo.
	$scope.$on("centrisUndo", function undo(event, param) {
		if (param.type === "announcement-delete") {
			AnnouncementsResource.editAnnouncement(param.id.instance,
				param.id.announcement,
				undefined).success(function(data) {
				// TODO:
				centrisNotify.success("Tókst!");
				$scope.announcements.push(data);
			});
		}
	});
});
