"use strict";

describe("AnnouncementsController", function() {

	beforeEach(module("announcementsApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));
	beforeEach(module("ui.bootstrap"));
	beforeEach(module("mockCourseInstanceAnnouncements"));

	var scope, rootScope, ctrl, mockResource, cNotify;

	var mockRoleProvider = {
		isTeacherInCourse: function(courseInstanceID) {
			return {
				then: function(fn) {
					fn(true);
				}
			};
		}
	};

	var mockDlg = {
		userPressedSave: true,
		dlgResult: undefined,
		show: function() {
			return {
				then: function(fn) {
					if (mockDlg.userPressedSave) {
						fn(mockDlg.dlgResult);
					} else {

					}
				}
			};
		}
	};

	describe("loading data success", function() {
		beforeEach(inject(function($rootScope, $controller, MockAnnouncementsResource, centrisNotify) {
			scope = $rootScope.$new();
			rootScope = $rootScope;
			mockResource = MockAnnouncementsResource;
			cNotify = centrisNotify;
			spyOn(mockResource, "getAnnouncements").and.callThrough();
			spyOn(mockResource, "getAnnouncementDetail").and.callThrough();
			spyOn(mockResource, "addAnnouncement").and.callThrough();
			spyOn(mockResource, "editAnnouncement").and.callThrough();
			spyOn(mockResource, "deleteAnnouncement").and.callThrough();
			spyOn(cNotify, "success").and.callThrough();
			spyOn(cNotify, "error").and.callThrough();
			spyOn(cNotify, "successWithUndo").and.callThrough();
			spyOn(mockDlg, "show").and.callThrough();
			ctrl = $controller("AnnouncementsController", {
				$scope: scope,
				AnnouncementsResource: mockResource,
				CourseInstanceRoleService: mockRoleProvider,
				AnnouncementDlg: mockDlg
			});

			// Reset global variables
			mockDlg.userPressedSave = true;
		}));

		it("should define functions and variables", function() {
			// Variables:
			expect(scope.announcements).toBeDefined();

			// Functions:
			expect(scope.addAnnouncement).toBeDefined();
			expect(scope.editAnnouncement).toBeDefined();
			expect(scope.deleteAnnouncement).toBeDefined();
		});

		it("should load list of announcements from resource", function() {
			expect(mockResource.getAnnouncements).toHaveBeenCalled();
			expect(scope.announcements[0].Body).toEqual("This is announcement");
			expect(scope.announcements[1].Body).toEqual("This is another");
		});

		// Adding Announcements
		it("should call addAnnouncement when a valid announcement is provided", function() {
			scope.addAnnouncement();
			expect(mockDlg.show).toHaveBeenCalled();
			expect(mockResource.addAnnouncement).toHaveBeenCalled();
			expect(cNotify.success).toHaveBeenCalled();
			expect(scope.announcements[2].Body).toEqual("This is a detailed announcement body");
		});

		it("should NOT addAnnouncement if save is not pressed", function() {
			mockDlg.userPressedSave = false;
			scope.addAnnouncement();
			expect(mockDlg.show).toHaveBeenCalled();
			expect(mockResource.addAnnouncement).not.toHaveBeenCalled();
			expect(cNotify.success).not.toHaveBeenCalled();
			expect(cNotify.error).not.toHaveBeenCalled();
			expect(scope.announcements.length).toEqual(2);
		});

		it("should NOT addAnnouncement if there was an error", function() {
			mockResource.success = false;
			scope.addAnnouncement();
			expect(mockDlg.show).toHaveBeenCalled();
			expect(mockResource.addAnnouncement).toHaveBeenCalled();
			expect(cNotify.error).toHaveBeenCalled();
			expect(scope.announcements.length).toEqual(2);
		});

		// Editing Announcements
		it("should call editAnnouncement when a valid changes are provided", function() {
			scope.editAnnouncement(scope.announcements[1]);
			expect(mockDlg.show).toHaveBeenCalled();
			expect(mockResource.editAnnouncement).toHaveBeenCalled();
			expect(cNotify.success).toHaveBeenCalled();
			expect(scope.announcements[1].Body).toEqual("This is a changed detailed announcement body");
		});

		it("should NOT call editAnnouncement if save is not pressed", function() {
			mockDlg.userPressedSave = false;
			scope.editAnnouncement(scope.announcements[1]);
			expect(mockDlg.show).toHaveBeenCalled();
			expect(mockResource.editAnnouncement).not.toHaveBeenCalled();
			expect(cNotify.success).not.toHaveBeenCalled();
			expect(cNotify.error).not.toHaveBeenCalled();
			expect(scope.announcements[1].Body).toEqual("This is another");
		});

		it("should NOT change the announcement if there was an error", function() {
			mockResource.success = false;
			scope.editAnnouncement(scope.announcements[1]);
			expect(mockDlg.show).toHaveBeenCalled();
			expect(mockResource.editAnnouncement).toHaveBeenCalled();
			expect(cNotify.error).toHaveBeenCalled();
			expect(scope.announcements[1].Body).toEqual("This is another");
		});

		// Deleting Announcements
		it("should call deleteAnnouncement when a valid changes are provided", function() {
			scope.deleteAnnouncement(scope.announcements[1].ID);
			expect(mockDlg.show).not.toHaveBeenCalled();
			expect(mockResource.deleteAnnouncement).toHaveBeenCalled();
			expect(cNotify.successWithUndo).toHaveBeenCalled();
			expect(scope.announcements.length).toEqual(1);
		});

		it("should NOT add an announcement if there was an error", function() {
			mockResource.success = false;
			scope.deleteAnnouncement(scope.announcements[1].ID);
			expect(mockDlg.show).not.toHaveBeenCalled();
			expect(mockResource.deleteAnnouncement).toHaveBeenCalled();
			expect(scope.announcements.length).toEqual(2);
		});

		// Undoing Announcements
		it("should return a deleted item when undo is pressed", function() {
			scope.deleteAnnouncement(scope.announcements[1].ID);
			expect(scope.announcements.length).toEqual(1);
			rootScope.$broadcast("centrisUndo", {type: "announcement-delete", id: 2});
			expect(mockDlg.show).not.toHaveBeenCalled();
			expect(mockResource.deleteAnnouncement).toHaveBeenCalled();
			expect(scope.announcements[1].Body).toEqual("This is a changed detailed announcement body");
			expect(scope.announcements.length).toEqual(2);
		});

		it("should not return a deleted item when undo is not pressed", function() {
			scope.deleteAnnouncement(scope.announcements[1].ID);
			expect(scope.announcements.length).toEqual(1);
			rootScope.$broadcast("centrisUndo", {type: "not-announcement-delete", id: 2});
			expect(mockDlg.show).not.toHaveBeenCalled();
			expect(mockResource.deleteAnnouncement).toHaveBeenCalled();
			expect(scope.announcements[1]).toBe(undefined);
			expect(scope.announcements.length).toEqual(1);
		});
	});

	describe("loading data error", function() {
		beforeEach(inject(function($rootScope, $controller, MockAnnouncementsResource, centrisNotify) {
			scope = $rootScope.$new();
			mockResource = MockAnnouncementsResource;
			mockResource.successGetList = false;
			cNotify = centrisNotify;
			spyOn(mockResource, "getAnnouncements").and.callThrough();
			spyOn(mockResource, "getAnnouncementDetail").and.callThrough();
			spyOn(cNotify, "success").and.callThrough();
			spyOn(cNotify, "error").and.callThrough();
			spyOn(mockDlg, "show").and.callThrough();
			ctrl = $controller("AnnouncementsController", {
				$scope: scope,
				AnnouncementsResource: mockResource,
				CourseInstanceRoleService: mockRoleProvider,
				AnnouncementDlg: mockDlg
			});
		}));

		it("should define functions and variables", function() {
			// Variables:
			expect(scope.announcements).toBeDefined();

			// Functions:
			expect(scope.addAnnouncement).toBeDefined();
			expect(scope.editAnnouncement).toBeDefined();
			expect(scope.deleteAnnouncement).toBeDefined();
		});

		it("should NOT have a list of announcements from the resource", function() {
			expect(mockResource.getAnnouncements).toHaveBeenCalled();
			expect(scope.announcements.length).toEqual(0);
		});

		it("should display an error message", function() {
			expect(cNotify.error).toHaveBeenCalled();
		});
	});
});
