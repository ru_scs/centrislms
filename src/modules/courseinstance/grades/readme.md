# Courseinstance grades

*	This module displays grades for the selected course.
*	It displays a list of students in course and their grades for 
	each assignment.

*	It shows with colors if the student fails or did not attend the test.

*	Mock data shows students which fail on test, fail on assignment grades, didn't show up for test and students who pass normally.

*	TODO:   API needed, 
			link to individual assignment for student,
			functionality for closing course

*	Known issue the grade chart has some inherited issues from angular-chart chart.js 
	where if the window is resized outside the charts tab it doesn't render the chart when
	its tab is clicked until you resize teh window. You can see the issue being descussed in 
	the angular-chart issue tracker here https://github.com/nnnick/Chart.js/issues/961 