"use strict";

angular.module("gradesApp").factory("CloseCourseDlg",
	function CloseCourseDlg($uibModal) {

		return {
			closeAfterFinalExam: function closeAfterFinalExam() {
				var templateInstance = {
					templateUrl: "courseinstance/grades/components/close-dlg/close-course-dialog-final.tpl.html"
				};
				var modalInstance = $uibModal.open(templateInstance);
				return modalInstance.result;
			},

			closeAfterRetakeExam: function closeAfterRetakeExam() {
				var templateInstance = {
					templateUrl: "courseinstance/grades/components/close-dlg/close-course-dialog-retake.tpl.html"
				};
				var modalInstance = $uibModal.open(templateInstance);
				return modalInstance.result;
			}
		};
	});
