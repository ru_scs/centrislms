"use strict";

describe("CourseGradesController ", function() {

	// $scope, $stateParams, $translate, GradesAppResource,
	// CourseInstanceRoleService, CourseInstanceService, CoursesHomeResource
	var scope, translate, resource, roleService, instanceService, courseHomeResource, ctrl;

	var mockStateParams = {
		courseInstanceID: 54321
	};
	beforeEach(module("gradesApp", "courseHomeApp", "sharedServices", "courseInstanceShared"));
	beforeEach(inject(function(MockGradeAppResource) {
		resource = MockGradeAppResource;
	}));
	beforeEach(inject(function(mockTranslate) {
		translate = mockTranslate.mockTranslate;
	}));
	beforeEach(inject(function(MockCourseInstanceRoleService) {
		roleService = MockCourseInstanceRoleService;
	}));
	beforeEach(inject(function(MockCourseHomeResource) {
		courseHomeResource = MockCourseHomeResource;
	}));
	beforeEach(inject(function(MockCourseInstanceService) {
		instanceService = MockCourseInstanceService;
	}));

	/* describe("general setup", function() {
		beforeEach(function() {
			resource.successGetStudentGrades = true;
		});

		beforeEach(inject(function($rootScope, $controller, $stateParams) {
			scope = $rootScope.$new();
			console.log(scope);
			console.log("HEYHJO");
			ctrl  = $controller("CourseGradesController", {
				$scope:                       scope,
				$translate:                   translate,
				$stateParams:                 mockStateParams,
				GradesAppResource:            resource,
				CourseInstanceRoleService:    roleService,
				CourseInstanceService:        instanceService,
				CoursesHomeResource:          courseHomeResource
			});
		}));

		it("should define scope variables and functions", function() {
			expect(scope.students).toBeDefined();
			expect(scope).toBeDefined();
			expect(scope.loadingData).toBe(true);
			expect(scope.myInfo).toBeDefined();
			expect(scope.assignments).toBeDefined();
			expect(scope.grades).toBeDefined();
			expect(scope.csvObject).toBeDefined();
			expect(scope.csvHeader).toBeDefined();
			expect(scope.estimateGrade).toBeDefined();
			expect(scope.showGradeButton).toBeDefined();
			expect(scope.expectedResult).toBeDefined();
			expect(scope.series).toBeDefined();
			expect(scope.labels).toBeDefined();
			expect(scope.gradeData).toBeDefined();
			expect(scope.options).toBeDefined();
			expect(scope.colours).toBeDefined();
			expect(scope.tabSwitch).toBeDefined();
			expect(scope.toggleShowGradeButton).toBeDefined();
			expect(scope.toggleEstimateGrade).toBeDefined();
		});

		it ("should not be in 'estimateGrade' mode initially", function() {
			expect(scope.estimateGrade).toBe(false);
		});

		it ("should toggle the 'estimateGrade' mode correctly", function() {
			scope.toggleEstimateGrade();
			expect(scope.estimateGrade).toBe(true);
			scope.toggleEstimateGrade();
			expect(scope.estimateGrade).toBe(false);
		});

		it ("should toggle the 'tabSwitch' mode correctly", function() {
			scope.tabSwitch(1);
			expect(scope.tab1).toBe(true);
			expect(scope.tab2).toBe(false);
			scope.tabSwitch(2);
			expect(scope.tab1).toBe(false);
			expect(scope.tab2).toBe(true);
		});

		it ("should toggle the 'showGradeButton' mode correctly", function() {
			var showGradeBtnFirst = scope.showGradeButton;
			scope.toggleShowGradeButton();
			var showGradeBtnSecond = scope.showGradeButton;
			expect(showGradeBtnFirst).not.toEqual(showGradeBtnSecond);
		});

		it ("should put values into the csv header when requested", function() {
			scope.makeCSVHeader();
			expect(scope.csvHeader.length).not.toBe(0);
		});
	});
	*/
});

// How it was before changes. Might help the next person to work on this code to look at it.
/* var scope;
	var controller;
	var $controller;
	var $rootScope;
	var courseRoleService;
	var courseHomeResource;
	var translate;
	var instanceService;
	var roleService;
	var resource;

	beforeEach(module("gradesApp", "courseHomeApp", "sharedServices", "courseInstanceShared"));
	beforeEach(inject(function(_$rootScope_, _$controller_, mockTranslate, MockCourseInstanceRoleService,
		MockCourseHomeResource, MockCourseInstanceService) {
		$rootScope         = _$rootScope_;
		$controller        = _$controller_;
		translate          = mockTranslate.mockTranslate;
		courseRoleService  = MockCourseInstanceRoleService;
		courseHomeResource = MockCourseHomeResource;
		instanceService    = MockCourseInstanceService;
	}));



	var createControllerFunc = function(stateParams, res, courseServ, courseHomeResource, isTeacher) {
		scope = $rootScope.$new();
		roleService = courseRoleService.mockCourseInstanceRoleService(isTeacher);
		resource = res;

		spyOn(roleService, "isTeacherInCourse").and.callThrough();
		spyOn(resource, "getStudentGrades").and.callThrough();
		// spyOn(resource, "getAllStudentGrades").and.callThrough();
		// spyOn(resource, "getExpectedGrade").and.callThrough();
		// spyOn(resource, "closeCourse").and.callThrough();

		controller = $controller("CourseGradesController", {
			$scope:                    scope,
			$stateParams:              stateParams,
			$translate:                translate,
			GradesAppResource:         resource,
			CourseInstanceRoleService: roleService,
			CourseInstanceService:     courseServ,
			CoursesHomeResource:       courseHomeResource
		});
	};

	describe("general setup", function() {
		beforeEach(inject(function(MockGradeAppResource) {
			var resource = MockGradeAppResource.makeResource(true, true, true);
			var instanceServ = instanceService.mockCourseInstanceService(0);
			createControllerFunc(mockStateParams, resource, instanceServ, {}, true);
		}));

		it("should define scope variables and functions", function() {
			console.log(scope.students);
			expect(scope.students).toBeDefined();
			expect(scope.loadingData).toBeDefined();
			expect(scope.myInfo).toBeDefined();
			expect(scope.assignments).toBeDefined();
			expect(scope.grades).toBeDefined();
			expect(scope.csvObject).toBeDefined();
			expect(scope.csvHeader).toBeDefined();
			expect(scope.estimateGrade).toBeDefined();
			expect(scope.showGradeButton).toBeDefined();
			expect(scope.expectedResult).toBeDefined();
			expect(scope.series).toBeDefined();
			expect(scope.labels).toBeDefined();
			expect(scope.gradeData).toBeDefined();
			expect(scope.options).toBeDefined();
			expect(scope.colours).toBeDefined();

			expect(scope.tabSwitch).toBeDefined();
			expect(scope.toggleShowGradeButton).toBeDefined();
			expect(scope.toggleEstimateGrade).toBeDefined();

			// expectedGrade
			// buildExpectedGrades
			// closeCourse
			// changeFinalGrade
			// upgradeGradeGraph
			// initializeStudent
			// initializeTeacher
			// initialize
			// initializeStudentGrades
			// makeCSVHeader
			// getCSV
		});

		it ("should not be in 'estimateGrade' mode initially", function() {
			expect(scope.estimateGrade).toBe(false);
		});

		it("should try to get the role of the current user when loaded", function() {
			expect(roleService.isTeacherInCourse).toHaveBeenCalled();
		});

		it ("should toggle the 'estimateGrade' mode correctly", function() {
			scope.toggleEstimateGrade();
			expect(scope.estimateGrade).toBe(true);
			scope.toggleEstimateGrade();
			expect(scope.estimateGrade).toBe(false);
		});

		it ("should put values into the csv header when requested", function() {
			scope.makeCSVHeader();
			expect(scope.csvHeader.length).not.toBe(0);
		});

	});

	describe("when being loaded as a student ", function() {
		describe("and resource succeedes in loading data", function() {
			beforeEach(inject(function(MockGradeAppResource) {
				var resource = MockGradeAppResource.makeResource(true, true, true);
				var instanceServ = instanceService.mockCourseInstanceService(0);
				createControllerFunc(mockStateParams, resource, instanceServ, {}, false);
			}));

			it ("should call the appropriate scope function", function() {
				expect(resource.getStudentGrades).toHaveBeenCalled();
			});
		});

		describe("and resource fails in loading data", function() {

		});
	});

	describe("when being loaded as a teacher ", function() {
		describe("and resource succeedes in loading data", function() {

		});

		describe("and resource fails in loading data", function() {

		});
	});*/