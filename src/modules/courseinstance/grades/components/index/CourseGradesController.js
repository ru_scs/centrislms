"use strict";

angular.module("gradesApp").controller("CourseGradesController",
function CourseGradesController($scope, $stateParams, $translate, GradesAppResource,
	CourseInstanceRoleService, CourseInstanceService, CoursesHomeResource) {
	CourseInstanceService.getCurrentCourse().then(function(data) {
		$scope.courseData = data;
	});

	$scope.loadingData         = true;
	$scope.myInfo              = {};
	$scope.assignments         = [];
	$scope.students            = [];
	$scope.grades              = {};
	$scope.csvObject           = [];
	$scope.csvHeader           = [];
	// estimates a grade form remaining assignments ex: From remaining assignments what do I need to get 8 in finalgrade
	$scope.estimateGrade       = false;
	$scope.showGradeButton     = false;
	$scope.expectedResult      = "";

	// Variables for graph
	$scope.series              = ["FinalGrade"];
	$scope.labels              = _.range(0,10.5,0.5);
	$scope.gradeData           = [];
	$scope.options             = {
		responsive: true
	};

	// One object for each serie.
	$scope.colours             = [{
		fillColor:       "#428BCA", // "rgba(66,  139, 202, 1)",
		strokeColor:     "#E5E5E5", // "rgba(229, 229, 229, 0.8)",
		highlightFill:   "#D6222B", // "rgba(214, 34,  43,  1)",
		highlightStroke: "#E5E5E5"  // "rgba(229, 229, 229, 0.8)"
	}];

	// Booleans for active tab variables
	$scope.tab1 = true;
	$scope.tab2 = false;

	// Function for switching from tab 1 to 2 and loading graph for tab 2.
	$scope.tabSwitch = function tabSwitch(tab) {
		if (tab === 1) {
			$scope.tab1 = true;
			$scope.tab2 = false;
		} else if (tab === 2) {
			$scope.tab1 = false;
			$scope.tab2 = true;
		}
	};

	// Toggles if assignment grades are shown
	$scope.toggleShowGradeButton = function () {
		$scope.showGradeButton = !$scope.showGradeButton;
		return $scope.showGradeButton;
	};

	// Toggles student grade checking
	$scope.toggleEstimateGrade = function () {
		$scope.estimateGrade = !$scope.estimateGrade;
		return $scope.estimateGrade;
	};

	$scope.expectedGrade       = function expectedGrade() {
		GradesAppResource.getExpectedGrade($stateParams.courseInstanceID, $scope.calcGrades)
		.success(function success(data) {
			$scope.initializeStudent(data);
		});
	};
	// This function builds the array of objects we send to the server
	// when the student wants to calculate expected grade
	$scope.buildExpectedGrades = function buildExpectedGrades() {
		$scope.calcGrades = [];

		for (var i = 0; i < $scope.assignments.length; ++i) {
			var a = $scope.assignments[i];
			var grade = {
				ID: a.ID,
				Grade: $scope.grades[$scope.student.SSN][a.ID]
			};
			if (grade.Grade === undefined) {
				grade.Grade = 0;
			}
			$scope.calcGrades.push(grade);
		}
	};
	// Closes the course after final exam and retake exam
	$scope.closeCourse         = function closeCourse() {
		var gradesObject = [];

		$scope.students.forEach(function forEachStudent(student) {
			gradesObject.push({
				CourseID:  $stateParams.courseInstanceID,
				SSN:       student.SSN,
				Grade:     student.GivenFinalGrade
			});
		});

		var result = {
			CourseID:            $stateParams.courseInstanceID,
			StudentFinalGrades:  gradesObject
		};

		GradesAppResource.closeCourse($stateParams.courseInstanceID, result).success(function success() {
			if ($scope.courseData.Status === 1 || $scope.courseData.Status === 3) {
				CoursesHomeResource.getCourseInstance($scope.courseInstanceID).success(function(instance) {
					// Some child components might be interested in having
					// access to this data:
					CourseInstanceService.setCurrentCourse(instance);
					$scope.courseData = instance;
				});
			}
		}).then(function () {
			$scope.toggleShowGradeButton();
		});
	};

	// Updates and sends an object to the API to update the final grade of a student.
	$scope.changeFinalGrade    = function changeFinalGrade(ssn, grade) {
		$scope.students.forEach(function forEachStudent(student) {
			if (student.SSN === ssn) {
				student.GivenFinalGrade = grade;
			}
		});
	};

	// Sets variables for the graph and updates it.
	$scope.upgradeGradeGraph   = function upgradeGradeGraph() {
		var graphData    = [];
		// 21, because the grades from 0 to 10 (with 0.5 increment)
		// are 21.
		for (var i = 0; i < 21; i++) {
			graphData.push(0);
		}
		$scope.gradeData.push(graphData);
	};
	// Should work correctly once the fixes in the api routing have complete
	// Initializes the controller, fetches all grades for logged in student
	$scope.initializeStudent   = function initializeStudent(data) {
		$scope.estimateGrade             = false;
		$scope.student                   = data.Students[0];
		$scope.assignments               = data.Assessments;
		$scope.myInfo.SSN                = $scope.student.SSN;
		$scope.grades[$scope.myInfo.SSN] = {};

		$scope.student["Grades"].forEach(function forEachGrade(grade) {
			if (grade.AssessmentGrades.length > 0) {
				$scope.grades[$scope.myInfo.SSN][grade.AssessmentID.toString()] = grade.TotalAssessmentGrade;
			} else {
				$scope.grades[$scope.myInfo.SSN][grade.AssessmentID.toString()] = undefined;
			}
		});

		/* uncomment and fix this code once statistics have been fixed in the api.
		$scope.gradeData    = [];
		if (data.Statistics) {
			$scope.avgGrade     = data.Statistics.avgGrade;
			$scope.medGrade     = data.Statistics.medGrade;
			$scope.stdDiv       = data.Statistics.stdDiv;
			$scope.mode         = data.Statistics.mode;
			$scope.noOfGrades   = data.Statistics.noOfGrades;
			$scope.gradeData.push(data.Statistics.gradeData);
		}*/
	};

	// Initializes the controller and fetches all grades for the teacher
	$scope.initializeTeacher   = function initializeTeacher() {
		GradesAppResource.getStudentGrades($stateParams.courseInstanceID).success(function success(data) {
			$scope.assignments = {};
			$scope.grades      = [];
			$scope.students    = [];
			$scope.finalGrades = [];

			$scope.assignments  = data.Assessments;
			// We add this boolean property to each assessment,
			// such that we can specifically mark the
			// column where the final exam is.
			$scope.assignments.forEach(function (assessment) {
				assessment.isFinalExam = (assessment.AssessmentType === 4);
			});

			// This builds the student object currently needed by the scope.
			data.Students.forEach(function forEachStudent(student) {
				student.GivenFinalGrade = {};
				student.GivenFinalGrade = student.SuggestedFinalGrade;
				$scope.students.push(student);

				student.hasFinalGrade = (!(student.GivenFinalGrade === null || student.GivenFinalGrade === undefined));

				// Building the grade object used by the scope.
				$scope.grades[student.SSN] = {};
				student.Grades.forEach(function forEachGrade(grade) {
					if (grade.AssessmentGrades.length > 0) {
						$scope.grades[student.SSN][grade.AssessmentID.toString()] = grade.TotalAssessmentGrade;
					} else {
						$scope.grades[student.SSN][grade.AssessmentID.toString()] = undefined;
					}
				});

				// If the course has been closed after a final test we assign the db's value
				// to the given finalgrade
				if ($scope.courseData.Status > 1) {
					data.FinalGrades.forEach(function (fGrade) {
						if (student.SSN === fGrade.SSN) {
							student.GivenFinalGrade = fGrade.Grade >= 0.0 ?
								fGrade.ResultID === 9 ?
									"DNQ" : fGrade.ResultID === 7 ?
									"Finished" : fGrade.Grade : fGrade.Grade;
						}
					});
				}
			});
			/* uncomment and fix this code once statistics have been fixed in the api
			$scope.avgGrade     = data.Statistics.avgGrade;
			$scope.medGrade     = data.Statistics.medGrade;
			$scope.stdDiv       = data.Statistics.stdDiv;
			$scope.mode         = data.Statistics.mode;
			$scope.noOfGrades   = data.Statistics.noOfGrades;
			$scope.gradeData    = [];
			$scope.gradeData.push(data.Statistics.gradeData);*/

			$scope.makeCSVHeader();
			$scope.getCSV();
			$scope.loadingData = false;
		}).error(function () {
			$scope.loadingData = false;
		});
	};

	// Calls the right initialize controller depending on role in class
	$scope.initialize          = function initialize() {
		$scope.loadingData = true;
		$scope.upgradeGradeGraph();
		if ($scope.isTeacher) {
			$scope.initializeTeacher();
		} else {
			$scope.initializeStudentGrades();
		}
	};

	// Reverts calculated grades back to original
	$scope.initializeStudentGrades = function initializeStudentGrades() {
		GradesAppResource.getStudentGrades($stateParams.courseInstanceID).success(function success(data) {
			$scope.initializeStudent(data);
			$scope.loadingData = false;
		}).error(function () {
			$scope.loadingData = false;
		});
	};

	// Create the header for the CSV files that can be downloaded
	// it unfortunately has no usability other than just naming the
	// fields in the file it does not bind variable names to rows.
	$scope.makeCSVHeader       = function makeCSVHeader() {

		var lang = $translate.use() === "is";

		if (lang) {
			$scope.csvHeader.push("Kennitala");
			$scope.csvHeader.push("Nafn");
		} else {
			$scope.csvHeader.push("SSN");
			$scope.csvHeader.push("Name");
		}

		$scope.assignments.forEach(function forEachAssignment(assignment) {
			$scope.csvHeader.push("V" + assignment.ID);
		});

		if (lang) {
			$scope.csvHeader.push("Vegid");
			$scope.csvHeader.push("Runad");
			$scope.csvHeader.push("Loka");
		} else {
			$scope.csvHeader.push("Weighted");
			$scope.csvHeader.push("Rounded");
			$scope.csvHeader.push("Final");
		}
	};

	// Returns an object that that holds all data a teachers
	// would want in a csv file.
	$scope.getCSV              = function getCSV() {
		var lang = $translate.use() === "is";

		$scope.students.forEach(function forEachStudent(student) {
			var tmpStudent = {};

			if (lang) {
				tmpStudent["Kennitala"] = student.SSN;
				tmpStudent["Nafn"]      = student.FullName;
			} else {
				tmpStudent["SSN"]       = student.SSN;
				tmpStudent["Name"]      = student.FullName;
			}

			$scope.assignments.forEach(function forEachAssignment(assignment) {
				if ($scope.grades[student.SSN] !== undefined && $scope.grades[student.SSN][assignment.ID]) {
					tmpStudent["V" + assignment.ID] = $scope.grades[student.SSN][assignment.ID];
				} else {
					tmpStudent["V" + assignment.ID] = null;
				}
			});

			if (lang) {
				tmpStudent["Vegid"]     = student.WeightedAverage;
				tmpStudent["Runad"]     = student.RoundedWeightedAverage;
				tmpStudent["Loka"]      = student.GivenFinalGrade;
			} else {
				tmpStudent["Weighted"]  = student.WeightedAverage;
				tmpStudent["Rounded"]   = student.RoundedWeightedAverage;
				tmpStudent["Final"]     = student.GivenFinalGrade;
			}

			$scope.csvObject.push(tmpStudent);
		});
	};

	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(data) {
		$scope.isTeacher = data;
		$scope.initialize();
	});
})
.filter("FinalGr", function () { // Filters out students which did take finaltest
	return function(items) {
		var filtered = [];
		for (var i = 0; i < items.length; i++) {
			var item = items[i];
			if (item.FinalGrade !== "DNQ") {
				filtered.push(item);
			}
		}
		return filtered;
	};
})
.filter("NoTest", function () { // Filters out students which didn't take finaltest
	return function(items) {
		var filtered = [];
		for (var i = 0; i < items.length; i++) {
			var item = items[i];
			if (item.FinalGrade === "DNQ") {
				filtered.push(item);
			}
		}
		return filtered;
	};
});
