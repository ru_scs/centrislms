"use strict";

angular.module("gradesApp").factory("MockGradeAppResource",
function () {
	var mockObj = {
		FinalExamID: 1,
		Assessments: [{
			ID:                   1,
			Name:                 "Netpróf",
			AssessmentType:       2,
			Weight:               10.0,
			Required:             false,
			Count:                1
		}, {
			ID:                   2,
			Name:                 "Skilaverkefni 1",
			AssessmentType:       1,
			Weight:               15.0,
			Required:             false,
			Count:                1
		}, {
			ID:                   3,
			Name:                 "Skilaverkefni 2",
			AssessmentType:       1,
			Weight:               15.0,
			Required:             false,
			Count:                1
		}, {
			ID:                   4,
			Name:                 "Lokapróf",
			AssessmentType:       4,
			Weight:               60.0,
			Required:             false,
			Count:                1
		}]};

	var courseInstID = 2000;

	var mock = {
			successGetStudentGrades: true,
			apiMockObj: mockObj,
			getStudentGrades: function getStudentGrades(courseInstID) {
				return {
					success: function(fn) {
						if (mock.successGetStudentGrades === true) {
							fn(mock.apiMockObj);
						}
						return {
							error: function(fn) {
								if (mock.successGetStudentGradess === false) {
									fn();
								}
							}
						};
					}
				};
			}
		};
	return mock;
});

// }]; How it was. Maybe usefull to have this when
	// The next person starts working on this code
	/*return {
		makeResource: function(successGetAll, successGetStudent, successClose) {
			//return {
				getStudentGrades:               function(courseID) {
					// Should return the grades object.
					//return httpPromiseFromValue (APIobject);
				//},

				/*
				// Retrieves a grade object with all students registered
				// in the course with the given ID.
				getAllStudentGrades: function getAllStudentGrades(courseInstanceID) {
					// TODO:
					return httpPromiseFromValue ({});
				},

				getExpectedGrade: function(courseInstanceID, expectedGrades) {
					// TODO:
					return httpPromiseFromValue ({});
				},

				closeCourse:       function(courseID) {
					if (courseID === "24820") {
						return httpPromiseFromValue ("Course closed");
					}

					return httpPromiseFromValue (courseID, "Error: course not closed");
				}
			};
		}
	};
});*/

