"use strict";

// A resource to handle grades for courses, both for operations
// performed by the student, and those performed by the teacher.
angular.module("gradesApp").factory("GradesAppResource",
function GradesAppResource(CentrisResource) {
	return {
		// Retrieves the grade object for the currently logged in user,
		// If the user is a teacher then he can see the grades of all students
		// If the user is a student then he only sees his grades.
		getStudentGrades:  function getStudentGrades(courseInstanceID) {
			var param = {
				courseInstanceID: courseInstanceID
			};
			return CentrisResource.get("courses",":courseInstanceID/grades", param);
		}
		/*
		// Retrieves a grade object with all students registered
		// in the course with the given ID.
		// This function will probably be taken out because of changes in the api. Where a Token will determine
		// Whether the user calling the function is a Student or a Teacher so we will only have one end point.
		getAllStudentGrades: function getAllStudentGrades(courseInstanceID) {
			return $http.get(url + "courses/" + courseInstanceID + "/allgrades");
		},
		// We will need to change the last to functions when the functionality for them have been changed.
		// Retrieves an estimation grade object for the currently logged
		// in user that's is estimated from the supplied query parameters.
		getExpectedGrade: function(courseInstanceID, expectedGrades) {
			// Creates URL from expected grades - calls API
			// courses/courseID/grades?ID=GRADE&ID2=GRADE2
			var url = ENV.gradesEndpoint + "courses/" + courseInstanceID + "/grades/estimate?";

			var isFirst = true;
			for (var i = 0; i < expectedGrades.length; ++i) {
				if (!isFirst) {
					url += "&";
				}
				isFirst = false;
				if (expectedGrades[i].Grade !== undefined) {
					url += expectedGrades[i].ID + "=" + expectedGrades[i].Grade;
				}
			}

			return $http.get(url + "courses/" + url);
		},

		// Called by the teacher when the course should be closed.
		closeCourse:       function(courseInstanceID, gradesObject) {
			// Should flag a server side variable for closing course
			return CentrisResource.post("courses/close/:id", undefined, {id: courseInstanceID}, gradesObject);
		}*/
	};
});