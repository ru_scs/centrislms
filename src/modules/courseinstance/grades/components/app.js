"use strict";

angular.module("gradesApp", [
	"pascalprecht.translate",
	"ui.router"
]).config(function ($stateProvider) {
	$stateProvider.state("courseinstance.grades", {
		url:         "/grades",
		templateUrl: "courseinstance/grades/components/index/index.html",
		controller:  "CourseGradesController"
	});
});
