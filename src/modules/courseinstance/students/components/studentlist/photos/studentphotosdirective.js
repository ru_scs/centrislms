"use strict";

angular.module("studentsApp").directive("studentlistPhotos", function() {
	return {
		restrict: "E",
		scope: {
			students: "=ngModel"
		},
		templateUrl: "courseinstance/students/components/studentlist/photos/student-photos-directive.tpl.html"
	};
});
