"use strict";

angular.module("studentsApp").factory("CourseStudentsResource",
function (CentrisResource) {
	return {
		getStudentsInCourse: function( courseinstanceID ) {
			var param = {
				courseinstanceID: courseinstanceID
			};
			return CentrisResource.get("courses", ":courseinstanceID/students", param);
		}
	};
});
