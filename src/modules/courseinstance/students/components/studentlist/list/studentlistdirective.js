"use strict";

angular.module("studentsApp").directive("studentlistList", function() {
	return {
		restrict: "E",
		scope: {
			students: "=ngModel",
			table: "="
		},
		templateUrl: "courseinstance/students/components/studentlist/list/student-list-directive.tpl.html"
	};
});
