"use strict";

/*
 * Controller for listing all students in course
 */
angular.module("studentsApp").controller("StudentsController",
function StudentsController($scope, CourseStudentsResource, centrisNotify,
	$stateParams, $state, $location, $translate) {

	// Variables for csv file
	$scope.csvHeader = [];
	$scope.csvObject = [];

	$scope.courseData = {
		courses:  {},
		students: [],
		groups:   []
	};

	$scope.courseInstanceID = $stateParams.courseInstanceID;

	// Data table
	$scope.table = {
		columns: [
			{name: "students.Email",       visible: true,  alias: "Email"},
			{name: "students.SSN",         visible: true,  alias: "SSN"},
			{name: "students.Address",     visible: true,  alias: "Address"},
			{name: "students.Postal",      visible: false, alias: "Postal"},
			{name: "students.Mobile",      visible: true,  alias: "Mobile"},
			{name: "students.Department",  visible: false,  alias: "Department"},
			{name: "students.Major",       visible: false,  alias: "Major"},
			{name: "Group",                visible: true, alias: "Group"},
		],
		predicate: "Student.Name",
		toggle: function toggle(column) {
			column.visible = !column.visible;
		}
	};

	$scope.toggleColumn = function toggleColumn(column) {
		column.visible = !column.visible;
	};

	$scope.makeCSVHeader = function makeCSVHeader() {
		var lang = $translate.use() === "is";
		$scope.csvHeader = [];
		if (lang) {
			$scope.csvHeader.push("Nafn");
			$scope.csvHeader.push("Tölvupóstur");
			$scope.csvHeader.push("Kennitala");
			$scope.csvHeader.push("Heimilisfang");
			$scope.csvHeader.push("Símanúmer");
			$scope.csvHeader.push("Braut");
			$scope.csvHeader.push("Hópur");
		} else {
			$scope.csvHeader.push("Name");
			$scope.csvHeader.push("Email");
			$scope.csvHeader.push("SSN");
			$scope.csvHeader.push("Address");
			$scope.csvHeader.push("Phone Number");
			$scope.csvHeader.push("Major");
			$scope.csvHeader.push("Group");
		}

		$scope.getCSV();
	};

	// After students have been fetched we create
	// CSV file for the student list
	$scope.getCSV = function getCSV() {
		var lang = $translate.use() === "is";
		$scope.csvObject = [];
		$scope.courseData.students.forEach(function forEachStudent(student) {
			var tmpStudent = {};

			// TODO: there is always this interesting issue with the SSN,
			// if it starts with a 0, then Excel (and Google Docs) won"t
			// display the 0. One solution would be to stick a - between
			// the first 6 digits and the rest.
			if (lang) {
				tmpStudent["Nafn"]         = student.Student.Name;
				tmpStudent["Tölvupóstur"]  = student.Student.Email;
				tmpStudent["Kennitala"]    = student.Student.SSN;
				tmpStudent["Heimilisfang"] = student.Student.Address;
				tmpStudent["Símanúmer"]    = student.Student.MobilePhone;
				tmpStudent["Braut"]        = student.Student.Major.Name;
				tmpStudent["Hópur"]		   = student.Group.Name;
			} else {
				tmpStudent["Name"]         = student.Student.Name;
				tmpStudent["Email"]        = student.Student.Email;
				tmpStudent["SSN"]          = student.Student.SSN;
				tmpStudent["Address"]      = student.Student.Address;
				tmpStudent["Phone Number"] = student.Student.MobilePhone;
				tmpStudent["Major"]        = student.Student.Major.NameEN;
				tmpStudent["Group"]        = student.Group.Name;
			}

			$scope.csvObject.push(tmpStudent);
		});
	};

	// Fetch students in the course instance
	$scope.fetchStudents = function fetchStudents() {
		if ($scope.courseInstanceID !== undefined) {
			$scope.fetchingData = true;
			CourseStudentsResource.getStudentsInCourse($scope.courseInstanceID)
			.success(function successFetchingStudents(studentList) {
				$scope.fetchingData = false;
				$scope.courseData.students = studentList;
			})
			.error(function errorFetchingStudents(error) {
				$scope.fetchingData = false;
				centrisNotify.error("students.ErrorLoadingStudents");
			});
		}
	};

	$scope.fetchStudents();

	// INTRO for the student list
	$scope.IntroOptions = {
		steps: [
		{
			element: "#studenttable",
			position: "left",
			intro: ""
		}, {
			element: "#studentlist",
			position: "left",
			intro: ""
		}, {
			element: "#studentphotos",
			position: "right",
			intro: ""
		}, {
			element: "#studentactions",
			position: "left",
			intro: ""
		},],
		showStepNumbers:    false,
		exitOnOverlayClick: true,
		exitOnEsc:          true,
	};

	var introKeys = [
		"AngIntroStudentList.StudentTable",
		"AngIntroStudentList.StudentList",
		"AngIntroStudentList.StudentPhotos",
		"AngIntroStudentList.StudentActions",
	];

	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
		$scope.IntroOptions.steps[1].intro = translations[introKeys[1]];
		$scope.IntroOptions.steps[2].intro = translations[introKeys[2]];
		$scope.IntroOptions.steps[3].intro = translations[introKeys[3]];
	});

	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.$on("onCentrisIntro", function(event, args) {
		// Only show this if the current route is for our page:
		if ($state.current.name === "courseinstance.students") {
			$scope.ShowStudentListIntro();
		}
	});
});
