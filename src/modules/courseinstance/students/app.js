"use strict";

angular.module("studentsApp", ["pascalprecht.translate", "ui.router", "sharedServices"])
.config(function ($stateProvider) {
	$stateProvider.state("courseinstance.students", {
		url:         "/students",
		templateUrl: "courseinstance/students/components/studentlist/index.html",
		controller:  "StudentsController"
	});
});
