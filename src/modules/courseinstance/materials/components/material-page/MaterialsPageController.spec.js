"use strict";

describe("Testing MaterialsPageController", function() {

	var ctrl, scope, mockState, mockStateParams, mockMaterialsResource;

	mockState = {};
	mockStateParams = {courseInstanceID: 1, materialCategoryID: 1, pageID: 1};

	beforeEach(module("materialsApp", "sharedServices", "mockConfig"));

	beforeEach(inject(function($rootScope, $injector) {
		scope = $rootScope.$new();
		mockMaterialsResource = $injector.get("MockMaterialsResource");
	}));

	beforeEach(inject(function($controller) {
		ctrl = $controller("MaterialsPageController", {
			$scope: scope,
			$stateParams: mockStateParams,
			$state: mockState,
			MaterialsResource: mockMaterialsResource.mockMaterialsResources(true,true,true,true,true,true,true,true)
		});
	}));

	it("Should return page", inject(function($controller) {
		expect(scope.page).toBeDefined();
	}));
});
