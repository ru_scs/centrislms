"use strict";

angular.module("materialsApp").controller("MaterialsPageController",
function MaterialsPageController($scope, $state, $stateParams, MaterialsResource) {
	var courseInstanceID = $stateParams.courseInstanceID;
	var materialCategoryID = $stateParams.materialCategoryID;
	var pageID = $stateParams.pageID;

	MaterialsResource.getMaterialsPage(courseInstanceID, materialCategoryID, pageID)
	.success(function (data) {
		$scope.page = data;
	});
});
