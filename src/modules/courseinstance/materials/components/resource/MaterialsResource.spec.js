"use strict";

describe("Testing MaterialsResource", function() {
	var MaterialsResource, mock, mockCategories, mockCategoryObject, mockMaterialItem,
		mockFile;

	beforeEach(module("materialsApp", "sharedServices", "courseInstanceShared"));

	beforeEach(module("materialsApp", function($provide) {
		mock = {};
		mock.get  = jasmine.createSpy();
		mock.put  = jasmine.createSpy();
		mock.post = jasmine.createSpy();
		mock.remove = jasmine.createSpy();
		$provide.value("CentrisResource", mock);
	}));

	beforeEach(inject(function($injector) {
		mockCategories = $injector.get("MockCategoryResource");
		mockCategoryObject = mockCategories.getMockCategory(0);
		mockMaterialItem = mockCategoryObject.Items[1];
	}));

	beforeEach(inject(function(_MaterialsResource_) {
		MaterialsResource = _MaterialsResource_;
		spyOn(MaterialsResource, "getMaterialsInCourse").and.callThrough();
		spyOn(MaterialsResource, "addMaterialCategory").and.callThrough();
		spyOn(MaterialsResource, "addMaterialItem").and.callThrough();
		spyOn(MaterialsResource, "editMaterialCategory").and.callThrough();
		spyOn(MaterialsResource, "editMaterialItem").and.callThrough();
		spyOn(MaterialsResource, "deleteMaterialCategory").and.callThrough();
		spyOn(MaterialsResource, "deleteMaterialItem").and.callThrough();
		spyOn(MaterialsResource, "getMaterialsPage").and.callThrough();
	}));

	it("Should call get materials in course", function() {
		MaterialsResource.getMaterialsInCourse();
		expect(MaterialsResource.getMaterialsInCourse).toHaveBeenCalled();
		expect(mock.get).toHaveBeenCalled();
	});

	it("Should call add material category", function() {
		MaterialsResource.addMaterialCategory(1, mockCategoryObject[1]);
		expect(MaterialsResource.addMaterialCategory).toHaveBeenCalledWith(1,
		mockCategoryObject[1]);
		expect(mock.post).toHaveBeenCalled();
	});

	it("Should call add material item", function() {
		MaterialsResource.addMaterialItem(1, mockMaterialItem);
		expect(MaterialsResource.addMaterialItem).toHaveBeenCalledWith(1, mockMaterialItem);
		expect(mock.post).toHaveBeenCalled();
	});

	it("Should call edit material category", function() {
		MaterialsResource.editMaterialCategory(0, 0, mockCategoryObject);
		expect(MaterialsResource.editMaterialCategory).toHaveBeenCalledWith(0,0,
		mockCategoryObject);
		expect(mock.put).toHaveBeenCalled();
	});

	it("Shoud call edit material item with model", function() {
		MaterialsResource.editMaterialItem(1, 0, mockMaterialItem.Type, mockMaterialItem.ID,
		mockMaterialItem);
		expect(MaterialsResource.editMaterialItem).toHaveBeenCalledWith(1, 0, mockMaterialItem.Type, mockMaterialItem.ID,
		mockMaterialItem);
		expect(mock.put).toHaveBeenCalled();
	});

	it("Shoud call edit material witht undefined", function() {
		MaterialsResource.editMaterialItem();
		expect(MaterialsResource.editMaterialItem).toHaveBeenCalledWith();
		expect(mock.put).toHaveBeenCalled();
	});

	it("Should call delete material category", function() {
		MaterialsResource.deleteMaterialCategory(0,0);
		expect(MaterialsResource.deleteMaterialCategory).toHaveBeenCalledWith(0,0);
		expect(mock.remove).toHaveBeenCalled();
	});

	it("Should call delete material item", function() {
		MaterialsResource.deleteMaterialItem(0, 0, mockMaterialItem.Type,
			mockMaterialItem.ID);
		expect(MaterialsResource.deleteMaterialItem).toHaveBeenCalledWith(0,
			 0, mockMaterialItem.Type, mockMaterialItem.ID);
		expect(mock.remove).toHaveBeenCalled();
	});

	it("Should call get materials page", function() {
		MaterialsResource.getMaterialsPage();
		expect(MaterialsResource.getMaterialsPage).toHaveBeenCalled();
		expect(mock.get).toHaveBeenCalled();
	});
});
