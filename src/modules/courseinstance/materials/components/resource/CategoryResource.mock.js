"use strict";
angular.module("materialsApp")
.factory("MockCategoryResource", function (CentrisResource) {

	var mockItem1 = {
		ID: 1,
		Title: "LinkItemTitle1",
		URL: "https://docs.google.com/presentation/d/1qfD8Y5ulcx2Vu5UOHlv0ZDrPYo2dLbiXUKepijhe30M/" +
			"pub?start=false&loop=false&delayms=3000",
		Data: "",
		AuthorName: "Dabs", DatePublished: moment("12-07-2015", "DD-MM-YYYY"), IsVisible: true, Type: 1
	};
	var mockItem2 = {
		ID: 2,
		Title: "AttachmentItemTitle2",
		URL: "http://download.location.ru.is",
		Data: "blabla.pdf",
		File: {},
		AuthorName: "GGJ", DatePublished: moment("13-07-2015", "DD-MM-YYYY"), IsVisible: true, Type: 2
	};
	var mockItem3 = {
		ID: 3,
		Title: "PageItemTitle3",
		URL: "",
		AuthorName: "Dabs", DatePublished: moment("14-07-2015", "DD-MM-YYYY"), IsVisible: true, Type: 3
	};
	var mockItem4 = {
		ID: 4,
		Title: "LinkItemTitle4",
		URL: "https://www.youtube.com/watch?v=ElQJX9vnz1Im",
		Data: "",
		AuthorName: "GGJ", DatePublished: moment("15-07-2015", "DD-MM-YYYY"), IsVisible: true, Type: 1
	};
	var mockItem5 = {
		ID: 5,
		Title: "PageItemTitle5",
		URL: "",
		AuthorName: "Dabs", DatePublished: moment("16-07-2015", "DD-MM-YYYY"), IsVisible: true, Type: 3
	};
	var mockItem6 = {
		ID: 6,
		Title: "AttachmentItemTitle6",
		URL: "http://github.com",
		Data: "Blabla.pptx",
		AuthorName: "Dabs", DatePublished: moment("17-07-2015", "DD-MM-YYYY"), IsVisible: true, Type: 2
	};
	var mockItem7 = {
		ID: 7,
		Title: "LinkItemTitle7",
		URL: "https://docs.google.com/spreadsheets/d/1jrbZSTKhYvgsdstbFhdNiAigwv9y9PsZotmyP6WB5iU/pubhtmlm",
		Data: "",
		AuthorName: "GGJ", DatePublished: moment("18-07-2015", "DD-MM-YYYY"), IsVisible: true, Type: 1
	};
	var mockItem8 = {
		ID: 8,
		Title: "InvisibleLinkItemTitle8",
		URL: "https://vimeo.com/132328313",
		Data: "",
		AuthorName: "Dabs", DatePublished: moment("19-07-2015", "DD-MM-YYYY"), IsVisible: false, Type: 1
	};

	var mockCategory1 = { ID: 1, Title: "Category1", Items: [] };
	var mockCategory2 = { ID: 2, Title: "Category2", Items: [] };
	var mockCategory3 = { ID: 3, Title: "Category3", Items: [] };
	var mockCategory4 = { ID: 4, Title: "Category4", Items: [] };

	// Create mix of items
	mockCategory1.Items.push(mockItem1);
	mockCategory1.Items.push(mockItem2);
	mockCategory1.Items.push(mockItem3);
	mockCategory2.Items.push(mockItem4);
	mockCategory3.Items.push(mockItem5);
	mockCategory3.Items.push(mockItem6);
	mockCategory4.Items.push(mockItem7);
	mockCategory4.Items.push(mockItem8);

	var mockData = [ mockCategory1, mockCategory2, mockCategory3, mockCategory4];

	return {
		getMockCategories: function getMockCategories(courseInstanceID) {
			return mockData;
		},
		getMockCategory: function getMockCategory(id) {
			return mockData[id];
		}
	};
});
