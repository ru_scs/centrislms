"use strict";
angular.module("materialsApp")
.factory("MockMaterialsResource", function (MockCategoryResource) {
	return {
		mockMaterialsResources: function mockMaterialsResources(getMaterialsInCourse,
			addMaterialCategory, addMaterialItem, editMaterialItem, editMaterialCategory,
			deleteMaterialItem, deleteMaterialCategory, getMaterialsPage) {
			return {
				getMaterialsInCourse: function(id) {
					return {
						success: function(fn) {
							if (getMaterialsInCourse) {
								fn(MockCategoryResource.getMockCategories(id));
							}
							return {
								error: function(fn) {
									if (!getMaterialsInCourse) {
										fn();
									}
								}
							};
						}
					};
				},
				addMaterialCategory: function(id, model) {
					return {
						success: function(fn) {
							if (addMaterialCategory) {
								fn(id, model);
							}
							return {
								error: function(fn) {
									if (!addMaterialCategory) {
										fn();
									}
								}
							};
						}
					};
				},
				addMaterialItem: function(id, item) {
					return {
						success: function(fn) {
							if (MockCategoryResource.getMockCategory(id)) {
								fn(id, item);
							}
							return {
								error: function(fn) {
									if (!addMaterialItem) {
										fn();
									}
								}
							};
						}
					};
				},
				editMaterialItem: function(id, model) {
					return {
						success: function(fn) {
							if (editMaterialItem) {
								fn(id, model);
							}
							return {
								error: function(fn) {
									if (!editMaterialItem) {
										fn();
									}
								}
							};
						}
					};
				},
				editMaterialCategory: function(id, model) {
					return {
						success: function(fn) {
							if (editMaterialCategory) {
								fn(id, model);
							}
							return {
								error: function(fn) {
									if (!editMaterialCategory) {
										fn();
									}
								}
							};
						}
					};
				},
				deleteMaterialItem: function(courseInstanceID, categoryID, type, itemID) {
					return {
						success: function(fn) {
							if (deleteMaterialItem) {
								fn(courseInstanceID, categoryID, type, itemID, MockCategoryResource.getMockCategory(0));
							}
							return {
								error: function(fn) {
									if (!deleteMaterialItem) {
										fn();
									}
								}
							};
						}
					};
				},
				deleteMaterialCategory: function(courseInstanceID, categoryID) {
					return {
						success: function(fn) {
							if (deleteMaterialCategory) {
								fn(categoryID, MockCategoryResource.getMockCategory(0));
							}
							return {
								error: function(fn) {
									if (!deleteMaterialCategory) {
										fn();
									}
								}
							};
						}
					};
				},
				getMaterialsPage: function(id, categoryID, pageID) {
					return {
						success: function(fn) {
							if (getMaterialsPage) {
								fn(MockCategoryResource.getMockCategory(id));
							}
							return {
								error: function(fn) {
									if (!getMaterialsPage) {
										fn();
									}
								}
							};
						}
					};
				}
			};

		}
	};
});
