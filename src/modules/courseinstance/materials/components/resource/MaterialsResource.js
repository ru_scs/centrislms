"use strict";

angular.module("materialsApp").factory("MaterialsResource",
function MaterialsResource(CentrisResource) {
	return {
		getMaterialsInCourse: function getMaterialsInCourse(courseInstanceID) {
			var param = {
				courseInstanceID: courseInstanceID
			};
			return CentrisResource.get("courses", ":courseInstanceID/materials", param);
		},

		addMaterialCategory: function addMaterialCategory(courseInstanceID, model) {
			var param = {
				courseInstanceID: courseInstanceID
			};
			return CentrisResource.post("courses", ":courseInstanceID/materials/categories", param, model);
		},

		addMaterialItem: function addMaterialItem(courseInstanceID, model) {
			var fd = new FormData();
			fd.append("model", JSON.stringify(model));
			if (model.File) {
				fd.append("File0", model.File);
			}

			var param = {
				courseInstanceID: courseInstanceID
			};

			return CentrisResource.post("courses", ":courseInstanceID/materials/items", param, fd, true);
		},

		editMaterialCategory: function editMaterialCategory(courseInstanceID, materialCategoryID, model) {
			var param = {
				courseInstanceID:   courseInstanceID,
				materialCategoryID: materialCategoryID
			};
			return CentrisResource.put("courses", ":courseInstanceID/materials/categories/:materialCategoryID",
				param, model);
		},

		editMaterialItem: function editMaterialItem(courseInstanceID, materialCategoryID, itemType,
			materialItemID, model) {
			var param = {
				courseInstanceID:   courseInstanceID,
				materialCategoryID: materialCategoryID,
				itemType:           itemType,
				materialItemID:     materialItemID
			};

			if (model) {
				// Edit item
				var fd = new FormData();
				fd.append("model", JSON.stringify(model));
				if (model.File) {
					fd.append("File0", model.File);
				}

				return CentrisResource.put("courses",
					":courseInstanceID/materials/:materialCategoryID/items/:itemType/:materialItemID",
					param, fd, true);
			} else {
				// Undo Delete item
				return CentrisResource.put("courses",
					":courseInstanceID/materials/:materialCategoryID/items/:itemType/:materialItemID",
					param);
			}
		},

		deleteMaterialCategory: function deleteMaterialCategory(courseInstanceID, materialCategoryID) {
			var param = {
				courseInstanceID:   courseInstanceID,
				materialCategoryID: materialCategoryID
			};
			return CentrisResource.remove("courses", ":courseInstanceID/materials/categories/:materialCategoryID",
				param);
		},

		deleteMaterialItem: function deleteMaterialItem(courseInstanceID, materialCategoryID,
			itemType, materialItemID) {
			var param = {
				courseInstanceID:   courseInstanceID,
				materialCategoryID: materialCategoryID,
				itemType:           itemType,
				materialItemID:     materialItemID
			};
			return CentrisResource.remove("courses",
				":courseInstanceID/materials/:materialCategoryID/items/:itemType/:materialItemID",
				param);
		},

		getMaterialsPage: function getMaterialsPage(courseInstanceID, materialCategoryID, pageID) {
			var param = {
				courseInstanceID:   courseInstanceID,
				materialCategoryID: materialCategoryID,
				pageID:             pageID
			};
			return CentrisResource.get("courses",
				":courseInstanceID/materials/:materialCategoryID/pages/:pageID",
				param);
		}
	};
});
