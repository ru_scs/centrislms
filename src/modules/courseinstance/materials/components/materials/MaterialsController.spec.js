"use strict";

describe("Testing MaterialsController", function() {
	var ctrl, scope, rootScope, mockState, mockCourseInstance, mockCentrisNotify,
	mockCategories, mockMaterialCategories, mockNewMaterialItem, mockNewMaterialCategory,
	mockStateParams, mockMaterialsResource;

	beforeEach(module("materialsApp", "sharedServices", "mockConfig",
		"courseInstanceShared"));

	beforeEach(inject(function($rootScope, $injector) {
		rootScope = $rootScope;
		scope = $rootScope.$new();

		mockCategories = $injector.get("MockCategoryResource");
		mockCentrisNotify = $injector.get("mockCentrisNotify");
		mockMaterialsResource = $injector.get("MockMaterialsResource");
		mockCourseInstance = $injector.get("MockCourseInstanceRoleService");
		mockMaterialCategories = mockCategories.getMockCategories(0);
		mockCentrisNotify.error = jasmine.createSpy("error");
	}));

	describe("Testing as teacher", function() {

		mockStateParams = {
			courseInstanceID: 0
		};

		mockState = {};

		mockNewMaterialCategory = {
			show: function(obj) {
				return {
					then: function(fn) {
						fn(obj);
					}
				};
			}
		};

		mockNewMaterialItem = {
			show: function(obj) {
				return {
					then: function(fn) {
						fn(obj);
					}
				};
			}
		};

		describe("using success function for getting material resources", function() {

			beforeEach(inject(function($controller) {
				mockCentrisNotify.successWithUndo = jasmine.createSpy("successWithUndo");
				mockCentrisNotify.success = jasmine.createSpy("success");
				ctrl = $controller("MaterialsController", {
					$scope: scope,
					$stateParams: mockStateParams,
					$state: mockState,
					NewMaterialCategory: mockNewMaterialCategory,
					NewMaterialItem: mockNewMaterialItem,
					MaterialsResource: mockMaterialsResource.mockMaterialsResources(true,true,true,true,true,true,true,true),
					centrisNotify: mockCentrisNotify,
					CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(true),
				});
			}));

			it("should assign to isTeacher if user is a teacher", inject(function($controller) {
				expect(scope.isTeacher).toBe(true);
			}));

			it("should get all material and set fetchingData to false", function() {
				expect(scope.materialList.length).toEqual(mockMaterialCategories.length);
				expect(scope.fetchingData).toBe(false);
			});

			it("should call state go on courseinstance and item params", function() {
				mockState.go = jasmine.createSpy("go");
				scope.clickPageItem(0, 2);

				expect(mockState.go).toHaveBeenCalledWith("courseinstance.materialspage", {
					courseInstanceID: scope.courseInstanceID,
					materialCategoryID: scope.materialList[0].ID,
					pageID: scope.materialList[0].Items[2].ID
				});
			});

			it("should add new category to materialList", function() {
				var result = scope.materialList.length;
				scope.newCategory();
				expect(mockCentrisNotify.success).toHaveBeenCalledWith("materials.notify.NewCategorySuccess");
				expect(scope.materialList.length).toEqual(result + 1);
			});

			it("should add new material item to materiaList category", function() {
				scope.addMaterialItem(0);
				expect(mockCentrisNotify.success).toHaveBeenCalledWith("materials.notify.NewItemSuccess");
			});

			it("editMaterialItem should return success", function() {
				scope.editMaterialItem(0,0);
				expect(mockCentrisNotify.success).toHaveBeenCalledWith("materials.notify.EditItemSuccess");
			});

			it("editMaterialCategory should return success", function() {
				scope.editMaterialCategory(0);
				expect(mockCentrisNotify.success).toHaveBeenCalledWith("materials.notify.EditCategorySuccess");
			});

			it("should delete specific material item", function() {
				var undoParam = {
					type: "material-item-delete",
					id:   {
						item:          scope.materialList[0].Items[0],
						instance:      scope.courseInstanceID,
						categoryIndex: 0,
						itemIndex:     0,
						categoryID:    1
					}
				};
				var result = scope.materialList[0].Items.length;

				scope.deleteMaterialItem(0,0);
				expect(scope.materialList[0].Items.length).toBe(result - 1);
				expect(mockCentrisNotify.successWithUndo)
						.toHaveBeenCalledWith("materials.notify.DeleteItemSuccess", undoParam);
			});

			it("should delete specific material category", function() {
				var undoParam = {
					type: "material-category-delete",
					id:   {
						category:      scope.materialList[0],
						instance:      scope.courseInstanceID,
						categoryIndex: 0,
						categoryID:    1
					}
				};
				var result = scope.materialList.length;

				scope.deleteMaterialCategory(0);
				expect(scope.materialList.length).toBe(result - 1);
				expect(mockCentrisNotify.successWithUndo)
						.toHaveBeenCalledWith("materials.notify.DeleteCategorySuccess", undoParam);
			});

			it("should display success on undo delete category", function() {
				var param = {
					id:{
						instance:      0,
						categoryID:    1
					},
					type: "material-category-delete"
				};
				spyOn(rootScope, "$broadcast").and.callThrough();
				scope.$broadcast("centrisUndo", param);
				expect(rootScope.$broadcast).toHaveBeenCalled();
				expect(mockCentrisNotify.success)
						.toHaveBeenCalledWith("materials.notify.UndoDeleteCategorySuccess");
			});

			it("should display success on undo delete item", function() {
				var param = {
					id: {
						categoryID: 0,
						instance: 0,
						item: 0
					},
					type: "material-item-delete"
				};
				spyOn(rootScope, "$broadcast").and.callThrough();
				scope.$broadcast("centrisUndo", param);
				expect(rootScope.$broadcast).toHaveBeenCalled();
				expect(mockCentrisNotify.success)
						.toHaveBeenCalledWith("materials.notify.UndoDeleteItemSuccess");
			});
		});

		describe("using error function from materials resources", function() {

			beforeEach(inject(function($controller) {
				ctrl = $controller("MaterialsController", {
					$scope: scope,
					$stateParams: mockStateParams,
					NewMaterialCategory: mockNewMaterialCategory,
					NewMaterialItem: mockNewMaterialItem,
					MaterialsResource: mockMaterialsResource.mockMaterialsResources(true,false,false,false,false,false,false,false),
					centrisNotify: mockCentrisNotify,
					CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(true),
				});
			}));

			it("should call error in centrisNotify when it fails to add new category", function() {
				scope.newCategory();
				expect(mockCentrisNotify.error)
						.toHaveBeenCalledWith("materials.notify.NewCategoryError");
			});

			it("should call error in centrisNotify when it fails to add new category", function() {
				scope.addMaterialItem(0);
				expect(mockCentrisNotify.error)
						.toHaveBeenCalledWith("materials.notify.NewItemError");
			});

			it("should call error in centrisNotify when it fails to edit category", function() {
				scope.editMaterialCategory(0);
				expect(mockCentrisNotify.error)
						.toHaveBeenCalledWith("materials.notify.EditCategoryError");
			});

			it("should call error in centrisNotify when it fails to edit material item", function() {
				scope.editMaterialItem(0,0);
				expect(mockCentrisNotify.error)
						.toHaveBeenCalledWith("materials.notify.EditItemError");
			});

			it("should call error in centrisNotify when it fails to delete material item", function() {
				scope.deleteMaterialItem(0,0);
				expect(mockCentrisNotify.error)
						.toHaveBeenCalledWith("materials.notify.DeleteItemError");
			});

			it("should call error in centrisNotify when it fails to delete material category", function() {
				scope.deleteMaterialCategory(0);
				expect(mockCentrisNotify.error)
						.toHaveBeenCalledWith("materials.notify.DeleteCategoryError");
			});

			it("should display error on undo delete item", function() {
				var param = {
					id: {
						categoryID: 0,
						instance: 0,
						item: 0
					},
					type: "material-item-delete"
				};
				spyOn(rootScope, "$broadcast").and.callThrough();
				scope.$broadcast("centrisUndo", param);
				expect(rootScope.$broadcast).toHaveBeenCalled();
				expect(mockCentrisNotify.error)
						.toHaveBeenCalledWith("materials.notify.UndoDeleteItemError");
			});

			it("should display error on undo delete category", function() {
				var param = {
					id: {
						categoryID: 0,
						instance: 0,
						item: 0
					},
					type: "material-category-delete"
				};
				spyOn(rootScope, "$broadcast").and.callThrough();
				scope.$broadcast("centrisUndo", param);
				expect(rootScope.$broadcast).toHaveBeenCalled();
				expect(mockCentrisNotify.error)
						.toHaveBeenCalledWith("materials.notify.UndoDeleteCategoryError");
			});
		});
	});

	describe("Testing as student", function() {
		beforeEach(inject(function($controller, $state) {
			mockCentrisNotify.successWithUndo = jasmine.createSpy("successWithUndo");
			mockCentrisNotify.success = jasmine.createSpy("success");
			ctrl = $controller("MaterialsController", {
				$scope: scope,
				$stateParams: mockStateParams,
				$state: mockState,
				MaterialsResource: mockMaterialsResource.mockMaterialsResources(true,true,true,true,true,true),
				CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(false),
			});
		}));

		it("Should assign false to isTeacher if user is a student", function() {
			expect(scope.isTeacher).toBe(false);
		});

		it("should call state go on courseinstance and item params", function() {
			mockState.go = jasmine.createSpy("go");
			scope.clickPageItem(0, 2);

			expect(mockState.go).toHaveBeenCalledWith("courseinstance.materialspage", {
				courseInstanceID: scope.courseInstanceID,
				materialCategoryID: scope.materialList[0].ID,
				pageID: scope.materialList[0].Items[2].ID
			});
		});
	});
});
