"use strict";

angular.module("materialsApp").controller("MaterialsController",
function MaterialsController($scope, $state, $stateParams, dateTimeService,
	iconFinderService, MaterialsResource, centrisNotify, CourseInstanceRoleService,
	NewMaterialCategory, NewMaterialItem) {

	$scope.courseInstanceID = $stateParams.courseInstanceID;
	$scope.fetchingData = true;

	CourseInstanceRoleService.isTeacherInCourse($scope.courseInstanceID)
	.then(function(isTeacher) {
		$scope.isTeacher = isTeacher;
	});

	var formatTableItems = function formatTableItems(category) {
		angular.forEach(category.Items, function (item) {
			item.hasBeenPublished = hasBeenPublished(item);
			item.DatePublished = moment(item.DatePublished);

			if (item.Type === 1) {
				item.TypeText = "materials.types.Link";
				item.Icon = iconFinderService.getType(item.URL);
			} else if (item.Type === 2) {
				item.TypeText = "materials.types.Attachment";
				item.Icon = iconFinderService.getType(item.Data);
			} else if (item.Type === 3) {
				item.TypeText = "materials.types.Page";
				item.Icon = "centris";
			}
		});
	};

	var hasBeenPublished = function hasBeenPublished(item) {
		var present = moment();
		var datePublished = moment(item.DatePublished);
		// If the date is in the future then it's not published
		return !datePublished.isAfter(present);
	};

	MaterialsResource.getMaterialsInCourse($scope.courseInstanceID)
	.success(function(data) {
		angular.forEach(data, function (category) {
			formatTableItems(category);
		});
		$scope.fetchingData = false;
		$scope.materialList = data;
	});

	// This function is for redirecting to page items
	$scope.clickPageItem = function clickPageItem (categoryIndex, itemIndex) {
		var category = $scope.materialList[categoryIndex];
		var item = category.Items[itemIndex];
		if (item.Type === 3) {
			var param = {
				courseInstanceID: $scope.courseInstanceID,
				materialCategoryID: category.ID,
				pageID: item.ID
			};
			$state.go("courseinstance.materialspage", param);
		}
	};

	$scope.newCategory = function newCategory () {
		NewMaterialCategory.show().then(function(model) {
			MaterialsResource.addMaterialCategory($scope.courseInstanceID, model)
			.success(function (category) {
				centrisNotify.success("materials.notify.NewCategorySuccess");
				$scope.materialList.push(category);
			})
			.error(function (error) {
				centrisNotify.error("materials.notify.NewCategoryError");
			});
		});
	};

	$scope.addMaterialItem = function addMaterialItem (index) {
		NewMaterialItem.show($scope.materialList[index].ID).then(function(model) {
			MaterialsResource.addMaterialItem($scope.courseInstanceID, model)
			.success(function (category) {
				centrisNotify.success("materials.notify.NewItemSuccess");
				formatTableItems(category);
				$scope.materialList[index] = category;
			})
			.error(function (error) {
				centrisNotify.error("materials.notify.NewItemError");
			});
		});
	};

	$scope.editMaterialItem = function editMaterialItem (categoryIndex, itemIndex) {
		var item = $scope.materialList[categoryIndex].Items[itemIndex];
		var categoryID = $scope.materialList[categoryIndex].ID;
		NewMaterialItem.show(categoryID, item).then(function(model) {
			MaterialsResource.editMaterialItem($scope.courseInstanceID, categoryID, item.Type, item.ID, model)
			.success(function (category) {
				centrisNotify.success("materials.notify.EditItemSuccess");
				formatTableItems(category);
				$scope.materialList[categoryIndex] = category;
			})
			.error(function (error) {
				centrisNotify.error("materials.notify.EditItemError");
			});
		});
	};

	$scope.editMaterialCategory = function editMaterialCategory (categoryIndex) {
		var materialCategoryID = $scope.materialList[categoryIndex].ID;

		NewMaterialCategory.show($scope.materialList[categoryIndex])
		.then(function(model) {

			MaterialsResource.editMaterialCategory($scope.courseInstanceID, materialCategoryID, model)
			.success(function (category) {

				centrisNotify.success("materials.notify.EditCategorySuccess");
				formatTableItems(category);
				$scope.materialList[categoryIndex] = category;
			})
			.error(function (error) {

				centrisNotify.error("materials.notify.EditCategoryError");
			});
		});
	};

	$scope.deleteMaterialItem = function deleteMaterialItem (categoryIndex, itemIndex) {
		var category = $scope.materialList[categoryIndex];
		var item = category.Items[itemIndex];
		MaterialsResource.deleteMaterialItem($scope.courseInstanceID, category.ID, item.Type, item.ID)
		.success(function (data) {
			var undoParam = {
				type: "material-item-delete",
				id:   {
					item:          item,
					instance:      $scope.courseInstanceID,
					categoryIndex: categoryIndex,
					itemIndex:     itemIndex,
					categoryID:    category.ID
				}
			};
			centrisNotify.successWithUndo("materials.notify.DeleteItemSuccess", undoParam);

			$scope.materialList[categoryIndex].Items.splice(itemIndex, 1);
		})
		.error(function (error) {
			centrisNotify.error("materials.notify.DeleteItemError");
		});
	};

	$scope.deleteMaterialCategory = function deleteMaterialCategory (categoryIndex) {
		var category = $scope.materialList[categoryIndex];
		MaterialsResource.deleteMaterialCategory($scope.courseInstanceID, category.ID)
		.success(function (data) {
			var undoParam = {
				type: "material-category-delete",
				id:   {
					category:      category,
					instance:      $scope.courseInstanceID,
					categoryIndex: categoryIndex,
					categoryID:    category.ID
				}
			};
			centrisNotify.successWithUndo("materials.notify.DeleteCategorySuccess", undoParam);

			$scope.materialList.splice(categoryIndex, 1);
		})
		.error(function (error) {
			centrisNotify.error("materials.notify.DeleteCategoryError");
		});
	};

	// Feature hiding
	// $scope.toggleItemVisibility = function toggleItemVisibility(categoryIndex, itemIndex) {
	// 	var item = $scope.materialList[categoryIndex].Items[itemIndex];
	// 	var categoryID = $scope.materialList[categoryIndex].ID;
	// 	var model = {
	// 		Title:              item.Title,
	// 		MaterialCategoryID: categoryID,
	// 		DatePublished:      dateTimeService.momentToISO(moment(item.DatePublished, dateTimeService.dateFormat())),
	// 		IsVisible:          !item.IsVisible,
	// 		Type:               item.Type,
	// 		LinkUrl:            item.Type === 1 ? item.URL : "",
	// 		PageBody:           item.Type === 3 ? item.Data : "",
	// 		File:               null,
	// 		FriendlyFilename:   item.Type === 2 ? item.Data : ""
	// 	};

	// 	MaterialsResource.editMaterialItem($scope.courseInstanceID, categoryID, item.Type, item.ID, model)
	// 	.success(function (category) {
	// 		if (model.IsVisible){
	// 			centrisNotify.success("materials.notify.ToggleItemToVisibleSuccess");
	// 		} else {
	// 			centrisNotify.success("materials.notify.ToggleItemToInvisibleSuccess");
	// 		}
	// 		formatTableItems(category);
	// 		$scope.materialList[categoryIndex] = category;
	// 	})
	// 	.error(function (error) {
	// 		centrisNotify.error("materials.notify.ToggleItemVisibilityError");
	// 	});
	// };

	$scope.$on("centrisUndo", function undo(event, param) {
		var materialCategoryID = param.id.categoryID;
		var courseInstanceID = param.id.instance;
		if (param.type === "material-category-delete") {

			MaterialsResource.editMaterialCategory(courseInstanceID, materialCategoryID, undefined)
			.success(function (category) {

				centrisNotify.success("materials.notify.UndoDeleteCategorySuccess");
				$scope.materialList.splice(param.id.categoryIndex, 0, category);
			})
			.error(function (error) {
				centrisNotify.error("materials.notify.UndoDeleteCategoryError");
			});
		} else if (param.type === "material-item-delete") {
			var item = param.id.item;

			MaterialsResource.editMaterialItem(courseInstanceID, materialCategoryID, item.Type, item.ID, undefined)
			.success(function (category) {
				centrisNotify.success("materials.notify.UndoDeleteItemSuccess");
				formatTableItems(category);
				$scope.materialList[param.id.categoryIndex] = category;
			})
			.error(function (error) {
				centrisNotify.error("materials.notify.UndoDeleteItemError");
			});
		}
	});
});
