"use strict";

describe("Testing NewMaterialItemDlg", function () {
	var dialog, mockItem;

	mockItem = {
		ID: 1,
		Title: "LinkItemTitle1",
		URL: "www.google.com",
		Data: "",
		AuthorName: "Dabs", DatePublished: moment("12-07-2015", "DD-MM-YYYY"),
		IsVisible: true, Type: 1
	};

	beforeEach(module("materialsApp", "sharedServices", "mockConfig"));

	beforeEach(inject(function($injector) {
		dialog = $injector.get("NewMaterialItem");
	}));

	it("can get an instance of NewMaterialItem", function() {
		expect(dialog).toBeDefined();
	});

	it("NewMaterialItem.show should be defined", function() {
		dialog.show(mockItem);
		expect(dialog.show).toBeDefined();
	});
});
