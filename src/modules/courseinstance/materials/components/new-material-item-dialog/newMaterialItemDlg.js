"use strict";

angular.module("materialsApp").factory("NewMaterialItem",
function NewMaterialItem($uibModal) {
	return {
		show: function show(categoryID, oldItem) {
			var templateInstance = {
				controller:  "NewMaterialItemController",
				templateUrl: "courseinstance/materials/components/new-material-item-dialog/new-material-item-tpl.html",
				resolve: {
					categoryID: function() {
						return categoryID;
					},
					oldItem: function() {
						return oldItem;
					}
				}
			};

			var modalInstance = $uibModal.open(templateInstance);

			return modalInstance.result;
		}
	};
});
