"use strict";

angular.module("materialsApp").controller("NewMaterialItemController",
function NewMaterialItemController($scope, categoryID, oldItem, dateTimeService) {

	// Validation variables
	$scope.showError       = false;
	$scope.invalidDate     = false;
	$scope.invalidLinkUrl  = false;
	$scope.invalidFileName = false;
	$scope.invalidPageBody = false;

	var currDate = moment();

	$scope.model = {
		Title:              "",
		MaterialCategoryID: categoryID,
		DatePublished:      currDate.format(dateTimeService.dateFormat()),

		// Feature hiding, for now only use DatePublished to show/hide items
		// from students
		IsVisible:          true,
		Type:               1, // 1 = link, 2 = attachment, 3 = page
		LinkUrl:            "",
		PageBody:           "",
		File:               null,
		FriendlyFilename:   ""
	};

	// When item is being edited
	$scope.edit = false;

	if (oldItem) {
		$scope.edit = true;
		var dt = moment(oldItem.DatePublished);

		$scope.model = {
			Title:              oldItem.Title,
			MaterialCategoryID: categoryID,
			DatePublished:      dt.format(dateTimeService.dateFormat()),
			IsVisible:          oldItem.IsVisible,
			Type:               oldItem.Type,
			LinkUrl:            oldItem.Type === 1 ? oldItem.URL : "",
			PageBody:           oldItem.Type === 3 ? oldItem.Data : "",
			File:               null,
			FriendlyFilename:   oldItem.Type === 2 ? oldItem.Data : ""
		};
	}

	$scope.$watch("files", function() {
		if ($scope.files && $scope.files.length) {
			for (var i = 0; i < $scope.files.length; i++) {
				$scope.model.File = $scope.files[i];
				$scope.model.FriendlyFilename = $scope.files[i].name;
			}
		}
	});

	$scope.validateSelection = function validateSelection() {
		var isValid = true;

		// Reset validation variables
		$scope.showError       = false;
		$scope.invalidDate     = false;
		$scope.invalidLinkUrl  = false;
		$scope.invalidFileName = false;
		$scope.invalidPageBody = false;

		// Validation for Link, File and Page input fields
		// Done here instead of html5 validation because only one of them
		// is required
		if ($scope.model.Type === 1 && $scope.model.LinkUrl === "") {
			isValid = false;
			$scope.invalidLinkUrl = true;
		} else if ($scope.model.Type === 2 && $scope.model.FriendlyFilename === "") {
			isValid = false;
			$scope.invalidFileName = true;
		} else if ($scope.model.Type === 3 && $scope.model.PageBody === "") {
			isValid = false;
			$scope.invalidPageBody = true;
		}

		if ($scope.model.Type === 1) {
			// If the link doesn"t start with http/https then add http
			// This is to prevent the link bein interpreted as relative
			if ($scope.model.LinkUrl.indexOf("http") > -1 === false) {
				$scope.model.LinkUrl = "http://" + $scope.model.LinkUrl;
			}
		}
		// Validation for date
		var present = moment();
		var datePublished = moment($scope.model.DatePublished, dateTimeService.dateFormat());

		// If the topic is being edited, the baseline is the old publish date
		if ($scope.edit) {
			present = moment(oldItem.DatePublished, dateTimeService.dateFormat());
		}

		// If the date is not today or later then it"s invalid
		if (!datePublished.isAfter(present) &&
			!datePublished.isSame(present, "day")) {
			isValid = false;
			$scope.invalidDate = true;
		}

		if (!isValid) {
			$scope.showError = true;
		} else {
			// Date needs to be in machine readable format for api
			$scope.model.DatePublished = dateTimeService.momentToISO(moment($scope.model.DatePublished,
				dateTimeService.dateFormat()));
			$scope.$close($scope.model);
		}
	};
});
