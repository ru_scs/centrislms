"use strict";

describe("Testing NewMaterialItemController", function() {
	var scope, ctrl, mockItem1, mockItem2, mockItem3, mockDateTimeService;

	beforeEach(module("materialsApp", "sharedServices"));

	mockItem1 = {
		ID: 1,
		Title: "LinkItemTitle1",
		URL: "www.google.com",
		Data: "",
		AuthorName: "Dabs", DatePublished: moment("12-07-2015", "DD-MM-YYYY"),
		IsVisible: true, Type: 1
	};
	mockItem2 = {
		ID: 2,
		Title: "AttachmentItemTitle2",
		URL: "http://download.location.ru.is",
		Data: "blabla.pdf",
		File: {},
		AuthorName: "GGJ", DatePublished: moment("13-07-2015", "DD-MM-YYYY"),
		IsVisible: true, Type: 2
	};
	mockItem3 = {
		ID: 3,
		Title: "PageItemTitle3",
		URL: "",
		AuthorName: "Dabs", DatePublished: moment("14-07-2015", "DD-MM-YYYY"),
		IsVisible: true, Type: 3
	};

	beforeEach(inject(function($rootScope, $injector) {
		scope = $rootScope.$new();
		mockDateTimeService = $injector.get("dateTimeService");
	}));

	describe("Testing with valid old Item of type 1", function() {
		beforeEach(inject(function($controller) {
			scope.$close = jasmine.createSpy("$close");
			ctrl = $controller("NewMaterialItemController", {
				$scope: scope,
				categoryID: 1,
				oldItem: mockItem1,
				dateTimeService: mockDateTimeService
			});
		}));

		it("Should give scope right values with valid old item of type 1", function() {
			expect(scope.showError).toBe(false);
			expect(scope.invalidDate).toBe(false);
			expect(scope.invalidLinkUrl).toBe(false);
		});

		describe("Testing validateSeleection", function() {

			it("Should close with model if item is valid", function() {
				scope.validateSelection();
				expect(scope.$close).toHaveBeenCalledWith(scope.model);
			});

			it("Should showError if LinkUrl is invalid", function() {
				scope.model.LinkUrl = "";
				scope.validateSelection();
				expect(scope.invalidLinkUrl).toBe(true);
				expect(scope.showError).toBe(true);
			});
		});
	});

	describe("Testing with valid old Item of type 2", function() {
		beforeEach(inject(function($controller) {
			scope.$close = jasmine.createSpy("$close");
			ctrl = $controller("NewMaterialItemController", {
				$scope: scope,
				categoryID: 1,
				oldItem: mockItem2,
				dateTimeService: mockDateTimeService
			});
		}));

		it("Should give scope right values with valid old item", function() {
			expect(scope.showError).toBe(false);
			expect(scope.invalidDate).toBe(false);
			expect(scope.invalidLinkUrl).toBe(false);
			expect(scope.invalidFileName).toBe(false);
		});

		describe("Testing validateSeleection", function() {

			it("Should close with model if item is valid", function() {
				scope.validateSelection();
				expect(scope.$close).toHaveBeenCalledWith(scope.model);
			});

			it("Should showError if FriendlyFilename is invalid", function() {
				scope.model.FriendlyFilename = "";
				scope.validateSelection();
				expect(scope.invalidFileName).toBe(true);
				expect(scope.showError).toBe(true);
			});
		});
	});

	describe("Testing with valid old Item of type 3", function() {
		beforeEach(inject(function($controller) {
			scope.$close = jasmine.createSpy("$close");
			ctrl = $controller("NewMaterialItemController", {
				$scope: scope,
				categoryID: 1,
				oldItem: mockItem3,
				dateTimeService: mockDateTimeService
			});
		}));

		it("Should give scope right values with valid old item", function() {
			expect(scope.showError).toBe(false);
			expect(scope.invalidDate).toBe(false);
			expect(scope.invalidPageBody).toBe(false);
		});

		describe("Testing validateSeleection", function() {

			it("Should close with model if item is valid", function() {
				scope.validateSelection();
				expect(scope.$close).toHaveBeenCalledWith(scope.model);
			});

			it("Should showError if PageBody is invalid", function() {
				scope.model.PageBody = "";
				scope.validateSelection();
				expect(scope.invalidPageBody).toBe(true);
				expect(scope.showError).toBe(true);
			});
		});
	});

	describe("Testing with new item", function() {
		beforeEach(inject(function($controller) {
			scope.$close = jasmine.createSpy("$close");
			ctrl = $controller("NewMaterialItemController", {
				$scope: scope,
				categoryID: 1,
				oldItem: null,
				dateTimeService: mockDateTimeService
			});
		}));

		describe("Testing validateSeleection with new type 1 item", function() {

			it("Should close with model if item is valid", function() {
				scope.model.Title = "Valid title";
				scope.model.LinkUrl = "www.valid.com";
				scope.validateSelection();
				expect(scope.$close).toHaveBeenCalledWith(scope.model);
			});

			it("Should showError if datePublished is invalid", function() {
				scope.model.DatePublished = moment("14-07-2015", "DD-MM-YYYY");
				scope.model.Title = "Valid title";
				scope.model.LinkUrl = "www.valid.com";
				scope.validateSelection();
				expect(scope.invalidDate).toBe(true);
				expect(scope.showError).toBe(true);
			});
		});
	});
});
