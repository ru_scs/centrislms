"use strict";

angular.module("materialsApp", ["pascalprecht.translate", "ui.router"])
.config(function ($stateProvider) {
	$stateProvider.state("courseinstance.materials", {
		url:         "/materials",
		templateUrl: "courseinstance/materials/components/materials/index.html",
		controller:  "MaterialsController"
	});
	$stateProvider.state("courseinstance.materialspage", {
		url:         "/materials/:materialCategoryID/pages/:pageID",
		templateUrl: "courseinstance/materials/components/material-page/index.html",
		controller:  "MaterialsPageController"
	});
});
