"use strict";

angular.module("materialsApp").controller("NewMaterialCategoryController",
function NewMaterialCategoryController($scope, oldCategory) {

	if (oldCategory) {
		$scope.edit = true;
		$scope.model = {
			Title: oldCategory.Title
		};
	} else {
		$scope.edit = false;
		$scope.model = {
			Title: ""
		};
	}

	$scope.validateSelection = function validateSelection(isValid) {
		$scope.showError = false;
		if (!isValid) {
			$scope.showError = true;
		} else {
			$scope.$close($scope.model);
		}
	};
});
