"use strict";

angular.module("materialsApp").factory("NewMaterialCategory",
function NewMaterialCategory($uibModal) {
	return {
		show: function show(oldCategory) {
			var templateInstance = {
				controller:  "NewMaterialCategoryController",
				templateUrl: "courseinstance/materials/components/new-material-category-dialog/new-material-category-tpl.html",
				resolve: {
					oldCategory: function() {
						return oldCategory;
					}
				}
			};

			var modalInstance = $uibModal.open(templateInstance);
			return modalInstance.result;
		}
	};
});
