"use strict";

describe("Testing NewMaterialCategoryController", function() {
	var ctrl, scope, mockOldCategory;

	beforeEach(module("materialsApp", "sharedServices"));

	mockOldCategory = { ID: 1, Title: "Valid title", Items: [] };

	beforeEach(inject(function($rootScope) {
		scope = $rootScope.$new();
	}));

	describe("Testing with oldCategory", function() {
		beforeEach(inject(function($controller) {
			scope.$close = jasmine.createSpy("$close");
			ctrl = $controller("NewMaterialCategoryController", {
				$scope: scope,
				oldCategory: mockOldCategory
			});
		}));

		describe("testing validateSelection", function() {

			it("Should close with valid category", function() {
				scope.validateSelection(true);
				expect(scope.$close).toHaveBeenCalledWith(scope.model);
			});

			it("Should showError with invalid category", function() {
				scope.validateSelection(false);
				expect(scope.showError).toBe(true);
			});
		});
	});

	describe("Testing with new catagory", function() {
		beforeEach(inject(function($controller) {
			scope.$close = jasmine.createSpy("$close");
			ctrl = $controller("NewMaterialCategoryController", {
				$scope: scope,
				oldCategory: null
			});
		}));

		describe("testing validateSelection", function() {

			it("Should close with valid category", function() {
				scope.validateSelection(true);
				expect(scope.$close).toHaveBeenCalledWith(scope.model);
			});

			it("Should showError with invalid category", function() {
				scope.validateSelection(false);
				expect(scope.showError).toBe(true);
			});
		});
	});
});
