"use strict";

describe("Testing newMaterialCategoryDlg", function() {
	var dialog, mockCategory;

	mockCategory = { ID: 1, Title: "Valid title", Items: [] };

	beforeEach(module("materialsApp", "sharedServices", "mockConfig"));

	beforeEach(inject(function($injector) {
		dialog = $injector.get("NewMaterialCategory");
	}));

	it("can get an instance of NewMaterialCategory", function() {
		expect(dialog).toBeDefined();
	});

	it("NewMaterialCategory.show should be defined", function() {
		dialog.show(mockCategory);
		expect(dialog.show).toBeDefined();
	});
});
