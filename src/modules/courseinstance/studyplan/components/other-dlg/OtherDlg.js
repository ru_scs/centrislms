"use strict";

angular.module("studyPlanApp").factory("OtherDlg",
function OtherDlg($uibModal) {
	return {
		show: function show(text, ID) {
			var modalInstance = $uibModal.open({
				templateUrl: "courseinstance/studyplan/components/other-dlg/other-dialog.tpl.html",
				controller:  "OtherController",
				resolve:     {
					modalParam: function() {
						return {
							text: text,
							id:   ID
						};
					}
				}
			});
			return modalInstance.result;
		}
	};
});