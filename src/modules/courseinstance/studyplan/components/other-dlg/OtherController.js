"use strict";

angular.module("studyPlanApp").controller("OtherController",
function($scope, $uibModalInstance, modalParam) {
	$scope.model = {
		ID:    modalParam.id,
		Text:  modalParam.text,
	};

	$scope.defaultFocus = true;

	$scope.submitEvent = function submitEvent(valid) {
		if (valid) {
			$uibModalInstance.close($scope.model);
		} else {
			$scope.showError = true;
		}
	};
});