"use strict";

describe("Testing OtherDlg", function () {

	var dialog, mockText, mockItems, mockID;

	mockText = "Small text";
	mockID = 1337;

	beforeEach(module("studyPlanApp", "sharedServices", "mockConfig"));
	beforeEach(inject(function($injector) {
		dialog = $injector.get("OtherDlg");
	}));

	it("can get an instance of TopicsNewDlg", function() {
		expect(dialog).toBeDefined();
	});

	it("TopicsNewDlg.show should be defined", function() {
		dialog.show(mockText, mockID);
		expect(dialog.show).toBeDefined();
	});
});
