"use strict";

describe("Testing OtherController", function() {
	var ctrl, scope, mockSubmodalInstance, mockModalParam;

	mockSubmodalInstance = {};

	mockModalParam = {
		id: 1,
		text: "Text example"
	};

	beforeEach(module("studyPlanApp"));
	beforeEach(inject(function($rootScope) {
		scope = $rootScope.$new();
		mockSubmodalInstance.close = jasmine.createSpy("close");
	}));

	beforeEach(inject(function($controller) {
		ctrl = $controller("OtherController", {
			$scope: scope,
			$uibModalInstance: mockSubmodalInstance,
			modalParam: mockModalParam
		});
	}));

	it("should set defaultFocus as true", function() {
		expect(scope.defaultFocus).toBe(true);
	});

	it("should call submodal close", function() {
		scope.submitEvent(true);
		expect(mockSubmodalInstance.close).toHaveBeenCalledWith(scope.model);
	});

	it("should set showError to true", function() {
		scope.submitEvent(false);
		expect(scope.showError).toBe(true);
	});
});
