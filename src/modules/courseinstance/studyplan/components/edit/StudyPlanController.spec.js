"use strict";
describe("Testing StudyPlanController", function() {
	var ctrl, scope, mockStateParams, mockStudyPlanResource,
			mockStudyPlanList, defaultStudyPlan, mockCentrisNotify;

	beforeEach(module("studyPlanApp", "sharedServices", "mockConfig"));

	beforeEach(inject(function($rootScope) {
		scope = $rootScope.$new();
	}));

	beforeEach(inject(function($injector) {
		mockCentrisNotify = $injector.get("mockCentrisNotify");
		mockStudyPlanResource = $injector.get("MockStudyPlanResource");
		mockStudyPlanList = $injector.get("MockStudyPlanList")[0];
		mockCentrisNotify.error = jasmine.createSpy("error");
		mockCentrisNotify.success = jasmine.createSpy("success");
	}));

	mockStateParams = {
		courseInstanceID: 1
	};

	defaultStudyPlan = {
		Unit:                     "Vika",
		Stints:                   [],
		HasLectureColumn:         true,
		HasAssignmentColumn:      true,
		HasReadingMaterialColumn: true,
		HasOtherColumn:           true
	};

	describe("testing with a valid StudyPlan object", function() {
		beforeEach(inject(function($controller) {
			ctrl = $controller("StudyPlanController", {
				$scope: scope,
				$stateParams: mockStateParams,
				StudyPlanResource: mockStudyPlanResource.mockStudyPlanResource(0, true, true),
				centrisNotify: mockCentrisNotify
			});
		}));

		it("should get course study plan", function() {
			expect(scope.fetchingData).toBe(false);
			expect(scope.StudyPlan).toEqual(mockStudyPlanList);
		});

		it("should display a message when save is successful", function() {
			var startLength = scope.StudyPlan.Stints.length;
			scope.saveStudyPlan();
			expect(scope.validation.noColumnsSelected).toBe(false);
			expect(mockCentrisNotify.success).toHaveBeenCalledWith("studyplan.Msg.StudyPlanUpdated");
			expect(scope.StudyPlan.Stints.length).not.toEqual(startLength);
		});

		it("should not allow the creation of a study plan with no columns", function() {
			var startLength = scope.StudyPlan.Stints.length;
			scope.validation.noColumnsSelected = true;
			scope.StudyPlan.HasLectureColumn = false;
			scope.StudyPlan.HasAssignmentColumn = false;
			scope.StudyPlan.HasReadingMaterialColumn = false;
			scope.StudyPlan.HasOtherColumn = false;
			scope.saveStudyPlan();
			expect(scope.StudyPlan.Stints.length).toEqual(startLength);
		});

		it("should not push stints if stint count is more than 12", function() {
			scope.StudyPlan.Stints = [{ID: 1}, {ID: 2}, {ID: 3}, {ID: 4}, {ID: 5},
			{ID: 6}, {ID: 7}, {ID: 8}, {ID: 9}, {ID: 10}, {ID: 11}, {ID: 12}, {ID: 13}];
			var startLength = scope.StudyPlan.Stints.length;
			scope.saveStudyPlan();
			expect(scope.StudyPlan.Stints.length).toEqual(startLength);
		});

		it("should change weeks into days if StudyPlan unit equals Dagur", function() {
			scope.StudyPlan.Unit = "Dagur";
			scope.saveStudyPlan();
			expect(scope.addIncrement).toEqual("days");
		});
	});

	describe("testing with an invalid StudyPlan object", function() {
		beforeEach(inject(function($controller) {
			ctrl = $controller("StudyPlanController", {
				$scope: scope,
				$stateParams: mockStateParams,
				StudyPlanResource: mockStudyPlanResource.mockStudyPlanResource(1, true),
			});
		}));

		it("should use the default studyplan if ID is invalid", function() {
			expect(scope.StudyPlan).toEqual(defaultStudyPlan);
		});
	});

	describe("testing with an empty Stints object", function() {
		beforeEach(inject(function($controller) {
			ctrl = $controller("StudyPlanController", {
				$scope: scope,
				$stateParams: mockStateParams,
				StudyPlanResource: mockStudyPlanResource.mockStudyPlanResource(2, true),
			});
		}));

		it("should not use date from Stints if there are none", function() {
			expect(scope.StudyPlan.Stints.length).toBe(0);
		});
	});

	describe("testing the error function for get courses study plan", function() {
		beforeEach(inject(function($controller) {
			ctrl = $controller("StudyPlanController", {
				$scope: scope,
				$stateParams: mockStateParams,
				StudyPlanResource: mockStudyPlanResource.mockStudyPlanResource(0, false, false),
				centrisNotify: mockCentrisNotify
			});
		}));

		it("should use default study plan if there is an error", function() {
			expect(scope.StudyPlan).toEqual(defaultStudyPlan);
			expect(scope.fetchingData).toBe(false);
		});

		it("should display an error message when saving fails", function() {
			scope.saveStudyPlan();
			expect(scope.validation.noColumnsSelected).toBe(false);
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("studyplan.Msg.ErrorSavingStudyPlan");
		});
	});
});
