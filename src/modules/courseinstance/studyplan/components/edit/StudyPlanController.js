"use strict";

angular.module("studyPlanApp").controller("StudyPlanController",
function StudyPlanController($scope, $state, $stateParams, StudyPlanResource, centrisNotify, dateTimeService) {

	$scope.options = {
		Quantity: 12,
		Begins:   moment().format(dateTimeService.dateFormat()),
	};

	// Set the current study plan to some reasonable defaults:
	$scope.StudyPlan = {
		Unit:                     "Vika",
		Stints:                   [],
		HasLectureColumn:         true,
		HasAssignmentColumn:      true,
		HasReadingMaterialColumn: true,
		HasOtherColumn:           true
	};

	$scope.validation = {
		noColumnsSelected: false
	};

	$scope.fetchingData = true;
	StudyPlanResource.getCourseStudyPlan($stateParams.courseInstanceID).success(function(studyplan) {
		$scope.fetchingData = false;
		if (studyplan.ID > 0) {
			$scope.StudyPlan = studyplan;
			if ($scope.StudyPlan.Stints.length > 0) {
				$scope.options.Begins = moment($scope.StudyPlan.Stints[0].DateFrom).format(dateTimeService.dateFormat());
			}
		}
	}).error(function(err) {
		$scope.fetchingData = false;
	});

	$scope.saveStudyPlan = function saveStudyPlan() {

		$scope.validation.noColumnsSelected = false;

		// Minor validation: creating a study plan with no columns
		// is not allowed!
		if (!$scope.StudyPlan.HasLectureColumn &&
			!$scope.StudyPlan.HasAssignmentColumn &&
			!$scope.StudyPlan.HasReadingMaterialColumn &&
			!$scope.StudyPlan.HasOtherColumn) {
			$scope.validation.noColumnsSelected = true;
			return;
		}

		// TODO: this will have to be revisited
		// to ensure it works when editing!
		var prevStintCount = $scope.StudyPlan.Stints.length;
		if (prevStintCount < $scope.options.Quantity) {

			// Set the start date of the first stint added.
			// If this is the first stint of the course, then we should set it to
			// the start date of the course, of course!
			var beginDate  = dateTimeService.mergeDateAndTime($scope.options.Begins, "00:00");
			if (prevStintCount !== 0) {
				beginDate = moment($scope.StudyPlan.Stints[prevStintCount - 1].DateTo).add(1, "days");
			}

			// Calculate the start and end dates for
			// the given stint. We set the end stint to
			// a date one "unit" below the start of the next stint.
			// Example: the first stint starts at 10. jan. 2015 (at 00:00)
			// and the next starts one week later, or at 17. jan 2015 (also at 00:00).
			// Then the first stint ends at 16. jan 2015 23:59.
			// Same would go for stints of type "date".
			// In that case, the first stint starts at 10. jan 2015
			// at midnight, and ends at 23:59. The next stint starts
			// the next day.
			$scope.addIncrement = "weeks";
			if ($scope.StudyPlan.Unit === "Dagur") {
				$scope.addIncrement = "days";
			}

			var addedCount = $scope.options.Quantity - prevStintCount;
			for (var i = 0; i < addedCount; i++) {

				var endDate = beginDate.clone().add(1, $scope.addIncrement).endOf("day");
				endDate     = endDate.subtract(1, "days").endOf("day");
				var stint = {
					Lectures:        [],
					Assignments:     [],
					ReadingMaterial: [],
					Other:           [],
					Description:     "",
					Summary:         "",
					Title:           $scope.StudyPlan.Unit + " " + (prevStintCount + i + 1),
					DateFrom:        dateTimeService.momentToISO(beginDate),
					DateTo:          dateTimeService.momentToISO(endDate)
				};

				$scope.StudyPlan.Stints.push(stint);

				beginDate.add(1, $scope.addIncrement);
			}
		}

		StudyPlanResource
		.saveCourseStudyPlan($stateParams.courseInstanceID, $scope.StudyPlan)
		.success(function(studyplan) {
			centrisNotify.success("studyplan.Msg.StudyPlanUpdated");
		}).error(function() {
			centrisNotify.error("studyplan.Msg.ErrorSavingStudyPlan");
		});
	};

	$scope.selectedWeek = 0;

	// TODO: translate!
	$scope.Units = [{
		Title: "Vika",
		ID:    0
	}, {
		Title: "Dagur",
		ID:    1
	}];
});
