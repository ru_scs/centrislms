"use strict";

angular.module("studyPlanApp").factory("StintDlg",
function StintDlg($uibModal) {
	return {
		show: function show(title, desc, ID) {
			var modalInstance = $uibModal.open({
				templateUrl: "courseinstance/studyplan/components/stint-dlg/stint-dialog.tpl.html",
				controller:  "StintController",
				resolve:     {
					modalParam: function() {
						return {
							title: title,
							desc:  desc,
							id:    ID
						};
					}
				}
			});
			return modalInstance.result;
		}
	};
});