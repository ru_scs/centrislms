"use strict";

describe("Testing StintDlg", function () {

	var dialog, mockTitle, mockDesc, mockID;

	mockTitle = "Title";
	mockDesc = "Testing attribute";
	mockID = 1337;

	beforeEach(module("studyPlanApp", "sharedServices", "mockConfig"));
	beforeEach(inject(function($injector) {
		dialog = $injector.get("StintDlg");
	}));

	it("can get an instance of TopicsNewDlg", function() {
		expect(dialog).toBeDefined();
	});

	it("TopicsNewDlg.show should be defined", function() {
		dialog.show(mockTitle, mockDesc, mockID);
		expect(dialog.show).toBeDefined();
	});
});
