"use strict";

angular.module("studyPlanApp").controller("StintController",
function StintController($scope, $uibModalInstance, modalParam) {
	$scope.model = {
		ID:          modalParam.id,
		Title:       modalParam.title,
		Description: modalParam.desc
	};

	$scope.defaultFocus = true;

	$scope.submitEvent = function submitEvent(valid) {
		if (valid) {
			$uibModalInstance.close($scope.model);
		} else {
			$scope.showError = true;
		}
	};
});