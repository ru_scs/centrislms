"use strict";

/*
 * A study plan stint may contain multiple lines, where each line
 * belongs to a category (i.e. lecture, assignment, readingmaterial, other)
 */
angular.module("studyPlanApp").directive("studyPlanLine",
function studyPlanLine(iconFinderService) {
	return {
		restrict:    "E",
		templateUrl: "courseinstance/studyplan/components/line/study-plan-line.tpl.html",
		transclude:  true,
		require:     "^studyPlanStint",
		scope: {
			line:      "=ngModel",
			allowEdit: "="
		},
		link: function (scope, element, attributes) {
			angular.forEach(scope.line.Items, function(item) {
				item.Type = iconFinderService.getType(item.Url);
			});
		}
	};
});