"use strict";
describe("Testing studyPlanLine directive", function() {

	var scope, compile, element, httpBackend, mockStudyPlanList;

	describe("with allow edit property", function() {
		beforeEach(module("studyPlanApp", "sharedServices", "mockConfig"));
		beforeEach(inject(function($rootScope, $compile, $httpBackend, $injector) {
			element = angular.element(
				"<study-plan-line " +
				"ng-model='line'" +
				" allow-edit='allowEdit'>" +
				"</study-plan-line>"
			);

			$httpBackend.when("GET", "courseinstance/studyplan/components/line/study-plan-line.tpl.html")
			.respond("<div></div>");
			scope = $rootScope.$new();
			mockStudyPlanList = $injector.get("MockStudyPlanList");
			scope.Lines = mockStudyPlanList[0].Stints[0].Assignments.Lines;
			scope.line = scope.Lines[0];
			compile = $compile(element)(scope);
			scope.$digest();
		}));

		it("should define the directive", function() {
			expect(compile).toBeDefined();
			// console.log(compile);
			// httpBackend.flush();
		});
	});
});
