"use strict";

describe("Testing StudyPlanViewer directive", function() {
	var scope, compile, element;

	beforeEach(module("studyPlanApp", "sharedServices", "mockConfig", "courseInstanceShared"));
	beforeEach(inject(function($rootScope, $compile) {
		element = angular.element(
				"<study-plan-viewer></study-plan-viewer>"
		);

		scope = $rootScope.$new();
		compile = $compile(element)(scope);
	}));

	it("should define the directive", function() {
		expect(compile).toBeDefined();
	});
});
