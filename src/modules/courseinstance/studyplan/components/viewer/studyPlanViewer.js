"use strict";

angular.module("studyPlanApp").directive("studyPlanViewer",
function studyPlanViewer() {
	return {
		restrict: "E",
		scope: {
		},
		controller: "StudyPlanViewerController",
		templateUrl: "courseinstance/studyplan/components/viewer/index.html"
	};
});