"use strict";

describe("StudyPlanViewerController", function() {
	var ctrl, scope, mockStudyPlanResource, mockCentrisNotify, mockCourseInstanceRoleService,
			mockState, mockTranslate, translations;
	translations = [];

	beforeEach(module("studyPlanApp", "sharedServices", "courseInstanceShared"));

	beforeEach(inject(function($rootScope, $injector) {
		scope = $rootScope.$new();
		mockCourseInstanceRoleService = $injector.get("MockCourseInstanceRoleService");
		mockCentrisNotify = $injector.get("mockCentrisNotify");
		mockStudyPlanResource = $injector.get("MockStudyPlanResource");
		mockTranslate = $injector.get("mockTranslate").mockTranslate;
	}));

	describe("testing teacher StudyPlan", function() {
		beforeEach(inject(function($controller) {
			ctrl = $controller("StudyPlanViewerController", {
				$scope: scope,
				CourseInstanceRoleService: mockCourseInstanceRoleService.mockCourseInstanceRoleService(true),
				centrisNotify: mockCentrisNotify,
				$state: mockState,
				StudyPlanResource: mockStudyPlanResource.mockStudyPlanResource(0, true),
				$translate: mockTranslate
			});
		}));

		it("scope teacher should be true", function() {
			expect(scope.isTeacher).toBe(true);
			expect(scope.loadingData).toBe(false);
		});

		it("should give error message when loading StudyPlan", inject(function($controller) {
			mockCentrisNotify.error = jasmine.createSpy("error");
			ctrl = $controller("StudyPlanViewerController", {
				$scope: scope,
				CourseInstanceRoleService: mockCourseInstanceRoleService.mockCourseInstanceRoleService(true),
				centrisNotify: mockCentrisNotify,
				$state: mockState,
				StudyPlanResource: mockStudyPlanResource.mockStudyPlanResource(false),
				$translate: mockTranslate
			});

			expect(scope.loadingData).toBe(false);
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("studyplan.Msg.ErrorLoadingStudyPlan");
		}));
	});

	describe("testing student StudyPlan", function() {
		beforeEach(inject(function($controller) {
			ctrl = $controller("StudyPlanViewerController", {
				$scope: scope,
				CourseInstanceRoleService: mockCourseInstanceRoleService.mockCourseInstanceRoleService(false),
				centrisNotify: mockCentrisNotify,
				$state: mockState,
				StudyPlanResource: mockStudyPlanResource.mockStudyPlanResource(true)
			});
		}));

		it("scope teacher should be false", function() {
			expect(scope.isTeacher).toBe(false);
			expect(scope.loadingData).toBe(false);
		});
	});

	describe("testing the scope.$on function", function() {
		mockState = {
			current: {
				name: "courseinstance"
			}
		};

		beforeEach(inject(function($controller) {
			ctrl = $controller("StudyPlanViewerController", {
				$scope: scope,
				CourseInstanceRoleService: mockCourseInstanceRoleService.mockCourseInstanceRoleService(true),
				centrisNotify: mockCentrisNotify,
				$state: mockState,
				StudyPlanResource: mockStudyPlanResource.mockStudyPlanResource(true)
			});
		}));

		it("state.current.name should be course instance and ShowCourseInstanceIntro should be called", function() {
			var param;
			scope.ShowCourseInstanceIntro = jasmine.createSpy();
			spyOn(scope, "$broadcast").and.callThrough();
			scope.$broadcast("onCentrisIntro", param);
			expect(scope.ShowCourseInstanceIntro).toHaveBeenCalled();
		});

		it("state.current.name should not be course instance and ShowCourseInstanceIntro should not be called",
		inject(function($controller) {
			mockState.current.name = "SomethingElse";
			ctrl = $controller("StudyPlanViewerController", {
				$scope: scope,
				CourseInstanceRoleService: mockCourseInstanceRoleService.mockCourseInstanceRoleService(true),
				centrisNotify: mockCentrisNotify,
				$state: mockState,
				StudyPlanResource: mockStudyPlanResource.mockStudyPlanResource(true)
			});
			var param;
			scope.ShowCourseInstanceIntro = jasmine.createSpy();
			spyOn(scope, "$broadcast").and.callThrough();
			scope.$broadcast("onCentrisIntro", param);
			expect(scope.ShowCourseInstanceIntro).not.toHaveBeenCalled();
		}));
	});
});
