"use strict";

/**
 * StudyPlanViewerController takes care of displaying a study plan in
 * a course. It uses directives/components extensively to delegate
 * displaying duties of certain parts, partly because those components
 * are then reused while editing.
 */
angular.module("studyPlanApp").controller("StudyPlanViewerController",
function StudyPlanViewerController($scope, $state, $translate, $stateParams,
	StudyPlanResource, centrisNotify, CourseInstanceRoleService) {

	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(isTeacher) {
		$scope.isTeacher = isTeacher;
	});

	// Since this is (currently) a "parent" controller, i.e. it belongs to a ui-view
	// which hosts all courseinstance subpages, an instance of this class will be created
	// for each request within a course instance. However, we only want to load the data if
	// we are actually about to display the front page of the course.
	if ($state.current.name === "courseinstance") {
		$scope.loadingData = true;
		StudyPlanResource.getCourseStudyPlan($stateParams.courseInstanceID).success(function(studyplan) {
			$scope.loadingData = false;
			$scope.studyPlan   = studyplan;
		}).error(function() {
			$scope.loadingData = false;
			centrisNotify.error("studyplan.Msg.ErrorLoadingStudyPlan");
		});
	}

	// Define all the steps in the help section:
	$scope.IntroOptions = {
		steps: [{
			element: "#courseinstancecontent",
			position: "left",
			intro: ""
		}, {
			element: "#courseStep2",
			position: "right",
			intro: ""
		}, {
			element: "#courseStep3",
			position: "right",
			intro: ""
		}, {
			element: "#courseStep4",
			position: "left",
			intro: ""
		}, {
			element: "#courseStep5",
			position: "right",
			intro: ""
		}],
		showStepNumbers:    false,
		exitOnOverlayClick: true,
		exitOnEsc:          true,
	};

	// Calling the translate files for each step and putting them into an array
	var introKeys = [
		"AngIntroCourse.Syllabus",
		"AngIntroCourse.Collapse",
		"AngIntroCourse.MarkedAsViewed",
		"AngIntroCourse.RightSidebar",
		"AngIntroCourse.GoBack",
	];

	// Setting the translates as the text for each step
	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
		$scope.IntroOptions.steps[1].intro = translations[introKeys[1]];
		$scope.IntroOptions.steps[2].intro = translations[introKeys[2]];
		$scope.IntroOptions.steps[3].intro = translations[introKeys[3]];
		$scope.IntroOptions.steps[4].intro = translations[introKeys[4]];
	});

	// Calling the translate files for each button and putting them into an array
	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	// Setting the translates as the text for each button
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.$on("onCentrisIntro", function(event, args) {
		// Only show this if the current route is for our page:
		if ($state.current.name === "courseinstance") {
			$scope.ShowCourseInstanceIntro();
		}
	});
});
