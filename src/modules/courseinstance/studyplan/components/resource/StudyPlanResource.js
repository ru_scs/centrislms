"use strict";

angular.module("studyPlanApp").factory("StudyPlanResource", function(CentrisResource) {
	return {
		getCourseStudyPlan: function getCourseStudyPlan(courseInstanceID) {
			var param = {
				id: courseInstanceID
			};

			return CentrisResource.get("courses", ":id/studyplan", param);
		},
		saveCourseStudyPlan: function saveCourseStudyPlan(courseInstanceID, data) {
			var param = {
				id: courseInstanceID
			};

			return CentrisResource.post("courses", ":id/studyplan", param, data);
		},
		saveStintDetails: function saveStintDetails(courseInstanceID, stint) {
			var param = {
				cid: courseInstanceID,
				sid: stint.ID
			};
			return CentrisResource.put("courses", ":cid/studyplan/:sid", param, stint);
		},
		saveOtherLine: function saveOtherLine(courseInstanceID, stintID, data) {
			var param = {
				cid: courseInstanceID,
				sid: stintID
			};

			return CentrisResource.post("courses", ":cid/studyplan/:sid/other", param, data);
		},
		saveReadingLine: function saveReadingLine(courseInstanceID, stintID, data) {
			var param = {
				cid: courseInstanceID,
				sid: stintID
			};

			return CentrisResource.post("courses", ":cid/studyplan/:sid/readingmaterial", param, data);
		},
		removeReadingLine: function removeReadingLine(courseInstanceID, stintID, lineID) {
			var param = {
				cid: courseInstanceID,
				sid: stintID,
				lid: lineID
			};

			return CentrisResource.remove("courses", ":cid/studyplan/:sid/readingmaterial?lineID=:lid", param);
		},
		removeOtherLine: function removeOtherLine(courseInstanceID, stintID, lineID) {
			var param = {
				cid: courseInstanceID,
				sid: stintID,
				lid: lineID
			};

			return CentrisResource.remove("courses", ":cid/studyplan/:sid/other?lineID=:lid", param);
		},
		saveLineCompleted: function saveLineCompleted(courseInstanceID, stintID, line) {

			var type = "reading";
			if (line.Type === "Other") {
				type = "other";
			}

			var param = {
				cid:  courseInstanceID,
				sid:  stintID,
				iid:  line.ID,
				type: type
			};

			// TODO: send type differently!

			return CentrisResource.put("courses", ":cid/studyplan/:sid/:type/:iid/completed", param);
		}

	};
});