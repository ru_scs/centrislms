"use strict";
angular.module("studyPlanApp").factory("MockStudyPlanList", function () {
	return [{
		HasAssignmentColumn: true,
		HasLectureColumn: true,
		HasOtherColumn: true,
		HasReadingMaterialColumn: true,
		ID: 18,
		Stints: [{
			Assignments: {
				Lines: [{
					AssignmentType: null,
					Completed: false,
					ID: 38268,
					Items: [{
						HasLectureColumn: true
					}],
					Text: "Dæmatímaverkefni 1",
					Type: "Assignment"
				}]
			},
			ReadingMaterial: {
				Lines: [{
					AssignmentType: null,
					Completed: false,
					ID: 38268,
					Items: [{
						HasLectureColumn: true
					}],
					Text: "Dæmatímaverkefni 1",
					Type: "Reading Material"
				}]
			},
			Other: {
				Lines: [{
					AssignmentType: null,
					Completed: false,
					ID: 38268,
					Items: [{
						HasLectureColumn: true
					}],
					Text: "Dæmatímaverkefni 1",
					Type: "Other"
				}]
			},
			Completed: false,
			CompletedPercentage: 66,
			DateFrom: "2016-01-11T00:00:00",
			DateTo: "2016-01-17T23:59:59",
			Description: "Kynning á áfanganum, grunnatriðin í JavaScript",
			ID: 112,
			Lectures: {
				Lines: [{
					AssignmentType: null,
					Completed: true,
					ID: 38268,
					Items: [{
						HasLectureColumn: true
					}],
					Text: "Dæmatímaverkefni 1",
					Type: "Assignment"
				}]
			},
			Summary: "",
			Title: "Vika 1"
		}, {
			Lectures: {
				Lines: [{
					AssignmentType: null,
					Completed: true,
					ID: 38268,
					Items: [{
						HasLectureColumn: true
					}],
					Text: "Dæmatímaverkefni 1",
					Type: "Assignment"
				}]
			},
			Assignments: {
				Lines: [{
					AssignmentType: null,
					Completed: true,
					ID: 38268,
					Items: [{
						HasLectureColumn: true
					}],
					Text: "Dæmatímaverkefni 1",
					Type: "Assignment"
				}]
			},
			ReadingMaterial: {
				Lines: [{
					AssignmentType: null,
					Completed: true,
					ID: 38268,
					Items: [{
						HasLectureColumn: true
					}],
					Text: "Dæmatímaverkefni 1",
					Type: "Reading Material"
				}]
			},
			Other: {
				Lines: [{
					AssignmentType: null,
					Completed: true,
					ID: 38268,
					Items: [{
						HasLectureColumn: true
					}],
					Text: "Dæmatímaverkefni 1",
					Type: "Other"
				}]
			},
			Completed: false,
			CompletedPercentage: 66,
			DateFrom: "2016-01-11T00:00:00",
			DateTo: "2016-01-17T23:59:59",
			Description: "Kynning á áfanganum, grunnatriðin í JavaScript",
			ID: 112,
			Summary: "",
			Title: "Vika 1"
		},
	],
		Unit: "Vika"
	},{
		ID: -1
	},{
		ID: 30,
		Stints: []
	},{
			HasAssignmentColumn: true,
			HasLectureColumn: false,
			HasOtherColumn: true,
			HasReadingMaterialColumn: true,
			ID: 18,
			Stints: [{
				Assignments: {
					Lines: [{
						AssignmentType: null,
						Completed: false,
						ID: 38268,
						Items: [{
							HasLectureColumn: true
						}],
						Text: "Dæmatímaverkefni 1",
						Type: "Assignment"
					}]
				},
				ReadingMaterial: {
					Lines: [{
						AssignmentType: null,
						Completed: false,
						ID: 38268,
						Items: [{
							HasLectureColumn: true
						}],
						Text: "Dæmatímaverkefni 1",
						Type: "Reading Material"
					}]
				},
				Other: {
					Lines: [{
						AssignmentType: null,
						Completed: false,
						ID: 38268,
						Items: [{
							HasLectureColumn: true
						}],
						Text: "Dæmatímaverkefni 1",
						Type: "Other"
					}]
				},
				Completed: false,
				CompletedPercentage: 66,
				DateFrom: "2016-01-11T00:00:00",
				DateTo: "2016-01-17T23:59:59",
				Description: "Kynning á áfanganum, grunnatriðin í JavaScript",
				ID: 112,
				Lectures: {
					Lines: [{
						AssignmentType: null,
						Completed: true,
						ID: 38268,
						Items: [{
							HasLectureColumn: true
						}],
						Text: "Dæmatímaverkefni 1",
						Type: "Assignment"
					}]
				},
				Summary: "",
				Title: "Vika 1"
			}, {
				Lectures: {
					Lines: [{
						AssignmentType: null,
						Completed: true,
						ID: 38268,
						Items: [{
							HasLectureColumn: true
						}],
						Text: "Dæmatímaverkefni 1",
						Type: "Assignment"
					}]
				},
				Assignments: {
					Lines: [{
						AssignmentType: null,
						Completed: true,
						ID: 38268,
						Items: [{
							HasLectureColumn: true
						}],
						Text: "Dæmatímaverkefni 1",
						Type: "Assignment"
					}]
				},
				ReadingMaterial: {
					Lines: [{
						AssignmentType: null,
						Completed: true,
						ID: 38268,
						Items: [{
							HasLectureColumn: true
						}],
						Text: "Dæmatímaverkefni 1",
						Type: "Reading Material"
					}]
				},
				Other: {
					Lines: [{
						AssignmentType: null,
						Completed: true,
						ID: 38268,
						Items: [{
							HasLectureColumn: true
						}],
						Text: "Dæmatímaverkefni 1",
						Type: "Other"
					}]
				},
				Completed: false,
				CompletedPercentage: 66,
				DateFrom: "2016-01-11T00:00:00",
				DateTo: "2016-01-17T23:59:59",
				Description: "Kynning á áfanganum, grunnatriðin í JavaScript",
				ID: 112,
				Summary: "",
				Title: "Vika 1"
			},
		],
			Unit: "Vika"
		},{
				HasAssignmentColumn: true,
				HasLectureColumn: false,
				HasOtherColumn: false,
				HasReadingMaterialColumn: true,
				ID: 18,
				Stints: [{
					Assignments: {
						Lines: [{
							AssignmentType: null,
							Completed: false,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Assignment"
						}]
					},
					ReadingMaterial: {
						Lines: [{
							AssignmentType: null,
							Completed: false,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Reading Material"
						}]
					},
					Other: {
						Lines: [{
							AssignmentType: null,
							Completed: false,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Other"
						}]
					},
					Completed: false,
					CompletedPercentage: 66,
					DateFrom: "2016-01-11T00:00:00",
					DateTo: "2016-01-17T23:59:59",
					Description: "Kynning á áfanganum, grunnatriðin í JavaScript",
					ID: 112,
					Lectures: {
						Lines: [{
							AssignmentType: null,
							Completed: true,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Assignment"
						}]
					},
					Summary: "",
					Title: "Vika 1"
				}, {
					Lectures: {
						Lines: [{
							AssignmentType: null,
							Completed: true,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Assignment"
						}]
					},
					Assignments: {
						Lines: [{
							AssignmentType: null,
							Completed: true,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Assignment"
						}]
					},
					ReadingMaterial: {
						Lines: [{
							AssignmentType: null,
							Completed: true,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Reading Material"
						}]
					},
					Other: {
						Lines: [{
							AssignmentType: null,
							Completed: true,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Other"
						}]
					},
					Completed: false,
					CompletedPercentage: 66,
					DateFrom: "2016-01-11T00:00:00",
					DateTo: "2016-01-17T23:59:59",
					Description: "Kynning á áfanganum, grunnatriðin í JavaScript",
					ID: 112,
					Summary: "",
					Title: "Vika 1"
				},
			],
				Unit: "Vika"
			},{
				HasAssignmentColumn: true,
				HasLectureColumn: false,
				HasOtherColumn: false,
				HasReadingMaterialColumn: false,
				ID: 18,
				Stints: [{
					Assignments: {
						Lines: [{
							AssignmentType: null,
							Completed: false,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Assignment"
						}]
					},
					ReadingMaterial: {
						Lines: [{
							AssignmentType: null,
							Completed: false,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Reading Material"
						}]
					},
					Other: {
						Lines: [{
							AssignmentType: null,
							Completed: false,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Other"
						}]
					},
					Completed: false,
					CompletedPercentage: 66,
					DateFrom: "2016-01-11T00:00:00",
					DateTo: "2016-01-17T23:59:59",
					Description: "Kynning á áfanganum, grunnatriðin í JavaScript",
					ID: 112,
					Lectures: {
						Lines: [{
							AssignmentType: null,
							Completed: true,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Assignment"
						}]
					},
					Summary: "",
					Title: "Vika 1"
				},{
					Lectures: {
						Lines: [{
							AssignmentType: null,
							Completed: true,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Assignment"
						}]
					},
					Assignments: {
						Lines: [{
							AssignmentType: null,
							Completed: true,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Assignment"
						}]
					},
					ReadingMaterial: {
						Lines: [{
							AssignmentType: null,
							Completed: true,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Reading Material"
						}]
					},
					Other: {
						Lines: [{
							AssignmentType: null,
							Completed: true,
							ID: 38268,
							Items: [{
								HasLectureColumn: true
							}],
							Text: "Dæmatímaverkefni 1",
							Type: "Other"
						}]
					},
					Completed: false,
					CompletedPercentage: 66,
					DateFrom: "2016-01-11T00:00:00",
					DateTo: "2016-01-17T23:59:59",
					Description: "Kynning á áfanganum, grunnatriðin í JavaScript",
					ID: 112,
					Summary: "",
					Title: "Vika 1"
				}],
				Unit: "Vika"
			}];
});
