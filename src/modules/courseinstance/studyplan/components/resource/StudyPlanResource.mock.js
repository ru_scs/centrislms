"use strict";
angular.module("studyPlanApp")
.factory("MockStudyPlanResource", function (MockStudyPlanList) {
	return {
		mockStudyPlanResource: function mockStudyPlanResource(i,
				getCourseStudyPlan, saveCourseStudyPlan,
				saveOtherLine, saveReadingLine) {
			return {
				getCourseStudyPlan: function(id) {
					return {
						success: function(fn) {
							if (getCourseStudyPlan) {
								fn(MockStudyPlanList[i]);
							}
							return {
								error: function(fn) {
									if (!getCourseStudyPlan) {
										fn();
									}
								}
							};
						}
					};
				},
				saveCourseStudyPlan: function(id) {
					return {
						success: function(fn) {
							if (saveCourseStudyPlan) {
								fn(MockStudyPlanList[i]);
							}
							return {
								error: function(fn) {
									if (!saveCourseStudyPlan) {
										fn();
									}
								}
							};
						}
					};
				},
				saveOtherLine: function(courseInstanceID, stintID, line) {
					return {
						success: function(fn) {
							if (saveOtherLine) {
								fn(MockStudyPlanList[i].Stints[0].Other.Lines[0]);
							}
							return {
								error: function(fn) {
									if (!saveOtherLine) {
										fn();
									}
								}
							};
						}
					};
				},
				saveReadingLine: function(courseInstanceID, stintID, line) {
					return {
						success: function(fn) {
							if (saveReadingLine) {
								fn(MockStudyPlanList[i].Stints[0].ReadingMaterial.Lines[0]);
							}
							return {
								error: function(fn) {
									if (!saveReadingLine) {
										fn();
									}
								}
							};
						}
					};
				},
				saveLineCompleted: function(courseInstanceID, stintID, line) {
					return {
						success: function(fn) {
							if (saveReadingLine) {
								fn(MockStudyPlanList[i].Stints[0].ReadingMaterial.Lines[0]);
							}
							return {
								error: function(fn) {
									if (!saveReadingLine) {
										fn();
									}
								}
							};
						}
					};
				},
				saveStintDetails: function(courseInstanceID, stint) {
					return {
						success: function(fn) {
							fn(MockStudyPlanList[i].Stints[0].ReadingMaterial.Lines[0]);
						}
					};
				},
				removeReadingLine: function(courseInstanceID, stintID, lineID) {
					return {
						success: function(fn) {
							fn(MockStudyPlanList[i].Stints[0].ReadingMaterial.Lines[0]);
						}
					};
				},
				removeOtherLine: function(courseInstanceID, stintID, lineID) {
					return {
						success: function(fn) {
							fn(MockStudyPlanList[i].Stints[0].Other.Lines[0]);
						}
					};
				}
			};
		}
	};
});
