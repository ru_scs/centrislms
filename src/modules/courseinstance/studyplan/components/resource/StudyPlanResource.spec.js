"use strict";

describe("Testing StudyPlanResource", function () {

	var StudyPlanResource, mock, mockStudyPlanObject;

	beforeEach(module("studyPlanApp", "sharedServices"));

	beforeEach(module("studyPlanApp", function($provide) {
		mock = {};
		mock.get = jasmine.createSpy();
		mock.put = jasmine.createSpy();
		mock.post = jasmine.createSpy();
		mock.remove = jasmine.createSpy();
		$provide.value("CentrisResource", mock);
	}));

	beforeEach(inject(function(_StudyPlanResource_, $injector) {
		StudyPlanResource = _StudyPlanResource_;
		mockStudyPlanObject = $injector.get("MockStudyPlanList")[0];
		spyOn(StudyPlanResource, "getCourseStudyPlan").and.callThrough();
		spyOn(StudyPlanResource, "saveCourseStudyPlan").and.callThrough();
		spyOn(StudyPlanResource, "saveStintDetails").and.callThrough();
		spyOn(StudyPlanResource, "saveOtherLine").and.callThrough();
		spyOn(StudyPlanResource, "saveReadingLine").and.callThrough();
		spyOn(StudyPlanResource, "removeReadingLine").and.callThrough();
		spyOn(StudyPlanResource, "removeOtherLine").and.callThrough();
		spyOn(StudyPlanResource, "saveLineCompleted").and.callThrough();
	}));

	it("should call the get course study plan function", function() {
		StudyPlanResource.getCourseStudyPlan();
		expect(StudyPlanResource.getCourseStudyPlan).toHaveBeenCalled();
		expect(mock.get).toHaveBeenCalled();
	});

	it("should call the save course study plan function", function() {
		StudyPlanResource.saveCourseStudyPlan();
		expect(StudyPlanResource.saveCourseStudyPlan).toHaveBeenCalled();
		expect(mock.post).toHaveBeenCalled();
	});

	it("should call the save stint details function", function() {
		StudyPlanResource.saveStintDetails(1, mockStudyPlanObject.Stints);
		expect(StudyPlanResource.saveStintDetails).toHaveBeenCalledWith(1, mockStudyPlanObject.Stints);
		expect(mock.put).toHaveBeenCalled();
	});

	it("should call the save other line function", function() {
		StudyPlanResource.saveOtherLine(1, 2, mockStudyPlanObject.Stints[0].Assignments.Lines[0]);
		expect(StudyPlanResource.saveOtherLine).toHaveBeenCalledWith(1, 2,
				mockStudyPlanObject.Stints[0].Assignments.Lines[0]);
		expect(mock.post).toHaveBeenCalled();
	});

	it("should call the save reading line function", function() {
		StudyPlanResource.saveReadingLine(3, 4, mockStudyPlanObject.Stints[0].Assignments.Lines[0]);
		expect(StudyPlanResource.saveReadingLine).toHaveBeenCalledWith(3, 4,
				mockStudyPlanObject.Stints[0].Assignments.Lines[0]);
		expect(mock.post).toHaveBeenCalled();
	});

	it("should call the remove reading line function", function() {
		StudyPlanResource.removeReadingLine(3, 4, mockStudyPlanObject.Stints[0].Assignments.Lines[0]);
		expect(StudyPlanResource.removeReadingLine).toHaveBeenCalledWith(3, 4,
				mockStudyPlanObject.Stints[0].Assignments.Lines[0]);
		expect(mock.remove).toHaveBeenCalled();
	});

	it("should call the remove other line function", function() {
		StudyPlanResource.removeOtherLine(3, 4, mockStudyPlanObject.Stints[0].Assignments.Lines[0]);
		expect(StudyPlanResource.removeOtherLine).toHaveBeenCalledWith(3, 4,
				mockStudyPlanObject.Stints[0].Assignments.Lines[0]);
		expect(mock.remove).toHaveBeenCalled();
	});

	it("should call the save line completed function, type should be reading", function() {
		StudyPlanResource.saveLineCompleted(3, 4, mockStudyPlanObject.Stints[0].Assignments.Lines[0]);
		expect(StudyPlanResource.saveLineCompleted).toHaveBeenCalledWith(3, 4,
				mockStudyPlanObject.Stints[0].Assignments.Lines[0]);
		expect(mock.put).toHaveBeenCalled();
	});

	it("should call the save line completed function, type should be other", function() {
		mockStudyPlanObject.Stints[0].Assignments.Lines[0].Type = "Other";
		StudyPlanResource.saveLineCompleted(3, 4, mockStudyPlanObject.Stints[0].Assignments.Lines[0]);
		expect(StudyPlanResource.saveLineCompleted).toHaveBeenCalledWith(3, 4,
				mockStudyPlanObject.Stints[0].Assignments.Lines[0]);
		expect(mock.put).toHaveBeenCalled();
	});
});
