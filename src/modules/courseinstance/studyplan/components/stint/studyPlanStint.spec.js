"use strict";

describe("Testing studyPlanStint directive", function() {
	var scope, compile, element, httpBackend, mockStudyPlanList, subElement, template;

	beforeEach(module("studyPlanApp", "sharedServices", "mockConfig"));

	describe("with allow edit property", function() {
		beforeEach(inject(function($rootScope, $compile, $httpBackend, $injector) {
			httpBackend = $injector.get("$httpBackend");
			element = angular.element(
					"<study-plan-stint " +
						"allow-edit='allowEdit'" +
						"ng-model='StudyPlan.Stints[0]'" +
						"studyplan='StudyPlan'>" +
					"</study-plan-stint>"
			);
			subElement = angular.element(
				"<study-plan-line " +
				"ng-model='line'" +
				" allow-edit='allowEdit'>" +
				"</study-plan-line>"
			);
			element.innerHTML = subElement;
			scope = $rootScope.$new();
			compile = $compile(element)(scope);

			httpBackend.when("GET", "courseinstance/studyplan/components/line/study-plan-line.tpl.html")
			.respond("<div></div>");
			httpBackend.when("GET", "courseinstance/studyplan/components/stint/study-plan-stint.tpl.html")
			.respond("<div></div>");

			mockStudyPlanList = $injector.get("MockStudyPlanList");
			scope.StudyPlan = mockStudyPlanList[0];
			scope.Lines = mockStudyPlanList[0].Stints[0].Assignments.Lines;
			scope.line = scope.Lines[0];
			scope.$digest();
		}));

		it("should define the directive", function() {
			expect(compile).toBeDefined();
			httpBackend.flush();
		});

		it("should have columncount as 3", function() {
			scope.StudyPlan = mockStudyPlanList[3];
			expect(compile).toBeDefined();
			httpBackend.flush();
		});

		it("should have columncount as 2", function() {
			scope.StudyPlan = mockStudyPlanList[4];
			expect(compile).toBeDefined();
			httpBackend.flush();
		});

		it("should have columncount as 1", function() {
			scope.StudyPlan = mockStudyPlanList[5];
			expect(compile).toBeDefined();
			httpBackend.flush();
		});
	});

	describe("not with allow edit property", function() {
		beforeEach(inject(function($rootScope, $compile, $httpBackend, $injector) {
			element = angular.element(
					"<study-plan-stint " +
						"ng-model='StudyPlan.Stints[selectedWeek]'" +
						"studyplan='StudyPlan'>" +
					"</study-plan-stint>"
			);

			$httpBackend.when("GET", "courseinstance/studyplan/components/stint/study-plan-stint.tpl.html")
			.respond("<div></div>");
			scope = $rootScope.$new();
			mockStudyPlanList = $injector.get("MockStudyPlanList");
			scope.StudyPlan = mockStudyPlanList;
			compile = $compile(element)(scope);
			httpBackend = $httpBackend;
		}));

		it("should define the directive", function() {
			expect(compile).toBeDefined();
			httpBackend.flush();
		});
	});
});
