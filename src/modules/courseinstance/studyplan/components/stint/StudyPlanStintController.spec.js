"use strict";

describe("Testing StudyPlanStintController", function() {
	var ctrl, scope, mockStateParams, mockDlg,
			mockStudyPlanResource, mockCentrisNotify, mockState,
			mockStudyPlanList, mockLine, mockStint;

	mockStateParams = {
		courseInstanceID: 1
	};

	mockDlg = {
		show: function() {
			return {
				then: function(fn) {
					var line = {};
					fn(line);
				}
			};
		}
	};

	mockLine = {
	};

	beforeEach(module("studyPlanApp", "sharedServices",
			"courseInstanceShared", "mockConfig"));

	describe("testing the the success functions", function() {
		beforeEach(inject(function($rootScope, $injector) {
			scope = $rootScope.$new();
			mockCentrisNotify = $injector.get("mockCentrisNotify");
			mockStudyPlanResource = $injector.get("MockStudyPlanResource").mockStudyPlanResource(0, true, true, true, true);
			mockStudyPlanList = $injector.get("MockStudyPlanList")[0];
			mockStint = mockStudyPlanList.Stints[1];
		}));

		beforeEach(inject(function($controller) {
			mockCentrisNotify.success = jasmine.createSpy("success");
			ctrl = $controller("StudyPlanStintController", {
				$scope: scope,
				centrisNotify: mockCentrisNotify,
				StudyPlanResource:  mockStudyPlanResource,
				$stateParams: mockStateParams,
				ReadingDlg: mockDlg,
				OtherDlg: mockDlg,
				StintDlg: mockDlg
			});
		}));

		it("should define scope", function() {
			expect(scope).toBeDefined();
		});

		it("should call markAsViewed with line completed as true, and save", function() {
			scope.recalcStintProgress = jasmine.createSpy("recalcStintProgress");
			mockLine.Completed = false;
			scope.line = mockLine;
			scope.stint = mockStint;
			scope.markAsViewed();
			expect(scope.recalcStintProgress).toHaveBeenCalled();
		});

		it("should call markAsViewed with line completed as true, and not save", function() {
			mockStudyPlanResource.saveLineCompleted = jasmine.createSpy("saveLineCompleted");
			mockLine.Completed = true;
			scope.line = mockLine;
			scope.markAsViewed();
			expect(mockStudyPlanResource.saveLineCompleted).not.toHaveBeenCalled();
		});

		it("should return true for col.lines giving a complete line", function() {
			var col = {
				Lines: [{
					Completed: true
				}]
			};
			expect(scope.recalcColumnProgress(col)).toBe(true);
		});

		it("should return false for col.lines not giving a complete line", function() {
			var col = {
				Lines: [{
					Completed: false
				}]
			};
			expect(scope.recalcColumnProgress(col)).toBe(false);
		});

		it("should add 1 to scope.stint.Other.Lines", function() {
			scope.stint = mockStint;
			var length = scope.stint.Other.Lines.length;
			scope.addOther();
			expect(length).toEqual(scope.stint.Other.Lines.length - 1);
		});

		it("should add 1 to scope.stint.ReadingMaterial.Lines", function() {
			scope.stint = mockStint;
			var length = scope.stint.ReadingMaterial.Lines.length;
			scope.addReading();
			expect(length).toEqual(scope.stint.ReadingMaterial.Lines.length - 1);
		});

		it("should call editStint and saveStintDetails", function() {
			scope.stint = mockStint;
			var event = {
				stopPropagation: function() {
				}
			};
			scope.editStint(event);
			expect(mockCentrisNotify.success).toHaveBeenCalledWith("studyplan.Msg.StintUpdated");
		});

		it("should call editLine and saveReadingLine", function() {
			mockLine.Type = "Reading Material";
			scope.line = mockLine;
			scope.stint = mockStint;
			scope.editLine();
			expect(mockCentrisNotify.success).toHaveBeenCalledWith("studyplan.Msg.StudyPlanUpdated");
		});

		it("should call editLine and saveOtherLine", function() {
			mockLine.Type = "Other";
			scope.line = mockLine;
			scope.stint = mockStint;
			scope.editLine();
			expect(mockCentrisNotify.success).toHaveBeenCalledWith("studyplan.Msg.StudyPlanUpdated");
		});

		it("should call removeLine and removeOther", function() {
			mockLine.Type = "Other";
			scope.line = mockLine;
			scope.stint = mockStint;
			var length = scope.stint.Other.Lines.length;
			scope.removeLine();
			expect(scope.stint.Other.Lines.length).toEqual(length - 1);
		});

		it("should call removeLine and removeReadingLine", function() {
			mockLine.Type = "Reading Material";
			scope.line = mockLine;
			scope.stint = mockStint;
			var length = scope.stint.ReadingMaterial.Lines.length;
			scope.removeLine();
			expect(scope.stint.ReadingMaterial.Lines.length).toEqual(length - 1);
		});
	});

	describe("Testing recalculations functions", function() {
		beforeEach(inject(function($rootScope, $injector) {
			scope = $rootScope.$new();
			mockCentrisNotify = $injector.get("mockCentrisNotify");
			mockStudyPlanResource = $injector.get("MockStudyPlanResource").mockStudyPlanResource(0, true, true, true, true);
			mockStudyPlanList = $injector.get("MockStudyPlanList")[0];
		}));

		beforeEach(inject(function($controller) {
			ctrl = $controller("StudyPlanStintController", {
				$scope: scope,
				centrisNotify: mockCentrisNotify,
				StudyPlanResource: mockStudyPlanResource,
				$stateParams: mockStateParams,
				ReadingDlg: mockDlg,
				OtherDlg: mockDlg,
				StintDlg: mockDlg
			});
		}));

		it("should recalc Stint progress and change stint.completed to true", function() {
			scope.stint = mockStint;
			var complete = scope.stint.Completed;
			scope.recalcStintProgress();
			expect(complete).not.toBe(scope.stint.Completed);
		});

		it("should recalc Stint progress and not change stint.completed to true", function() {
			scope.stint = mockStint;
			scope.stint.Lectures.Lines[0].Completed = false;
			scope.stint.Completed = false;
			var complete = scope.stint.Completed;
			scope.recalcStintProgress();
			expect(complete).toBe(scope.stint.Completed);
		});
	});

	describe("Calling error functions of StudyPlanResource", function() {
		beforeEach(inject(function($rootScope, $injector) {
			scope = $rootScope.$new();
			mockCentrisNotify = $injector.get("mockCentrisNotify");
			mockStudyPlanResource = $injector.get("MockStudyPlanResource").mockStudyPlanResource(0, true, true, false, false);
			mockStudyPlanList = $injector.get("MockStudyPlanList")[0];
		}));

		beforeEach(inject(function($controller) {
			ctrl = $controller("StudyPlanStintController", {
				$scope: scope,
				centrisNotify: mockCentrisNotify,
				StudyPlanResource: mockStudyPlanResource,
				$stateParams: mockStateParams,
				ReadingDlg: mockDlg,
				OtherDlg: mockDlg,
				StintDlg: mockDlg
			});
		}));

		it("should try to add other lines", function() {
			scope.stint = mockStint;
			var length = scope.stint.Other.Lines.length;
			scope.addOther();
			expect(length).toEqual(scope.stint.Other.Lines.length);
		});

		it("should try to add readingmaterial lines", function() {
			scope.stint = mockStint;
			var length = scope.stint.ReadingMaterial.Lines.length;
			scope.addReading();
			expect(length).toEqual(scope.stint.ReadingMaterial.Lines.length);
		});

		it("should call markAsViewed with line completed as true, save and call the error function", function() {
			scope.recalcStintProgress = jasmine.createSpy("recalcStintProgress");
			mockLine.Completed = false;
			scope.line = mockLine;
			scope.stint = mockStudyPlanList.Stints[1];
			scope.markAsViewed();
			expect(scope.recalcStintProgress).not.toHaveBeenCalled();
		});

		it("should activate watch function not change stintclass", inject(function($controller) {
			ctrl = $controller("StudyPlanStintController", {
				$scope: scope,
				StudyPlanResource: mockStudyPlanResource,
				$stateParams: mockStateParams
			});
			var preClass = scope.stintClass;
			scope.$apply();
			expect(scope.stintClass).toEqual(preClass);
		}));
	});
});
