"use strict";

/*
 * This component handles displaying a stint in a study plan.
 * It handles editing as well (if enabled).
 */
angular.module("studyPlanApp").directive("studyPlanStint",
function studyPlanStint() {
	return {
		restrict:      "E",
		templateUrl:   "courseinstance/studyplan/components/stint/study-plan-stint.tpl.html",
		scope: {
			stint:     "=ngModel",
			studyplan: "="
		},
		link: function(scope, elem, attrs) {
			// Check if allow-edit property is present
			if ("undefined" !== typeof attrs.allowEdit) {
				scope.allowEdit = true;
			} else {
				scope.allowEdit = false;
			}
		},
		controller: "StudyPlanStintController"
	};
});