"use strict";

angular.module("studyPlanApp").controller("StudyPlanStintController",
function StudyPlanStintController($scope, $stateParams, ReadingDlg,
	OtherDlg, StintDlg, StudyPlanResource, centrisNotify) {
	$scope.markAsViewed = function markAsViewed() {
		var stint = $scope.stint;
		var line  = $scope.line;

		if (!line.Completed) {
			StudyPlanResource
			.saveLineCompleted($stateParams.courseInstanceID, stint.ID, line)
			.success(function(data) {
				// We don't really do anything here...
				line.Completed = true;
				// ...except ensure that the UI reflects this change!

				// Also: ensure we update the "Completed" property of the parent stint
				// of the line which was completed!
				$scope.recalcStintProgress();

			}).error(function() {
				// Should we notify?
			});
		}
	};

	$scope.editStint = function editStint($event) {
		$event.stopPropagation(); // Prevents parent's ng-click from running
		var stint = $scope.stint;
		StintDlg.show(stint.Title, stint.Description, stint.ID).then(function(data) {
			stint.Title       = data.Title;
			stint.Description = data.Description;
			StudyPlanResource
			.saveStintDetails($stateParams.courseInstanceID, stint)
			.success(function(data) {
				centrisNotify.success("studyplan.Msg.StintUpdated");
			});
		});
	};

	$scope.editLine = function editLine() {
		var line = $scope.line;
		switch (line.Type) {
			case "Reading Material": {
				ReadingDlg.show(line.Text, line.Items, line.ID).then(function(modalLine) {
					StudyPlanResource
					.saveReadingLine($stateParams.courseInstanceID, $scope.stint.ID, modalLine)
					.success(function(data) {
						line.Text  = data.Text;
						line.Items = data.Items;
						centrisNotify.success("studyplan.Msg.StudyPlanUpdated");
					});
				});
				break;
			}
			case "Other": {
				OtherDlg.show(line.Text, line.ID).then(function(modalLine) {
					StudyPlanResource
					.saveOtherLine($stateParams.courseInstanceID, $scope.stint.ID, modalLine)
					.success(function(data) {
						line.Text = data.Text;
						centrisNotify.success("studyplan.Msg.StudyPlanUpdated");
					});
				});
				break;
			}
		}
	};

	$scope.removeLine = function removeLine() {
		var line = $scope.line;
		var index = $scope.$index;
		switch (line.Type) {
			case "Reading Material": {
				StudyPlanResource
				.removeReadingLine($stateParams.courseInstanceID, $scope.stint.ID, line.ID)
				.success(function(data) {
					$scope.stint.ReadingMaterial.Lines.splice(index, 1);
				});
				break;
			}
			case "Other": {
				StudyPlanResource
				.removeOtherLine($stateParams.courseInstanceID, $scope.stint.ID, line.ID)
				.success(function() {
					$scope.stint.Other.Lines.splice(index, 1);
				});
				break;
			}
		}
	};

	$scope.addReading = function addReading() {
		ReadingDlg.show().then(function(line) {
			StudyPlanResource
			.saveReadingLine($stateParams.courseInstanceID, $scope.stint.ID, line)
			.success(function(data) {
				$scope.stint.ReadingMaterial.Lines.push(data);
			})
			.error(function() {
				// TODO: Add some kind of error message
			});
		});
	};

	$scope.addOther = function addOther() {
		OtherDlg.show().then(function(line) {
			StudyPlanResource
			.saveOtherLine($stateParams.courseInstanceID, $scope.stint.ID, line)
			.success(function(data) {
				$scope.stint.Other.Lines.push(data);
			})
			.error(function() {
				// TODO: Add some kind of error message
			});
		});
	};

	// This function ensures that the Stint.Completed property is updated
	// i.e. if all the child lines are completed.
	// TODO: we might add some percentage calculation later,
	// in order to show how many percentage of the stint are completed.
	$scope.recalcStintProgress = function recalcStintProgress() {
		if ($scope.recalcColumnProgress($scope.stint.Lectures) &&
			$scope.recalcColumnProgress($scope.stint.Assignments) &&
			$scope.recalcColumnProgress($scope.stint.ReadingMaterial) &&
			$scope.recalcColumnProgress($scope.stint.Other)) {
			$scope.stint.Completed = true;
		}
	};

	$scope.recalcColumnProgress = function recalcColumnProgress(col) {
		for (var i = 0; i < col.Lines.length; ++i) {
			var line = col.Lines[i];
			if (line.Completed !== true) {
				return false;
			}
		}
		return true;
	};

	// Ensure stint columns have a reasonable default display:
	$scope.stintClass = "col-xs-12 col-md-3";
	$scope.$watch("studyplan", function(newValue, oldValue) {
		if (newValue) {
			var columnCount = 0;
			if (newValue.HasLectureColumn) {
				columnCount++;
			}
			if (newValue.HasAssignmentColumn) {
				columnCount++;
			}
			if (newValue.HasReadingMaterialColumn) {
				columnCount++;
			}
			if (newValue.HasOtherColumn) {
				columnCount++;
			}

			// Not entirely best practices, i.e. we're
			// refering to CSS classes here...
			if (columnCount === 1) {
				$scope.stintClass = "col-xs-12";
			} else if (columnCount === 2) {
				$scope.stintClass = "col-xs-12 col-md-6";
			} else if (columnCount === 3) {
				$scope.stintClass = "col-xs-12 col-md-4";
			} else if (columnCount === 4) {
				$scope.stintClass = "col-xs-12 col-md-3";
			}
		}
	});
});
