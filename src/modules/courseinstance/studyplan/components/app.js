"use strict";

angular.module("studyPlanApp", [
	"pascalprecht.translate",
	"ui.router",
	"courseInstanceShared"
]).config(function($stateProvider) {
	$stateProvider.state("courseinstance.studyplan", {
		url:         "/studyplan",
		templateUrl: "courseinstance/studyplan/components/edit/index.html",
		controller:  "StudyPlanController"
	});
});
