"use strict";

angular.module("studyPlanApp").factory("ReadingDlg",
function ReadingDlg($uibModal) {
	return {
		show: function show(text, items, ID) {
			var modalInstance = $uibModal.open({
				templateUrl: "courseinstance/studyplan/components/reading-dlg/reading-dialog.tpl.html",
				controller:  "ReadingController",
				resolve:     {
					modalParam: function() {
						return {
							id:    ID,
							text:  text,
							items: items
						};
					}
				}
			});
			return modalInstance.result;
		}
	};
});