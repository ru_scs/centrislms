"use strict";

describe("Testing ReadingController", function() {
	var ctrl, scope, mockSubmodalInstance, mockModalParam,
			mockIconFinderService, mockItem;

	mockSubmodalInstance = {};

	mockModalParam = {
		id: 1,
		title: "Hello",
		desc: "A greeting"
	};

	mockIconFinderService = {
		getType: function(item) {
			return "yt";
		}
	};

	mockItem = "youtube.com";

	beforeEach(module("studyPlanApp"));
	beforeEach(inject(function($rootScope) {
		scope = $rootScope.$new();
		mockSubmodalInstance.close = jasmine.createSpy("close");
	}));

	beforeEach(inject(function($controller) {
		ctrl = $controller("ReadingController", {
			$scope: scope,
			$uibModalInstance: mockSubmodalInstance,
			modalParam: mockModalParam,
			iconFinderService: mockIconFinderService
		});
	}));

	it("should assign newItem as empty string", function() {
		expect(scope.newItem).toEqual("");
	});

	it("should do nothing", function() {
		var length = scope.model.Items.length;
		scope.addItem();
		expect(scope.model.Items.length).toEqual(length);
	});

	it("should add to scope.model.items", function() {
		var length = scope.model.Items.length;
		scope.newItem = mockItem;
		scope.addItem();
		expect(scope.model.Items.length).toEqual(length + 1);
	});

	it("should call submodal close", function() {
		scope.submitEvent(true);
		expect(mockSubmodalInstance.close).toHaveBeenCalledWith(scope.model);
	});

	it("should set showError to true", function() {
		scope.submitEvent(false);
		expect(scope.showError).toBe(true);
	});

	it("should remove item from the scope.model.items", function() {
		scope.newItem = mockItem;
		scope.addItem();
		scope.newItem = mockItem;
		scope.addItem();
		var length = scope.model.Items.length;
		scope.removeItem();
		expect(scope.model.Items.length).toEqual(length - 1);
	});
});
