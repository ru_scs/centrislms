"use strict";

angular.module("studyPlanApp").controller("ReadingController",
function($scope, $uibModalInstance, modalParam, iconFinderService) {
	$scope.model = {
		ID:     modalParam.id,
		Text:   modalParam.text,
		Items:  angular.copy(modalParam.items) || [],
		Type:   "Reading Material",
		Viewed: false
	};

	$scope.newItem = "";

	$scope.addItem = function addItem() {
		if ($scope.newItem !== "") {
			$scope.model.Items.push({
				Url:  $scope.newItem,
				Type: iconFinderService.getType($scope.newItem)
			});
			$scope.newItem = "";
		}
	};

	$scope.submitEvent = function submitEvent(valid) {
		if (valid) {
			$uibModalInstance.close($scope.model);
		} else {
			$scope.showError = true;
		}
	};

	$scope.removeItem = function removeItem() {
		$scope.model.Items.splice(this.$index, 1);
	};
});