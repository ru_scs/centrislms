"use strict";

describe("Testing ReadingDlg", function () {

	var dialog, mockText, mockItems, mockID;

	mockText = "Small text";
	mockItems = [];
	mockID = 1337;

	beforeEach(module("studyPlanApp", "sharedServices", "mockConfig"));
	beforeEach(inject(function($injector) {
		dialog = $injector.get("ReadingDlg");
	}));

	it("can get an instance of TopicsNewDlg", function() {
		expect(dialog).toBeDefined();
	});

	it("TopicsNewDlg.show should be defined", function() {
		dialog.show(mockText, mockItems, mockID);
		expect(dialog.show).toBeDefined();
	});
});
