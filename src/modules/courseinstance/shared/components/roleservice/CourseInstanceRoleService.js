"use strict";

/*
 * This component can be used to determine the role of the currently
 * logged in user in a given course instance.
 */
angular.module("courseInstanceShared").service("CourseInstanceRoleService",
function ($q) {

	// Constructor for the role object
	// Contains the role flags and an array of object containing
	// deferred promises and a function to decode which flag is being checked
	// for example:
	// promise = {
	// 	promiseObject: deferred,
	// 	roleFn: decodeRoleForTeacher
	// }
	var RoleObject = function RoleObject(role) {
		this.role     = role;
		this.promises = [];
	};

	// Returns true if the teacher flag is set in the role
	var decodeRoleForTeacher = function decodeRoleForTeacher(role) {
		return (role & 4) === 4;
	};

	// Empty by default
	var roleMap = {
	};

	return {

		// Small optimization: at startup, the app fetches a list of
		// all courses for the logged-in user, both where the user is
		// a student and a teacher. We can optimize by saving these
		// information, and this function allows the HomeController
		// to do that.
		// Also checks if there are any waiting promises to be resolved
		setCourseRole: function setCourseRole(courseInstanceID, role) {
			// Ensure the type of the key is consistent,
			// i.e. that it is the same, both when the entry is
			// created, and when requested:
			var key = courseInstanceID.toString();

			// If the courseInstanceID is not in the map, then create new
			// RoleObject
			if (!roleMap[key]) {
				roleMap[key] = new RoleObject(role);
			} else {
				roleMap[key].role = role;
			}

			// Loop through all unresolved promises in the RoleObject and resolve them
			for (var i = roleMap[key].promises.length - 1; i >= 0; i--) {
				var promise = roleMap[key].promises.pop();
				promise.promiseObject.resolve(promise.roleFn(role));
			}
		},

		// Returns a promise object. The promise will be resolved
		// with true if the logged in person is a teacher in a given
		// course, but false otherwise.
		isTeacherInCourse: function isTeacherInCourse(courseInstanceID) {
			// Again, ensure that the type of the key is the same
			// as above:
			var key = courseInstanceID.toString();

			var deferred = $q.defer();

			// If the courseInstanceID is not in the map, then create new
			// RoleObject
			if (!roleMap[key]) {
				roleMap[key] = new RoleObject(0);
			}

			// Check if the course is already in our map:
			var role = roleMap[key].role;

			// If the role has already been set, resolve
			if (role) {
				deferred.resolve(decodeRoleForTeacher(role));
			} else {
				// Push a new promise to be resolved when the role is set
				roleMap[key].promises.push({
					promiseObject: deferred,
					roleFn: decodeRoleForTeacher
				});
			}

			return deferred.promise;
		}
	};
});
