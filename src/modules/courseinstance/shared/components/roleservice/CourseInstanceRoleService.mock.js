"use strict";

/**
 * This mock object should have the exact same interface as the proper
 * CourseInstanceRoleService object.
 * Note that this version always returns true when asked if the current
 * user is a teacher in a course (TODO: change that!)
 */
angular.module("courseInstanceShared").factory("MockCourseInstanceRoleService",
function MockCourseInstanceRoleService() {
	return {
		mockCourseInstanceRoleService: function mockCourseInstanceRoleService(isTeacher) {
			// Unit tests can set this value to true/false, depending on their needs:
			return {
				setCourseRole: function(courseInstanceID, role) {
					// Nothing to do here currently, as there are no unit tests
					// which exercise this function.
				},
				isTeacherInCourse: function(courseInstanceID) {
					return {
						then: function(fn) {
							fn(isTeacher);
						}
					};
				}
			};
		}
	};
});
