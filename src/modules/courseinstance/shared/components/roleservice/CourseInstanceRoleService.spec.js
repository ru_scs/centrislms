"use strict";

describe("CourseInstanceRoleService", function() {
	beforeEach(module("courseInstanceShared"));

	var service;
	var scope;

	beforeEach(inject(function($rootScope, _CourseInstanceRoleService_) {
		scope   = $rootScope.$new();
		service = _CourseInstanceRoleService_;
	}));

	it ("should define public functions", function() {
		expect(service.setCourseRole).toBeDefined();
		expect(service.isTeacherInCourse).toBeDefined();
	});

	it ("should return a promise when requesting role status", function() {
		var result = service.isTeacherInCourse(12345);
		expect(result).toBeDefined();
		expect(result.then).toBeDefined();
	});

	it ("should return that a given user is teacher when previously set", function() {
		service.setCourseRole(12345, true);
		service.isTeacherInCourse(12345).then(function(isTeacher) {
			expect(isTeacher).toBeTrue();
		});
	});
	// TODO: define other tests!
});
