"use strict";

angular.module("courseInstanceShared").factory("FileUtils",
function FileUtils() {
	return {
		// Borrowed from here:
		// http://stackoverflow.com/questions/15900485/correct-way-to-convert-size-in-bytes-to-kb-mb-gb-in-javascript
		formatBytes: function formatBytes(bytes, decimals) {
			if (bytes === 0) {
				return "0 Byte";
			}

			var k     = 1000;
			var dm    = decimals + 1 || 3;
			var sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
			var i     = Math.floor(Math.log(bytes) / Math.log(k));

			return (bytes / Math.pow(k, i)).toPrecision(dm) + " " + sizes[i];
		}
	};
});