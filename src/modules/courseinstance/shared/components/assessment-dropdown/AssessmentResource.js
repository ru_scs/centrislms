"use strict";

angular.module("courseInstanceShared").factory("AssessmentResource",
function AssessmentResource(CentrisResource) {

	return {
		// Returns the assessments defined in a course
		getCourseAssessments: function getCourseAssessments(courseInstanceID, type) {
			var param = {
				courseInstanceID: courseInstanceID,
				type: type
			};
			return CentrisResource.get("courses", ":courseInstanceID/assessment?type=:type", param);
		}
	};
});
