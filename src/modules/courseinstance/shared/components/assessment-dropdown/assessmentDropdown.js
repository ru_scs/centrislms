"use strict";

/**
 * This directive will be used to display a list of course assessment items,
 * and allowing the user (probably a teacher) to choose between them.
 */
angular.module("courseInstanceShared").directive("assessmentDropdown",
function assessmentDropdown(AssessmentResource) {
	return {
		restrict: "E",
		require:  "ngModel",
		template: "<select class='form-control' " +
				"ng-model='assessment' " +
				"ng-change='onChangeAssessment()' " +
				"ng-options='obj.ID as obj.SelectText for obj in model.assessments'> " +
				"</select>",
		scope: {
			onUpdateAssessment:    "&",
			courseInstanceId:      "=",
			assessmentType:        "=",
			assessment:            "=ngModel"
		},
		link: function(scope, element, attributes) {

			scope.model = {
				assessments: []
			};

			var undefinedObj = {
				ID:         "",
				SelectText: "Ekki hluti af lokaeinkunn", // TODO: translate!
				Name:       ""
			};

			scope.$watch("assessment", function(newValue, oldValue) {
				// console.log("$watch assessment");
				// console.log(newValue);
				// console.log(oldValue);
			});

			// Load the assessments for the course:
			AssessmentResource.getCourseAssessments(scope.courseInstanceId, scope.assessmentType)
			.success(function(data) {
				// First, we add the object which represents "no selection"
				scope.model.assessments.push(undefinedObj);

				scope.assessment = parseInt(scope.assessment, 10);

				for (var i = 0; i < data.Items.length; i++) {

					var item = data.Items[i];
					if (item.IsAvailable || item.ID === scope.assessment) {

						// Create a custom property on each item, will
						// be used when displaying the item:
						item.SelectText = item.Name + " - " + item.Weight + "%";
						scope.model.assessments.push(item);
					}
				}
			});

			scope.onChangeAssessment = function onChangeAssessment() {
				for (var i = 0; i < scope.model.assessments.length; ++i) {
					var current = scope.model.assessments[i];
					if (current.ID === scope.assessment) {
						scope.onUpdateAssessment({assessment: current});
						break;
					}
				}
			};
		}
	};
});
