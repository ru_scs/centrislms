"use strict";

/*
 * This component displays the status of a project (i.e. assignment/onlineexam).
 * More specifically, it will show graphically how many students have handed in
 * the given project, how many of those have been graded, and how many of those
 * have been published to students.
 */
angular.module("courseInstanceShared").directive("projectStatus",
function projectStatus ($sce, $translate) {
	return {
		restrict: "E",
		templateUrl: "courseinstance/shared/components/project-status/project-status.html",
		scope: {
			studentCount: "=",
			handinCount: "=",
			gradedCount: "="
		},
		link: function (scope, element, attributes) {

			var buildHtmlString = function buildHtmlString() {

				var translateStrings = [
					"cishared.ProjectStatus.Of",
					"cishared.ProjectStatus.HandedIn",
					"cishared.ProjectStatus.AreGraded"
				];

				$translate(translateStrings).then(function(data) {
					var str = "<div>";
					str += scope.handinCount;
					str += " " + data["cishared.ProjectStatus.Of"];
					str += " " + scope.studentCount;
					str += " " + data["cishared.ProjectStatus.HandedIn"];
					str += "</div></div>";

					if (scope.gradedCount) {
						str += scope.gradedCount;
						str += " " + data["cishared.ProjectStatus.Of"];
						str += " " + scope.handinCount;
						str += " " + data["cishared.ProjectStatus.AreGraded"];
						str += "</div>";
					}
					scope.htmlString = $sce.trustAsHtml(str);
				});
			};

			buildHtmlString();
		}
	};
});
