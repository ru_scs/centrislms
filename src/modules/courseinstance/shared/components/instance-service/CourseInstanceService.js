"use strict";

/*
 * This service manages the "current" course instance being viewed at any time.
 * The object is fetched by the root home controller for a course instance,
 * and child controllers can then fetch the instance via this service. Since
 * we don't know exactly when the data will be received, child controllers
 * (or components) must use .then() to get access to the data when it arrives.
 */
angular.module("courseInstanceShared").factory("CourseInstanceService",
function CourseInstanceService($q) {
	var currentCourse  = {};
	var courseDeferred = $q.defer();

	return {

		// Note: this should only be called by CourseHomeController,
		// since it will take care of loading the instance.
		setCurrentCourse: function setCurrentCourse(course) {
			currentCourse = course;
			courseDeferred.resolve(currentCourse);
		},

		// The same applies to this function, it will get called by
		// CourseHomeController when that controller is loaded.
		// If not, the child components would always get the first
		// course instance loaded in each session, and never any subsequent
		// instances, since the promise is resolved the first time
		// and can't be resolved (apparently). Anyway, the easiest
		// way around that is simply to create a new promise/deferred.
		resetCurrentCourse: function resetCurrentCourse() {
			currentCourse = {};
			courseDeferred = $q.defer();
		},

		// Child component/controllers should call this method to get
		// access to the current course, and NOT any of the others above!
		getCurrentCourse: function getCurrentCourse() {
			return courseDeferred.promise;
		}
	};
});
