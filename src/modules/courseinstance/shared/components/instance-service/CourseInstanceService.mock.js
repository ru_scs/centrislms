"use strict";

angular.module("courseInstanceShared")
.factory("MockCourseInstanceService", function MockCourseInstanceService() {
	return {
		mockCourseInstanceService: function mockCourseInstanceService(getCurrentCourse) {
			return {
				setCurrentCourse: function(course) {

				},
				resetCurrentCourse: function() {

				},
				getCurrentCourse: function() {
					return {
						then: function(fn) {
							var course;
							if (getCurrentCourse === 0) {
								course = {
									DateBegin: "01/01/2010",
									DateEnd: "09/09/2055"
								};
							} else if (getCurrentCourse === 1) {
								course = {
									DateBegin: "01/01/2010",
									DateEnd: "02/02/2010"
								};
							}
							fn(course);
							return {
								error: function(fn) {
									if (getCurrentCourse === 2) {
										fn();
									}
								}
							};
						}
					};
				}
			};
		}
	};
});
