"use strict";

describe("CourseInstanceService", function() {
	beforeEach(module("courseInstanceShared"));

	var service;
	var scope;

	var currentCourse = {
		ID: 24842,
		Name: "HR Starfsnám"
	};

	beforeEach(inject(function($rootScope, _CourseInstanceService_) {
		scope = $rootScope.$new();
		service = _CourseInstanceService_;
	}));

	it ("should define public functions", function() {
		expect(service.setCurrentCourse).toBeDefined();
		expect(service.resetCurrentCourse).toBeDefined();
		expect(service.getCurrentCourse).toBeDefined();
	});

	it ("should return a promise which will be resolved when the current course is set", function() {
		service.getCurrentCourse().then(function(course) {
			expect(course).toEqual(currentCourse);
		});

		service.setCurrentCourse(currentCourse);
		scope.$apply();
	});

	it ("should reset the current course correctly", function() {
		service.setCurrentCourse(currentCourse);
		service.resetCurrentCourse();
		service.getCurrentCourse().then(function(course) {
			expect(course).toEqual({});
		});
		scope.$apply();
	});
});