"use strict";

angular.module("courseInstanceShared").directive("studentCard",
function studentCard() {
	return {
		restrict: "E",
		scope: {
			student: "=",
			allowEdit:   "=",
			onRemove:    "&"
		},
		templateUrl: "courseinstance/shared/components/student-card/student-card.html",
		link: function(scope, element, attributes) {
			scope.internalOnRemove = function internalOnRemove() {
				if (scope.onRemove) {
					scope.onRemove({member: scope.student});
				}
			};
		}
	};
});
