# Course Instance shared

This module contains various shared components and files, which are used in
more than one module/app in a course instance, but are otherwise specific
for a course instance.