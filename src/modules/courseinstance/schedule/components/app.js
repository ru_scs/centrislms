"use strict";

angular.module("courseScheduleApp", [
	"pascalprecht.translate",
	"ui.router"
]).config(function ($stateProvider) {
	$stateProvider.state("courseinstance.courseschedule", {
		url:         "/schedule",
		templateUrl: "courseinstance/schedule/components/index/index.html",
		controller:  "CourseScheduleController"
	});
});
