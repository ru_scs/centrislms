"use strict";

angular.module("courseScheduleApp").factory("CourseScheduleResource",
function (CentrisResource, centrisNotify) {
	return {
		/**
		 * Get schedule for given course by date range
		 * @param formattedDateRange
		 * @returns Promise object
		 */
		getBookings: function(courseInstanceID, startDate, endDate) {
			var firstDate  = startDate.format("YYYY-MM-DDTHH:mm:ss");
			var secondDate = endDate.format("YYYY-MM-DDTHH:mm:ss");
			var range      = firstDate + "," + secondDate;

			var param = {
				courseInstanceID: courseInstanceID, range: range
			};

			return CentrisResource.get("courses", ":courseInstanceID/schedule", param);
		}
	};
});