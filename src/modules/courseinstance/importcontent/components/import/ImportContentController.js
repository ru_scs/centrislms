"use strict";

angular.module("importContentApp").controller("ImportContentController",
function ImportContentController($scope, $state, $location, $stateParams,
	CoursesHomeResource, ImportContentResource, toastr) {
	$scope.importableList = [];
	$scope.addedContent = [];
	$scope.courseInstanceID = $stateParams.courseInstanceID;

	// Only teacher allowed
	// Note : isTeacher is assigned by parent controller
	if (!$scope.isTeacher) {
		$location.path("/");
	}

	// Loads a "CourseTemplate" with all CourseInstances and importable content
	// in each course instance
	var loadCourses = function (courseID, courseInstanceID) {
		ImportContentResource.getOtherCourses(courseID, courseInstanceID)
		.success(function(data) {
			if (data.length !== 0) {
				// Create the "CourseTemplate"
				var course = {
					Name:            data[0].Name,
					NameEN:          data[0].NameEN,
					CourseInstances: []
				};
				// Make semester user friendly and add course instances to
				// course template
				for (var i = 0; i < data.length; i++) {
					var semester = "";
					semester += data[i].Semester.substring(0, 4);
					semester += "-";
					semester += data[i].Semester.substring(4, 5);
					data[i].Semester = semester;
					course.CourseInstances.push(data[i]);
				}
				$scope.importableList.push(course);
			}
		}).error(function() {
			// TODO: translate + centrisNotify
			toastr.error("Could not fetch courses");
		});
	};

	// If a user enters the url to import (not using navigation i sidebar), then $scope.courseInstance
	// will be undefined which will make loadcourse() to fail. So we need to get the courseInstance
	// if the $scope.courseInstance is undefined and then call loadCourses() otherwise loadCourses()
	if ($scope.courseInstance === undefined) {
		CoursesHomeResource.getCourseInstance($scope.courseInstanceID)
		.success(function (data) {
			$scope.courseInstance = data;
			loadCourses($scope.courseInstance.CourseID, $scope.courseInstanceID);
		});
	} else {
		loadCourses($scope.courseInstance.CourseID, $scope.courseInstanceID);
	}

	// Show/hide importables when clicked
	$scope.onCourseClicked = function onCourseClicked(course) {
		course.details = !course.details;
		course.active  = !course.active;
	};

	// Adds/removes objects to import list
	$scope.addContent = function(course, importCont, radioCheck) {
		// Create obj to add to addedContent List
		var obj = {
			Type:             importCont.Type,
			Name:             course.Name,
			NameEN:           course.NameEN,
			Semester:         course.Semester,
			CourseInstanceID: course.CourseInstanceID
		};

		if (importCont.radio) {
			// Add to addedContent list
			$scope.addedContent.push(obj);
		} else {
			// Remove from added content list
			for (var i = 0; i < $scope.addedContent.length; i++) {
				if ($scope.addedContent[i].Name                 === obj.Name &&
						$scope.addedContent[i].CourseInstanceID === obj.CourseInstanceID &&
						$scope.addedContent[i].Semester         === obj.Semester &&
						$scope.addedContent[i].Type             === obj.Type) {
					$scope.addedContent.splice(i, 1);
					break;
				}
			}
		}
	};

	// Import selected objects
	$scope.importObjects = function importObjects () {
		var importList = [];

		for (var i = 0; i < $scope.addedContent.length; i++) {
			var importObj = {
				CourseInstanceID: $scope.addedContent[i].CourseInstanceID,
				Type:             $scope.addedContent[i].Type
			};
			importList.push(importObj);
		}
		ImportContentResource.importObjects($scope.courseInstanceID, importList)
		.success(function(data) {
			// TODO: translate + centrisNotify
			toastr.success("Import successfull");
		})
		.error(function(data) {
			// TODO: translate + centrisNotify
			toastr.error("Import not successfull due to," + data);
		});
	};

	// This function calls loadCourse() for the course search
	$scope.selectCourse = function(asyncData) {
		loadCourses(asyncData.courseid, $scope.courseInstanceID);
	};

	// Loads course instance importable object when
	// semester is chosen from the dropdown
	$scope.update = function (course, courseInstance) {
		course.imports = courseInstance.ImportableContent;
		course.details = true;
		course.active  = true;
		course.selectedCourseInstance = courseInstance;
		var importables = courseInstance.ImportableContent;

		// Check if any imports are selected
		for (var i = 0; i < importables.length; i++) {
			for (var j = 0; j < $scope.addedContent.length; j++) {
				if (importables[i].Type === $scope.addedContent[j].Type &&
					courseInstance.CourseInstanceID === $scope.addedContent[j].CourseInstanceID) {
					importables[i].radio = true;
				}
			}
		}
	};
});
