"use strict";

angular.module("importContentApp")
.factory("ImportContentResource", function (CentrisResource) {
	/* Fyrirlestrar
		Verkefni
		Bækur
		Hæfniviðmið
		Námsmat
		Annað efni */
	return {
		getOtherCourses: function getOtherCourses(courseID, courseInstanceID) {
			var param = {
				courseID: courseID,
				courseInstanceToID: courseInstanceID
			};
			return CentrisResource.get("courses", ":courseID/import", param);
		},
		importObjects: function importObjects(courseInstanceID, model) {
			var param = {
				courseInstanceID: courseInstanceID
			};
			return CentrisResource.post("courses", ":courseInstanceID/import", param, model);
		}
	};

});