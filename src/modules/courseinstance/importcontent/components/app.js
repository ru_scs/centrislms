"use strict";

angular.module("importContentApp", [
	"pascalprecht.translate",
	"ui.router"
]).config(function ($stateProvider) {
	$stateProvider.state("courseinstance.importcontent", {
		url:         "/import",
		templateUrl: "courseinstance/importcontent/components/import/index.html",
		controller:  "ImportContentController"
	});
});