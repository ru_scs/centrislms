"use strict";

angular.module("onlineExamsApp").controller("TemplatesController",
function ($scope, $stateParams, $location, $window, QuestionService, ExamsResource, centrisNotify, WarningDlg) {

	$scope.isEditing      = false;

	var courseID   = $stateParams.courseInstanceID;
	var templateID = $stateParams.templateID;

	if (templateID) {
		$scope.loadingData = true;
		ExamsResource.getOnlineExamTemplateByID(courseID, templateID).success(function(data) {
			$scope.loadingData = false;
			$scope.template = {
				Title:               data.Title,
				Description:         data.Description,
				Questions:           data.Questions,
				RandomQuestionOrder: data.RandomQuestionOrder,
				RandomAnswersOrder:  data.RandomAnswersOrder
			};

			QuestionService.addClientProperties($scope.template.Questions);

			$scope.isEditing       = true;
		}).error(function() {
			$scope.loadingData = false;
			centrisNotify.error("onlineExamTemplates.ErrorLoadingTemplate");
		});
	} else {
		$scope.template = {
			Title:               "",
			Description:         "",
			Questions:           [],
			RandomAnswersOrder:  false,
			RandomQuestionOrder: false
		};
	}

	// Validation for template form
	// Checks if title/question array is empty
	$scope.validationFlagsForm = {
		templateTitleMissing: false,
		questionsMissing:     false
	};

	// Adds new questions to the questions array, at the correct index.
	$scope.addQuestion = function (type, index) {
		var questions = $scope.template.Questions;
		QuestionService.createRequestedQuestion(questions, type, index);
		var newObject = questions[index];
		if (newObject) {
			for (var i = 0; i < questions.length; ++i) {
				questions[i].hasFocus = false;
			}
			newObject.hasFocus = true;
		}
		$scope.validationFlagsForm.questionsMissing = false;
	};

	// If question template is clicked rather than dragged to a position it is added to the front of the template.
	$scope.addOnClick = function(type) {
		$scope.addQuestion(type, 0);
	};

	// Grabs dropped items and inserts in the right place
	// in the questions array. Takes both new questions
	// and complated ones (with data)
	$scope.onDrop = function ($data, index) {
		if ($data.operation === "Create") {
			$scope.addQuestion($data.questionType, index);
		}

		if ($data.operation === "Move") {
			var questions = $scope.template.Questions;
			QuestionService.moveRequestedQuestion(questions, $data.questionIndex, index);
		}
	};

	// Remove selected question from questions array
	$scope.deleteQuestion = function deleteQuestion(index) {
		// If user wants to delete the question this function is called.
		var delFunc = function() {
			$scope.template.Questions.splice(index, 1);
		};
		// Ask the user if s/he is sure s/he wants to delete the question.
		WarningDlg.show("onlineExamTemplates.ConfirmDeleteQuestion", "onlineExamTemplates.DeleteQuestionTitle")
		.then(delFunc);
	};

	// Add a new answer to a question
	$scope.addAnswerOptionToQuestion = function addAnswerOptionToQuestion(index) {
		var questionAddTo = $scope.template.Questions[index];
		QuestionService.addAnswerOption(questionAddTo);
	};

	// Removes selected answer from a question
	$scope.removeAnswerOptionFromQuestion = function removeAnswerOptionFromQuestion(questionIndex, answerIndex) {
		var questionRemoveFrom = $scope.template.Questions[questionIndex];
		QuestionService.removeAnswerOption(questionRemoveFrom, answerIndex);
	};

	// Calls validate, if successful it should move to next view
	$scope.saveTemplate = function () {
		$scope.compressAll();
		// Checks if template has a title
		if ($scope.template.Title === "" || $scope.template.Title === undefined) {
			$scope.validationFlagsForm.templateTitleMissing = true;
		} else {
			$scope.validationFlagsForm.templateTitleMissing = false;
		}

		// Checks if there are no questions in template
		// (else if) Then it goes through all questions in array and checks if questions
		// are valid
		if ($scope.template.Questions.length === 0) {
			$scope.validationFlagsForm.questionsMissing = true;
		} else if (QuestionService.questionValidation($scope.template.Questions) &&
			!($scope.validationFlagsForm.templateTitleMissing)) {

			// If save to API fails we keep a copy of template.

			// Slice the object before submitting to API
			for (var i = 0; i < $scope.template.Questions.length; i++) {
				var question = $scope.template.Questions[i];
				delete question.validationFlags;
				delete question.questionHidden;
				delete question.hasFocus;
				for (var j = 0; j < question.Answers.length; j++) {
					delete question.Answers[j].validationFlags;
				}
			}

			if ($scope.isEditing) {
				ExamsResource.editOnlineExamTemplate(courseID, templateID, $scope.template)
				.success(function(template) {
					centrisNotify.success("onlineExamTemplates.SuccessMessage");
					$location.path("/courses/" + courseID + "/onlineexams");
				}).error(function(template) {
					centrisNotify.error("onlineExamTemplates.ErrorMessage");
					$location.path();
				});
			} else {
				ExamsResource.addOnlineExamTemplate(courseID, $scope.template)
				.success(function(template) {
					centrisNotify.success("onlineExamTemplates.SuccessMessage");
					$location.path("/courses/" + courseID + "/onlineexams");
				}).error(function(template) {
					centrisNotify.error("onlineExamTemplates.ErrorMessage");
					$location.path();
				});
			}
		} else {
			// For better user experience:
			// In case of missing info, page scrolls to top and toastr message notifies
			// user that there are errors on page that he/she should check.
			$window.scrollTo(0,0);
			centrisNotify.error("onlineExamTemplates.InfoMissing");
		}
	};

	$scope.cancelTemplate = function() {
		// TODO... UX..are you sure?
		// If user wants to cancel the template this function is called.
		var cancelFunc = function() {
			$location.path("/courses/" + courseID + "/onlineexams");
		};
		// Ask the user if s/he is sure s/he wants to cancel the template.
		WarningDlg.show("onlineExamTemplates.ConfirmCancelTemplate", "onlineExamTemplates.CancelTemplateTitle")
		.then(cancelFunc);
	};

	// Shows/hides a specific question by changing boolean value on questionHidden
	$scope.toggleShow = function (question) {
		question.questionHidden = !question.questionHidden;
	};

	// Sets questionHidden = true for all questions
	$scope.compressAll = function () {
		for (var i = 0; i < $scope.template.Questions.length; i++) {
			$scope.template.Questions[i].questionHidden = true;
		}
	};

	// Sets questionHidden = false for all questions
	$scope.expandAll = function () {
		for (var i = 0; i < $scope.template.Questions.length; i++) {
			$scope.template.Questions[i].questionHidden = false;
		}
	};
});

// For correct scrolling up when dragging a question up.
// Needs to be outside controller for scope reasons.
// TODO: Get the functionality inside the controller.
function dragging(event) {
		var height = $(".allQuestions").height();
		var header = $(".templateInfo").height();
		var windowHeight = $(document).height();
		var dropArea = $(".dropArea").height();
		var top = $(window).scrollTop();
		// scrollLimit: where to start scrolling up. Calculated from the height of the div's on the page
		// and then 150 extra offset.
		var scrollLimit = windowHeight - height - header - dropArea - 150;
		// mouse pos: event.pageY needs to be offset by the amount of scroll that has occurred
		// to get correct mouse pos on the visible screen.
		if (event.pageY - top <= scrollLimit) {
			$(window).scrollTop($(window).scrollTop() - 1);
		}
	}
