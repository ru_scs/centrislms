"use strict";

describe("TemplatesController", function () {

	beforeEach(module("onlineExamsApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));

	var scope, ctrl, notify, resource, service;
	var createController;

	var mockStateParams = {
		courseInstanceID: 55442,
		templateID: null
	};

	var mockLocation = {
		path: function() {}
	};

	var mockWarningDlg = {
		show: function() {
			return {
				then: function(fn) {
					fn();
				}
			};
		}
	};

	function addQuestionTypeSingle(index) {
		var data = {operation: "Create", questionType: "SingleChoice"};
		scope.onDrop(data, index);
	}

	function addQuestionTypeMultiple(index) {
		var data = {operation: "Create", questionType: "MultipleChoice"};
		scope.onDrop(data, index);
	}

	function createValidTemplate() {
		scope.template.Title = "Smu";
		addQuestionTypeSingle();
		var question = scope.template.Questions[0];
		question.Text = "What?";
		question.Weight = 100;
		question.Answers[0].Text = "a";
		question.Answers[1].Text = "a";
		question.Answers[2].Text = "a";
		question.Answers[3].Text = "a";
		question.Answers[4].Text = "a";
		question.Answers[4].IsCorrect = true;
	}

	beforeEach(inject(function ($rootScope, $controller, QuestionService, mockExamsResource, mockCentrisNotify) {

		notify   = mockCentrisNotify;
		resource = mockExamsResource;
		service  = QuestionService;

		createController = function () {
			scope = $rootScope.$new();

			spyOn(mockLocation, "path").and.callThrough();
			spyOn(resource, "getOnlineExamTemplateByID").and.callThrough();
			spyOn(resource, "addOnlineExamTemplate").and.callThrough();
			spyOn(resource, "editOnlineExamTemplate").and.callThrough();
			spyOn(notify, "success").and.callThrough();
			spyOn(notify, "error").and.callThrough();
			spyOn(mockWarningDlg, "show").and.callThrough();

			ctrl = $controller("TemplatesController", {
				$scope:          scope,
				$stateParams:    mockStateParams,
				$location:       mockLocation,
				QuestionService: QuestionService,
				ExamsResource:   resource,
				centrisNotify:   mockCentrisNotify,
				WarningDlg:      mockWarningDlg
			});
		};
	}));

	describe("general behaviour", function() {
		beforeEach(function() {
			createController();
		});

		it ("should define a few scope variables and functions", function() {
			expect(scope.validationFlagsForm).toBeDefined();
			expect(scope.template).toBeDefined();

			expect(scope.onDrop).toBeDefined();
			expect(scope.deleteQuestion).toBeDefined();
			expect(scope.addAnswerOptionToQuestion).toBeDefined();
			expect(scope.removeAnswerOptionFromQuestion).toBeDefined();
			expect(scope.saveTemplate).toBeDefined();
			expect(scope.cancelTemplate).toBeDefined();
			expect(scope.toggleShow).toBeDefined();
			expect(scope.compressAll).toBeDefined();
			expect(scope.expandAll).toBeDefined();
		});

		it("should create a new question of type single", function() {
			expect(scope.template.Questions.length).toBe(0);
			addQuestionTypeSingle(0);

			// Was a question added to the questions array?
			expect(scope.template.Questions.length).toBe(1);
			// Was the correct question type added?
			expect(scope.template.Questions[0].Type).toBe(service.TYPE_SINGLE);
		});

		it ("should take the user to the frontpage when the user cancels the edit", function() {
			scope.cancelTemplate();
			expect(mockWarningDlg.show).toHaveBeenCalled();
		});

		it ("should hide all questions when requested", function() {
			addQuestionTypeSingle(0);
			addQuestionTypeSingle(1);
			addQuestionTypeSingle(2);
			scope.compressAll();

			for (var i = 0; i < scope.template.Questions.length; ++i) {
				expect(scope.template.Questions[i].questionHidden).toBe(true);
			}
		});

		it ("should show all questions when requested", function() {
			addQuestionTypeSingle(0);
			addQuestionTypeSingle(1);
			addQuestionTypeSingle(2);
			scope.expandAll();

			for (var i = 0; i < scope.template.Questions.length; ++i) {
				expect(scope.template.Questions[i].questionHidden).toBe(false);
			}
		});

		it ("should delete a question when requested", function() {
			addQuestionTypeSingle(0);
			addQuestionTypeSingle(1);
			addQuestionTypeMultiple(2);

			scope.deleteQuestion(1);
			expect(mockWarningDlg.show).toHaveBeenCalled();
		});

		it ("should add an answer option", function() {
			addQuestionTypeSingle(0);
			addQuestionTypeSingle(1);
			addQuestionTypeMultiple(2);

			scope.addAnswerOptionToQuestion(1);

			expect(scope.template.Questions[1].Answers.length).toBe(6);
		});

		it ("should remove an answer option", function() {
			addQuestionTypeSingle(0);
			addQuestionTypeSingle(1);
			addQuestionTypeMultiple(2);

			scope.removeAnswerOptionFromQuestion(0, 0);
			expect(scope.template.Questions[0].Answers.length).toBe(4);
		});

		it ("should correctly toggle the visibility of a single question", function() {
			var question = {
				questionHidden: true
			};
			scope.toggleShow(question);
			expect(question.questionHidden).toBe(false);
			scope.toggleShow(question);
			expect(question.questionHidden).toBe(true);
		});

		it("should move a question according to specific parameters", function() {

			expect(scope.template.Questions.length).toBe(0);

			addQuestionTypeSingle(0);
			addQuestionTypeMultiple(1);

			// The questions array should at this point contain two questions:
			expect(scope.template.Questions.length).toBe(2);

			// Now we will move question with ID = 1 (multiple choice) to index 0:
			var data = {operation: "Move", questionIndex: 1};
			var index = 0;
			scope.onDrop(data, index);

			// The number of elements should stay the same:
			expect(scope.template.Questions.length).toBe(2);

			// ...but the multi choice question should now be the first one:
			expect(scope.template.Questions[0].Type).toBe(service.TYPE_MULTIPLE);
		});

		it ("should not allow saving of a template with no title", function() {
			createValidTemplate();
			scope.template.Title = "";

			scope.saveTemplate();

			expect(resource.addOnlineExamTemplate).not.toHaveBeenCalled();
			expect(resource.editOnlineExamTemplate).not.toHaveBeenCalled();
		});

		it ("should not allow saving of a template with no questions", function() {
			scope.template.Title = "Smu";
			// Note: no questions added
			scope.saveTemplate();
			expect(resource.addOnlineExamTemplate).not.toHaveBeenCalled();
			expect(resource.editOnlineExamTemplate).not.toHaveBeenCalled();
		});

		it ("should not allow saving of a template where any question doesn't define the weight", function() {
			createValidTemplate();
			scope.template.Questions[0].Weight = 0;

			scope.saveTemplate();

			expect(resource.addOnlineExamTemplate).not.toHaveBeenCalled();
			expect(resource.editOnlineExamTemplate).not.toHaveBeenCalled();
		});

	});

	describe("when in create mode", function() {
		beforeEach(function() {
			createController();
		});

		it ("should NOT call any resource method at startup", function() {
			expect(resource.getOnlineExamTemplateByID).not.toHaveBeenCalled();
		});

		it ("should call resource add new method when saving", function() {
			createValidTemplate();
			scope.saveTemplate();
			expect(resource.addOnlineExamTemplate).toHaveBeenCalled();
		});
	});

	describe( "when in create mode and resource fails in saving", function() {
		beforeEach(function() {
			resource.successAddTemplate = false;
			createController();
		});

		it ("should notify user when saving fails", function() {
			createValidTemplate();
			scope.saveTemplate();
			expect(notify.error).toHaveBeenCalled();
		});
	});

	describe ("when in edit mode", function() {
		// The object our mock resource will return when requested
		// to fetch a given template by ID:
		var mockTemplate = {
			Title:              "Gaman",
			Description:         "Enn meira gaman",
			RandomQuestionOrder: true,
			RandomAnswersOrder:  true,
			Questions:           []
		};

		beforeEach(function() {
			resource.getTemplateReply  = mockTemplate;
			mockStateParams.templateID = 1;
		});

		describe ("and resource succeeds when saving", function() {
			beforeEach(function() {
				createController();
			});

			// Actually this should hold true wether saving succeeds or not...
			it ("should call resource method to fetch a given template", function() {
				expect(resource.getOnlineExamTemplateByID).toHaveBeenCalledWith(mockStateParams.courseInstanceID, 1);
			});

			it ("should try to save template after edit with valid data", function() {
				createValidTemplate();
				scope.saveTemplate();
				expect(resource.editOnlineExamTemplate).toHaveBeenCalled();
				expect(notify.success).toHaveBeenCalled();
			});
		});

		describe ("and resource fails when saving", function() {
			beforeEach(function() {
				resource.successEditTemplate = false;
				createController();
			});

			it ("should notify user when save fails", function() {
				createValidTemplate();
				scope.saveTemplate();
				expect(notify.error).toHaveBeenCalled();
			});
		});
	});
});
