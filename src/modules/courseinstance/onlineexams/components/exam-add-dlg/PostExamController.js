"use strict";

angular.module("onlineExamsApp").controller("PostExamController",
function ($scope, $stateParams, $location, modalParam, $translate, dateTimeService, ExamsResource, centrisNotify) {

	$scope.dateFormat       = dateTimeService.dateFormat();
	$scope.courseInstanceID = $stateParams.courseInstanceID;

	// Set the date as tomorrow for the default date of online exams.
	var today = moment();
	var tomorrow = today.add(1, "days");

	if (modalParam.start === undefined) {
		modalParam.start = moment();
	}
	if (modalParam.end === undefined) {
		modalParam.end = moment(tomorrow).hour(23).minute(59);
	}

	$scope.template = {
		TemplateID:          modalParam.id,
		Title:               modalParam.name,
		Description:         modalParam.description,
		StartDate:           modalParam.start.format($scope.dateFormat),
		EndDate:             modalParam.end.format($scope.dateFormat),
		fromTime:            modalParam.start.format("HH:mm"),
		toTime:              modalParam.end.format("HH:mm"),
		MinutesAllowed:      null,
		RandomQuestions:     false,
		AssessmentID:        undefined,
		Ratio:               undefined,
		GradePublishType:    1,
		SolutionPublishType: 2
	};

	$scope.serveExam = function() {

		// Check if timeLimited has been checked and then unchecked.
		if (!$scope.template.timeLimited) {
			$scope.template.MinutesAllowed = null;
		}

		var courseID = $stateParams.courseInstanceID;

		if (!$scope.validateTemplate($scope.template)) {
			// post to API.
			var exam = $scope.postExamObj($scope.template);
			ExamsResource.postOnlineExam(courseID, exam)
			.success(function(exam) {
				centrisNotify.success("postOnlineExam.SuccessMessage");
				$scope.$close(exam);
			}).error(function(exam) {
				$scope.$dismiss();
				centrisNotify.error("postOnlineExam.ErrorMessage");
			});
		}
	};

	$scope.postExamObj = function (template) {

		var mergeStart = dateTimeService.mergeDateAndTime(template.StartDate, template.fromTime);
		var mergeEnd   = dateTimeService.mergeDateAndTime(template.EndDate, template.toTime);

		var exam = {
			TemplateID:          template.TemplateID,
			Title:               template.Title,
			Description:         template.Description,
			StartDate:           dateTimeService.momentToISO(mergeStart),
			EndDate:             dateTimeService.momentToISO(mergeEnd),
			MinutesAllowed:      template.MinutesAllowed,
			RandomQuestions:     template.RandomQuestions,
			AssessmentID:        template.AssessmentID,
			GradePublishType:    template.GradePublishType,
			SolutionPublishType: template.SolutionPublishType
		};
		return exam;
	};

	$scope.validateTemplate = function (template) {
		$scope.showError = false;

		if (template.StartDate === undefined ||
			template.EndDate === undefined ||
			template.GradePublishType === undefined ||
			template.SolutionPublishType === undefined) {
			$scope.showError = true;
		}
		return $scope.showError;
	};
});