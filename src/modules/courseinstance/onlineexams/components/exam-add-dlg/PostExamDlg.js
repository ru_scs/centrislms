"use strict";

angular.module("onlineExamsApp").factory("PostExamDlg",
function PostExamDlg($translate, $uibModal) {
	return {
		show: function show(template) {
			var modalInstance = $uibModal.open({
				controller: "PostExamController",
				templateUrl:"courseinstance/onlineexams/components/exam-add-dlg/post-exam-dialog.html",
				resolve: {
					modalParam: function () {
						return {
							id:            template.ID,
							name:          template.Name,
							description:   template.Description,
							start:         template.StartDate,
							end:           template.EndDate
						};
					}
				}
			});
			return modalInstance.result;
		}
	};
});
