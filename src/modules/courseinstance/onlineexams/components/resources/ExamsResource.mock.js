"use strict";

angular.module("onlineExamsApp").factory("mockExamsResource",
function mockExamsResource(MockHelpers) {

	var mock = {
		successGetInstanceList: true,
		successGetTemplateList: true,
		successAddTemplate: true,
		successAddInstance: true,
		successEditTemplate: true,
		successDeleteTemplate: true,
		successGetTemplateByID: true,
		getReply: {},
		getInstanceListReply: [],
		getTemplateReply: {},
		getTemplateListReply: {},
		addTemplateReply: {},
		editTemplateReply: {},
		deleteTemplateReply: {},
		addInstanceReply: {},

		getOnlineExams: function(ID) {
			return MockHelpers.mockHttpPromise(mock.getInstanceListReply, mock.successGetInstanceList);
		},
		getOnlineExamTemplates: function(ID) {
			return MockHelpers.mockHttpPromise(mock.getTemplateListReply, mock.successGetTemplateList);
		},
		postOnlineExam: function(ID, exam) {
			return MockHelpers.mockHttpPromise(mock.addInstanceReply, mock.successAddInstance);
		},
		addOnlineExamTemplate: function(ID, template) {
			return MockHelpers.mockHttpPromise(mock.addTemplateReply, mock.successAddTemplate);
		},
		editOnlineExamTemplate: function(ID, templateID, template) {
			return MockHelpers.mockHttpPromise(mock.editTemplateReply, mock.successEditTemplate);
		},
		deleteOnlineExamTemplate: function(ID, templateID) {
			return MockHelpers.mockHttpPromise(mock.deleteTemplateReply, mock.successDeleteTemplate);
		},
		getOnlineExamTemplateByID: function(courseInstanceID, templateID) {
			return MockHelpers.mockHttpPromise(mock.getTemplateReply, mock.successGetTemplateByID);
		}
	};

	return mock;
});
