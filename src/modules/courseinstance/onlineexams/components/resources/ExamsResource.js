"use strict";

angular.module("onlineExamsApp").factory("ExamsResource",
function ExamsResource(CentrisResource) {
	return {
		getOnlineExams: function(ID) {
			return CentrisResource.get("courses/:id","onlineexams", {id:ID});
		},
		getOnlineExamTemplates: function(ID) {
			return CentrisResource.get("courses/:id", "onlineexamtemplates", {id:ID});
		},
		postOnlineExam: function(ID, exam) {
			return CentrisResource.post("courses", ":id/onlineexams" ,{id:ID}, exam);
		},
		addOnlineExamTemplate: function(ID, template) {
			return CentrisResource.post("courses", ":id/onlineexamtemplates", {id:ID}, template);
		},
		editOnlineExamTemplate: function(ID, templateID, template) {
			var param = {
				id:         ID,
				templateID: templateID
			};
			return CentrisResource.put("courses", ":id/onlineexamtemplates/:templateID", param, template);
		},
		deleteOnlineExamTemplate: function(ID, templateID) {
			var param = {
				id:         ID,
				templateID: templateID
			};
			return CentrisResource.remove("courses", ":id/onlineexamtemplates/:templateID", param);
		},
		getOnlineExamTemplateByID: function(courseInstanceID, templateID) {
			var param = {
				id:         courseInstanceID,
				templateID: templateID
			};
			return CentrisResource.get("courses", ":id/onlineexamtemplates/:templateID", param);
		}
	};
});
