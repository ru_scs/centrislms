"use strict";

angular.module("onlineExamsApp").factory("mockExamDetailsResource",
function mockExamDetailsResource(MockHelpers) {
	var mock = {
		successGetDetails: true,
		getDetailsReply: {},
		successGetStudentGrade: true,
		getStudentGradeReply: {},
		getOnlineExamDetails: function getOnlineExamDetails(ID, examid) {
			return MockHelpers.mockHttpPromise(mock.getDetailsReply, mock.successGetDetails);
		},
		saveStudentSolution: function saveStudentSolution(ID, examid, data) {
		},
		turnInStudentSolution: function turnInStudentSolution(ID, examid, data) {
		},
		getOnlineExamResults: function getOnlineExamResults(ID, examid) {
		},
		getStudentAnswersToQuestion: function getStudentAnswersToQuestion(ID, examid, qID) {
		},
		gradeStudentAnswer: function gradeStudentAnswer(ID, examID, qID, SSN, data) {
		},
		addCorrectMultipleAnswer: function addCorrectMultipleAnswer(ID, examID, qID, data) {
		},
		getOnlineExamByStudent: function getOnlineExamByStudent(courseInstanceID, examid, SSN) {
		}
	};

	return mock;
});