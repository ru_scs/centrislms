"use strict";

angular.module("onlineExamsApp").factory("ExamDetailsResource",
function ExamDetailsResource(CentrisResource) {
	return {
		getOnlineExamDetails: function getOnlineExamDetails(ID, examid) {
			return CentrisResource.get("courses",":id/onlineexams/:examid", { id: ID, examid: examid });
		},
		saveStudentSolution: function saveStudentSolution(ID, examid, data) {
			return CentrisResource.post("courses", ":id/onlineexams/:examid/solutions", { id: ID, examid: examid }, data);
		},
		turnInStudentSolution: function turnInStudentSolution(ID, examid, data) {
			return CentrisResource.post("courses", ":id/onlineexams/:examid/finalsolutions", { id: ID, examid: examid }, data);
		},
		getOnlineExamResults: function getOnlineExamResults(ID, examid) {
			return CentrisResource.get("courses", ":id/onlineexams/:examid/results",  { id: ID, examid: examid });
		},
		getStudentAnswersToQuestion: function getStudentAnswersToQuestion(ID, examid, qID) {
			var param = {
				id: ID,
				examid: examid,
				qid: qID
			};
			return CentrisResource.get("courses", ":id/onlineexams/:examid/studentanswers/:qid",  param);
		},
		gradeStudentAnswer: function gradeStudentAnswer(ID, examID, qID, SSN, data) {
			var param = {
				id: ID,
				examid: examID,
				qid: qID,
				ssn: SSN
			};
			return CentrisResource.post("courses", ":id/onlineexams/:examid/studentanswer/:qid/:ssn", param, data);
		},
		addCorrectMultipleAnswer: function addCorrectMultipleAnswer(ID, examID, qID, data) {
			var param = {
				id: ID,
				examid: examID,
				qid: qID,
			};
			return CentrisResource.post("courses", ":id/onlineexams/:examid/studentanswers/:qid", param, data);
		},
		getOnlineExamByStudent: function getOnlineExamByStudent(courseInstanceID, examid, SSN) {
			var param = {
				id:     courseInstanceID,
				examid: examid,
				SSN:    SSN
			};
			return CentrisResource.get("courses/:id","onlineexams/:examid/examanswers/:SSN", param);
		}
	};
});
