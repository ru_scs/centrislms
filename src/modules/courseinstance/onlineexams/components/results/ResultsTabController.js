"use strict";

/**
 * This controller takes care of displaying details about a particular exam,
 * from the perspective of a course teacher.
 */
angular.module("onlineExamsApp").controller("OnlineExamsTabController",
function OnlineExamsTabController($scope, $state, $stateParams, $translate, centrisNotify, ExamDetailsResource) {

	$scope.result = [];
	$scope.loadingData = true;
	ExamDetailsResource.getOnlineExamResults($stateParams.courseInstanceID, $stateParams.examid)
	.success(function (data) {
		$scope.results     = data;
		$scope.loadingData = false;
	}).error(function () {
		$scope.loadingData = false;
		centrisNotify.error("onlineExamsResults.Messages.NoResultsToDisplay");
	});

	$scope.tabs = {
		list: [{
			heading: "onlineExamsResults.Results.StudentList",
			route:   "courseinstance.onlineexams.results.studentlist",
			active:  false
		}, {
			heading: "onlineExamsResults.Results.Statistics",
			route:   "courseinstance.onlineexams.results.statistics",
			active:  false
		}],
		// Broadcasts the current tab route
		broadcast: function broadcast(route) {
			$scope.$broadcast(route);
		},
		// Changes the current route
		go: function go(route) {
			$state.go(route);
		},
		active: function active(route) {
			return $state.is(route);
		}
	};

	// Listening after state change event
	$scope.$on("$stateChangeSuccess", function stateChangeSuccess() {
		$scope.tabs.list.forEach(function forEachTab(tab) {
			tab.active = $state.is(tab.route);
		});
	});
});