"use strict";

angular.module("onlineExamsApp").controller("StudentListController",
function StudentListController($scope, $stateParams) {
	// variable to get the courseInstanceID for exam profile
	$scope.courseInstanceID = $stateParams.courseInstanceID;
});