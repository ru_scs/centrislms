"use strict";

angular.module("onlineExamsApp").directive("onlineexamsResultsStudentlist",
function onlineexamsResultsStudentlist() {
	return {
		restrict: "E",
		scope: {
			submissions: "=ngModel",
			examname: "=",
			examid: "=",
		},
		templateUrl: "courseinstance/onlineexams/components/results/studentlist/studentlist.html",
		link: function (scope, element, attributes) {
		}
	};
});