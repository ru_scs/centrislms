"use strict";

angular.module("onlineExamsApp").directive("onlineexamsStatistics",
function onlineexamsStatistics() {
	return {
		restrict: "E",
		scope: {
			answers: "=ngModel",
		},
		controller: "StatisticsTabController",
		templateUrl: "courseinstance/onlineexams/components/results/statistics/statistics.html",
		link: function (scope, element, attributes) {
		}
	};
});