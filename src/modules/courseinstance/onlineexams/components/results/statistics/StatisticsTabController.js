"use strict";

/**
 * This controller takes care of displaying statistical information
 * about the results of a given onli
 */
angular.module("onlineExamsApp").controller("StatisticsTabController",
function StatisticsTabController($scope, $stateParams, $translate) {

	$scope.courseInstanceID = $stateParams.courseInstanceID;
	$scope.examid = $stateParams.examid;
	$scope.$watch("answers", function (newValue, oldValur) {
		if (newValue) {
			var question;
			var arrValues = [];
			var arrLabels = [];
			for (var i = 0; i < newValue.length; i++) {
				question = newValue[i];
				arrValues = [];
				arrLabels = [];
				var c = "A";
				for (var j = 0; j < question.Answers.length; j++) {
					var answer = question.Answers[j];
					arrValues.push(answer.Count);
					arrLabels.push($translate.instant("onlineexamStatistics.answer") + " " + String.fromCharCode(c.charCodeAt(0) + j));
				}
				question.values = [arrValues];
				question.labels = arrLabels;
			}
		}
	});
});
