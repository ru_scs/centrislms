"use strict";

describe("QuestionService", function() {

	beforeEach(module("onlineExamsApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));

	var service;
	var questionsArray;

	beforeEach(inject(function(QuestionService) {
		service = QuestionService;
		questionsArray = [];
	}));

	it("should check if the factory is defined and not NULL", function() {
		expect(service).toBeDefined();
		expect(service).not.toBeNull();
	});

	it("should add a new answer to a specific question", function() {
		// Setting up the test - pushing two questions into the array.
		questionsArray.push(service.createSingleChoiceQuestion());
		questionsArray.push(service.createMultipleChoiceQuestion());

		// Adding an answer to a specific question - double check if addded to correct question:
		expect(questionsArray[0].Answers.length).toBe(5);
		expect(questionsArray[1].Answers.length).toBe(5);
		service.addAnswerOption(questionsArray[0]);
		service.addAnswerOption(questionsArray[0]);
		expect(questionsArray[0].Answers.length).toBe(7);
		expect(questionsArray[1].Answers.length).toBe(5);
	});

	it("should remove a specific answer from a specific question", function() {
		// Setting up the test - pushing three different questions into the array.
		questionsArray.push(service.createSingleChoiceQuestion());
		questionsArray.push(service.createSingleChoiceQuestion());
		questionsArray.push(service.createShortTextQuestion());
		// Modifying the answers:
		questionsArray[1].Answers[0].Text = "alpha";
		questionsArray[1].Answers[1].Text = "beta";
		questionsArray[1].Answers[2].Text = "gamma";
		questionsArray[1].Answers[3].Text = "delta";
		questionsArray[1].Answers[3].Text = "epsilon";

		// Now we remove the answer with Text = "gamma" and check results
		var questionID = 1;
		var answerIndex = 2;
		expect(questionsArray[1].Answers.length).toBe(5);
		expect(questionsArray[1].Answers[2].Text).toBe("gamma");
		service.removeAnswerOption(questionsArray[1], answerIndex);
		expect(questionsArray[1].Answers.length).toBe(4);
		expect(questionsArray[1].Answers[2].Text).not.toBe("gamma");
	});

	it("should insert a specific question type (single choice) into questionsArray", function() {
		var questionType = "SingleChoice";
		var index = 0;

		expect(questionsArray.length).toBe(0);
		service.createRequestedQuestion(questionsArray, questionType, index);
		expect(questionsArray.length).toBe(1);
		expect(questionsArray[0].Type).toBe(1);
	});

	it("should insert a specific question type (multiple choice) into questionsArray", function() {
		var questionType = "MultipleChoice";
		var index = 0;

		expect(questionsArray.length).toBe(0);
		service.createRequestedQuestion(questionsArray, questionType, index);
		expect(questionsArray.length).toBe(1);
		expect(questionsArray[0].Type).toBe(2);
	});

	it ("should never leave a single or multiple question with fewer than 2 answer options", function() {
		questionsArray.push(service.createSingleChoiceQuestion());
		questionsArray.push(service.createMultipleChoiceQuestion());
		expect(questionsArray[0].Answers.length).toBe(5);
		service.removeAnswerOption(questionsArray[0], 4);
		service.removeAnswerOption(questionsArray[0], 3);
		service.removeAnswerOption(questionsArray[0], 2);
		service.removeAnswerOption(questionsArray[0], 1);
		expect(questionsArray[0].Answers.length).toBe(2);

		expect(questionsArray[1].Answers.length).toBe(5);
		service.removeAnswerOption(questionsArray[1], 1);
		service.removeAnswerOption(questionsArray[1], 1);
		service.removeAnswerOption(questionsArray[1], 1);
		service.removeAnswerOption(questionsArray[1], 1);
		expect(questionsArray[1].Answers.length).toBe(2);
	});

	it("should insert a specific question type (short text) into questionsArray", function() {
		var questionType = "ShortText";
		var index = 0;

		expect(questionsArray.length).toBe(0);
		service.createRequestedQuestion(questionsArray, questionType, index);
		expect(questionsArray.length).toBe(1);
		expect(questionsArray[0].Type).toBe(3);
	});

	it("should insert a specific question type (essay) into questionsArray", function() {
		var questionType = "Essay";
		var index = 0;

		expect(questionsArray.length).toBe(0);
		service.createRequestedQuestion(questionsArray, questionType, index);
		expect(questionsArray.length).toBe(1);
		expect(questionsArray[0].Type).toBe(4);
	});

	it("should move a specific question in the questionsArray to a new index", function() {
		// Setting up the test - pushing three different questions into the array.
		questionsArray.push(service.createSingleChoiceQuestion());
		questionsArray.push(service.createMultipleChoiceQuestion());
		questionsArray.push(service.createShortTextQuestion());

		// Now we will move question with ID = 0 to index = 2
		// Currently: Question with ID = 0 (SingleChoice) is at index = 0
		// After transaction: Question with ID = 0 (SingleChoice) is at index 2
		var questionIndex = 0;
		var newIndex = 2;
		expect(questionsArray.length).toBe(3);
		expect(questionsArray[0].Type).toBe(1);
		expect(questionsArray[1].Type).toBe(2);
		expect(questionsArray[2].Type).toBe(3);
		service.moveRequestedQuestion(questionsArray, questionIndex, newIndex);
		expect(questionsArray[0].Type).toBe(2);
		expect(questionsArray[1].Type).toBe(3);
		expect(questionsArray[2].Type).toBe(1);
	});

	it("should validate if all required fields are filled out", function() {
		// Setting up the test - pushing two different question types into the array.
		// Don't need to specifically test with "Short Text" or "Essay" questions.
		questionsArray.push(service.createSingleChoiceQuestion());
		questionsArray.push(service.createMultipleChoiceQuestion());

		// Single choice question (question 1 of 2)
		questionsArray[0].Text = "This is a single choice question";
		questionsArray[0].Weight = 100;
		for (var i = 0; i < questionsArray[0].Answers.length; i++) {
			questionsArray[0].Answers[i].Text = "This is answer " + i;
		}
		questionsArray[0].Answers[4].IsCorrect = true;

		// Multiple choice question (question 2 of 2)
		questionsArray[1].Text = "This is a multiple choice question";
		questionsArray[1].Weight = 100;
		service.addAnswerOption(questionsArray[1]);
		for (var j = 0; j < questionsArray[1].Answers.length; j++) {
			questionsArray[1].Answers[j].Text = "This is answer " + j;
		}
		questionsArray[1].Answers[0].IsCorrect = true;
		questionsArray[1].Answers[1].IsCorrect = true;

		expect(service.questionValidation(questionsArray)).toBe(true);
	});

	it("should NOT validate if a question doesn't have text", function() {
		// Setting up the test - pushing a single choice question into the array.
		questionsArray.push(service.createSingleChoiceQuestion());

		// Single choice question
		questionsArray[0].Text = ""; // Empty on purpose, should not validate
		// Ensure validation of the answer options won't interfere with the
		// validation of the question
		for (var i = 0; i < (questionsArray[0].Answers.length); i++) {
			questionsArray[0].Answers[i].Text = "this is answer " + i;
		}
		questionsArray[0].Answers[0].IsCorrect = true;

		expect(service.questionValidation(questionsArray)).toBe(false);
		// TODO: expect that the correct validation property has been set
	});

	it ("should NOT validate if some question doesn't define the weight", function() {
		questionsArray.push(service.createSingleChoiceQuestion());
		questionsArray[0].Text = "Smuuu";
		questionsArray[0].Weight = 0; // Empty on purpose
		// Ensure validation of the answer options won't interfere with the
		// validation of the question
		for (var i = 0; i < (questionsArray[0].Answers.length); i++) {
			questionsArray[0].Answers[i].Text = "this is answer " + i;
		}
		questionsArray[0].Answers[0].IsCorrect = true;
		expect(service.questionValidation(questionsArray)).toBe(false);
	});

	it("should NOT validate if no answer option is marked as true in a single choice question", function() {
		// Setting up the test - pushing a single choice question into the array.
		questionsArray.push(service.createSingleChoiceQuestion());

		// Single choice question
		questionsArray[0].Text = "A single choice question";
		for (var i = 0; i < (questionsArray[0].Answers.length); i++) {
			questionsArray[0].Answers[i].Text = "this is answer " + i;
		}
		// Note: no Answer has the Correct property set to true

		expect(service.questionValidation(questionsArray)).toBe(false);
		// TODO: expect that the correct validation property has been set
	});

	it("should NOT validate if some answer option is missing the text in a single choice question", function() {
		// Setting up the test - pushing a single choice question into the array.
		questionsArray.push(service.createSingleChoiceQuestion());

		// Single choice question
		questionsArray[0].Text = "A single choice question";
		// This loop will only input answer texts to the first four answers
		for (var i = 0; i < (questionsArray[0].Answers.length - 1); i++) {
			questionsArray[0].Answers[i].Text = "this is answer " + i;
		}
		questionsArray[0].Answers[0].IsCorrect = true;

		expect(service.questionValidation(questionsArray)).toBe(false);
		// TODO: expect that the correct validation property has been set
	});

	it("should NOT validate if no answer option is marked as true in a multiple choice question", function() {
		// Setting up the test - pushing a single choice question into the array.
		questionsArray.push(service.createMultipleChoiceQuestion());

		// Single choice question
		questionsArray[0].Text = "A single choice question";
		for (var i = 0; i < (questionsArray[0].Answers.length); i++) {
			questionsArray[0].Answers[i].Text = "this is answer " + i;
		}
		// Note: no Answer has the Correct property set to true

		expect(service.questionValidation(questionsArray)).toBe(false);
		// TODO: expect that the correct validation property has been set
	});

	it("should NOT validate if some answer option is missing the text in a multiple choice question", function() {
		// Setting up the test - pushing a single choice question into the array.
		questionsArray.push(service.createMultipleChoiceQuestion());

		// Single choice question
		questionsArray[0].Text = "A single choice question";
		// This loop will only input answer texts to the first four answers
		for (var i = 0; i < (questionsArray[0].Answers.length - 1); i++) {
			questionsArray[0].Answers[i].Text = "this is answer " + i;
		}
		questionsArray[0].Answers[0].IsCorrect = true;

		expect(service.questionValidation(questionsArray)).toBe(false);
		// TODO: expect that the correct validation property has been set
	});

});
