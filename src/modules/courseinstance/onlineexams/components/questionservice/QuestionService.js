"use strict";

angular.module("onlineExamsApp").factory("QuestionService",
function QuestionService() {

	var questionService = {};

	// Note: these constants should match those defined
	// in the API.
	questionService.TYPE_SINGLE   = 1;
	questionService.TYPE_MULTIPLE = 2;
	questionService.TYPE_TEXT     = 3;
	questionService.TYPE_ESSAY    = 4;

	questionService.addClientPropertiesToAnswerOption = function(answer) {
		answer.validationFlags = {
			questionAnswerTextMissing: false
		};
	};

	questionService.addClientPropertiesToQuestion = function(question) {
		question.questionHidden = false;
		question.validationFlags = {
			questionsTextMissing:   false,
			weightMissing: false,
			questionCorrectMissing: false
		};

		for (var j = 0; j < question.Answers.length; ++j) {
			var answer = question.Answers[j];
			questionService.addClientPropertiesToAnswerOption(answer);
		}
	};

	questionService.addClientProperties = function addClientProperties(questionArray) {
		for (var i = 0; i < questionArray.length; ++i) {
			var question = questionArray[i];
			questionService.addClientPropertiesToQuestion(question);
		}
	};

	// Creates a new answer object and adds it to the parameter question object
	questionService.addAnswerOption = function addAnswerOption(questionObj) {
		var answerObject = {
			Text:      "",
			IsCorrect: false
		};
		questionObj.Answers.push(answerObject);

		questionService.addClientPropertiesToAnswerOption(answerObject);
	};

	// Removes a specific answer from the parameter question object
	questionService.removeAnswerOption = function removeAnswerOption(questionObj, answerToRemove) {
		if (questionObj.Answers.length > 2) {
			questionObj.Answers.splice(answerToRemove, 1);
		}
	};

	questionService.createSingleChoiceQuestion = function () {
		var question = {
			Type:    questionService.TYPE_SINGLE,
			Text:    "",
			Hint:    "",
			Answers: [],
			Weight:  0
		};

		questionService.addClientPropertiesToQuestion(question);

		// Adding the default and minimum number of answers to the question:
		questionService.addAnswerOption(question);
		questionService.addAnswerOption(question);
		questionService.addAnswerOption(question);
		questionService.addAnswerOption(question);
		questionService.addAnswerOption(question);

		return question;
	};

	questionService.createMultipleChoiceQuestion = function () {
		var question = {
			Type:    questionService.TYPE_MULTIPLE,
			Text:    "",
			Hint:    "",
			Answers: [],
			Weight:  0
		};

		questionService.addClientPropertiesToQuestion(question);

		// Adding the default and minimum number of answers to the question:
		questionService.addAnswerOption(question);
		questionService.addAnswerOption(question);
		questionService.addAnswerOption(question);
		questionService.addAnswerOption(question);
		questionService.addAnswerOption(question);

		return question;
	};

	questionService.createShortTextQuestion = function () {
		var question = {
			Type:    questionService.TYPE_TEXT,
			Text:    "",
			Hint:    "",
			Answers: [],
			Weight:  0
		};

		questionService.addClientPropertiesToQuestion(question);

		return question;
	};

	questionService.createEssayQuestion = function() {
		var question = {
			Type:    questionService.TYPE_ESSAY,
			Text:    "",
			Hint:    "",
			Answers: [],
			Weight:  0
		};
		questionService.addClientPropertiesToQuestion(question);

		return question;
	};

	// This is a wrapper function for creating any question object.
	// The user will drag a particular question object to a droppable element and this
	// function will be called to create the dragged question.
	questionService.createRequestedQuestion = function(questionsArray, questionType, index) {
		var questionObject = null;
		if (questionType === "SingleChoice") {
			questionObject = questionService.createSingleChoiceQuestion();
		} else if (questionType === "MultipleChoice") {
			questionObject = questionService.createMultipleChoiceQuestion();
		} else if (questionType === "ShortText") {
			questionObject = questionService.createShortTextQuestion();
		} else if (questionType === "Essay") {
			questionObject = questionService.createEssayQuestion();
		}
		if (questionObject !== null) {
			questionsArray.splice(index, 0, questionObject);
		}
	};

	// This is the "sortable" feature of the app.
	// The user is able to "sort" questions (move them from one index to another).
	questionService.moveRequestedQuestion = function(questionsArray, questionIndex, newIndex) {
		// Remove exactly one item from the original position:
		var questionsToMove = questionsArray.splice(questionIndex, 1);

		// ... and add that item back to the new index, without
		// removing any item (hence 0 as the second parameter):
		questionsArray.splice(newIndex, 0, questionsToMove[0]);
	};

	// This function takes care of validating the questions
	// and (optionally) their answer options.
	questionService.questionValidation = function(questionsArray) {
		var validated = true;
		for (var i = 0; i < questionsArray.length; i++) {
			var question = questionsArray[i];
			// The "Text" property is common among all the questions
			if (question.Text === "") {
				question.validationFlags.questionsTextMissing = true;
				validated = false;
				question.questionHidden = false;
			} else {
				question.validationFlags.questionsTextMissing = false;
			}

			if (question.Weight === 0 || question.Weight === "0" ||
				question.Weight === null || question.Weight === undefined) {
				question.validationFlags.weightMissing = true;
				validated = false;
				question.questionHidden = false;
			} else {
				question.validationFlags.weightMissing = false;
			}

			// Validating variables specific to single choice questions
			if (question.Type === questionService.TYPE_SINGLE) {

				// All the answers must have text
				var hasCorrectAnswer = false;
				for (var j = 0; j < question.Answers.length; j++) {
					if (question.Answers[j].Text === "") {
						question.Answers[j].validationFlags.questionAnswerTextMissing = true;
						validated = false;
						question.questionHidden = false;
					} else {
						question.Answers[j].validationFlags.questionAnswerTextMissing = false;
					}

					if (question.Answers[j].IsCorrect) {
						hasCorrectAnswer = true;
					}
				}

				// An answer must be marked as the correct one
				if (!hasCorrectAnswer) {
					question.validationFlags.questionCorrectMissing = true;
					validated = false;
					question.questionHidden = false;
				} else {
					question.validationFlags.questionCorrectMissing = false;
				}
			}

			// Validating variables specific to multiple choice questions
			if (question.Type === questionService.TYPE_MULTIPLE) {

				// All the answers must have text
				var correctAnswerCount = 0;
				for (var k = 0; k < questionsArray[i].Answers.length; k++) {
					if (questionsArray[i].Answers[k].Text === "") {
						questionsArray[i].Answers[k].validationFlags.questionAnswerTextMissing = true;
						validated = false;
						questionsArray[i].questionHidden = false;
					} else {
						questionsArray[i].Answers[k].validationFlags.questionAnswerTextMissing = false;
					}

					// Checking if there is atleast one answer correct
					if (questionsArray[i].Answers[k].IsCorrect) {
						correctAnswerCount++;
					}
				}

				if (correctAnswerCount === 0) {
					questionsArray[i].validationFlags.questionCorrectMissing = true;
					validated = false;
					questionsArray[i].questionHidden = false;
				} else {
					questionsArray[i].validationFlags.questionCorrectMissing = false;
				}
			}
		}
		return validated;
	};

	return questionService;
});
