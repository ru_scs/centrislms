"use strict";

angular.module("onlineExamsApp").directive("questionEditor", function() {
	return {
		restrict: "E",
		scope: {
			title:    "=ngModel",
			isValid:  "="
		},
		templateUrl: "courseinstance/onlineexams/components/question-editor/question-editor-directive.tpl.html"
	};
});
