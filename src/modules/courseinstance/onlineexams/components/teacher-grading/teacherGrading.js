"use strict";

angular.module("onlineExamsApp").directive("teacherGrading",
function teacherGrading() {
	return {
		restrict: "E",
		templateUrl: "courseinstance/onlineexams/components/teacher-grading/teacher-grading.html",
		// The scope object has the following properties:
		// * obj
		//   An object which has an "Answer" property, which itself contains
		//   the properties StudentGrade and TeacherComment, and these
		//   properties will be bound to the input fields in this directive.
		// * onSave
		//   A method which will be called when the user wants to save the
		//   teacher grade. The "obj" parameter will be passed to the method.
		// * weight
		//   How much the question weights, as a number from 1 to 100.
		scope: {
			obj:    "=",
			onSave: "&",
			weight: "=" // Actually we only use one-way data binding, i.e. to display the weight
		},
		link: function (scope, element, attributes) {
			scope.onSaveGrade = function onSaveGrade() {
				if (scope.onSave) {
					scope.onSave({obj: scope.obj});
				}
			};
		}
	};
});