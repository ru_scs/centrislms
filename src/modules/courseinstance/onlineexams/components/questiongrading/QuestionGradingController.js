"use strict";

angular.module("onlineExamsApp").controller("QuestionGradingController",
function QuestionGradingController($scope, $stateParams, ExamDetailsResource, $translate, centrisNotify) {

	$scope.getStudentAnswers = function getStudentAnswers() {
		$scope.loadingData = true;
		ExamDetailsResource.getStudentAnswersToQuestion($stateParams.courseInstanceID,
			$stateParams.examID,
			$stateParams.questionID).success(function(data) {
				$scope.loadingData = false;
				$scope.studentanswers = data;
				for (var i = 0; i < data.StudentAnswers.length; i++) {
					for (var k = 0; k < data.Question.AnswerOptions.length; k++) {
						if (data.StudentAnswers[i].Answer.SelectedOptions[0] ===  data.Question.AnswerOptions[k].ID) {
							// answerID.push(data.Question.AnswerOptions[k].ID);
							if (data.Question.AnswerOptions[k].IsCorrect) {
								data.StudentAnswers[i].IsCorrectAnswer = true;
							}
						}
					}
				}
			}).error(function() {
				$scope.loadingData = false;
			});
	};

	$scope.getStudentAnswers();

	// This function is called to put grade and comment on textquestions
	$scope.submitText = function submitText(answer) {
		// Object for the grade and comment from teacher
		var data = {
			Comment: answer.Answer.TeachersComment,
			Grade:   answer.Answer.StudentGrade
		};

		ExamDetailsResource.gradeStudentAnswer($stateParams.courseInstanceID,
			$stateParams.examID,
			$stateParams.questionID,
			answer.Student.SSN, data).success(function (data) {
				centrisNotify.success("questionGrading.GradeCommentAdded");
			}).error(function (data) {
				centrisNotify.error("questionGrading.GradeCommentFailed");
			});
	};

	// This function is called to give every student correct for one question
	// or to add correct answeroption
	$scope.submitMultiple = function submitMultiple(selans) {
		var data = {};
		if (selans === undefined) {
			data = {
				AllCorrect: "giveAllCorrect"
			};
		} else {
			selans.AllCorrect = true;
			data = {
				AllCorrect: selans.IsCorrect,
				answerID: selans.ID
			};
		}

		ExamDetailsResource.addCorrectMultipleAnswer($stateParams.courseInstanceID,
			$stateParams.examID,
			$stateParams.questionID,
			data).success(function (data) {
				centrisNotify.success("questionGrading.AllGradesChanged");
				$scope.getStudentAnswers(data);
			}).error(function (data) {
				centrisNotify.error("questionGrading.NotSuccessfull");
			});
	};
});
