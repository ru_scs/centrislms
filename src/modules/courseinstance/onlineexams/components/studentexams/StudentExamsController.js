"use strict";

/*
 * This controller takes care of displaying all online exams visible to a student.
 */
angular.module("onlineExamsApp").controller("StudentExamsController",
function StudentExamsController($scope, $stateParams, $location, $filter, ExamsResource, $translate, $state) {

	$scope.examInstances = [];
	$scope.courseInstanceID = $stateParams.courseInstanceID;
	var orderBy = $filter("orderBy");

	$scope.fetchingData = true;
	ExamsResource.getOnlineExams($stateParams.courseInstanceID)
	.success(function (exams) {
		$scope.fetchingData  = false;
		$scope.examInstances = exams;
	}).error(function(err) {
		$scope.fetchingData = false;
	});

	$scope.orderExams = function (predicate, reverse) {
		$scope.examInstances = orderBy($scope.examInstances, predicate, reverse);
	};

	// Default sorting, sorted by exam name ascending
	$scope.orderExams("Status", false);

	$scope.beginExam = function(examID) {
		// Fer inn á síðuna sem ætti að birta netprófið ef það er inni nú þegar
		$location.path("/courses/" + $scope.courseInstanceID + "/onlineexams/" + examID);
	};

	// INTRO otions
	$scope.IntroOptions = {
		steps: [{
			element: "#examtablename",
			position: "right",
			intro: ""
		}, {
			element: "#examtablestatus",
			position: "left",
			intro: ""
		}, {
			element: "#examtableduedate",
			position: "right",
			intro: ""
		}, {
			element: "#exameableweight",
			position: "left",
			intro: ""
		}, {
			element: "#examtablegrade",
			position: "left",
			intro: ""
		}, {
			element: "#examtableinfo",
			position: "left",
			intro: ""
		}, {
			element: "#examtablerange",
			position: "left",
			intro: ""
		}],
		showStepNumbers:    false,
		exitOnOverlayClick: true,
		exitOnEsc:          true,
	};

	var introKeys = [
		"AngIntroExams.ExamTableName",
		"AngIntroExams.ExamTableStatus",
		"AngIntroExams.ExamTableDueDate",
		"AngIntroExams.ExamTableWeight",
		"AngIntroExams.ExamTableGrade",
		"AngIntroExams.ExamTableInfo",
		"AngIntroExams.ExamTableRange"
	];

	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
		$scope.IntroOptions.steps[1].intro = translations[introKeys[1]];
		$scope.IntroOptions.steps[2].intro = translations[introKeys[2]];
		$scope.IntroOptions.steps[3].intro = translations[introKeys[3]];
		$scope.IntroOptions.steps[4].intro = translations[introKeys[4]];
		$scope.IntroOptions.steps[5].intro = translations[introKeys[5]];
		$scope.IntroOptions.steps[6].intro = translations[introKeys[6]];
	});

	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.$on("onCentrisIntro", function(event, args) {
		// Only show this if the current route is for our page:
		if ($state.current.name === "courseinstance.studentexams") {
			$scope.ShowOnlineExamIntro();
		}
	});
});
