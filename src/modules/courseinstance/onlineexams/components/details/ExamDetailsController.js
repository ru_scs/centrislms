"use strict";

/*
 * ExamDetailsController displays the details of an exam, from the perspective
 * of a student. It is also used by the teacher, both when viewing the exam,
 * and when grading the submission from a single student.
 */
angular.module("onlineExamsApp").controller("ExamDetailsController",
function ExamDetailsController($scope, ExamDetailsResource, $stateParams, centrisNotify, WarningDlg,
	$q, CourseInstanceRoleService) {

	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(isTeacher) {
		$scope.isTeacher = isTeacher;
	});

	var courseID = $stateParams.courseInstanceID;
	var examID   = $stateParams.examid;
	var SSN      = $stateParams.SSN;
	$scope.courseInstanceID = $stateParams.courseInstanceID;
	$scope.examid           = $stateParams.examid;
	$scope.inGradingMode    = false;

	// Error codes constants
	var ERR = {
		HANDIN_NOT_FOUND:   "ONLINE_EXAM_FINAL_SOLUTION_NOT_FOUND",
		STUDENT_NOT_ACTIVE: "PERSON_IS_NOT_A_STUDENT"
	};

	$scope.examEnd            = moment();
	$scope.onlineExam         = {};
	$scope.studentSolution    = {};
	$scope.onlineExamResult   = {};
	$scope.studentHasHandedIn = false;
	$scope.fetchingData       = true;
	$scope.autoGradableExam   = true;
	$scope.showExamGrade      = false;

	// This page/view/component is used both to display the exam to a student,
	// and also for a teacher to grade a single submission from a student.
	// Here we select which one we're doing:
	if ($stateParams.SSN) {
		// The caller is a teacher which is grading a single submission.
		// Load the given submission:
		ExamDetailsResource.getOnlineExamByStudent(courseID, examID, $stateParams.SSN)
		.success(function (data) {
			$scope.inGradingMode = true;
			$scope.onlineExam    = data;
			$scope.fetchingData  = false;

			// Getting the students right answer
			for (var k = 0; k < data.Questions.length; k++) {
				if (data.Questions[k].Question.Type === 1 || data.Questions[k].Question.Type === 2) {
					for (var l = 0; l < data.Questions[k].Question.AnswerOptions.length; l++) {
						if (data.Questions[k].Question.AnswerOptions[l].ID === data.Questions[k].Answer.SelectedOptions[0]) {
							data.Questions[k].Question.AnswerOptions[l].studentAnswer = true;
						}
					}
				}
			}
		})
		.error(function (data) {
			centrisNotify.error("answerExam.GetExamError");
			$scope.fetchingData = false;
		});
	} else {
		// No grading is taking place. HOWEVER, we still don't know
		// if the user is a teacher or a student. The API knows however,
		// and will only return the data the user is allowed to access.
		// We use that to determine what to display.
		ExamDetailsResource.getOnlineExamDetails(courseID, examID)
		.success(function (data) {
			$scope.fetchingData = false;
			$scope.onlineExam   = data;
			$scope.examEnd      = moment($scope.onlineExam.EndDate);

			// Calculate the "real" deadline.
			// NOTE: doesn't yet handle the case when the student
			// opens the exam, saves, and then reopens!
			if (data.MinutesAllowed > 0) {
				var timeLimitDate = moment().add(data.MinutesAllowed, "minutes");
				if (timeLimitDate < $scope.examEnd) {
					$scope.examEnd = timeLimitDate;
				}
			}

			if ($scope.onlineExam.AutoGradeRatio === 0) {
				$scope.autoGradableExam = false;
			}

			// We display the exam questions if ONE of the following is valid:
			// a) the user is a teacher
			// b) the user is a student, AND:
			//    i) the exam is active (i.e. between start and end date)
			//       and the student hasn't yet handed in his/her final solution
			//       OR
			//    ii) the exam has been closed
			if (!$scope.isTeacher) {
				$scope.studentHasHandedIn = $scope.onlineExam.StudentHandIn.FinishDate !== null;

				if (!$scope.studentHasHandedIn) {
					$scope.showExamGrade = false;
					var answer = [];

					angular.forEach(data.Questions, function (obj) {
						// Fill object with existing answers from student
						var ans  = [];
						switch (obj.Question.Type) {
							// Single choice question
							case 1: {
								ans = obj.Answer.SelectedOptions; // StudentAnswers;
								break;
							}
							// Multiple choice question
							case 2: {
								for (var i = 0; i < obj.Question.AnswerOptions.length; i ++) {
									var contains = false;
									for (var j = 0; j < obj.Answer.SelectedOptions.length; j++) {
										if (obj.Question.AnswerOptions[i].ID === obj.Answer.SelectedOptions[j]) {
											contains = true;
										}
									}
									if (contains) {
										ans.push(obj.Question.AnswerOptions[i].ID);
									} else {
										ans.push(false);
									}
								}
								break;
							}
						}
						var studentAnswer = {
							QuestionID: obj.Question.ID,
							Answers:    ans,
							Text:       obj.Answer.Text
						};
						answer.push(studentAnswer);
					});
					$scope.studentSolution.Answers = answer;
				} else {
					// Student has handed in his/her solution.
					// If the grade is available, we display it.
					$scope.showExamGrade                   = data.StudentHandIn.Grade !== null;
					$scope.onlineExamResult.Grade          = data.StudentHandIn.Grade;
					$scope.onlineExamResult.AutoGradeRatio = data.AutoGradeRatio;
				}
			}
		})
		.error(function (data) {
			$scope.fetchingData = false;
			centrisNotify.error("answerExam.GetExamError");
		});
	}

	// This function will be run when a user presses the "Submit Exam" button
	$scope.submitAnswers = function() {
		// First, we check if the student has answered all questions.
		// If not, the student will be given the option to cancel the submit
		// operations. Since the warning dialog is async, and returns a promise,
		// we must be a little bit creative in the following code, since we
		// obviously don't want to interfere with the save process if all
		// questions were answered.
		var deferred = $q.defer();
		var allAnswered = true;
		var thenFunc = function() {
			deferred.resolve();
		};
		for (var k = 0; k < $scope.studentSolution.Answers.length; k++) {
			var hasSingleOrMultiAnswer = false;
			for (var m = 0; m < $scope.studentSolution.Answers[k].Answers.length; m++) {
				if ($scope.studentSolution.Answers[k].Answers[m] !== false) {
					hasSingleOrMultiAnswer = true;
					break;
				}
			}
			if (hasSingleOrMultiAnswer === false && $scope.studentSolution.Answers[k].Text.length === 0) {
				allAnswered = false;
				WarningDlg.show().then(thenFunc);
				break;
			}
		}

		if (allAnswered === true) {
			deferred.resolve();
		}

		deferred.promise.then(function() {
			$scope.fetchingData = true;
			ExamDetailsResource.turnInStudentSolution(courseID, examID, $scope.studentSolution)
			.success(function (data) {
				$scope.studentHasHandedIn = true;
				$scope.fetchingData       = false;
				$scope.onlineExamResult   = data;
				$scope.showExamGrade      = data === null;
			})
			.error(function () {
				$scope.fetchingData = false;
				centrisNotify.error("postStudentExam.CouldNotTurnInExam");
			});
		});
	};

	// Calls the save API method for saving students answers for an exam
	$scope.saveAnswers = function () {
		ExamDetailsResource.saveStudentSolution(courseID, examID, $scope.studentSolution)
		.success(function (data) {
			centrisNotify.success("postStudentExam.SaveSuccess");
		})
		.error(function (message) {
			// We only want to alert the user of a saving error if the exam is open.
			if (!$scope.examClosed()) {
				centrisNotify.error("postStudentExam.CouldNotSaveSolution");
			}
		});
	};

	// Checks if the exam is closed
	$scope.examClosed = function () {
		var dateNow = moment();

		if (dateNow.isAfter($scope.examEnd) || dateNow.isSame($scope.examEnd)) {
			return true;
		}
		return false;
	};

	$scope.examPartiallyGradable = function () {
		if ($scope.onlineExamResult) {
			var ratio = $scope.onlineExamResult.AutoGradeRatio;
			return (ratio < 100 && ratio > 0);
		}
		return 0;
	};

	// Serves as the autosaving function. Called when the user exits the current view.
	$scope.$on("$destroy", function () {
		/* We only want to save the student's answers if (s)he hasn't already handed in
		   the exam and the exam has not closed
		   AND the user isn't in fact a teacher! */
		if (!$scope.studentHasHandedIn && !$scope.examClosed() && !$scope.isTeacher) {
			$scope.saveAnswers();
		}
	});

	// This function is used to determine wether we should display the
	// questions (and the answer options) or not.
	// For teachers, this should always be true. For students it varies.
	$scope.shouldShowExamQuestions = function shouldShowExamQuestions() {
		if ($scope.isTeacher) {
			return true;
		} else {
			/*
			if ($scope.studentHasHandedIn) {
				// In this case, the questions should be displayed
				// depending on the exam settings.
			} else {
				return true;
			}*/
			return !$scope.studentHasHandedIn && !$scope.examClosed();
		}
	};
	// Here we are grading the exam, one exam per one student
	// this function is called for all question types.
	$scope.submitText = function submitText(question) {
		// Object for the grade and comment from teacher
		var data = {
			Comment: question.Answer.TeachersComment,
			Grade:   question.Answer.StudentGrade
		};

		ExamDetailsResource.gradeStudentAnswer(courseID,
			examID,
			question.Question.ID,
			SSN, data).success(function (data) {
				centrisNotify.success("onlineExams.GradeCommentAdded");
			}).error(function (data) {
				centrisNotify.error("onlineExams.GradeCommentFailed");
			});
	};
});
