"use strict";

describe("ExamDetailsController", function() {

	beforeEach(module("onlineExamsApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));
	beforeEach(module("courseInstanceShared"));

	var scope;
	var ctrl;
	var resource;
	var notify;
	var roleServiceFactory;
	var roleService;
	var isTeacher = false;
	var createController;

	var mockStateParams = {
		courseInstanceID: 100,
		examid: 110,
		SSN: "1203735289"
	};

	var mockWarningDlg = {
		show: function() {
			return {
				then: function(fn) {
					fn();
				}
			};
		}
	};

	beforeEach(inject(function($rootScope, $controller, MockCourseInstanceRoleService,
		mockCentrisNotify, $q, mockExamDetailsResource) {

		roleServiceFactory = MockCourseInstanceRoleService;
		notify             = mockCentrisNotify;
		resource           = mockExamDetailsResource;

		createController = function() {
			scope       = $rootScope.$new();
			roleService = roleServiceFactory.mockCourseInstanceRoleService(isTeacher);

			// Set up spies:
			spyOn(resource, "getOnlineExamDetails").and.callThrough();

			// Create the actual controller:
			ctrl = $controller("ExamDetailsController", {
				$scope:                    scope,
				ExamDetailsResource:       resource,
				$stateParams:              mockStateParams,
				centrisNotify:             notify,
				WarningDlg:                mockWarningDlg,
				$q:                        $q,
				CourseInstanceRoleService: roleService
			});
		};
	}));

	describe("when student views an exam", function() {

		beforeEach(function() {
			isTeacher           = false;
			mockStateParams.SSN = null;
			resource.getDetailsReply = {
				EndDate: "2016-05-25 23:59"/*,
				MinutesAllowed: 100*/
			};
			resource.getStudentGradeReply = {
				// TODO
			};
			createController();
		});

		xit ("should load exam details", function() {
			expect(resource.getOnlineExamDetails).toHaveBeenCalled();
		});

		describe("and has not completed the exam", function() {
			// TODO
		});

		describe("and has completed the exam", function() {
			// TODO
		});
	});

	describe("when a teacher opens the exam", function() {

		beforeEach(function() {
			// TODO
		});

		describe("and is grading a submission from a single student", function() {
			// TODO
		});

		describe("and is viewing the exam", function() {
			// TODO
		});
	});
});