"use strict";

angular.module("onlineExamsApp",
["pascalprecht.translate",
"ui.router",
"sharedServices",
"textAngular",
"ang-drag-drop",
"ui.bootstrap",
"chart.js",
"ngSanitize",
"fcsa-number"]).config(function ($stateProvider) {
	$stateProvider.state("courseinstance.onlineexams", {
		url:         "/onlineexams",
		templateUrl: "courseinstance/onlineexams/components/index/exams.html",
		controller:  "ExamsController"
	});
	$stateProvider.state("courseinstance.edittemplate", {
		url:         "/templates/:templateID",
		templateUrl: "courseinstance/onlineexams/components/templates/templates.html",
		controller:  "TemplatesController"
	});
	$stateProvider.state("courseinstance.newtemplate", {
		url:         "/templates",
		templateUrl: "courseinstance/onlineexams/components/templates/templates.html",
		controller:  "TemplatesController"
	});
	$stateProvider.state("courseinstance.savetemplate", {
		url:         "/savetemplate",
		templateUrl: "courseinstance/onlineexams/html/savetemplate.html",
		controller:  "TemplatesController"
	});
	$stateProvider.state("courseinstance.studentonlineexams", {
		url:         "/studentonlineexams",
		templateUrl: "courseinstance/onlineexams/components/studentexams/studentexams.html",
		controller:  "StudentExamsController"
	});
	$stateProvider.state("courseinstance.onlineexams.examdetails", {
		url:         "/:examid",
		templateUrl: "courseinstance/onlineexams/components/details/examdetails.html",
		controller:  "ExamDetailsController"
	});
	$stateProvider.state("courseinstance.onlineexams.examstudentdetails", {
		url:         "/:examid/student/:ssn",
		templateUrl: "courseinstance/onlineexams/components/details/examdetails.html",
		controller:  "ExamDetailsController"
	});
	$stateProvider.state("courseinstance.onlineexams.studentgrades", {
		url:         "/:examid/solutions",
		templateUrl: "courseinstance/onlineexams/components/studentgrades/studentgrades.html",
		controller:  "ExamDetailsController"
	});
	$stateProvider.state("courseinstance.onlineexams.results", {
		url:         "/:examid/results",
		templateUrl: "courseinstance/onlineexams/components/results/tabview.html",
		controller:  "OnlineExamsTabController"
	});
	$stateProvider.state("courseinstance.onlineexams.results.studentlist", {
		url:         "/studentlist"
	});
	$stateProvider.state("courseinstance.onlineexams.results.statistics", {
		url:         "/statistics"
	});
	$stateProvider.state("courseinstance.onlineexams.results.settings", {
		url:         "/settings"
	});
	$stateProvider.state("courseinstance.onlineexams.questiongrading", {
		url:         "/:examID/questiongrading/:questionID",
		templateUrl: "courseinstance/onlineexams/components/questiongrading/questiongrading.html",
		controller:  "QuestionGradingController"
	});
	$stateProvider.state("courseinstance.onlineexams.examanswers", {
		url:         "/:examid/examanswers/:SSN",
		templateUrl: "courseinstance/onlineexams/components/details/examdetails.html",
		controller:  "ExamDetailsController"
	});
});
