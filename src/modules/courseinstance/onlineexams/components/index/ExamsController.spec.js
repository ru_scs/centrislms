"use strict";

describe("ExamsController", function() {

	beforeEach(module("onlineExamsApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));

	var scope, ctrl, notify;

	// Mock data used for the tests
	var testDates1 = {
		start: new Date("January 15, 2014 00:00:00").toISOString(),
		end: new Date("January 17, 2014 23:59:00").toISOString()
	};
	var testDates2 = {
		create: new Date("January 13, 2014 09:45:00").toISOString(),
	};

	var mockResource = {
		getOnlineExams: function() {
			var data = [{
				Name: "Weekly Exam 1, introduction",
				StartDate: testDates1.start,
				EndDate: testDates1.end,
				Status: 2,
				CompletedCount: 100,
				StudentCount: 200,
				AverageGrade: 8.9
			}];

			return {
				success: function(fn) {
					fn(data);
				}
			};
		},

		getOnlineExamTemplates: function() {
			var data = [{
				ID: 1,
				Name: "Weekly Exam 1, html5 and new functionality",
				QuestionCount: 12,
				DateCreated: testDates2.create
			}];

			return {
				success: function(fn) {
					fn(data);
				}
			};
		},

		deleteOnlineExamTemplate: function() {
			return {
				then: function(fn) {
					fn({});
				}
			};
		}
	};

	var mockDlg = {
		show: function() {
			return {
				then: function(fn) {
					fn({});
				}
			};
		}
	};

	beforeEach(inject(function($rootScope, $controller, mockTranslate, mockCentrisNotify) {
		scope = $rootScope.$new();
		notify = mockCentrisNotify;
		spyOn(mockResource, "getOnlineExams").and.callThrough();
		spyOn(mockResource, "getOnlineExamTemplates").and.callThrough();
		spyOn(mockResource, "deleteOnlineExamTemplate").and.callThrough();
		spyOn(mockDlg, "show").and.callThrough();
		ctrl = $controller("ExamsController", {
			$scope: scope,
			$stateParams: {},
			ExamsResource: mockResource,
			PostExamDlg: mockDlg,
			$translate: mockTranslate.mockTranslate,
			$state: {},
			centrisNotify: mockCentrisNotify
		});
	}));

	/*	There is something really funky going on with these tests.
		Sometimes they work, sometimes they give us the error
		"TypeError: null is not an object (evaluating 'currentSpec.expect')"
		but this seems to be very inconsistent, i.e. sometimes works and
		sometimes doesn't, even though no changes have been made to the test file. */
	it("should check if exams array is defined and not NULL", function() {
		expect(scope.exams).toBeDefined();
	});

	it("should check if templates array is defined and not NULL", function() {
		expect(scope.templates).toBeDefined();
	});

	it("should check if getCourseExams() was called", function() {
		expect(mockResource.getOnlineExams).toHaveBeenCalled();
	});

	it("should check if getCourseTemplates() was called", function() {
		expect(mockResource.getOnlineExamTemplates).toHaveBeenCalled();
	});

	it("should check if exams array contains the right data", function() {
		expect(scope.exams).toContain({
			Name: "Weekly Exam 1, introduction",
			Status: 2,
			StartDate: testDates1.start,
			EndDate: testDates1.end,
			CompletedCount: 100,
			StudentCount: 200,
			AverageGrade: 8.9
		});
	});

	it("should check if templates array contains the right data", function() {
		expect(scope.templates[0].Name).toEqual("Weekly Exam 1, html5 and new functionality");
		expect(mockResource.getOnlineExamTemplates).toHaveBeenCalled();
	});

	it("should attempt to display dialog when asked to publish an online exam", function() {
		var prevLength = scope.exams.length;
		scope.serveMe();
		expect(mockDlg.show).toHaveBeenCalled();
		expect(scope.exams.length).toBe(prevLength + 1);
	});

	it("should remove deleted template", function() {
		spyOn(notify, "successWithUndo").and.callThrough();
		var prevCount = scope.templates.length;
		scope.deleteTemplate({
			ID: 1
		});
		expect(mockResource.deleteOnlineExamTemplate).toHaveBeenCalled();
		expect(notify.successWithUndo).toHaveBeenCalled();
		expect(scope.templates.length).toBe(prevCount - 1);
	});
});
