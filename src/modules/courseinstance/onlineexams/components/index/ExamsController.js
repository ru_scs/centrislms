"use strict";

angular.module("onlineExamsApp").controller("ExamsController",
function ExamsController($scope, $stateParams, ExamsResource, PostExamDlg,
	$translate, $state, centrisNotify) {
	$scope.exams        = [];
	$scope.templates    = [];
	$scope.fetchingData = true;
	var getMethods      = 2;
	$scope.hasExams     = false;
	$scope.hasTemplates = false;

	var courseID = $stateParams.courseInstanceID;

	// Pulling all course exams for this course
	ExamsResource.getOnlineExams(courseID)
	.success(function(data) {
		$scope.exams = data;
		getMethods--;
		updateFetchingData();
		if ($scope.exams.length !== 0) {
			$scope.hasExams = true;
		}
	});

	// Pulling all templates for this course
	ExamsResource.getOnlineExamTemplates(courseID)
	.success(function(data) {
		$scope.templates = data;
		getMethods--;
		updateFetchingData();
		if ($scope.templates.length !== 0) {
			$scope.hasTemplates = true;
		}
	});

	// Checks if the controller has finished loading all data
	function updateFetchingData() {
		if (getMethods === 0) {
			$scope.fetchingData = false;
		}
	}

	$scope.serveMe = function (template) {
		PostExamDlg.show(template).then(function(data) {
			$scope.exams.push(data);
		});
	};

	// This function is calld when user wants to delete a template for online exam in the course.
	// The user can undo the action by pressing undo button that appears with the success message.
	$scope.deleteTemplate = function (template) {
		$scope.spinner = false;
		var promise = ExamsResource.deleteOnlineExamTemplate($scope.courseInstanceID, template.ID);
		promise.then(
		function (res) {
			var undoParam = {
				type: "template-delete",
				id: {
					templateID:     template.ID,
					instance:       $scope.courseInstanceID
				}
			};
			centrisNotify.successWithUndo("onlineExams.DeleteSuccessMessage", undoParam);
			_.remove($scope.templates, function (o) {
				return o.ID === template.ID;
			});
		}, function (err) {
			centrisNotify.error("onlineExams.DeleteErrorMessage");
		});
	};

	// This event gets broadcast whenever an undo command
	// is issued. We check that it matches an operation we know how to undo.
	$scope.$on("centrisUndo", function undo(event, param) {
		if (param.type === "template-delete") {
			var template = {
				ID: param.id.templateID
			};
			ExamsResource.editOnlineExamTemplate(param.id.instance, template.ID).success(function (data) {
				centrisNotify.success("onlineExams.UndoSuccess");
				$scope.templates.push(data);
			})
			.error(function(dError) {
				console.log(dError);
				centrisNotify.error("onlineExams.NotSuccessfull");
			});
		}
	});
	// INTRO otions
	$scope.IntroOptions = {
		steps: [{
			element: "#examtable",
			position: "left",
			intro: ""
		}, {
			element: "#examtablecompletedcount",
			position: "left",
			intro: ""
		}, {
			element: "#examtableaveragegrade",
			position: "right",
			intro: ""
		}, {
			element: "#examtableuserview",
			position: "left",
			intro: ""
		}, {
			element: "#examtablestats",
			position: "left",
			intro: ""
		}, {
			element: "#examtablesettings",
			position: "left",
			intro: ""
		}, {
			element: "#examtableaddtemplate",
			position: "left",
			intro: ""
		}, {
			element: "#examtabletemplate",
			position: "left",
			intro: ""
		}, {
			element: "#examtableserve",
			position: "left",
			intro: ""
		}],
		showStepNumbers:    false,
		exitOnOverlayClick: true,
		exitOnEsc:          true,
	};

	var introKeys = [
		"AngIntroExams.ExamTable",
		"AngIntroExams.ExamTableCompletedCount",
		"AngIntroExams.ExamTableAverageGrade",
		"AngIntroExams.ExamTableUserView",
		"AngIntroExams.ExamTableStats",
		"AngIntroExams.ExamTableSettings",
		"AngIntroExams.ExamTableAddTemplate",
		"AngIntroExams.ExamTabletemplate",
		"AngIntroExams.ExamTableServe"
	];

	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
		$scope.IntroOptions.steps[1].intro = translations[introKeys[1]];
		$scope.IntroOptions.steps[2].intro = translations[introKeys[2]];
		$scope.IntroOptions.steps[3].intro = translations[introKeys[3]];
		$scope.IntroOptions.steps[4].intro = translations[introKeys[4]];
		$scope.IntroOptions.steps[5].intro = translations[introKeys[5]];
		$scope.IntroOptions.steps[6].intro = translations[introKeys[6]];
		$scope.IntroOptions.steps[7].intro = translations[introKeys[7]];
		$scope.IntroOptions.steps[8].intro = translations[introKeys[8]];
	});

	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.$on("onCentrisIntro", function(event, args) {
		// Only show this if the current route is for our page:
		if ($state.current.name === "courseinstance.onlineexams") {
			$scope.ShowOnlineExamIntro();
		}
	});
});
