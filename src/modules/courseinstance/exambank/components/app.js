"use strict";

angular.module("examBankApp", [
	"pascalprecht.translate",
	"ui.router"
]).config(function ($stateProvider) {
	$stateProvider.state("courseinstance.exambank", {
		url:         "/exambank",
		templateUrl: "courseinstance/exambank/components/index/index.html",
		controller:  "ExamBankController"
	});
});
