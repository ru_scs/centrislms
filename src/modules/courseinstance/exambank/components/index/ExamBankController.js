"use strict";

/*
 * This controller handles displaying the list of final exams in a course,
 * as well as allowing the teacher to create/edit entris in said bank.
 */
angular.module("examBankApp").controller("ExamBankController",
function ExamBankController($scope, $state, $stateParams, ExamBankResource, AddExamToBankDlg, centrisNotify) {
	$scope.examBank     = [];
	$scope.fetchingData = true;
	ExamBankResource.getExamsByCourseId($stateParams.courseInstanceID).success(function (data) {
		$scope.fetchingData = false;
		$scope.examBank = data;
	})
	.error(function (data) {
		$scope.fetchingData = false;
		centrisNotify.error("examBankApp.Notify.FetchError");
	});

	$scope.addExam = function addExam() {
		AddExamToBankDlg.show().then(function(exam) {
			ExamBankResource.addExam(exam, $stateParams.courseInstanceID).success(function(data) {
				$scope.examBank.push(data);
				centrisNotify.success("exambank.Notify.UploadSuccess");
			})
			.error(function (data) {
				centrisNotify.error("exambank.Notify.UploadError");
			});
		});
	};

	$scope.deleteExam = function(exam) {
		var promise = ExamBankResource.deleteExamInCourse($scope.courseInstanceID, exam);
		promise.then(
		function (res) {
			centrisNotify.success("exambank.Notify.DeleteSuccess");
			_.remove($scope.examBank, function (o) {
				return o.ID === exam.ID;
			});
		}, function (err) {
			centrisNotify.error("exambank.Notify.DeleteError");
		});
	};
});
