"use strict";

angular.module("examBankApp").factory("ExamBankResource",
function ExamBankResource(CentrisResource) {
	return {
		getExamsByCourseId: function getExamsByCourseId(courseInstanceID) {
			return CentrisResource.get("courses", ":courseInstanceID/exambank", {courseInstanceID: courseInstanceID});
		},
		addExam: function addExam(exam, courseInstanceID) {
			var fd = new FormData();
			fd.append("Title",      exam.Title);
			fd.append("Semester",   exam.Semester);

			if (exam.File.length > 0) {
				for (var i = 0 ; i < exam.File.length ; i++) {
					fd.append("File" + i, exam.File[i]);
				}
			}

			var param = {
				courseInstanceID: courseInstanceID
			};

			return CentrisResource.post("courses", ":courseInstanceID/exambank", param, fd, true);
		},
		deleteExamInCourse: function(courseInstanceID, exam) {
			var param = {
				courseInstanceID: courseInstanceID,
				id: exam.ID
			};
			return CentrisResource.remove("courses", ":courseInstanceID/exambank/:id", param);
		}
	};
});