"use strict";

describe("ExamBankController", function() {

	beforeEach(module("examBankApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));
	beforeEach(module("ui.bootstrap"));

	var scope, controller;

	beforeEach(inject(function($rootScope, $controller, mockCentrisNotify) {
		scope = $rootScope.$new();
		controller = $controller("ExamBankController", {
			$scope: scope,
			centrisNotify: mockCentrisNotify
		});
	}));

	it("should define all functions and variables", function() {
		// Variables
		expect(scope.addExam).toBeDefined();
		expect(scope.deleteExam).toBeDefined();
	});

});