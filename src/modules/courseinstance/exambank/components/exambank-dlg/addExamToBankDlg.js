"use strict";

angular.module("examBankApp").factory("AddExamToBankDlg",
function addExamToBankDlg($uibModal) {
	return {
		show: function show() {
			var templateInstance = {
				controller:  "AddExamToBankController",
				templateUrl: "courseinstance/exambank/components/exambank-dlg/add-exam-to-bank.tpl.html"
			};

			var modalInstance = $uibModal.open(templateInstance);

			return modalInstance.result;
		}
	};
});

angular.module("examBankApp").controller("AddExamToBankController",
function AddExamToBankController($scope) {
	$scope.defaultFocus = true;
	$scope.model = {
		Title:      $scope.filename,
		File:       $scope.File,
		Semester:   $scope.semester
	};

	$scope.validateSelection = function validateSelection(isValid) {

		$scope.showError = false;
		if (!isValid) {
			$scope.showError = true;
		} else {
			$scope.$close($scope.model);
		}
	};
});