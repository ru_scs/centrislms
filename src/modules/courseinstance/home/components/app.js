"use strict";

angular.module("courseHomeApp", [
	"pascalprecht.translate",
	"ui.router",
	"sharedServices",
	"textAngular",
	"courseInstanceShared"
]).config(function ($stateProvider) {
	$stateProvider.state("courseinstance", {
		url:         "/courses/:courseInstanceID",
		views: {
			"": {
				templateUrl: "courseinstance/home/components/index/index.html",
				controller:  "CourseHomeController"
			},
			"sidebar@courseinstance": {
				templateUrl: "courseinstance/home/components/sidebar/dropdown.html"
			}
		}
	});
});
