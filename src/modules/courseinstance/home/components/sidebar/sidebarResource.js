"use strict";

/**
*
*/
angular.module("courseHomeApp").factory("SidebarResource",
function SidebarResource() {

	var links = [
		{
			stateName: "courseinstance()",
			teacherUrl: "",
			studentUrl: "",
			languageKey: "Home.Sidebar.FrontPage",
			activeEq: true
		},
		{
			stateName: "courseinstance.coursedetails()",
			teacherUrl: "/details",
			studentUrl: "/details",
			languageKey: "Home.Sidebar.About",
			activeEq: false
		},
		{
			stateName: "courseinstance.announcements()",
			teacherUrl: "/announcements",
			studentUrl: "/announcements",
			languageKey: "Home.Sidebar.Announcements",
			activeEq: false
		},
		{
			stateName: "courseinstance.students()",
			teacherUrl: "/students",
			studentUrl: "/students",
			languageKey: "Home.Sidebar.Students",
			activeEq: false
		},
		{
			stateName: "courseinstance.grades()",
			teacherUrl: "/grades",
			studentUrl: "/grades",
			languageKey: "Home.Sidebar.Grades",
			activeEq: false
		},
		{
			stateName: "courseinstance.books()",
			teacherUrl: "/books",
			studentUrl: "/books",
			languageKey: "Home.Sidebar.Books",
			activeEq: false
		},
		{
			stateName: "courseinstance.assignments()",
			teacherUrl: "/assignments",
			studentUrl: "/assignments",
			languageKey: "Home.Sidebar.Assignments",
			activeEq: false
		},
		{
			userRole: "teacher",
			stateName: "courseinstance.onlineexams()",
			teacherUrl: "/onlineexams",
			studentUrl: "/studentonlineexams",
			languageKey: "Home.Sidebar.OnlineExams",
			activeEq: false
		},
		{
			userRole: "student",
			stateName: "courseinstance.studentonlineexams()",
			teacherUrl: "/onlineexams",
			studentUrl: "/studentonlineexams",
			languageKey: "Home.Sidebar.OnlineExams",
			activeEq: false
		},
		{
			stateName: "courseinstance.topics()",
			teacherUrl: "/topics",
			studentUrl: "/topics",
			languageKey: "Home.Sidebar.Lectures",
			activeEq: false
		},
		{
			stateName: "courseinstance.exambank()",
			teacherUrl: "/exambank",
			studentUrl: "/exambank",
			languageKey: "Home.Sidebar.ExamBank",
			activeEq: false
		},
		{
			stateName: "courseinstance.courseschedule()",
			teacherUrl: "/schedule",
			studentUrl: "/schedule",
			languageKey: "Home.Sidebar.Schedule",
			activeEq: false
		},
		{
			stateName: "courseinstance.materials()",
			teacherUrl: "/materials",
			studentUrl: "/materials",
			languageKey: "Home.Sidebar.Materials",
			activeEq: false
		},
		{
			userRole: "teacher",
			stateName: "courseinstance.importcontent()",
			teacherUrl: "",
			studentUrl: "",
			languageKey: "Home.Sidebar.Import",
			activeEq: false
		}
	];

	return {
		getLinks: function() {
			return links;
		},
		getLocations: function() {
			var locations = {};
			for (var i = 0; i < links.length; i++) {
				locations[links[i].stateName.substring(0, links[i].stateName.length - 2)] = links[i];
			}
			return locations;
		}
	};
});
