"use strict";
angular.module("courseHomeApp")
.factory("CoursesHomeResource", function (CentrisResource) {
	return {
		getCourseInstance: function getCourseInstance(courseInstanceID) {
			var param = {
				id: courseInstanceID
			};

			return CentrisResource.get("courses", ":id", param);
		}
	};
});
