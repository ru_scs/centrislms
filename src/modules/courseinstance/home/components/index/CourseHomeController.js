"use strict";

angular.module("courseHomeApp").controller("CourseHomeController",
function ($scope,
	$stateParams,
	$window,
	CoursesHomeResource,
	CourseInstanceRoleService,
	CourseInstanceService,
	SidebarResource) {

	CourseInstanceService.resetCurrentCourse();

	$scope.courseInstanceID = $stateParams.courseInstanceID;
	$scope.dropdownClosed = true;
	$scope.reverseDropdown = function reverseDropdown () {
		$scope.dropdownClosed = !$scope.dropdownClosed;
	};

	angular.element($window).bind("resize", function() {
		$scope.$apply(function() {
			$scope.dropdownClosed = true;
		});
	});

	// Used for displaying the semester in the HTML.
	$scope.semester = "";

	CoursesHomeResource.getCourseInstance($scope.courseInstanceID).success(function(instance) {
		$scope.courseInstance = instance;
		$scope.semester = "";
		$scope.semester += instance.Semester.substring(0, 4);
		$scope.semester += "-";
		$scope.semester += instance.Semester.substring(4, 5);
		CourseInstanceRoleService.setCourseRole(instance.ID, instance.UserRole);

		// Some child components might be interested in having
		// access to this data:
		CourseInstanceService.setCurrentCourse($scope.courseInstance);
	});

	$scope.sidebarLinks = SidebarResource.getLinks();

	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(isTeacher) {
		$scope.isTeacher = isTeacher;
		$scope.userRole = isTeacher ?  "teacher" : "student";
	});
});
