"use strict";

angular.module("courseHomeApp").factory("MockCourseHomeResource", function() {
	return {
		makeCourseHomeResource: function makeCourseHomeResource(successLoading, data) {
			return {
				getCourseInstance: function getCourseInstance(id) {
					return {
						success: function(fnSuccess) {
							if (successLoading) {
								fnSuccess(data);
							}
							return {
								error: function(fnError) {
									if (!successLoading) {
										fnError();
									}
								}
							};
						}
					};
				}
			};
		}
	};
});
