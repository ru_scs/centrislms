# About this module

This module contains the front page of a course instance.
This includes the courseinstance menu which can be found in
the sidebar component.