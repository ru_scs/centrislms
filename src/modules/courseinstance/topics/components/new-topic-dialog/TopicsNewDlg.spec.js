"use strict";

describe("Testing TopicsNewDlg", function () {

	var dialog,
	mockValidTopicObject = {
		ID: 1,
		Title: "Best Topic",
		DatePublished: "2016-04-23T00:00:00",
		HasDiscussionBoard: true,
		isNewTopic: true,
		length: "",
		viewed: true,
		topicName: "",
		Items: [{
			id: 1,
			Title: "Item",
			IsMain: true,
			FileName: "Best File"
		},{
			id: 2,
			Title: "Item2",
			IsMain: true
		}]
	};

	beforeEach(module("topicsApp", "sharedServices", "mockConfig"));
	beforeEach(inject(function($injector) {
		dialog = $injector.get("TopicsNewDlg");
	}));

	it("can get an instance of TopicsNewDlg", function() {
		expect(dialog).toBeDefined();
	});

	it("TopicsNewDlg.show should be defined", function() {
		dialog.show(mockValidTopicObject);
		expect(dialog.show).toBeDefined();
	});
});
