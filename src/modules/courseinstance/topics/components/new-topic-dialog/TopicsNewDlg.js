"use strict";

angular.module("topicsApp").factory("TopicsNewDlg",
function TopicsNewDlg($uibModal) {
	return {
		show: function show(topicObject) {
			var modalInstance = $uibModal.open( {
				controller:  "TopicsNewController",
				templateUrl: "courseinstance/topics/components/new-topic-dialog/topics-new-dlg.tpl.html",
				windowClass: "new-topics-modal",
				resolve: {
					topicObject: function() {
						return topicObject;
					}
				}
			});
			return modalInstance.result;
		}
	};
});
