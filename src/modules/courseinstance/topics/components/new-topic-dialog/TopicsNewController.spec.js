"use strict";

describe("Testing TopicsNewController", function() {
	var ctrl, scope, mockCentrisNotify, mockDateTimeService,
			mockUpload, mockValidTopicObject, mockIconFinderService,
			mockUndefinedTopicObject, courseInstanceService;

	mockValidTopicObject = {
		ID: 1,
		Title: "Best Topic",
		DatePublished: "2016-04-23T00:00:00",
		HasDiscussionBoard: true,
		isNewTopic: true,
		length: "",
		viewed: true,
		topicName: "",
		Items: [{
			id: 1,
			Title: "Item",
			IsMain: true,
			FileName: "Best File"
		},{
			id: 2,
			Title: "Item2",
			IsMain: true
		}]
	};
	mockUndefinedTopicObject = undefined;
	mockIconFinderService = {};

	beforeEach(module("topicsApp", "sharedServices", "courseInstanceShared"));

	beforeEach(inject(function($rootScope, $injector) {
		scope = $rootScope.$new();
		mockDateTimeService = $injector.get("dateTimeService");
		mockIconFinderService = $injector.get("iconFinderService");
		courseInstanceService = $injector.get("MockCourseInstanceService");
	}));

	describe("Testing with a valid topicObject", function() {
		beforeEach(inject(function($controller) {
			ctrl = $controller("TopicsNewController", {
				$scope: scope,
				Upload: mockUpload,
				topicObject: mockValidTopicObject,
				dateTimeService: mockDateTimeService,
				iconFinderService: mockIconFinderService
			});
		}));

		describe("testing the validFileNames function", function() {
			it("should return true if all files have a valid title", function() {
				expect(scope.validFileNames()).toBe(true);
			});

			it("should return false if any file has an invalid title", inject(function($controller) {
				var mockObject = mockValidTopicObject;
				mockObject.Items[0].Title = "";
				ctrl = $controller("TopicsNewController", {
					$scope: scope,
					Upload: mockUpload,
					topicObject: mockObject,
					dateTimeService: mockDateTimeService,
					iconFinderService: mockIconFinderService
				});
				expect(scope.validFileNames()).toBe(false);
			}));
		});

		describe("testing the saveLink function", function() {
			it("should show error for unvalid new link name or unvalid new link link", function() {
				scope.model.newLinkName = undefined;
				scope.saveLink();
				expect(scope.showLinkErrors).toBe(true);
				scope.model.newLinkName = "name";

				scope.model.newLinkLink = undefined;
				scope.showLinkErrors = false;
				scope.saveLink();
				expect(scope.showLinkErrors).toBe(true);
			});

			it("should populate the topicContent with the correct information", function() {
				scope.model.newLinkName = "name";
				scope.model.newLinkLink = "www.url.com";
				scope.saveLink();
				expect(scope.model.topicContent[0]).toEqual({
					title: "name",
					file: {
						name: "http://www.url.com"
					},
					isLink: true
				});

				scope.model.newLinkName = "anotherName";
				scope.model.newLinkLink = "http://www.anotherUrl.com";
				scope.saveLink();
				expect(scope.model.topicContent[0]).toEqual({
					title: "anotherName",
					file: {
						name: "http://www.anotherUrl.com"
					},
					isLink: true
				});
				expect(scope.model.newLinkName).toEqual("");
				expect(scope.model.newLinkLink).toEqual("");
			});
		});

		describe("testing the enterInLink function", function() {
			it("should call saveLink if keyEvent is correct", function() {
				spyOn(scope, "saveLink");
				scope.enterInLink({which: 13});
				expect(scope.saveLink).toHaveBeenCalled();
			});

			it("should not call saveLink if keyEvent is incorrect", function() {
				spyOn(scope, "saveLink");
				scope.enterInLink({which: 10});
				expect(scope.saveLink).not.toHaveBeenCalled();
			});
		});

		it("should give scope.model the right values with a valid topic", function() {
			expect(scope.model.isNewTopic).toBe(false);
			expect(scope.model.showErrors).toBe(false);
			expect(scope.model.isValidDate).toBe(true);
			expect(scope.model.topicName).toEqual("Best Topic");
			expect(scope.model.newLinkName).toEqual("");
			expect(scope.model.newLinkLink).toEqual("");
			expect(scope.model.hasDiscussionBoard).toBe(true);
			expect(scope.model.discussionBoardCheckDisabled).toBe(true);
			var rightDate = scope.getRightDate();
			expect(rightDate).toEqual("23. April 2016");
		});

		it("should editOutFile of the given index", function() {
			var contentTitle = scope.model.topicContent[0].title;
			scope.model.editOutFile(0);
			expect(scope.model.topicContent[0].title).not.toEqual(contentTitle);
		});

		it("should test the else if topicObject does not have discussion board", inject(function($controller) {
			var mockObject = mockValidTopicObject;
			mockObject.HasDiscussionBoard = false;
			ctrl = $controller("TopicsNewController", {
				$scope: scope,
				Upload: mockUpload,
				topicObject: mockObject,
				dateTimeService: mockDateTimeService,
				iconFinderService: mockIconFinderService
			});

			expect(scope.model.discussionBoardCheckDisabled).toBe(false);
		}));

		it("should build Return Object",function() {
			var mockObj = {
				Title: "Best Topic",
				Viewed: mockValidTopicObject.viewed,
				HasDiscussionBoard: mockValidTopicObject.HasDiscussionBoard,
			};
			var returnObj = scope.buildReturnObject({});
			expect(returnObj.Title).toEqual(mockObj.Title);
			expect(returnObj.Viewed).toEqual(mockObj.Viewed);
			expect(returnObj.HasDiscussionBoard).toEqual(mockObj.HasDiscussionBoard);
		});

		describe("checking changeDate function", function() {
			it("should call chD and get valid date", inject(function($controller) {
				ctrl = $controller("TopicsNewController", {
					$scope: scope,
					Upload: mockUpload,
					topicObject: mockValidTopicObject,
					dateTimeService: mockDateTimeService,
					iconFinderService: mockIconFinderService,
					CourseInstanceService: courseInstanceService.mockCourseInstanceService(0)
				});
				scope.chD();
				expect(scope.model.isValidDate).toBe(true);
			}));

			it("should call chD and get valid date", inject(function($controller) {
				ctrl = $controller("TopicsNewController", {
					$scope: scope,
					Upload: mockUpload,
					topicObject: mockValidTopicObject,
					dateTimeService: mockDateTimeService,
					iconFinderService: mockIconFinderService,
					CourseInstanceService: courseInstanceService.mockCourseInstanceService(1)
				});
				scope.chD();
				expect(scope.model.isValidDate).toBe(false);
			}));
		});

		describe("testing the saveTopic function", function() {
			beforeEach(inject(function($controller) {
				scope.$close = jasmine.createSpy("$close");
				ctrl = $controller("TopicsNewController", {
					$scope: scope,
					Upload: mockUpload,
					topicObject: mockValidTopicObject,
					dateTimeService: mockDateTimeService,
					iconFinderService: mockIconFinderService
				});
			}));

			it("should save a topic if it is valid", function() {
				scope.model.topicName = "Valid name";
				scope.model.isValidDate = true;
				scope.model.topicContent.length = 2;
				scope.validFileNames = function() {
					return true;
				};
				scope.saveTopic();
				expect(scope.$close).toHaveBeenCalledWith(scope.buildReturnObject(scope.model));
			});

			it("should not save a topic if it topicName is not valid", function() {
				scope.model.topicName = undefined;
				scope.model.isValidDate = true;
				scope.model.topicContent.length = 2;
				scope.saveTopic();
				expect(scope.$close).not.toHaveBeenCalled();
			});

			it("should not save a topic if it date is not valid", function() {
				scope.model.topicName = "Valid name";
				scope.model.isValidDate = false;
				scope.model.topicContent.length = 2;
				scope.saveTopic();
				expect(scope.$close).not.toHaveBeenCalled();
			});

			it("should not save a topic if it content length is not valid", function() {
				scope.model.topicName = "Valid name";
				scope.model.isValidDate = true;
				scope.model.topicContent.length = 0;
				scope.saveTopic();
				expect(scope.$close).not.toHaveBeenCalled();
			});

			it("should not save a topic if it filnames is not valid", function() {
				scope.model.topicName = "Valid name";
				scope.model.isValidDate = true;
				scope.model.topicContent.length = 2;
				scope.validFileNames = function() {
					return false;
				};
				scope.saveTopic();
				expect(scope.$close).not.toHaveBeenCalled();
			});
		});
	});

	describe("testing with undefined topicObject", function() {
		beforeEach(inject(function($controller) {
			ctrl = $controller("TopicsNewController", {
				$scope: scope,
				Upload: mockUpload,
				topicObject: mockUndefinedTopicObject,
				dateTimeService: mockDateTimeService,
				iconFinderService: mockIconFinderService
			});
		}));

		it("should return dateTimeService dateformat for moment when object defined", function() {
			var rightDate = scope.getRightDate();
			expect(rightDate).not.toEqual("23. April 2016");
		});

		it("should build Return Object", function() {
			var returnObj = scope.buildReturnObject({});
			expect(returnObj.HasDiscussionBoard).toEqual(true);
			expect(returnObj.Viewed).toEqual(false);
		});
	});

	describe("testing upload and files functions", function() {
		beforeEach(inject(function($controller) {
			ctrl = $controller("TopicsNewController", {
				$scope: scope,
				Upload: mockUpload,
				topicObject: mockValidTopicObject,
				dateTimeService: mockDateTimeService,
				iconFinderService: mockIconFinderService
			});
		}));
		it("should call scope.upload with undefined scope.files", function() {
			scope.upload = jasmine.createSpy("upload");
			scope.$apply();
			scope.$apply();
			expect(scope.upload).toHaveBeenCalledWith(undefined);
		});

		it("should upload with 2 valid files", function() {
			var files = [{
				Name: "First file"
			}, {
				Name: "Second file"
			}];
			var length = scope.model.topicContent.length;
			scope.upload(files);
			expect(scope.model.topicContent.length).toEqual(length + 2);
		});

		it("should upload invalid files", function() {
			var length = scope.model.topicContent.length;
			scope.upload(undefined);
			expect(scope.model.topicContent.length).toEqual(length);
		});
	});
});
