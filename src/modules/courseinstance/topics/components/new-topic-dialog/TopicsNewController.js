"use strict";

angular.module("topicsApp").controller("TopicsNewController",
function TopicsNewController($scope, $stateParams, Upload, topicObject,
		dateTimeService, iconFinderService, CourseInstanceService) {

	$scope.getRightDate = function getRightDate() {
		if (topicObject) {
			return moment(topicObject.DatePublished).format(dateTimeService.dateFormat());
		} else {
			return moment().format(dateTimeService.dateFormat());
		}
	};

	$scope.model = {
		topicContent:                 [],
		isNewTopic:                   true,
		showErrors:                   false,
		isValidDate:                  true,
		topicName:                    "",
		newLinkName:                  "",
		newLinkLink:                  "",
		hasDiscussionBoard:           true,
		discussionBoardCheckDisabled: false,
		datePublished:                $scope.getRightDate(),
		editOutFile:                  function editOutFile(index) {
			this.topicContent.splice(index, 1);
		}
	};

	// Way of working with existing content
	if (topicObject) {
		$scope.model.isNewTopic = false;
		$scope.model.topicName  = topicObject.Title;
		$scope.model.ID = topicObject.ID;

		// For date validation
		$scope.model.oldDatePublished = topicObject.DatePublished;

		$scope.model.hasDiscussionBoard = topicObject.HasDiscussionBoard;
		if (topicObject.HasDiscussionBoard) {
			$scope.model.discussionBoardCheckDisabled = true;
		}
		for (var i = 0; i < topicObject.Items.length; i++) {
			$scope.model.topicContent.push({
				title: topicObject.Items[i].Title,
				isMain: topicObject.Items[i].IsMain,
				isLink: (topicObject.Items[i].FileName ? false : true),
				file:  {
					id: topicObject.Items[i].ID,
					name: (topicObject.Items[i].FileName ? topicObject.Items[i].FileName : topicObject.Items[i].Url)
				}
			});
		}
	}

	$scope.$watch("files", function() {
		$scope.upload($scope.files);
	});

	$scope.upload = function upload(files) {
		if (files && files.length) {

			var isFirst = $scope.model.topicContent.length === 0;
			for (var i = 0; i < files.length; i++) {
				$scope.model.topicContent.push({
					title:  "",
					file:   files[i],
					isMain: isFirst
				});
				isFirst = false;
			}
		}
	};

	$scope.saveLink = function saveLink() {
		if (!$scope.model.newLinkName || !$scope.model.newLinkLink) {
			$scope.showLinkErrors = true;
			return;
		} else {
			$scope.showLinkErrors = false;

			// If the link doesn't contain http then add http
			// This is to prevent the link being interpreted as relative
			if ($scope.model.newLinkLink.indexOf("http") > -1 === false) {
				$scope.model.newLinkLink = "http://" + $scope.model.newLinkLink;
			}

			$scope.model.topicContent.unshift({
				title: $scope.model.newLinkName,
				file:  {
					name: $scope.model.newLinkLink
				},
				isLink: true
			});
		}
		$scope.model.newLinkName = "";
		$scope.model.newLinkLink = "";
	};

	$scope.enterInLink = function enterInLink(keyEvent) {
		if (keyEvent.which === 13) {
			$scope.saveLink();
		}
	};

	// Returns different object for new topic and existing topic
	$scope.buildReturnObject = function buildReturnObject(obj) {
		var viewed;
		var links       = [];
		var attachments = [];
		var files       = [];
		if (!topicObject) {
			viewed = false;
		} else {
			viewed = topicObject.viewed;
		}

		// Split the content into links and attachments
		angular.forEach($scope.model.topicContent, function(content) {

			if (content.isLink) {
				// Format the link object correctly
				links.push({
					Title:  content.title,
					Url:    content.file.name,
					IsMain: content.isMain,
					Type:   iconFinderService.getType(content.file.name),
					ID:     content.file.id
				});
			} else {
				files.push(content);
				attachments.push({
					Title:            content.title,
					FriendlyFilename: content.file.name,
					IsMain:           content.isMain,
					Type:             iconFinderService.getType(content.file.name),
					ID:               content.file.id
				});
			}
		});
		var returnObj =  {
			Title:              $scope.model.topicName,
			Viewed:             viewed,
			files:              files,
			Links:              links,
			Attachments:        attachments,
			HasDiscussionBoard: $scope.model.hasDiscussionBoard,
			DatePublished:      dateTimeService.momentToISO(moment($scope.model.datePublished, dateTimeService.dateFormat()))
		};

		if (!$scope.model.isNewTopic) {
			returnObj.ID = $scope.model.ID;
		}
		return returnObj;
	};

	$scope.chD = function chD() {
		CourseInstanceService.getCurrentCourse().then(function(course) {
			var topicDate = dateTimeService.getComparableDate($scope.model.datePublished);
			var courseBegin = dateTimeService.getComparableDate(course.DateBegin);
			var courseEnd = dateTimeService.getComparableDate(course.DateEnd);
			if (topicDate >= courseBegin && topicDate <= courseEnd) {
				$scope.model.isValidDate = true;
			} else {
				$scope.model.isValidDate = false;
			}
		});
	};

	$scope.validFileNames = function validFileNames() {
		for (var i = 0; i < $scope.model.topicContent.length; i++) {
			if (!$scope.model.topicContent[i].title) {
				return false;
			}
		}
		return true;
	};

	$scope.saveTopic = function saveTopic() {
		$scope.model.showErrors = true;
		if (!$scope.model.topicName) {
			return;
		}
		if (!$scope.model.isValidDate) {
			return;
		}
		if (!$scope.model.topicContent.length) {
			return;
		}
		if (!$scope.validFileNames()) {
			return;
		}
		$scope.$close($scope.buildReturnObject($scope.model));
	};
});
