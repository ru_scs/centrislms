"use strict";

describe("Testing createNewTopicContent directive", function() {
	var scope, compile, element, httpBackend;

	beforeEach(module("topicsApp", "sharedServices", "mockConfig"));
	beforeEach(inject(function($rootScope, $compile, $httpBackend) {
		element = angular.element(
				"<create-new-topic-content content='content' index='index' model='model'> </create-new-topic-content>"
		);
		$httpBackend.when("GET", "courseinstance/topics/components/new-topic-dialog/create-new-topic-content.tpl.html")
			.respond("<div></div>");
		scope = $rootScope.$new();
		scope.content = {
			title: "name",
			file: {
				name: "http://www.url.com"
			},
			isLink: true,
			isMain: false
		};
		compile = $compile(element)(scope);
		httpBackend = $httpBackend;
	}));

	it("should define the directive", function() {
		expect(compile).toBeDefined();
		httpBackend.flush();
	});
});
