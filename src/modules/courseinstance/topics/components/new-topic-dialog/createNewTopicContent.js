"use strict";

angular.module("topicsApp").directive("createNewTopicContent", function (iconFinderService) {
	return {
		restrict: "E",
		templateUrl: "courseinstance/topics/components/new-topic-dialog/create-new-topic-content.tpl.html",
		scope: {
			content: "=",
			index:   "=",
			model:   "="
		},
		link: function(scope, attrs, elem) {
			// TODO - decide if we use the other identifier
			scope.item = iconFinderService.getType(scope.content.file.name);
		}
	};
});