"use strict";
angular.module("topicsApp")
.factory("MockLecturesResource", function (CentrisResource) {

	// Content
	var mockLectureContent1 = {
		title: "Video from the lecture",
		name: "https://www.youtube.com/watch?v=C4m79m-hzfc",
		type: "yt",
		typeTip: "YouTube"
	};
	var mockLectureContent2 = {
		title: "Slides from the lecture on google drive",
		name: "https://goo.gl/8NMqEr",
		type: "drive",
		typeTip: "GoogleDrive"
	};
	var mockLectureContent3 = {
		title: "Slides on pdf form",
		name: "https://www.youtube.com/watch?v=C4m79m-hzfc",
		type: "pdf",
		typeTip: "pdf"
	};
	var mockLectureContent4 = {
		title: "Slides on power point form",
		name: "https://www.youtube.com/watch?v=C4m79m-hzfc",
		type: "ppt",
		typeTip: "PowerPoint"
	};

	// Lectures
	var mockLecture1  = {
		dateImported: new Date(2015, 0, 12),
		viewed: false,
		content:  [mockLectureContent1, mockLectureContent2, mockLectureContent3, mockLectureContent4],
		title: "1.1 Introduction to Javascript",
		editable: true
	};
	var mockLecture2  = {
		dateImported: new Date(2015, 0, 13),
		viewed: false,
		content:  [mockLectureContent1, mockLectureContent2, mockLectureContent3, mockLectureContent4],
		title: "1.2 Javascript functions",
		editable: true
	};
	var mockLecture3  = {
		dateImported: new Date(2015, 0, 14),
		viewed: false,
		content:  [mockLectureContent1, mockLectureContent2, mockLectureContent3, mockLectureContent4],
		title: "1.3 Javascript arrays",
		editable: true
	};
	var mockLecture4  = {
		dateImported: new Date(2015, 0, 15),
		viewed: false,
		content:  [mockLectureContent1, mockLectureContent2, mockLectureContent3, mockLectureContent4],
		title: "2.1 Introduction to Html5",
		editable: true};
	var mockLecture5  = {
		dateImported: new Date(2015, 0, 16),
		viewed: false,
		content:  [mockLectureContent1, mockLectureContent2, mockLectureContent3, mockLectureContent4],
		title: "2.2 Html5 Canvas",
		editable: true
	};
	var mockLecture6  = {
		dateImported: new Date(2015, 0, 17),
		viewed: false,
		content:  [mockLectureContent1, mockLectureContent2, mockLectureContent3, mockLectureContent4],
		title: "2.3 Html5 Forms",
		editable: true
	};
	var mockLecture7  = {
		dateImported: new Date(2015, 0, 18),
		viewed: false,
		content:  [mockLectureContent1, mockLectureContent2, mockLectureContent3, mockLectureContent4],
		title: "2.4 Html5 Audio",
		editable: true
	};
	var mockLecture8  = {
		dateImported: new Date(2015, 0, 19),
		viewed: false, content:  [mockLectureContent1, mockLectureContent2, mockLectureContent3, mockLectureContent4],
		title: "2.5 Html5 Video",
		editable: true
	};
	var mockLecture9  = {
		dateImported: new Date(2015, 0, 20),
		viewed: false,
		content:  [mockLectureContent1, mockLectureContent2, mockLectureContent3, mockLectureContent4],
		title: "3.1 Introduction to C#",
		editable: true
	};
	var mockLecture10 = {
		dateImported: new Date(2015, 0, 21),
		viewed: false,
		content:  [mockLectureContent1, mockLectureContent2, mockLectureContent3, mockLectureContent4],
		title: "3.2 C# rules and regulations",
		editable: true
	};
	var mockLecture11 = {
		dateImported: new Date(2015, 0, 22),
		viewed: false,
		content:  [mockLectureContent1, mockLectureContent2, mockLectureContent3, mockLectureContent4],
		title: "3.3 C# controllers",
		editable: true
	};

	// All Lectures
	var mockLectures = {
		lectures: []
	};

	mockLectures.lectures.push(mockLecture1);
	mockLectures.lectures.push(mockLecture2);
	mockLectures.lectures.push(mockLecture3);
	mockLectures.lectures.push(mockLecture4);
	mockLectures.lectures.push(mockLecture5);
	mockLectures.lectures.push(mockLecture6);
	mockLectures.lectures.push(mockLecture7);
	mockLectures.lectures.push(mockLecture8);
	mockLectures.lectures.push(mockLecture9);
	mockLectures.lectures.push(mockLecture10);
	mockLectures.lectures.push(mockLecture11);

	return {
		getLectures: function getLectures(courseInstanceID) {
			return mockLectures.lectures;
		},
		getLecture: function getLecture(id) {
			return mockLectures.lectures[id];
		}
	};
});
