"use strict";

describe("Testing TopicResource", function () {
	var TopicsResource, mock, mockTopicObject;

	beforeEach(module("topicsApp", "sharedServices", "courseInstanceShared"));

	beforeEach(module("topicsApp", function($provide) {
		mock = {};
		mock.get = jasmine.createSpy();
		mock.put = jasmine.createSpy();
		mock.post = jasmine.createSpy();
		mock.remove = jasmine.createSpy();
		$provide.value("CentrisResource", mock);

		mockTopicObject = {
			title: "name",
			files: [{
				file: {
					name: "http://www.url1.com"
				}
			},{
				file: {
					name: "http://www.url2.com"
				}
			}],
			isLink: true
		};
	}));

	beforeEach(inject(function(_TopicsResource_) {
		TopicsResource = _TopicsResource_;
		spyOn(TopicsResource, "getAllTopicByCourseInstanceID").and.callThrough();
		spyOn(TopicsResource, "getTopicInTopic").and.callThrough();
		spyOn(TopicsResource, "editTopic").and.callThrough();
		spyOn(TopicsResource, "createNewTopic").and.callThrough();
		spyOn(TopicsResource, "deleteTopic").and.callThrough();
		spyOn(TopicsResource, "topicViewed").and.callThrough();
	}));

	it("should call get all topics", function() {
		TopicsResource.getAllTopicByCourseInstanceID();
		expect(TopicsResource.getAllTopicByCourseInstanceID).toHaveBeenCalled();
		expect(mock.get).toHaveBeenCalled();
	});

	it("should call get topic in topic", function() {
		TopicsResource.getTopicInTopic();
		expect(TopicsResource.getTopicInTopic).toHaveBeenCalled();
		expect(mock.get).toHaveBeenCalled();
	});

	it("should call edit topic with an undefined object", function() {
		TopicsResource.editTopic();
		expect(TopicsResource.editTopic).toHaveBeenCalled();
		expect(mock.put).toHaveBeenCalled();
	});

	it("should call edit topic with a valid object", function() {
		TopicsResource.editTopic(1, 1, mockTopicObject);
		expect(TopicsResource.editTopic).toHaveBeenCalledWith(1, 1, mockTopicObject);
		expect(mock.put).toHaveBeenCalled();
	});

	it("should call edit topic with an object with no files", function() {
		mockTopicObject.files = [];
		TopicsResource.editTopic(2, 2, mockTopicObject);
		expect(TopicsResource.editTopic).toHaveBeenCalledWith(2, 2, mockTopicObject);
		expect(mock.put).toHaveBeenCalled();
	});

	it("should call create topic", function() {
		mockTopicObject.files = [];
		TopicsResource.createNewTopic(3, mockTopicObject);
		expect(TopicsResource.createNewTopic).toHaveBeenCalledWith(3, mockTopicObject);
		expect(mock.post).toHaveBeenCalled();
	});

	it("should call create topic", function() {
		TopicsResource.createNewTopic(4, mockTopicObject);
		expect(TopicsResource.createNewTopic).toHaveBeenCalledWith(4, mockTopicObject);
		expect(mock.post).toHaveBeenCalled();
	});

	it("should call delete topic", function() {
		TopicsResource.deleteTopic(4, mockTopicObject);
		expect(TopicsResource.deleteTopic).toHaveBeenCalledWith(4, mockTopicObject);
		expect(mock.remove).toHaveBeenCalled();
	});

	it("should call topic viewed", function() {
		TopicsResource.topicViewed(4, 3).success(function() {
			expect(TopicsResource.topicViewed).toHaveBeenCalledWith(4, 3);
		}).error(function() {
		});
	});
});
