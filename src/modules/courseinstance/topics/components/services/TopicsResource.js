"use strict";

angular.module("topicsApp")
.factory("TopicsResource", function (CentrisResource) {
	return {
		getAllTopicByCourseInstanceID: function getAllTopicByCourseInstanceID(courseID) {
			return CentrisResource.get("courses", ":courseInstanceID/topics", {courseInstanceID: courseID});
		},
		getTopicInTopic: function getTopicInTopic(courseinstanceID, topicID) {
			var param = {
				courseinstanceID: courseinstanceID,
				topicID: topicID
			};
			return CentrisResource.get("courses", ":courseinstanceID/topics/:topicID", param);
		},
		editTopic: function editTopic(courseInstanceID, topicID, topicObject) {
			var param = {
				courseInstanceID: courseInstanceID,
				topicID:          topicID
			};

			if (topicObject === undefined) {
				// Undo delete
				return CentrisResource.put("courses", ":courseInstanceID/topics/:topicID", param);
			} else {
				var fd = new FormData();
				fd.append("model", JSON.stringify(topicObject));

				if (topicObject.files.length > 0) {
					for (var i = 0 ; i < topicObject.files.length ; i++) {
						fd.append("File" + i, topicObject.files[i].file);
					}
				}
				return CentrisResource.put("courses", ":courseInstanceID/topics/:topicID", param, fd, true);
			}
		},
		createNewTopic: function createNewTopic(courseInstanceID, topicObject) {
			var fd = new FormData();
			fd.append("model", JSON.stringify(topicObject));

			if (topicObject.files.length > 0) {
				for (var i = 0 ; i < topicObject.files.length ; i++) {
					fd.append("File" + i, topicObject.files[i].file);
				}
			}
			return CentrisResource.post("courses", ":courseInstanceID/topics", { courseInstanceID: courseInstanceID }, fd, true);
		},
		deleteTopic: function deleteTopic(courseInstanceID, topicObject) {
			var param = {
				courseInstanceID: courseInstanceID,
				topicID:          topicObject.ID
			};
			return CentrisResource.remove("courses", ":courseInstanceID/topics/:topicID", param);
		},
		topicViewed: function topicViewed(courseInstanceID, index) {
			return {
				// MockTopicsResource.addNew(topicObject);
				success: function(fn) {
					fn();
					return {
						error: function(fn) {
							return;
						}
					};
				}
			};
		},
	};
});
