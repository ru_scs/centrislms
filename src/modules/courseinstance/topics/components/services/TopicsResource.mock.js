"use strict";
angular.module("topicsApp")
.factory("MockTopicsResource", function (MockLecturesResource) {
	return {
		mockTopicsResources: function mockTopicsResources(getTopics, topicViewed,
				deleteTopic, editTopic, createNewTopic, getTopicInTopic) {
			return {
				getAllTopicByCourseInstanceID: function(id) {
					return {
						success: function(fn) {
							if (getTopics) {
								fn(MockLecturesResource.getLectures(id));
							}
							return {
								error: function(fn) {
									if (!getTopics) {
										fn();
									}
								}
							};
						}
					};
				},
				topicViewed: function(id, index) {
					return {
						success: function(fn) {
							if (topicViewed) {
								fn(MockLecturesResource.getLectures(id), index);
							}
							return {
								error: function(fn) {
									if (!topicViewed) {
										fn();
									}
								}
							};
						}
					};
				},
				deleteTopic: function(index) {
					return {
						success: function(fn) {
							if (deleteTopic) {
								fn(index, MockLecturesResource.getLectures(index));
							}
							return {
								error: function(fn) {
									if (!deleteTopic) {
										fn();
									}
								}
							};
						}
					};
				},
				editTopic: function(index) {
					return {
						success: function(fn) {
							if (editTopic) {
								fn(index, MockLecturesResource.getLectures(index));
							}
							return {
								error: function(fn) {
									if (!editTopic) {
										fn();
									}
								}
							};
						}
					};
				},
				createNewTopic: function(courseId, obj) {
					return {
						success: function(fn) {
							if (createNewTopic) {
								fn(courseId, obj);
							}
							return {
								error: function(fn) {
									if (!createNewTopic) {
										fn();
									}
								}
							};
						}
					};
				},
				getTopicInTopic: function(courseId, topicId) {
					return {
						success: function(fn) {
							var topic;
							if (getTopicInTopic === 0) {
								topic = MockLecturesResource.getLecture(1);
								topic.Items = [{
									Type: "yt",
									Url: "https://youtube.com",
									IsMain: true
								}];
								fn(topic);
							} else if (getTopicInTopic === 1) {
								topic = MockLecturesResource.getLecture(1);
								topic.Items = [{
									Type: "yt",
									Url: "https://youtube.com",
									IsMain: false
								}];
								fn(topic);
							}
							return {
								error: function(fn) {
									if (getTopicInTopic === 2) {
										fn();
									}
								}
							};
						}
					};
				}
			};
		 }
	};
});
