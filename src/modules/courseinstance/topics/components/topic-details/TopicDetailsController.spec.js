"use strict";

describe("TopicDetailsController", function() {
	var ctrl, scope, mockCentrisNotify, mockTopicsResource,
			mockWindow, mockLectures;

	mockWindow = {
		location: {
			href: "url"
		}
	};
	beforeEach(module("topicsApp", "sharedServices", "mockConfig"));

	beforeEach(inject(function($rootScope, $injector) {
		scope = $rootScope.$new();
		mockCentrisNotify = $injector.get("mockCentrisNotify");
		mockTopicsResource = $injector.get("MockTopicsResource");
		mockLectures = $injector.get("MockLecturesResource");
		mockCentrisNotify.error = jasmine.createSpy("error");
	}));

	describe("using the success function for getTopicInTopic and item is main", function() {
		beforeEach(inject(function($controller) {
			ctrl = $controller("TopicDetailsController", {
				$scope: scope,
				TopicsResource: mockTopicsResource.mockTopicsResources(true, true, true, true, true, 0),
				centrisNotify: mockCentrisNotify,
				$window: mockWindow
			});
		}));

		it("should return correct status", function() {
			var testStatus = {
				isFirstOpen: true,
				isFirstDisabled: false
			};
			expect(scope.status).toEqual(testStatus);
		});

		it("should change location to item url", function() {
			var item = {
				Type: "invalid",
				Url: "test"
			};
			scope.itemClick(item);
			expect(mockWindow.location.href).toEqual(item.Url);
		});

		it("should not change location because setEmbed works", function() {
			var item = {
				Type: "yt",
				Url: "test"
			};
			var tempUrl = mockWindow.location.href;
			scope.itemClick(item);
			expect(mockWindow.location.href).toEqual(tempUrl);
		});

		describe("testing the setEmbed function", function() {
			beforeEach(inject(function($controller) {
				ctrl = $controller("TopicDetailsController", {
					$scope: scope,
					TopicsResource: mockTopicsResource.mockTopicsResources(true, true, true, true, true, 0),
					centrisNotify: mockCentrisNotify
				});
			}));

			it("should return true if the correct item is used", function() {
				expect(scope.setEmbed({Type: "yt"})).toBe(true);
				expect(scope.setEmbed({Type: "vimeo"})).toBe(true);
				expect(scope.setEmbed({Type: "drive", Url: "presentation"})).toBe(true);
				expect(scope.setEmbed({Type: "ppt"})).toBe(true);
				expect(scope.setEmbed({Type: "mp4"})).toBe(true);
				expect(scope.setEmbed({Type: "webm"})).toBe(true);
				expect(scope.setEmbed({Type: "ogg"})).toBe(true);
			});

			it("should return false if the item is invalid", function() {
				expect(scope.setEmbed({Type: "invalid"})).toBe(false);
			});
		});
		describe("testing the noPreview function", function() {
			beforeEach(inject(function($controller) {
				ctrl = $controller("TopicDetailsController", {
					$scope: scope,
					TopicsResource: mockTopicsResource.mockTopicsResources(true, true, true, true, true, 0),
					centrisNotify: mockCentrisNotify
				});
			}));

			it("should return false if item Type is in supportedTypes", function() {
				expect(scope.noPreview({Type: "yt"})).toBe(false);
				expect(scope.noPreview({Type: "vimeo"})).toBe(false);
				expect(scope.noPreview({Type: "mp4"})).toBe(false);
			});

			it("should return true if item Type is not in supportedTypes", function() {
				expect(scope.noPreview({Type: "vmw"})).toBe(true);
			});
		});

		describe("testing the else statement when isMain is false", function() {
			beforeEach(inject(function($controller) {
				ctrl = $controller("TopicDetailsController", {
					$scope: scope,
					TopicsResource: mockTopicsResource.mockTopicsResources(true, true, true, true, true, 1)
				});
			}));

			it("should set scope.loadingData to false", function() {
				expect(scope.loadingData).toBe(false);
			});
		});
	});

	describe("testing the error function in TopicInTopic", function() {
		beforeEach(inject(function($controller) {
			mockCentrisNotify.error = jasmine.createSpy("error");
			ctrl = $controller("TopicDetailsController", {
				$scope: scope,
				TopicsResource: mockTopicsResource.mockTopicsResources(true, true, true, true, true, 2),
				centrisNotify: mockCentrisNotify
			});
		}));

		it("should set call centrisNoify error with ErrorLoadingTopic", function() {
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("topics.ErrorLoadingTopic");
		});
	});
});
