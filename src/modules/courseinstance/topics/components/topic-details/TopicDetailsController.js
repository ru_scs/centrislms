"use strict";

angular.module("topicsApp").controller("TopicDetailsController",
function TopicDetailsController ($scope, centrisNotify, $stateParams,
	$state, $location, $window, $sce, TopicsResource, iconFinderService) {
	$scope.theVideo           = "."; // If it is empty it results in a runtime error
	$scope.videoFile          = ".";
	$scope.topicid            = $stateParams.topicid;
	$scope.hasDiscussionBoard = false;
	$scope.courseInstanceID   = $stateParams.courseInstanceID;
	$scope.embed              = {
		yt:     false,
		gSlide: false,
		ppt:    false,
		videoFile:    false
	};

	$scope.loadingData = true;

	// Controls whether only one accordion compartment is open at a time
	$scope.oneAtATime = true;

	// Controls whether main item is open & disabled on load
	$scope.status = {
		isFirstOpen: true,
		isFirstDisabled: false
	};

	// Item types that can be previewed in accordion
	var supportedTypes = [
		"yt",
		"vimeo",
		"mp4"
	];

	// Set the embed elements for the given item if supported
	// returns true if successfull, false otherwhise
	$scope.setEmbed = function setEmbed(item) {
		var url;
		if (item.Type === "yt" || item.Type === "vimeo") {
			$scope.theVideo     = item.Url;
			$scope.embed.yt     = true;
			$scope.embed.gSlide = false;
			$scope.embed.ppt    = false;
			$scope.embed.videoFile	= false;
			return true;
		} else if (item.Type === "drive" && item.Url.indexOf("presentation") !== -1) {
			var indexOfPub = item.Url.indexOf("pub");
			url = item.Url.substr(0, indexOfPub) + "embed" + item.Url.substr(indexOfPub + 3);

			$scope.thePresentation = $sce.trustAsResourceUrl(url);
			$scope.embed.gSlide    = true;
			$scope.embed.yt        = false;
			$scope.embed.ppt       = false;
			return true;
		} else if (item.Type === "ppt" ||
			item.Type === "pptx" ||
			item.Type === "doc" ||
			item.Type === "docx" ||
			item.Type === "xls" ||
			item.Type === "xlsx") {
			// TODO: this won't work on the dev.nem subnet, since it is not accessible
			// from the outside world, but the office app needs that kind of access!

			var officeApps = "http://view.officeapps.live.com/op/view.aspx?src=";
			var content = "https://dl.dropboxusercontent.com/u/368343/GLU1/2006-01-10.ppt";
			url = officeApps + $window.encodeURIComponent(content);

			$scope.thePresentation = $sce.trustAsResourceUrl(url);
			$scope.embed.gSlide    = false;
			$scope.embed.yt        = false;
			$scope.embed.ppt       = true;
			return true;
			// TODO: add pdf preview support!

		} else if (item.Type === "mp4" || item.Type === "webm" || item.Type === "ogg") {

			$scope.videoFile    = $sce.trustAsResourceUrl(item.Url);
			$scope.embed.yt     = false;
			$scope.embed.gSlide = false;
			$scope.embed.ppt    = false;
			$scope.embed.videoFile    = true;

			return true;

		} else {
			return false;
		}
	};

	// Fetch the data and if there is a main content embed it
	TopicsResource.getTopicInTopic($scope.courseInstanceID, $scope.topicid)
	.success(function(topic) {
		$scope.topic = topic;
		$scope.hasDiscussionBoard = topic.HasDiscussionBoard;
		angular.forEach(topic.Items, function(item) {
			if (item.IsMain) {
				$scope.setEmbed(item);
			}
		});
		$scope.loadingData = false;
	}).error(function() {
		centrisNotify.error("topics.ErrorLoadingTopic");
		$scope.loadingData = false;
	});

	// When a topic item is clicked, try to embed, else redirect
	$scope.itemClick = function(item) {
		if (!$scope.setEmbed(item)) {
			$window.location.href = item.Url;
		}
	};

	// Handle items that can't be displayed in accordion, header stays shut if true
	$scope.noPreview = function(item) {
		if (_.includes(supportedTypes, item.Type)) {
			return false;
		} else {
			return true;
		}
	};
});
