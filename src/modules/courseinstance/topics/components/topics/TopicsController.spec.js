"use strict";

describe("TopicsController", function() {
	var ctrl, scope, rootScope, mockCourseInstanceStudent, mockCourseInstanceTeacher,
			mockStateParams, mockCentrisNotify, mockTopicDlg, mockCourseInstance,
			mockTopicsResource, mockTopics, mockLectures, mockState;

	beforeEach(module("topicsApp", "sharedServices", "mockConfig",
			"courseInstanceShared"));

	beforeEach(inject(function($rootScope, $injector) {
		rootScope = $rootScope;
		scope = $rootScope.$new();
		mockLectures = $injector.get("MockLecturesResource");
		mockCentrisNotify = $injector.get("mockCentrisNotify");
		mockTopicsResource = $injector.get("MockTopicsResource");
		mockCourseInstance = $injector.get("MockCourseInstanceRoleService");
		mockCentrisNotify.error = jasmine.createSpy("error");
	}));

	describe("Testing as teacher", function() {
		mockStateParams = {
			courseInstanceID: 1
		};

		mockState = {};

		mockTopicDlg = {
			show: function(obj) {
				return {
					then: function(fn) {
						fn(obj);
					}
				};
			}
		};

		describe("using success function for getting topics", function() {

			beforeEach(inject(function($controller) {
				mockCentrisNotify.successWithUndo = jasmine.createSpy("successWithUndo");
				mockCentrisNotify.success = jasmine.createSpy("success");
				ctrl = $controller("TopicsController", {
					$scope: scope,
					$stateParams: mockStateParams,
					TopicsNewDlg: mockTopicDlg,
					TopicsResource: mockTopicsResource.mockTopicsResources(true, true, true, true, true),
					CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(true),
					centrisNotify: mockCentrisNotify
				});
			}));

			it("should assign true to isTeacher if user is a teacher", inject(function($controller) {
				expect(scope.isTeacher).toBe(true);
			}));

			it("should get all topics and set loading to false", function() {
				expect(scope.allTopics).toEqual(mockLectures.getLectures(1));
				expect(scope.loadingData).toBe(false);
			});

			it("should mark a topic as viewed", function() {
				var results = mockLectures.getLecture(0).viewed;
				scope.markAsViewed(0);
				expect(results).toEqual(!mockLectures.getLecture(0).viewed);
			});

			it("should delete a specified topic", function() {
				var undoParam = {
					type: "topic-delete",
					id:   {
						topic:    scope.allTopics[1].ID,
						instance: scope.courseInstanceID,
						index:    1
					}
				};
				var results = mockLectures.getLectures().length;
				scope.deleteTopic(1);
				expect(scope.allTopics.length).toBe(results - 1);
				expect(mockCentrisNotify.successWithUndo)
						.toHaveBeenCalledWith("topics.Topics_Delete_Success", undoParam);
			});

			it("should perform no action with wrong param type", function() {
				var param = {
					id: {
						instance: 2,
						topic: 1
					},
					type: "topic-other"
				};
				spyOn(rootScope, "$broadcast").and.callThrough();
				scope.$broadcast("centrisUndo", param);
				expect(rootScope.$broadcast).toHaveBeenCalled();
				expect(mockCentrisNotify.success).not.toHaveBeenCalled();
				expect(mockCentrisNotify.error).not.toHaveBeenCalled();
			});

			// TODO: re-enable and fix the test bug!!!
			// "TypeError: Attempted to assign to readonly property."
			xit("should open a dialog for edit topic", function() {
				scope.editTopic(3);
				expect(mockCentrisNotify.success)
						.toHaveBeenCalledWith("topics.Topics_Edit_Success");
			});

			// Same here, fix the same error as above.
			xit("should display success on undo delete", function() {
				var param = {
					id: {
						instance: 2,
						topic: 1
					},
					type: "topic-delete"
				};
				spyOn(rootScope, "$broadcast").and.callThrough();
				scope.$broadcast("centrisUndo", param);
				expect(rootScope.$broadcast).toHaveBeenCalled();
				expect(mockCentrisNotify.success)
						.toHaveBeenCalledWith("topics.Topics_Undo_Delete_Success");
			});

			it("should open a dialog to create a new topic", function() {
				scope.newTopics();
				expect(mockCentrisNotify.success)
						.toHaveBeenCalledWith("topics.Topics_Save_Lect");
			});

			describe("using the error function for topic delete", function() {
				beforeEach(inject(function($controller) {
					ctrl = $controller("TopicsController", {
						$scope: scope,
						$stateParams: mockStateParams,
						TopicsResource: mockTopicsResource.mockTopicsResources(true, true, false, true, true),
						CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(true),
						centrisNotify: mockCentrisNotify
					});
				}));

				it("should call error in centrisNotify when it fails to delete a topic", function() {
					scope.deleteTopic(1);
					expect(mockCentrisNotify.error)
							.toHaveBeenCalledWith("topics.Topics_Delete_Error");
				});
			});

			describe("using the error function for topic viewed", function() {
				beforeEach(inject(function($controller) {
					ctrl = $controller("TopicsController", {
						$scope: scope,
						$stateParams: mockStateParams,
						TopicsResource: mockTopicsResource.mockTopicsResources(true, false, true, true, true),
						CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(true),
						centrisNotify: mockCentrisNotify
					});
				}));

				it("should call error function when it fails to mark topic as viewed", function() {
					scope.markAsViewed(0);
					expect(mockCentrisNotify.error)
							.toHaveBeenCalledWith("topics.Topic_Marking_Topic_As_Viewed_error");
				});
			});
		});

		describe("using the error function for edit topic", function() {
			beforeEach(inject(function($controller) {
				ctrl = $controller("TopicsController", {
					$scope: scope,
					$stateParams: mockStateParams,
					TopicsNewDlg: mockTopicDlg,
					TopicsResource: mockTopicsResource.mockTopicsResources(true, true, true, false, true),
					CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(true),
					centrisNotify: mockCentrisNotify
				});
			}));

			it("should display error if edit topic fails", function() {
				scope.editTopic(1);
				expect(mockCentrisNotify.error)
						.toHaveBeenCalledWith("topics.Topics_Edit_Error");
			});

			it("should display success on undo delete", function() {
				var param = {
					id: {
						instance: 2,
						topic: 1
					},
					type: "topic-delete"
				};
				spyOn(rootScope, "$broadcast").and.callThrough();
				scope.$broadcast("centrisUndo", param);
				expect(rootScope.$broadcast).toHaveBeenCalled();
				expect(mockCentrisNotify.error)
						.toHaveBeenCalledWith("topics.Topics_Undo_Delete_Error");
			});
		});

		describe("using the error function for new topic", function() {
			beforeEach(inject(function($controller) {
				ctrl = $controller("TopicsController", {
					$scope: scope,
					$stateParams: mockStateParams,
					TopicsNewDlg: mockTopicDlg,
					TopicsResource: mockTopicsResource.mockTopicsResources(true, true, true, true, false),
					CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(true),
					centrisNotify: mockCentrisNotify
				});
			}));

			it("should display error if new topic fails", function() {
				scope.newTopics();
				expect(mockCentrisNotify.error)
						.toHaveBeenCalledWith("topics.Topics_Save_Lect_Error");
			});
		});

		describe("using the error function for getting all topics", function() {

			beforeEach(inject(function($controller) {
				ctrl = $controller("TopicsController", {
					$scope: scope,
					$stateParams: mockStateParams,
					TopicsResource: mockTopicsResource.mockTopicsResources(false, true, true, true, true),
					CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(true),
					centrisNotify: mockCentrisNotify
				});
			}));

			it("should give error, set loading to false, call centrisNotify with error message", function() {
				expect(scope.loadingData).toBe(false);
				expect(mockCentrisNotify.error).toHaveBeenCalledWith("topics.Topic_Get_All_Topics_error");
			});
		});

	});

	describe("Student Intro", function() {

		var translate, translations = [];

		beforeEach(function() {
			translate = function mockTranslating(keys) {
				for (var i = 0; i < keys.length; i++) {
					translations[keys[i]] = keys[i];
				}
				return {
					then: function mockTranslating(fn) {
						fn(translations);
					}
				};
			};
		});

		beforeEach(inject(function($rootScope) {
			scope = $rootScope.$new();
			scope.$on = function(name, fn) {
				scope.ShowMyStudentLectureIntro = jasmine.createSpy();
				scope.ShowMyTeacherLectureIntro = jasmine.createSpy();
				if (name === "onCentrisIntro") {
					fn();
				}
			};
		}));
		describe("state current name === topics", function() {
			beforeEach(function() {
				mockState = {
					current: {
						name: "courseinstance.topics"
					}
				};
			});

			beforeEach(inject(function($rootScope, $controller, $state) {
				ctrl = $controller("TopicsController", {
					$scope: scope,
					$stateParams: mockStateParams,
					CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(false),
					$state: mockState,
					$translate: translate
				});
			}));

			it("should call ShowMyStudentLectureIntro", function() {
				expect(scope.ShowMyStudentLectureIntro).toHaveBeenCalled();
			});
		});

		describe("Teacher Intro", function() {

			beforeEach(inject(function($rootScope) {
				scope = $rootScope.$new();
				scope.$on = function(name, fn) {
					scope.ShowMyStudentLectureIntro = jasmine.createSpy();
					scope.ShowMyTeacherLectureIntro = jasmine.createSpy();
					scope.isTeacher = true;

					if (name === "onCentrisIntro") {
						fn();
					}
				};
			}));

			describe("state current name === topics", function() {

				beforeEach(function() {
					mockState = {
						current: {
							name: "courseinstance.topics"
						}
					};
				});

				beforeEach(inject(function($rootScope, $controller, $state) {
					ctrl = $controller("TopicsController", {
						$scope: scope,
						$stateParams: mockStateParams,
						CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(false),
						$state: mockState,
						$translate: translate
					});
				}));

				it("should call ShowMyTeacherLectureIntro", function() {
					expect(scope.ShowMyTeacherLectureIntro).toHaveBeenCalled();
				});
			});

			describe("state current name !== topics", function() {

				beforeEach(function() {
					mockState = {
						current: {
							name: "not.courseinstance.topics"
						}
					};
				});

				beforeEach(inject(function($rootScope, $controller, $state) {
					ctrl = $controller("TopicsController", {
						$scope: scope,
						$stateParams: mockStateParams,
						CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(false),
						$state: mockState,
						$translate: translate
					});
				}));

				it("should not call an intro", function() {
					expect(scope.ShowMyStudentLectureIntro).not.toHaveBeenCalled();
					expect(scope.ShowMyTeacherLectureIntro).not.toHaveBeenCalled();
				});
			});

		});

		describe("state current name !== topics", function() {

			beforeEach(function() {
				mockState = {
					current: {
						name: "not.courseinstance.topics"
					}
				};
			});

			beforeEach(inject(function($rootScope, $controller, $state) {
				ctrl = $controller("TopicsController", {
					$scope: scope,
					$stateParams: mockStateParams,
					CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(false),
					$state: mockState,
					$translate: translate
				});
			}));

			it("should not call an intro", function() {
				expect(scope.ShowMyStudentLectureIntro).not.toHaveBeenCalled();
				expect(scope.ShowMyTeacherLectureIntro).not.toHaveBeenCalled();
			});
		});

	});

	describe("Testing as student", function() {
		beforeEach(inject(function($rootScope, $controller, $state) {
			scope = $rootScope.$new();
			ctrl = $controller("TopicsController", {
				$scope: scope,
				$stateParams: mockStateParams,
				CourseInstanceRoleService: mockCourseInstance.mockCourseInstanceRoleService(false),
				$state: mockState
			});
		}));

		it("Should assign false to isTeacher if user is a student", function() {
			expect(scope.isTeacher).toBe(false);
		});

		it("Should call state.go, on the courseinstanceID and selected topic", function() {
			mockState.go = jasmine.createSpy("go");
			scope.topicDetails(1);
			expect(mockState.go).toHaveBeenCalledWith("courseinstance.topics.details",
					{courseInstanceID: scope.courseInstanceID, topicid: 1});
		});
	});
});
