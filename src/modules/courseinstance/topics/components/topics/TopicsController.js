"use strict";

angular.module("topicsApp").controller("TopicsController",
function TopicsController($scope, $state, $stateParams, TopicsNewDlg,
	TopicsResource, centrisNotify, CourseInstanceRoleService, dateTimeService, $translate) {

	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(isTeacher) {
		$scope.isTeacher = isTeacher;
	});
	$scope.courseInstanceID = $stateParams.courseInstanceID;

	$scope.loadingData = true;
	TopicsResource.getAllTopicByCourseInstanceID($scope.courseInstanceID)
	.success(function(data) {
		$scope.allTopics   = data;
		$scope.loadingData = false;
	})
	.error(function() {
		$scope.loadingData = false;
		centrisNotify.error("topics.Topic_Get_All_Topics_error");
	});
	$scope.markAsViewed = function markAsViewed(index) {
		TopicsResource.topicViewed($scope.courseInstanceID, index)
		.success(function(data) {
			$scope.allTopics[index].viewed = !$scope.allTopics[index].viewed;
		})
		.error(function() {
			centrisNotify.error("topics.Topic_Marking_Topic_As_Viewed_error");
		});
	};

	$scope.topicDetails = function topicDetails(topicID) {
		$state.go("courseinstance.topics.details", {courseInstanceID: $scope.courseInstanceID, topicid: topicID});
	};

	$scope.editTopic = function editTopics(index) {
		TopicsNewDlg.show($scope.allTopics[index]).then(function(unsavedTopic) {
			TopicsResource.editTopic($scope.courseInstanceID, unsavedTopic.ID, unsavedTopic)
			.success(function(data) {
				$scope.allTopics[index] = data;
				$scope.allTopics[index].ID = unsavedTopic.ID;
				centrisNotify.success("topics.Topics_Edit_Success");
			})
			.error(function() {
				centrisNotify.error("topics.Topics_Edit_Error");
			});
		});
	};

	$scope.deleteTopic = function deleteTopic(index) {
		TopicsResource.deleteTopic($scope.courseInstanceID, $scope.allTopics[index])
		.success(function(data) {
			var undoParam = {
				type: "topic-delete",
				id:   {
					topic:    $scope.allTopics[index].ID,
					instance: $scope.courseInstanceID,
					index:    index
				}
			};

			centrisNotify.successWithUndo("topics.Topics_Delete_Success", undoParam);

			$scope.allTopics.splice(index, 1);
		})
		.error(function(data) {
			centrisNotify.error("topics.Topics_Delete_Error");
		});
	};

	$scope.$on("centrisUndo", function undo(event, param) {
		if (param.type === "topic-delete") {
			TopicsResource.editTopic(param.id.instance, param.id.topic, undefined)
			.success(function(data) {
				data.ID = param.id.topic;
				centrisNotify.success("topics.Topics_Undo_Delete_Success");
				$scope.allTopics.splice(param.id.index, 0, data);
			})
			.error(function(data) {
				centrisNotify.error("topics.Topics_Undo_Delete_Error");
			});
		}
	});

	$scope.newTopics = function newTopics() {
		TopicsNewDlg.show().then(function(unsavedTopic) {
			TopicsResource.createNewTopic($scope.courseInstanceID, unsavedTopic)
			.success(function(data) {
				$scope.allTopics = data;
				centrisNotify.success("topics.Topics_Save_Lect");
			})
			.error(function() {
				centrisNotify.error("topics.Topics_Save_Lect_Error");
			});
		});
	};

	// Starting helper viewer
	// Defines the steps and calls the step id
	$scope.LectureTeacherOptions = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#lecture-teacher-helper-step2",
			position: "right",
			intro: ""
		}, {
		 element: "#lecture-teacher-helper-step3",
		 position: "right",
		 intro: ""
	 }, {
			element: "#lecture-teacher-helper-step4",
			position: "right",
			intro: ""
		}, {
			element: "#lecture-student-helper-step4",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};

	$scope.LectureStudentOptions = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#lecture-teacher-helper-step3",
			position: "right",
			intro: ""
		}, {
			element: "#lecture-student-helper-step4",
			position: "left",
			intro: ""
		}, {
			element: "#courseStep4",
			position: "left",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	// Calling the translate files for each step and putting them into an array
	var teacherIntroKeys = [
		"topics.Helper.Teacher.Step1",
		"topics.Helper.Teacher.Step2",
		"topics.Helper.Teacher.Step3",
		"topics.Helper.Teacher.Step4",
		"topics.Helper.Student.Step3" // Used the same id as in stundent
	];

	var studentIntroKeys = [
		"topics.Helper.Student.Step1",
		"topics.Helper.Student.Step2",
		"topics.Helper.Student.Step3",
		"topics.Helper.Student.Step4"
	];
	// Setting the translates as the text for each step
	$translate(teacherIntroKeys).then(function whenDoneTranslating(translations) {
		$scope.LectureTeacherOptions.steps[0].intro = translations[teacherIntroKeys[0]];
		$scope.LectureTeacherOptions.steps[1].intro = translations[teacherIntroKeys[1]];
		$scope.LectureTeacherOptions.steps[2].intro = translations[teacherIntroKeys[2]];
		$scope.LectureTeacherOptions.steps[3].intro = translations[teacherIntroKeys[3]];
		$scope.LectureTeacherOptions.steps[4].intro = translations[teacherIntroKeys[4]];
	});

	$translate(studentIntroKeys).then(function whenDoneTranslating(translations) {
		$scope.LectureStudentOptions.steps[0].intro = translations[studentIntroKeys[0]];
		$scope.LectureStudentOptions.steps[1].intro = translations[studentIntroKeys[1]];
		$scope.LectureStudentOptions.steps[2].intro = translations[studentIntroKeys[2]];
		$scope.LectureStudentOptions.steps[3].intro = translations[studentIntroKeys[3]];
	});

	// Calling the translate files for each button and putting them into an array
	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	// Setting the translates as the text for each button
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.LectureTeacherOptions.nextLabel = translations[introButtons[0]];
		$scope.LectureTeacherOptions.prevLabel = translations[introButtons[1]];
		$scope.LectureTeacherOptions.skipLabel = translations[introButtons[2]];
		$scope.LectureTeacherOptions.doneLabel = translations[introButtons[3]];
	});

	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.LectureStudentOptions.nextLabel = translations[introButtons[0]];
		$scope.LectureStudentOptions.prevLabel = translations[introButtons[1]];
		$scope.LectureStudentOptions.skipLabel = translations[introButtons[2]];
		$scope.LectureStudentOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.$on("onCentrisIntro", function(event, args) {
		// Only show this if the current route is for our page:
		if ($state.current.name === "courseinstance.topics") {
			if (!$scope.isTeacher) {
				$scope.ShowMyStudentLectureIntro();
			} else {
				$scope.ShowMyTeacherLectureIntro();
			}
		}
	});

});
