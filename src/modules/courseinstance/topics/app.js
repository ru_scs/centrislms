"use strict";

angular.module("topicsApp", ["pascalprecht.translate","ui.router",
	"youtube-embed", "videosharing-embed", "ui.bootstrap.popover"])
.config(function ($stateProvider) {
	$stateProvider.state("courseinstance.topics", {
		url:         "/topics",
		templateUrl: "courseinstance/topics/components/topics/index.html",
		controller:  "TopicsController"
	});
	$stateProvider.state("courseinstance.topics.details", {
		url:         "/:topicid",
		templateUrl: "courseinstance/topics/components/topic-details/topic-details.html",
		controller:  "TopicDetailsController"
	});
});
