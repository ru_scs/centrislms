# Courseinstance details

*	This module displays basic details about the selected course.
*	It renders different views in a tab block which contain more detailed
	information which is controlled by different controllers.

*	The CourseDetailController calls the API server for basic information about the 
	course and renders it dependant on the current locale selected.

*	Theres an implemented basic Karma test for the module but at the time of this 
	being written the karma tests were unrunnable and it couldn't be tested.