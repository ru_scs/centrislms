"use strict";

angular.module("courseDetailsApp").directive("courseDetailsTeachingMethods",
function courseDetailsTeachingMethods() {
	return {
		restrict:      "E",
		scope: {
			course:    "=ngModel",
			loading:   "="
		},
		controller:    "TeachingMethodsController",
		templateUrl:   "courseinstance/details/components/teachingmethods/index.html",
		link: function (scope, element, attributes) {
		}
	};
});