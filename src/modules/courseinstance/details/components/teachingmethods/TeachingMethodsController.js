"use strict";

/**
 * This controller/component takes care of displaying teaching methods for a given
 * course, and allows the teacher to edit it.
 */
angular.module("courseDetailsApp").controller("TeachingMethodsController",
function TeachingMethodsController($scope, $stateParams, CourseDetailsResource,
	centrisNotify, CourseInstanceRoleService) {

	$scope.courseInstanceID = $stateParams.courseInstanceID;

	// Status flags
	$scope.statusFlags = {
		edit: false
	};

	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(isTeacher) {
		$scope.isTeacher       = isTeacher;
	});

	// Toggles editing mode on and off
	$scope.toggleEdit = function toggleEdit() {
		$scope.statusFlags.edit = !$scope.statusFlags.edit;
		if ($scope.statusFlags.edit) {
			// Make a copy of the data, such that if the user
			// cancels the edit, the display will revert to the
			// original.
			$scope.desc = {
				teachingMethods:   $scope.course.TeachingMethods,
				teachingMethodsEN: $scope.course.TeachingMethodsEN
			};
		}
	};

	// Saves course changes
	$scope.save = function save() {
		var model = {
			TeachingMethods:   $scope.desc.teachingMethods,
			TeachingMethodsEN: $scope.desc.teachingMethodsEN,
		};
		CourseDetailsResource.saveTeachingMethods($scope.courseInstanceID, model)
		.success(function() {
			// Ensure the update will be reflected in the UI:
			$scope.course.TeachingMethods   = model.TeachingMethods;
			$scope.course.TeachingMethodsEN = model.TeachingMethodsEN;

			centrisNotify.success("teachingmethods.SaveSuccess");
		}).error(function() {
			centrisNotify.error("teachingmethods.SaveFailed");
		});
	};

	// Saves course changes and toggles edit mode
	$scope.saveAndQuit = function saveAndQuit() {
		$scope.save();
		$scope.toggleEdit();
	};

	// Toggles edit mode without saving
	$scope.quitWithoutSaving = function quitWithoutSaving() {
		$scope.toggleEdit();
	};
});