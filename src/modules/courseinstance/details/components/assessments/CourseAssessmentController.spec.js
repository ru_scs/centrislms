"use strict";

describe("CourseAssessmentController", function CourseDescriptionTestController() {

	beforeEach(module("mockConfig"));
	beforeEach(module("sharedServices"));
	beforeEach(module("courseDetailsApp"));
	beforeEach(module("courseInstanceShared"));

	// Dependencies
	var scope, controller;
	var createController;
	var mockResource;

	var mockCentrisNotify = {
		success: function() {
		},
		error: function() {
		}
	};

	// Controller injection.
	// Note that we ask Angular to inject the real CourseAssessmentFactory.
	// In effect, that means we are testing that factory as well.
	beforeEach(inject( function initController($rootScope, $controller, CourseAssessmentFactory,
		MockCourseInstanceRoleService, MockCourseDetailsResource) {

		// Closure, you sweet sweet beauty...
		createController = function(isTeacher, successLoading) {
			scope = $rootScope.$new();
			MockCourseInstanceRoleService.isTeacher = isTeacher;
			MockCourseDetailsResource.successLoadingAssessments = successLoading;
			mockResource = MockCourseDetailsResource;

			spyOn(mockCentrisNotify, "success").and.callThrough();
			spyOn(mockCentrisNotify, "error").and.callThrough();
			spyOn(mockResource, "putAssessment").and.callThrough();

			return $controller("CourseAssessmentController", {
				$scope:                    scope,
				$stateParams:              {},
				CourseAssessmentFactory:   CourseAssessmentFactory,
				CourseDetailsResource:     MockCourseDetailsResource,
				centrisNotify:             mockCentrisNotify,
				CourseInstanceRoleService: MockCourseInstanceRoleService.mockCourseInstanceRoleService(true)
			});
		};
	}));

	describe("when loading assessments returns an error", function() {
		beforeEach(function() {
			controller = createController(true, false);
		});

		it ("should display a centrisNotify error message on load fail", function() {
			expect(mockCentrisNotify.error).toHaveBeenCalled();
		});
	});

	describe("when the current user is a teacher", function() {

		beforeEach(function() {
			controller = createController(true, true);
		});

		it("should correctly set a scope property", function() {
			expect(scope.isTeacher).toBeDefined();
			expect(scope.isTeacher).toBe(true);
		});

		it("should toggle editing mode on/off", function () {
			scope.toggleEdit();
			expect(scope.editMode).toBe(true);
			scope.toggleEdit();
			expect(scope.editMode).toBe(false);
		});

		it("should add an assessment item if in edit mode", function () {
			var initialCount = scope.model.Items.length;
			scope.toggleEdit();
			expect(scope.model.Items.length).toBe(initialCount);
			scope.addAssignmentGroup();
			expect(scope.model.Items.length).toBe(initialCount + 1);
		});

		it("should NOT add an assessment item if edit mode is off", function () {
			var initialCount = scope.model.Items.length;
			expect(scope.model.Items.length).toBe(initialCount);
			scope.addAssignmentGroup();
			expect(scope.model.Items.length).toBe(initialCount);
		});

		it ("should increment the total count in an assessment item", function() {
			var lastItem = scope.model.Items[scope.model.Items.length - 1];
			var currentTotalCount = lastItem.TotalCount;
			scope.incrementTotalCount(lastItem);
			expect(lastItem.TotalCount).toBe(currentTotalCount + 1);
		});

		it ("should decrement the total count in an assessment item", function() {
			var lastItem = scope.model.Items[scope.model.Items.length - 1];
			var currentTotalCount = lastItem.TotalCount;
			scope.incrementTotalCount(lastItem);
			expect(lastItem.TotalCount).toBe(currentTotalCount + 1);
			scope.decrementTotalCount(lastItem);
			expect(lastItem.TotalCount).toBe(currentTotalCount);
		});

		it ("should not allow the total count to go below 1", function() {
			var firstItem = scope.model.Items[0];
			firstItem.TotalCount = 1;
			scope.decrementTotalCount(firstItem);
			expect(firstItem.TotalCount).toBe(1);
		});

		it("should increment how many assignments count in an assessment item", function () {
			var firstItem = scope.model.Items[0];
			firstItem.TotalCount = 2;
			firstItem.BestOfCount = 1;
			scope.incBestOfCount(firstItem);
			expect(firstItem.BestOfCount).toBe(2);
		});

		it("should decrement how many assignments count in an assessment item", function () {
			var firstItem = scope.model.Items[0];
			firstItem.TotalCount = 2;
			firstItem.BestOfCount = 2;
			scope.decBestOfCount(firstItem);
			expect(firstItem.BestOfCount).toBe(1);
		});

		it("should NOT decrement BestOfCount to below 1 in an assessment item", function () {
			var firstItem = scope.model.Items[0];
			firstItem.TotalCount = 2;
			firstItem.BestOfCount = 1;
			scope.decBestOfCount(firstItem);
			expect(firstItem.BestOfCount).toBe(1);
		});

		it("should NOT allow the BestOfCount in an assessment item to get larger than TotalCount", function () {
			var firstItem = scope.model.Items[0];
			firstItem.TotalCount = 2;
			firstItem.BestOfCount = 2;
			scope.incBestOfCount(firstItem);
			expect(firstItem.BestOfCount).toBe(2);
		});

		it ("should NOT allow BestOfCount to get bigger than TotalCount when decrementing TotalCount", function() {
			var firstItem = scope.model.Items[0];
			firstItem.TotalCount = 2;
			firstItem.BestOfCount = 2;
			scope.decrementTotalCount(firstItem);
			expect(firstItem.TotalCount).toBe(1);
			expect(firstItem.BestOfCount).toBe(1);
		});

		it("should delete an assignment item", function () {
			var initialCount = scope.model.Items.length;
			scope.deleteAssessmentItem(scope.model.Items[0]);
			expect(scope.model.Items.length).toBe(initialCount - 1);
		});

		it("should reset items when editing is cancelled", function() {
			var initialCount = scope.model.Items.length;
			scope.toggleEdit();
			scope.addAssignmentGroup();
			scope.cancelEdit();
			expect(scope.model.Items.length).toBe(initialCount);
		});

		it("should not allow submitting of assessment items with an empty name", function() {
			scope.toggleEdit();
			var currentCount = scope.model.Items.length;
			scope.addAssignmentGroup();
			var addedItem = scope.model.Items[currentCount];
			addedItem.Name = "";

			// This should trigger a save:
			scope.toggleEdit();

			// Assert:
			expect(addedItem.validation.nameInvalid).toBe(true);
			expect(mockResource.putAssessment).not.toHaveBeenCalled();
		});
	});
});
