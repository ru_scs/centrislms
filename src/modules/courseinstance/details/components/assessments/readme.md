# Course Assessment #

This submodule manages the course assessment for a course instance.

##### Structure #####

An Assessment Item is a collection of Assignments, all identical weight wise.
In most cases, a given assessment item will only contain a single assignment.
However, in order to support "best X out of Y assignments", this arrangement
is used.

The group itself contains a single weight, or a slice of the total course scale (the normal 100%).

Each Assessment Item can then be set as Required, which means that it is necessary
for the student to pass the given item in order to pass the course. Currently,
we assume that the decision what "passing" means is taken elsewhere, as it might
be different for students in undergraduate studies or graduate studies.

Finally, each assessment item may be set such that it will only count towards the
final grade if the grade the student gets for this item (assignment) is higher than
another item/assignment.

###### ***For example:*** ######

A group that amounts to **25%** of the final grade contains **5** assignments, and all **5** make up this total of **25%**. This means that each assignment weighs **5%**

Calculated as so:   
`assignment weight = group weight / how many assignments count = 25/5 = 5`

But say we want just **4** of these assignments to count for the group weight. This means that the previous calculation changes to:
`assignment weight = group weight / how many assignments count = 25/4 = 6.25`

We are still sitting on **5** assignments, all of them counting **6.25%**. However, because only **4** of them count for  the total, one becomes optional as only the **4 highest** are used to make up the final grade (or that is the idea, there is no connection between assignment grades and the course assessment parts yet).