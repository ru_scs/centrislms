"use strict";

/**
 * CourseAssessmentController loads assessment info about a course,
 * displays it, and allows the teacher to edit the assessment.
 * There are a number of rules which it enforces (see below).
 *
 * "OnlyIfHigherThan" business rules and implementation details:
 * - There are two items involved in those: the "master" and the "slave".
 *   The master is like "Final exam", which will equal a certain amount
 *   of the final grade. Then there is the slave which will also equal
 *   a certain amount, but only if it is higher than the master.
 *   We do this by linking those two together. In fact. all assessment
 *   items will receive their own master/slave objects, but they will be
 *   empty for most items. For those items that ARE linked together, the
 *   master and slave properties will "point" to the actual objects,
 *   i.e. the midterm exam object will have "master" pointing to the final
 *   exam object, and the final exam object will have the "slave" object
 *   pointing to the midterm exam object. There are rules regarding which
 *   objects can be in a master/slave relationship, and this is implemented
 *   in the CourseAssessmentFactory.shouldAddToMasterSlaveList() function.
 *   Due to the fact that there could be multiple items set as slaves to a
 *   single master, we need to store a list of slaves instead of a single object.
 * - Implementation details: The backend only stores the ID of the master
 *   object in the OnlyIfHigherThan property. This is also the only data
 *   we pass to the backend when saving. However, when a new assessment is being
 *   created, no real IDs have been allocated when we send the data! Therefore,
 *   we allocate "dummy" IDs, which are negative.
 * "Required" property business rules:
 * - According to school rules, if a final exam equals 20% or more of the
 *   final grade, it should be required to pass the exam in order to be
 *   able to pass the course. There is logic, both in the UI and in
 *   CourseAssessmentFactory.changeRequired() which enforces this.
 * "TotalCount" and "BestOfCount" business rules:
 * - These two properties reference when there are (for instance) 5 assignments,
 *   but only the 4 best will be used. The group itself has a certain weight
 *   (example: 12%), and that weight will be distributed evenly between
 *   items (in this case, 3% for each of the 4 best). The "TotalCount" property
 *   represents the total number of assignments in the group (5 in this case),
 *   and the "BestOfCount" property represents the number of items which will
 *   be used (4 in this case).
 *   There are a few obvious rules regarding this:
 *   * Neither number can go below 1
 *   * The BestOfCount can never exceed TotalCount
 */
angular.module("courseDetailsApp").controller("CourseAssessmentController",
function CourseAssessmentController($scope, $stateParams, CourseAssessmentFactory,
	CourseDetailsResource, centrisNotify, CourseInstanceRoleService) {

	// Note: these constants must match those used by the API.
	var ASSESSMENT_TYPE_ASSIGNMENT = 1;
	var ASSESSMENT_TYPE_FINALEXAM  = 4;

	// First, we set up a few required scope variables:
	$scope.graphLabels  = [];
	$scope.graphData    = [];
	$scope.graphOptions = {
		tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>" + "%",
		animationEasing: "easeOutCirc"
	};
	$scope.editMode         = false;
	$scope.generatedStrings = [];
	$scope.model            = {};
	$scope.cancelModel      = {};
	$scope.totalWeight      = 0;
	$scope.config           = {
		assessmentTypes: [{
			typeId: ASSESSMENT_TYPE_ASSIGNMENT,
			name:   "assessment.AssignmentType.Assignment"
		}, {
			typeId: 2,
			name:   "assessment.AssignmentType.OnlineExam"
		}, {
			typeId: 3,
			name:   "assessment.AssignmentType.MidtermExam"
		}, {
			typeId: ASSESSMENT_TYPE_FINALEXAM,
			name:   "assessment.AssignmentType.FinalExam"
		}, {
			typeId: 5,
			name:   "assessment.AssignmentType.RetakeExam"
		}/*, {
			typeId: 6,
			name:   "assessment.AssignmentType.Attendance"
		}*/]
	};

	// By default, we are NOT in edit mode. However, a teacher
	// can switch over to editing.
	$scope.toggleEdit = function toggleEdit() {

		// If we were previously in edit mode, we save and go back to display mode:
		if ($scope.editMode) {
			var errorsFound = CourseAssessmentFactory.validate($scope.model);

			if (errorsFound) {
				// A validation message will be displayed via different means,
				// i.e. we don't need to display anything here...
				return;
			}

			// Remove internal book-keeping data. We first need to make
			// a copy of the data and send that copy to the server, since
			// we want to support edit-close-edit-close behaviour. I.e. the
			// first edit-close cycle must not destroy our internal data structure!
			var cleanModel = angular.copy($scope.model);
			for (var i = 0; i < cleanModel.Items.length; ++i) {
				delete cleanModel.Items[i].master;
				delete cleanModel.Items[i].slaves;
				delete cleanModel.Items[i].validation;
				delete cleanModel.Items[i].focus;
			}

			// Send the cleaned data to the server:
			CourseDetailsResource.putAssessment($stateParams.courseInstanceID, cleanModel).success(function(data) {
				centrisNotify.success("assessment.Messages.SaveSuccessful");
			}).error(function(data) {
				centrisNotify.error("assessment.Messages.ErrorSavingData");
			});

			$scope.generatedStrings = CourseAssessmentFactory.generateAssessmentStrings($scope);
		} else {
			$scope.cancelModel = angular.copy($scope.model);
		}

		$scope.editMode = !$scope.editMode;
	};

	$scope.cancelEdit = function cancelEdit() {
		$scope.editMode = false;
		$scope.model    = $scope.cancelModel;
	};

	// A new assessment item is given a unique ID when created,
	// even though the backend will allocate the actual ID when
	// we save. However, due to the "OnlyIfHigherThan" feature,
	// each item must have some ID which can be used when linking
	// the "slave" assessment item (which will only be used if higher)
	// to the "master" assessment item.
	var nextUnassignedID = -1;

	// Adds an assignment group to the scope course
	$scope.addAssignmentGroup = function addAssignmentGroup() {

		if ($scope.editMode) {
			var item = {
				ID:                   nextUnassignedID,
				Name:                 "Skilaverkefni",
				AssessmentType:       ASSESSMENT_TYPE_ASSIGNMENT,
				OnlyIfHigherThan:     null,
				TotalCount:           1,
				BestOfCount:          1,
				RequiredToPassCourse: false,
				Weight:               10,

				// Internal bookkeeping properties:
				master:               {},
				slaves:               [],
				validation:           {},
				focus:                true
			};

			item.slaves.totalWeight = 0;
			$scope.model.Items.push(item);

			// Note: we DECREMENT the value, to ensure that new IDs will
			// always have unique negative IDs, positive IDs are an indication
			// that the item is a previously created one with an ID allocated
			// by the backend.
			nextUnassignedID--;

			$scope.totalWeight = $scope.calculateTotalWeight();
		}
	};

	// This function calculates the total weight of all items
	// and returns it.
	$scope.calculateTotalWeight = function calculateTotalWeight() {
		var total = 0.0;
		if ($scope.model) {
			if ($scope.model.Items) {
				for (var i = 0; i < $scope.model.Items.length; ++i) {
					var item = $scope.model.Items[i];
					// Skip those items which only count towards the final grade
					// if they are higher than another item:
					if (!item.OnlyIfHigherThan) {
						var value = parseInt(item.Weight, 10);
						if (value) {
							total += value;
						}
					}
				}
			}
		}

		return total;
	};

	$scope.incrementTotalCount = function incrementTotalCount(item) {
		// There are no restrictions on how large number this can be:
		item.TotalCount++;
	};

	$scope.decrementTotalCount = function decrementTotalCount(item) {
		// This number should never go below 0:
		if (item.TotalCount > 1) {
			item.TotalCount--;
		}

		// Ensure that there is never a mismatch between TotalCount
		// and BestOfCount, i.e. that the latter is never bigger than
		// the former.
		if (item.BestOfCount > item.TotalCount) {
			item.BestOfCount = item.TotalCount;
		}
	};

	// Decreases the number of assignments that count in a group
	$scope.decBestOfCount = function decBestOfCount(item) {
		// Again, this number can never go below 1.
		if (item.BestOfCount > 1) {
			item.BestOfCount--;
		}
	};

	// Increases the number of assignments that count in a group
	$scope.incBestOfCount = function incBestOfCount(item) {
		// This number can never go above TotalCount.
		if (item.BestOfCount < item.TotalCount) {
			item.BestOfCount++;
		}
	};

	$scope.onItemNameChange = function onItemNameChange(item) {
		item.validation.nameInvalid = false;
	};

	// Change the weight of a group.
	$scope.onItemWeightChanged = function onItemWeightChanged(item) {
		// Note: we don't actually change any weight here, we just
		// ensure that the weight change doesn't result in a rule
		// violation!

		// Well, ok we DO change the weight, i.e. from a string to an int.
		item.Weight = parseInt(item.Weight, 10);

		// Yes, we should technically not be calling this event handler here...
		$scope.onChangeAssessmentType(item);

		$scope.totalWeight = $scope.calculateTotalWeight();

		CourseAssessmentFactory.updateSlavesTotalWeight($scope.model);

		// If the weight change resulted in a master/slave
		// relationship going bad, we point that out to the user
		var goneBad = CourseAssessmentFactory.validateMasterSlaves($scope.model);
		if (goneBad) {
			centrisNotify.error("assessment.Messages.OnlyIfHigherNoLongerValid");
		}
	};

	// Called when there is a change in the master/slave dropdown
	$scope.onChangeItemMaster = function onChangeItemMaster(item) {

		// Do the actual work:
		CourseAssessmentFactory.updateMasterSlaveLink($scope.model, item);

		// This will result in a changed total weight:
		$scope.totalWeight = $scope.calculateTotalWeight();
	};

	// Removes an assessment item from the assessment.
	$scope.deleteAssessmentItem = function deleteAssessmentItem(item) {
		CourseAssessmentFactory.deleteAssessmentItem($scope.model, item);
		$scope.totalWeight = $scope.calculateTotalWeight();
	};

	// Used to determine if we should include certain items in the master
	// list
	$scope.itemIsEligibleAsMaster = function itemIsEligibleAsMaster(potentialMaster, potentialSlave) {
		return potentialSlave.ID !== potentialMaster.ID &&
			potentialMaster.master.ID === undefined &&
			potentialSlave.Weight <= potentialMaster.Weight;

		// Only include those items in the master list which are eligible
		// to be used as a master! This means:
		// - items cannot be masters/slaves with themselves
		// - items cannot form a chain, i.e. we cannot have slave->master->master
		// - items cannot be slaves of items with lower weight than themselves
	};

	var isItemAlwaysRequired = function isItemAlwaysRequired(item) {
		// BUSINESS RULE VALIDATION: the rule is that if a final exam equals
		// 20% or more of a final grade, then it *must* be required.
		return item.AssessmentType === ASSESSMENT_TYPE_FINALEXAM &&
			item.Weight >= 20;
	};

	// Called to determine if we should disable the "Required" checkbox
	$scope.isRequiredCheckboxDisabled = function isRequiredCheckboxDisabled(item) {
		return isItemAlwaysRequired(item);
	};

	// Called when there is a change in the assessment type.
	$scope.onChangeAssessmentType = function onChangeAssessmentType(item) {
		if (isItemAlwaysRequired(item)) {
			item.Required = true;
		}
	};

	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(isTeacher) {
		$scope.isTeacher       = isTeacher;
	});

	$scope.loadingData = true;
	// Load the assessment data from the backend:
	CourseDetailsResource.getAssessment($stateParams.courseInstanceID).success(function(data) {
		$scope.model            = data;
		$scope.generatedStrings = CourseAssessmentFactory.generateAssessmentStrings($scope);

		// Link together master/slave items (OnlyIfHigherThan):
		for (var i = 0; i < $scope.model.Items.length; ++i) {
			var item = $scope.model.Items[i];

			// Ensure all items have a valid validation object
			item.validation = {
				nameInvalid:   false,
				weightInvalid: false
			};

			item.focus = false;

			// Ensure ALL items have valid master/slave properties:
			if (!item.master) {
				item.master = {};
			}
			if (!item.slaves) {
				item.slaves = [];
				item.slaves.totalWeight = 0;
			}

			if (item.OnlyIfHigherThan) {
				// Found one, we must find the corresponding item and link them together:
				for (var j = 0; j < $scope.model.Items.length; ++j) {
					var innerItem = $scope.model.Items[j];
					if (innerItem.ID === item.OnlyIfHigherThan) {
						if (!innerItem.slaves) {
							innerItem.slaves = [];
							innerItem.slaves.totalWeight = 0;
						}
						innerItem.slaves.push(item);
						innerItem.slaves.totalWeight += item.Weight;

						item.master = $scope.model.Items[j];
						// The items have been linked, no need for further processing
						// in the inner loop:
						break;
					}
				}
			}
		}

		// Populate graph:
		_($scope.model.Items).forEach(function(v) {
			$scope.graphLabels.push(v.Name);
			$scope.graphData.push(v.Weight);
		});

		$scope.totalWeight  = $scope.calculateTotalWeight();
		$scope.loadingData = false;

	}).error(function() {
		$scope.loadingData = false;
		centrisNotify.error("assessment.Messages.ErrorLoadingData");
	});
});
