"use strict";

/*
 * CourseAssessmentFactory contains the business logic for course
 * assessment.
 */
angular.module("courseDetailsApp").factory("CourseAssessmentFactory",
function courseAssessmentFactory($translate) {

	var service = {

		// Removes an assignment item
		deleteAssessmentItem: function deleteAssignmentItem(model, item) {

			// Loop over each assessment item and check if we can find it:
			for (var i = 0; i < model.Items.length; i++) {
				var loopItem = model.Items[i];
				if (loopItem.ID === item.ID) {
					// Found it!

					// a) If any item had the current one as master,
					//    we must remove that association:
					for (var j = 0; j < loopItem.slaves.length; ++j) {
						loopItem.slaves[j].master = null;
						loopItem.slaves[j].OnlyIfHigherThan = null;
					}
					model.Items.splice(i, 1);

					// b) If this item had a master, (and was therefore a slave),
					// we must update the slave list for the master:
					if (loopItem.master.ID) {
						for (var k = 0; k < loopItem.master.slaves.length; ++k) {
							if (loopItem.master.slaves[k].ID === loopItem.ID) {
								loopItem.master.slaves.splice(k,1);
								loopItem.master.slaves.totalWeight -= loopItem.Weight;
								break;
							}
						}
					}
					break;
				}
			}
		},

		// Checks if there are any errors in the assessment and tries to correct them.
		// I.e. if an assignment group is extended by two groups.
		validate: function validate(model, item) {
			var errorFound = false;
			for (var i = 0; i < model.Items.length; i++) {
				var loopItem = model.Items[i];

				if (parseInt(loopItem.Weight, 10) === 0) {
					loopItem.validation.weightInvalid = true;
					errorFound = true;
				}

				if (loopItem.Name.length < 1) {
					loopItem.validation.nameInvalid = true;
					errorFound = true;
				}
			}

			return errorFound;
		},

		// Generates text strings describing the assessment.
		// TODO: there are still issues regarding declension,
		// i.e. "Það verður að ná 'Miðannarpróf' til að ná áfanganum" etc.
		generateAssessmentStrings: function generateAssessmentStrings(scope) {
			var strings = [];
			var translateStrings = [
				"assessment.GeneratedAssessmentStrings.weighs",
				"assessment.GeneratedAssessmentStrings.weigh",
				"assessment.GeneratedAssessmentStrings.ofFinal",
				"assessment.GeneratedAssessmentStrings.highestOf",
				"assessment.GeneratedAssessmentStrings.RequiredToPass",
				"assessment.GeneratedAssessmentStrings.toPassCourse",
				"assessment.GeneratedAssessmentStrings.Raises",
				"assessment.GeneratedAssessmentStrings.count",
				"assessment.GeneratedAssessmentStrings.each"
			];
			var translatedStrings = {};
			$translate(translateStrings).then(function(translated) {
				translatedStrings = translated;
				for (var i = 0; i < scope.model.Items.length; i++) {
					var item = scope.model.Items[i];
					var string = "";
					if (item.BestOfCount > 1) {
						string += item.BestOfCount;
						string += " ";
					}
					string += item.Name;
					if (item.BestOfCount > 1) {
						string += " ";
						string += translatedStrings["assessment.GeneratedAssessmentStrings.weigh"];
						string += " ";
					} else {
						string += " ";
						string += translatedStrings["assessment.GeneratedAssessmentStrings.weighs"];
						string += " ";
					}

					string += item.Weight;
					string += "% ";
					string += translatedStrings["assessment.GeneratedAssessmentStrings.ofFinal"];
					string += ". ";

					if (item.BestOfCount > 1) {
						string += item.TotalCount;
						string += " ";
						string += translatedStrings["assessment.GeneratedAssessmentStrings.highestOf"];
						string += ", ";
						string += (item.Weight / (item.BestOfCount * 1.0)).toFixed(2);
						string += "% ";
						string += translatedStrings["assessment.GeneratedAssessmentStrings.each"];
						string += ". ";
					}

					if (item.Required) {
						string += "<br /><strong>";
						string += translatedStrings["assessment.GeneratedAssessmentStrings.RequiredToPass"];
						string += " ";
						string += item.Name;
						string += " ";
						string += translatedStrings["assessment.GeneratedAssessmentStrings.toPassCourse"];
						string += ". ";
						string += "</strong>";
					}

					if (item.master.ID) {
						string += translatedStrings["assessment.GeneratedAssessmentStrings.Raises"];
						string += " ";
						string += item.master.Name + ".";
					}

					strings.push(string);
				}

				scope.generatedStrings = strings;
			});
		},
		// This function ensure that the master/slave relationship between "item"
		// and its potential master is up-to-date. If we are adding a relationship,
		// the item.master.ID has been set, but nothing else.
		updateMasterSlaveLink: function updateMasterSlaveLink(model, item) {

			var i = 0;
			var j = 0;

			var prevID = item.master.ID;
			// The ID comes as a string from the dropdown...
			item.master.ID = parseInt(item.master.ID, 10);

			if (item.master.ID) {

				// Handle if we're changing from one master to another,
				// i.e. in that case we must first remove the old link:
				if (item.OnlyIfHigherThan) {
					for (i = 0; i < model.Items.length; ++i) {
						if (model.Items[i].ID === item.OnlyIfHigherThan) {

							for (j = 0; j < model.Items[i].slaves.length; ++j) {
								if (model.Items[i].slaves[j].ID === item.ID) {
									model.Items[i].slaves.splice(j, 1);
									model.Items[i].slaves.totalWeight -= item.Weight;
								}
							}
						}
					}
				}

				for (i = 0; i < model.Items.length; ++i) {
					var loopItem = model.Items[i];
					if (loopItem.ID === item.master.ID) {
						item.master           = loopItem;
						item.OnlyIfHigherThan = loopItem.ID;
						loopItem.slaves.push(item);
						loopItem.slaves.totalWeight += item.Weight;
						break;
					}
				}
			} else {
				// In this case, the link is being broken!

				// Remove the slave object from the slave list in the master:
				for (i = 0; i < item.master.slaves.length; ++i ) {
					if (item.master.slaves[i].ID === item.ID) {
						item.master.slaves.splice(i, 1);
						item.master.slaves.totalWeight -= item.Weight;
						break;
					}
				}

				// First, we need to restore the ID of the master object:
				item.master.ID = item.OnlyIfHigherThan;

				// Then, we remove the master object link,
				// and replace it with a dummy object, with the proper
				// ID to ensure correct selection in the dropdown:
				item.master = {
					ID: prevID
				};
				item.OnlyIfHigherThan = null;
			}
		},

		validateMasterSlaves: function validateMasterSlaves(model) {
			var goneBad = false;
			for (var i = 0; i < model.Items.length; ++i) {
				var loopItem = model.Items[i];
				if (loopItem.master.ID) {
					if (loopItem.master.Weight < loopItem.Weight) {
						// Master/slave relationship gone bad! We must remove this
						// link.
						goneBad = true;

						// Find this item in the slave list:
						for (var j = 0; j < loopItem.master.slaves.length; ++j) {
							if (loopItem.master.slaves[j].ID === loopItem.ID) {
								loopItem.master.slaves.splice(j,1);
								loopItem.master.slaves.totalWeight -= loopItem.Weight;
								break;
							}
						}
						loopItem.master = {};
						loopItem.OnlyIfHigherThan = null;
					}
				}
			}

			return goneBad;
		},

		updateSlavesTotalWeight: function updateSlavesTotalWeight(model) {
			for (var i = 0; i < model.Items.length; ++i) {
				var item = model.Items[i];
				item.slaves.totalWeight = 0;
				for (var j = 0; j < item.slaves.length; ++j) {
					item.slaves.totalWeight += item.slaves[j].Weight;
				}
			}
		}
	};

	return service;
});
