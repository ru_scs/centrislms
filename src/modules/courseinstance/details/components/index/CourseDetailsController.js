"use strict";

angular.module("courseDetailsApp").controller("CourseDetailsController",
function CourseDetailsController($scope, $state, $stateParams, CourseDetailsResource,
	$translate, CourseInstanceRoleService, CourseInstanceService) {

	// For displaying semester seasons
	$scope.semester   = "";
	$scope.semesterNr = "";
	$scope.isTeacher  = false;

	// Our data about the course. Note that the data is split
	// in two. This is because the basic info is fetched by
	// one of our ancestor components, and instead of re-fetching
	// that information, we only fetch the details. However, we
	// also need access to the basic info, so we listen to an
	// event which will be broadcast from our ancestor component,
	// and get the instance data that way.
	$scope.course = {
		instance:  {},
		details:   {}
	};

	// Course details tabs
	$scope.tabs = {
		list: [
			{ route:"courseinstance.coursedetails",                   active: true  },
			{ route:"courseinstance.coursedetails.learningoutcomes",  active: false },
			{ route:"courseinstance.coursedetails.courseassessment",  active: false },
			{ route:"courseinstance.coursedetails.teachingmethods",   active: false },
		],
		// Changes the current route
		go: function go(route) {
			$scope.$broadcast(route);
			$state.go(route);
		}
	};

	// This event is emitted when the user switches to a new tab.
	$scope.$on("$stateChangeSuccess", function stateSuccessfullyChanged () {
		$scope.tabs.list.forEach(function forEachTab(tab) {
			tab.active = $state.is(tab.route);
		});
	});

	$scope.loadingData = true;
	// Calls API and gets the course instance details.
	CourseDetailsResource.getCourseDetails($stateParams.courseInstanceID).success(function success(details) {
		$scope.course.details  = details;
		$scope.semester        = details.Semester.substring(0,4);
		$scope.semesterNr      = details.Semester.charAt(4);
		$scope.loadingData     = false;
	}).error(function() {
		$scope.loadingData     = false;
	});

	// Get access to the base info about the course, which CourseHomeController
	// takes care of loading. However, we don't interact directly with that
	// controller, since it would simply be.... wrong. Instead, this service
	// takes care of storing the current course.
	CourseInstanceService.getCurrentCourse().then(function(course) {
		$scope.course.instance = course;
	});

	var setupIntroOptions = function setupIntroOptions() {
		// Defines the steps and calls the step id
		$scope.IntroOptions = {
			steps: [{
				element: "#coursedetailsheader",
				position: "left",
				intro: ""
			}, {
				element: "#coursedetailsteachers",
				position: "left",
				intro: ""
			}, {
				element: "#coursedetailsdescriptiontab",
				position: "left",
				intro: ""
			}, {
				element: "#coursedetailsoutcomestab",
				position: "left",
				intro: ""
			}, {
				element: "#coursedetailsassessmenttab",
				position: "left",
				intro: ""
			}, {
				element: "#coursedetailsteachingmethodstab",
				position: "left",
				intro: ""
			}],
			showStepNumbers:    false,
			exitOnOverlayClick: true,
			exitOnEsc:          true,
		};

		// Calling the translate files for each step and putting them into an array
		var introKeys = [
			"coursedetails.Intro.Header",
			"coursedetails.Intro.Teachers"
		];

		// We display different messages, depending on the role of the user in the course:
		if ($scope.isTeacher) {
			introKeys.push("coursedetails.TeachersIntro.Desc");
			introKeys.push("coursedetails.TeachersIntro.Outcomes");
			introKeys.push("coursedetails.TeachersIntro.Assessment");
			introKeys.push("coursedetails.TeachersIntro.Methods");
		} else {
			introKeys.push("coursedetails.StudentsIntro.Desc");
			introKeys.push("coursedetails.StudentsIntro.Outcomes");
			introKeys.push("coursedetails.StudentsIntro.Assessment");
			introKeys.push("coursedetails.StudentsIntro.Methods");
		}

		// Setting the translates as the text for each step
		$translate(introKeys).then(function whenDoneTranslating(translations) {
			for (var i = 0; i < introKeys.length; ++i) {
				$scope.IntroOptions.steps[i].intro = translations[introKeys[i]];
			}
		});

		// Calling the translate files for each button and putting them into an array
		var introButtons = [
			"AngularIntro.Next",
			"AngularIntro.Prev",
			"AngularIntro.Skip",
			"AngularIntro.Done"
		];

		// Setting the translates as the text for each button
		$translate(introButtons).then(function whenDoneTranslating(translations) {
			$scope.IntroOptions.nextLabel = translations[introButtons[0]];
			$scope.IntroOptions.prevLabel = translations[introButtons[1]];
			$scope.IntroOptions.skipLabel = translations[introButtons[2]];
			$scope.IntroOptions.doneLabel = translations[introButtons[3]];
		});
	};

	$scope.$on("onCentrisIntro", function(event, args) {
		// Only show this if the current route is for our page:
		var routeName = "courseinstance.coursedetails";
		if ($state.current.name.substr(0, routeName.length) === routeName) {
			$scope.ShowCourseDetailsIntro();
		}
	});

	// Request the role of the user.
	// NOTE: this does NOT (necessarily) mean that we require a separate API
	// request, since roles are cached! CourseInstanceRoleService takes
	// care of that for us.
	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(isTeacher) {
		$scope.isTeacher       = isTeacher;

		setupIntroOptions();
	});
});