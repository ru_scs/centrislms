"use strict";

describe("CourseDetailsController", function() {

	var MOCK_COURSE_ID = 1337;

	var mockCourse = {
		ID:                 MOCK_COURSE_ID,
		CourseID:           "G-31M-1337",
		Name:               "Geimvísindi",
		NameEN:             "Space Science",
		DateBegin:          "2258-04-29T13:19:11.240455+00:00",
		DateEnd:            "2258-04-29T13:19:11.240455+00:00",
		Semester:           "22582",
		RegisteredStudents: 85,
		MaxStudents:        3,
		Credits:            400,
		DepartmentID:       7,
		Teachers:           [{
			SSN:           "1234567890",
			FullName:      "Spock",
			Email:         "Spock@USSenterprise.gov",
			CellPhone:     "800-1515",
			HomePhone:     "909-1234",
			TypeID:        2,
			TeacherType:   "Vúlkani",
			TeacherTypeEN: "Vulcan"
		}, {
			SSN:           "0123456789",
			FullName:      "James Tiberius Kirk",
			Email:         "hotStuffKirk@free2play.net",
			CellPhone:     "900-0000",
			HomePhone:     "911-0000",
			TypeID:        1,
			TeacherType:   "Kafteinn",
			TeacherTypeEN: "Captain"
		}],
		HasFinalExam:        true,
		LanguageCode:        "es"
	};

	var mockStateParams = {
		courseInstanceID: MOCK_COURSE_ID
	};

	var mockState = {
		go: function(route) {

		},
		is: function(route) {
			return true;
		},
		current: {
			name: "courseinstance.coursedetails"
		}
	};

	var controller;
	var scope;
	var rootScope;
	var resource;
	var createController;
	var translate;
	var instService;
	var isTeacher = true;

	beforeEach(module("courseDetailsApp", "sharedServices", "mockConfig", "courseInstanceShared"));

	beforeEach(inject(function ($controller, $rootScope, MockCourseDetailsResource, mockTranslate,
		MockCourseInstanceRoleService, MockCourseInstanceService) {
		rootScope   = $rootScope;
		scope       = $rootScope.$new();
		resource    = MockCourseDetailsResource;
		translate   = mockTranslate.mockTranslate;
		instService = MockCourseInstanceService.mockCourseInstanceService(0);

		// The markup for the intro object declares this function,
		// so we have to do it ourselves!
		scope.ShowCourseDetailsIntro = function() {};

		spyOn(scope, "ShowCourseDetailsIntro");
		spyOn(scope, "$broadcast");
		spyOn(mockState, "go");

		createController = function() {
			var roleService = MockCourseInstanceRoleService.mockCourseInstanceRoleService(isTeacher);

			spyOn(resource, "getCourseDetails").and.callThrough();

			controller = $controller("CourseDetailsController", {
				$scope:                    scope,
				$state:                    mockState,
				$stateParams:              mockStateParams,
				CourseDetailsResource:     resource,
				$translate:                translate,
				CourseInstanceRoleService: roleService,
				CourseInstanceService:     instService
			});
		};
	}));

	describe("initialization", function() {
		beforeEach(function() {
			resource.courseDetailsReply = mockCourse;
			createController();
		});

		it ("should initialize properly", function() {
			expect(scope.course).toBeDefined();
		});

		it ("should call resource to fetch details about the current course", function() {
			expect(resource.getCourseDetails).toHaveBeenCalled();
			expect(scope.semester).toBe("2258"); // See the Semester property in our mock course
			expect(scope.semesterNr).toBe("2");
		});

		it ("should try to display the intro when the given message is broadcast", function() {
			rootScope.$broadcast("onCentrisIntro");
			expect(scope.ShowCourseDetailsIntro).toHaveBeenCalled();
		});

		it ("should NOT try to show intro if the current route is not for the given page", function() {
			mockState.current.name = "smuuu";
			rootScope.$broadcast("onCentrisIntro");
			expect(scope.ShowCourseDetailsIntro).not.toHaveBeenCalled();
		});

		it ("should switch to a new state when a tab is pressed", function() {
			scope.tabs.go({});
			expect(scope.$broadcast).toHaveBeenCalled();
			expect(mockState.go).toHaveBeenCalled();
		});
	});

});
