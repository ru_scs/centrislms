"use strict";

angular.module("courseDetailsApp").controller("LearningOutcomesDetailsController",
function LearningOutcomesDetailsController($scope, $stateParams, CourseDetailsResource,
	centrisNotify, CommentDlg) {

	$scope.status = {
		isFirstOpen: true,
		isFirstDisabled: false
	};

	// Edit mode starts as false
	$scope.isInEditMode                = false;
	$scope.copyOfLearningOutcomes      = [];
	$scope.learningOutcomeDescriptions = {};
	$scope.copyOfDescriptions          = {};
	$scope.allowEdit                   = $scope.isTeacher;
	// Note: $scope.isTeacher is inherited from parent scope!

	// Copies the original data that we start with to a temporary variable
	$scope.onEdit = function onEdit() {
		// We allow the user to edit the learning outcomes by setting isInEditMode to true
		$scope.isInEditMode = true;

		// Now, to prevent us from displaying bad data if the user
		// chooses to edit, and then cancels, we make a copy of our
		// learning outcomes, and store this copy such that we can
		// recall it if the user decides to cancel the edit.
		angular.copy($scope.learningOutcomes, $scope.copyOfLearningOutcomes);
		angular.copy($scope.learningOutcomeDescriptions, $scope.copyOfDescriptions);
	};

	// When this function is called, we set the edit mode to false
	// and then we save the data and send it to the API
	$scope.onSaveLearningOutcomes = function onSaveLearningOutcomes() {
		$scope.isInEditMode = false;

		var model = {
			Description:   $scope.learningOutcomeDescriptions.Description,
			DescriptionEN: $scope.learningOutcomeDescriptions.DescriptionEN,
			Items:         $scope.learningOutcomes
		};

		CommentDlg.show("tabLearningOutcomes.CommentMessage", "tabLearningOutcomes.CommentTitle")
		.then(function(comment) {
			model.Message = comment.Comment;
			CourseDetailsResource.saveLearningOutcomes($stateParams.courseInstanceID, model)
			.success(function() {
				centrisNotify.success("tabLearningOutcomes.LearningOutcomesSaved");
			})
			.error(function() {
				centrisNotify.error("tabLearningOutcomes.LearningOutcomesSavingError");
			});
		});
	};

	// That makes us able to take back the changes the user made to the learning outcomes
	$scope.onCancel = function onCancel() {
		$scope.isInEditMode = false;
		$scope.learningOutcomes = $scope.copyOfLearningOutcomes;
		$scope.learningOutcomeDescriptions = $scope.copyOfDescriptions;
	};

	$scope.loadingData = true;
	// Here we are fetching the data from the API
	CourseDetailsResource.getLearningOutcomes($stateParams.courseInstanceID).success(function(data) {
		// We start with edit mode set to false
		$scope.isInEditMode     = false;
		$scope.loadingData      = false;
		$scope.learningOutcomes = data.LearningOutcomes;
		$scope.learningOutcomeDescriptions = data;
	}).error(function() {
		$scope.loadingData  = false;
	});
});
