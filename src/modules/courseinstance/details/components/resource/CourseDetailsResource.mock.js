"use strict";

angular.module("courseDetailsApp").factory("MockCourseDetailsResource",
function MockCourseDetailsResource(MockHelpers) {

	// Declare some test data:
	var mockAssessments = {
		AssessmentDescription: "Smuuu",
		AssessmentDescriptionEN: "SmuuuEN",
		Items: [{
			ID:               1,
			Name:             "Skilaverkefni 1",
			Weight:           20,
			TotalCount:       1,
			BestOfCount:      1,
			Required:         false,
			OnlyIfHigherThan: null
		}, {
			ID:               2,
			Name:             "Miðannarpróf",
			Weight:           10,
			TotalCount:       1,
			BestOfCount:      1,
			Required:         false,
			OnlyIfHigherThan: 3
		}, {
			ID:               3,
			Name:             "Lokaprof",
			Weight:           70,
			TotalCount:       1,
			BestOfCount:      1,
			Required:         true,
			OnlyIfHigherThan: null
		}, {
			ID:               4,
			Name:             "Vikuleg netpróf",
			Weight:           10,
			TotalCount:       12,
			BestOfCount:      10,
			Required:         false,
			OnlyIfHigherThan: null
		}]
	};

	var mock = {
		successLoadingAssessments: true,
		successSavingAssessments: true,
		courseDetailsReply: {},
		successGetCourseDetails: true,

		getCourseDetails: function() {
			return MockHelpers.mockHttpPromise(mock.courseDetailsReply, mock.successGetCourseDetails);
		},
		getAssessment: function (id) {
			return MockHelpers.mockHttpPromise(angular.copy(mockAssessments), mock.successLoadingAssessments);
		},
		putAssessment: function(id, model) {
			return MockHelpers.mockHttpPromise({}, mock.successSavingAssessments);
		}
	};

	return mock;
});