"use strict";

angular.module("courseDetailsApp").factory("CourseDetailsResource",
function CourseDetailsResource(CentrisResource) {
	return {
		getCourseDetails: function getCourseDetails(courseInstanceID) {
			return CentrisResource.get("courses",
				":courseInstanceID/details",
				{courseInstanceID: courseInstanceID});
		},

		// ASSESSMENTS SECTION:

		// Gets the assessment for the specified course instance.
		getAssessment: function getAssessment(ID) {
			return CentrisResource.get("courses", ":id/assessment", {id:ID});
		},

		// Sends the assessment for the course instance
		// with the ID 'ID' to the server.
		putAssessment: function putAssessment(ID, assessment) {
			return CentrisResource.put("courses", ":id/assessment", {id:ID}, assessment);
		},

		// LEARNING OUTCOMES SECTION:

		// Fetches the learningOutcomes from the API.
		// NOTE: this is currently a separate API
		getLearningOutcomes: function getLearningOutcomes(courseInstanceID) {
			return CentrisResource.get("courses", ":courseInstanceID/learningoutcomes", {courseInstanceID: courseInstanceID});
		},

		// Saves the learning outcomes and sends them to the API
		saveLearningOutcomes: function saveLearningOutcomes(courseInstanceID, obj) {
			var param = {
				courseInstanceID: courseInstanceID
			};
			return CentrisResource.put("courses", ":courseInstanceID/learningoutcomes", param, obj);
		},

		// COURSE DESCRIPTION SECTION:
		updateCourseDescription: function updateCourseDescription(courseInstanceID, model) {
			var param = {
				id: courseInstanceID
			};

			return CentrisResource.put("courses", ":id/details/description", param, model);
		},

		// TEACHING METHODS SECTION:
		saveTeachingMethods: function saveTeachingMethods(courseInstanceID, model) {
			var param = {
				id: courseInstanceID
			};

			return CentrisResource.put("courses", ":id/details/teachingmethods", param, model);
		}
	};
});