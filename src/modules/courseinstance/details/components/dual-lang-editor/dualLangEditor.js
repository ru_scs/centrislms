"use strict";

/**
 * TODO: document!
 */
angular.module("courseDetailsApp").directive("dualLangEditor",
function dualLangEditor() {
	return {
		restrict: "E",
		templateUrl: "courseinstance/details/components/dual-lang-editor/editor.html",
		scope: {
			is: "=",
			en: "="
		},
		link: function(scope, element, attributes) {
			// TODO
		}
	};
});