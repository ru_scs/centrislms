"use strict";

angular.module("courseDetailsApp", ["pascalprecht.translate", "ui.router", "ui.bootstrap.popover"])
.config(function ($stateProvider) {
	$stateProvider.state("courseinstance.coursedetails", {
		url:         "/details",
		templateUrl: "courseinstance/details/components/index/index.html",
		controller:  "CourseDetailsController"
	})
	.state("courseinstance.coursedetails.courseassessment", {
		url:         "/assessment"
	})
	.state("courseinstance.coursedetails.learningoutcomes", {
		url:         "/learningoutcomes"
	})
	.state("courseinstance.coursedetails.teachingmethods", {
		url:         "/teachingmethods"
	});
});
