"use strict";

angular.module("courseDetailsApp").directive("courseDescription",
function courseDescriptionDirective() {
	return {
		restrict:      "E",
		scope: {
			course:    "=ngModel",
			loading:   "="
		},
		controller:    "CourseDescriptionController",
		templateUrl:   "courseinstance/details/components/description/index.html"
	};
});
