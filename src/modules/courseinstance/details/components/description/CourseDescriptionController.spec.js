"use strict";

describe("Testing CourseDescriptionController", function CourseDescriptionTestController() {

	beforeEach(module("courseInstanceShared"));
	beforeEach(module("courseDetailsApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));

	// Dependencies
	var scope;
	var controller;
	var notify;

	var resource = {
		saveSucceeded: true,
		updateCourseDescription: function(courseID, model) {
			return {
				success: function(fn) {
					if (resource.saveSucceeded) {
						fn();
					}
					return {
						error: function(fn) {
							if (!resource.saveSucceeded) {
								fn();
							}
						}
					};
				}
			};
		}
	};

	var mockCommentDlg = {
		show: function() {
			return {
				then: function(fn) {
					fn("This is a comment");
				}
			};
		}
	};

	// Mock data
	var mockDescription = "This description is lousy.";

	// Controller injection
	beforeEach(inject( function initController($rootScope, $controller, MockCourseInstanceRoleService, mockCentrisNotify) {
		scope = $rootScope.$new();

		notify = mockCentrisNotify;

		spyOn(notify, "error");
		spyOn(notify, "success");
		spyOn(mockCommentDlg, "show").and.callThrough();
		spyOn(resource, "updateCourseDescription").and.callThrough();

		controller = $controller("CourseDescriptionController", {
			$scope: scope,
			$stateParams: {},
			centrisNotify: mockCentrisNotify,
			CourseDetailsResource: resource,
			CourseInstanceRoleService: MockCourseInstanceRoleService.mockCourseInstanceRoleService(true),
			CommentDlg: mockCommentDlg
		});
	}));

	// Tests the editing toggle functionality
	it("should toggle edit mode on", inject(function testToggleEdit() {
		expect(scope.statusFlags.edit).toBe(false);
		scope.toggleEdit();
		expect(scope.statusFlags.edit).not.toBe(false);
	}));

	// Tests the saving functionality
	it("should create a draft object when in edit mode", inject(function testSave() {
		scope.editCourseDetail();
		expect(scope.draft).toBeDefined();
	}));

	// Tests quitting without saving course functionality
	it("should not change original description if goes to edit mode, " +
		"changes text, and then cancels", function testQuit() {
		scope.editCourseDetail();
		scope.quitWithoutSaving();
		// TODO: expect!!!
	});

	// Tests quitting and saving functionality
	it("should exit editing mode and save changes", inject(function testSaveAndQuit() {
		resource.saveSucceeded = true;
		scope.editCourseDetail();
		scope.draft.Description = "Modified";
		scope.draft.DescriptionEN = "ModifiedEN";
		scope.saveAndQuit();
		expect(mockCommentDlg.show).toHaveBeenCalled();
		expect(notify.success).toHaveBeenCalled();
	}));

	it("should exit editing but not update if save fails", function() {
		resource.saveSucceeded     = false;
		scope.course.Description   = "Original";
		scope.course.DescriptionEN = "OriginalEN";
		scope.editCourseDetail();
		scope.draft.Description    = "Modified";
		scope.draft.DescriptionEN  = "ModifiedEN";
		scope.saveAndQuit();

		expect(notify.error).toHaveBeenCalled();
		expect(scope.course.Description).toEqual("Original");
		expect(scope.course.DescriptionEN).toEqual("OriginalEN");
	});
});