"use strict";

/**
 * This component takes care of displaying the course description, and
 * allows the teacher to edit it.
 * @businessrule: CourseInstanceDescription
 */
angular.module("courseDetailsApp").controller("CourseDescriptionController",
function CourseDescriptionController($scope, $stateParams, centrisNotify,
	CourseDetailsResource, CourseInstanceRoleService, CommentDlg) {

	$scope.course      = {};
	$scope.statusFlags = {
		edit: false
	};

	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(isTeacher) {
		$scope.isTeacher       = isTeacher;
	});

	$scope.toggleEdit = function toggleEdit() {
		$scope.statusFlags.edit = !$scope.statusFlags.edit;
	};

	$scope.editCourseDetail = function editCourseDetail() {
		$scope.toggleEdit();
		$scope.draft = {
			Description:   $scope.course.Description,
			DescriptionEN: $scope.course.DescriptionEN
		};
	};

	$scope.quitWithoutSaving = function quitWithoutSaving() {
		$scope.toggleEdit();
	};

	$scope.saveAndQuit = function() {
		// Allow the teacher to provide some description with the edit:
		CommentDlg.show("coursedescription.CommentMessage", "coursedescription.CommentTitle").then(function(model) {
			$scope.draft.Message = model.Comment;
			CourseDetailsResource.updateCourseDescription($stateParams.courseInstanceID, $scope.draft)
			.success(function() {
				$scope.statusFlags.edit = !$scope.statusFlags.edit;
				centrisNotify.success("coursedescription.SaveSuccess");
				$scope.course.Description   = $scope.draft.Description;
				$scope.course.DescriptionEN = $scope.draft.DescriptionEN;
			}).error(function() {
				$scope.statusFlags.edit = !$scope.statusFlags.edit;
				centrisNotify.error("coursedescription.SaveFailed");
			});
		});
	};
});
