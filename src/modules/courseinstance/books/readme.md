# About this module

*	This module allows the user to add, delete, edit and view books in a course.

*	All the books are listed on a single page.

*	The "add" component regards the dialog where the user enters info about the book or edits.

*	The "index" component is the page visible to the user after selecting "books" on the side menu.

