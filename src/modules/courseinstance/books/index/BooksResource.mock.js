"use strict";

angular.module("mockCourseInstanceBooks", []).factory("mockBooksResource",function() {
		var mock = {
			successDelete: true,
			successAdd: true,
			successEdit: true,
			successGetList: true,
			successGetBookByISBN: true,
			book: {

				Book: {
					ISBN:		"",
					Title:       "This is the first book",
					Published:   "",
					Publisher:   "",
					Release:     "",
					Author:      "",
					Memo:  "This book is terrible, don't buy it",
					ISBN13:"",
					Required:    false
				}
			},
			getBooksByCourseID: function() {
				var data = [{
					Book: {
						ISBN:"",
						Title:       "This is the first book",
						Published:   "",
						Publisher:   "",
						Release:     "",
						Author:      "",
						CourseMemo:  "This book is terrible, don't buy it",
						Required:    false
					}
				}, {
					Book: {
						ISBN:"",
						Title:       "This is the second book",
						Published:   "",
						Publisher:   "",
						Release:     "",
						Author:      "",
						CourseMemo:  "This book is also terrible, don't buy any books",
						Required:    false
					}
				}];

				return {
					success: function (fnSuc) {
						if (mock.successGetList) {
							fnSuc(data);
						}
						return {
							error: function (fnErr) {
								fnErr();
							}
						};
					}
				};
			},
			addBookToCourse: function() {
				var res = {

					data: {
						Book:{
							ISBN:"",
							Title:       "This is a book",
							Published:   "",
							Publisher:   "",
							Release:     "",
							Author:      "Benedikt",
							CourseMemo:  "Good book",
							Required:    false
						}
					}
				};

				return {
					then: function (fnSuc, fnErr) {
						if (mock.successAdd) {
							fnSuc(res);
						} else {
							fnErr();
						}
					}
				};
			},
			editBookInCourse: function (book) {
				var res = {
					data: {
						Book: {
							ISBN:"",
							Title:       "This is an edited book",
							Published:   "",
							Publisher:   "",
							Release:     "",
							Author:      "Hoskuldur",
							Memo:  "Very Good book",
							ISBN13:"",
							Required:    false
						}
					}
				};

				return {
					then: function(fnSuc, fnErr) {
						if (mock.successEdit) {
							fnSuc(res);
						} else {
							fnErr();
						}
					}
				};
			},
			deleteBookInCourse: function() {
				return {
					then: function(fnSuc, fnErr) {
						if (mock.successDelete) {
							fnSuc();
						} else {
							fnErr();
						}
					}
				};
			},
			getBookByISBN: function() {
				var res = {
					data: {
						Title:     "This is a book",
						Publisher: "Benedikt",
						Author:    "Benni",
						ISBN:      "12345",
						ISBN13:    ""
					}
				};
				return {
					then: function(fnSuc, fnErr) {
						if (mock.successGetBookByISBN) {
							fnSuc(res);
						} else {
							fnErr();
						}
					}
				};
			}
		};

		return mock;
	});