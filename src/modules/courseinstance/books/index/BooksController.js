"use strict";

/*
 * This controller handles displaying a list of books in a course,
 * and allows the teacher to edit said list of books.
 */
angular.module("booksApp").controller("BooksController",
function BooksController($scope, $state, $translate, BooksResource, $stateParams,
	CourseInstanceRoleService, BookDlg, centrisNotify) {
	$scope.books            = [];
	$scope.courseInstanceID = $stateParams.courseInstanceID;
	$scope.covers           = [];

	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(isTeacher) {
		$scope.isTeacher = isTeacher;
	});

	var getCover = function getBookCover(obj) {
		if (obj.Book.ISBN !== "") {
			BooksResource.getBookCover(obj.Book.ISBN)
			.success(function (response) {
				// Here we get the exact url with the ISBN number of the book included.
				if (response.items) {
					obj.Cover = response.items[0].volumeInfo.imageLinks.smallThumbnail;
				}
			}).error(function () {
				obj.Cover = "styles/img/noimage.png";
			});
		} else {
			obj.Cover = "styles/img/noimage.png";
		}
	};

	// This function is called when the user wants to add a book to the course, it opens a dialog to put
	// in info about the book.
	$scope.addBook = function () {
		$scope.spinner = false;
		BookDlg.show($scope.courseInstanceID).then(function(model) {
			var promise = BooksResource.addBookToCourse($scope.courseInstanceID, model);
			promise.then(
			function (res) {
				centrisNotify.success("bookincourse.AddSuccessMessage");
				$scope.books.push(res.data);
				getCover(res.data);
			}, function (err) {
				centrisNotify.error("bookincourse.AddErrorMessage");
			});
		});
	};

	// This function is called when the user wants so edit a book in the course, it opens a dialog
	// where the user can change info about the book.
	$scope.editBook = function (book) {
		$scope.spinner = false;
		BookDlg.show($scope.courseInstanceID, book.Book).then(function(model) {
			var promise = BooksResource.editBookInCourse($scope.courseInstanceID, model);
			promise.then(
			function (res) {
				centrisNotify.success("bookincourse.EditSuccessMessage");
				book.Book.Title     = res.data.Book.Title;
				book.Book.Author    = res.data.Book.Author;
				book.Book.ISBN      = res.data.Book.ISBN;
				book.Book.ISBN13    = res.data.Book.ISBN13;
				book.Book.Memo      = res.data.Book.Memo;
				book.Book.Published = res.data.Book.Published;
				book.Book.Publisher = res.data.Book.Publisher;
				book.Book.Release   = res.data.Book.Release;
				book.Book.Required  = res.data.Book.Required;
			}, function (err) {
				centrisNotify.error("bookincourse.EditErrorMessage");
			});
		});
	};

	// This function is calld when user wants to delete a book in the course. The user can undo the action
	// by pressing undo button that appears with the success message.
	$scope.deleteBook = function (book) {
		$scope.spinner = false;
		var promise = BooksResource.deleteBookInCourse($scope.courseInstanceID, book.Book);
		promise.then(
		function (res) {
			var undoParam = {
				type: "book-delete",
				id: {
					bookID:     book.Book.ID,
					instance:   $scope.courseInstanceID
				}
			};
			centrisNotify.successWithUndo("books.DeleteSuccessMessage", undoParam);
			_.remove($scope.books, function (o) {
				return o.Book.ID === book.Book.ID;
			});
		}, function (err) {
			centrisNotify.error("books.DeleteErrorMessage");
		});
	};

	// This event gets broadcast whenever an undo command
	// is issued. We check that it matches an operation we know how to undo.
	$scope.$on("centrisUndo", function undo(event, param) {
		if (param.type === "book-delete") {
			var book = {
				ID: param.id.bookID
			};
			BooksResource.editBookInCourse(param.id.instance, book).success(function (data) {
				centrisNotify.success("books.UndoSuccess");
				$scope.books.push(data);
			});
		}
	});

	// Configurations for accordion display
	$scope.accordionConfig = {
		isOpen:     false,
		oneAtAtime: false,
		firstOpen:  true
	};

	$scope.bookProperties = [
		// Open property for accordion display
		{name: "open", defaultValue: false}
	];

	$scope.fetchingData = true;
	BooksResource.getBooksByCourseID($scope.courseInstanceID)
	.success(function (data) {
		$scope.fetchingData   = false;
		$scope.books          = data;

		// Ensure the data is sorted, such that all required books will
		// be displayed first! Once sorted by Required, the books should
		// be ordered by title.
		_.orderBy(data, ["Required", "Book.Title"], ["desc", "asc"]);

		angular.forEach(data, function (obj) {
			getCover(obj);
		});
	})
	.error(function () {
		$scope.fetchingData   = false;
		centrisNotify.error("books.GetBooksError");
	});

	// Defines the steps and calls the step id
	$scope.IntroOptions = {
		steps: [{
			element: "#books",
			position: "left",
			intro: ""
		}],
		showStepNumbers:     false,
		exitOnOverlayClick:  true,
		exitOnEsc:           true,
	};

	var introKeys = [
		"BooksIntroCourse.Book",
	];

	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
	});

	// Calling the translate files for each button and putting them into an array
	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	// Setting the translates as the text for each button
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.$on("onCentrisIntro", function(event, args) {
		if ($state.current.name === "courseinstance.books") {
			$scope.ShowBooksIntro();
		}
	});
});
