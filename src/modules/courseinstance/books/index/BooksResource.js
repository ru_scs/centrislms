"use strict";

angular.module("booksApp").factory("BooksResource",
function BooksResource(CentrisResource, $http) {
	return {
		getBooksByCourseID: function(courseInstanceID) {
			return CentrisResource.get("courses/:id","books", {id: courseInstanceID});
		},
		getBookCover: function(ISBN) {
			return $http.get("https://www.googleapis.com/books/v1/volumes?q=isbn:" + ISBN);
		},
		getBookByISBN: function(ISBN) {
			return CentrisResource.get("books/:isbn", "", {isbn: ISBN});
		},
		addBookToCourse: function(courseInstanceID, book) {
			return CentrisResource.post("courses/:id", "books", {id: courseInstanceID}, book);
		},
		editBookInCourse: function(courseInstanceID, book) {
			var param = {
				courseInstanceID: courseInstanceID,
				id: book.ID
			};
			return CentrisResource.put("courses", ":courseInstanceID/books/:id", param, book);
		},
		deleteBookInCourse: function(courseInstanceID, book) {
			var param = {
				courseInstanceID: courseInstanceID,
				id: book.ID
			};
			return CentrisResource.remove("courses", ":courseInstanceID/books/:id", param);
		}
	};
});
