	"use strict";

describe("BooksController", function() {

	beforeEach(module("booksApp"));
	beforeEach(module("mockConfig"));
	beforeEach(module("sharedServices"));
	beforeEach(module("ui.bootstrap"));
	beforeEach(module("mockCourseInstanceBookDlg"));
	beforeEach(module("mockCourseInstanceBooks"));

	var ctrl, mockResource, scope, mockBookDlg, mockCentris;

	var mockRoleProvider = {
		isTeacherInCourse: function(courseInstanceID) {
			return {
				then: function(fn) {
					fn(true);
				}
			};
		}
	};

	var mockStateParams = {
		courseInstanceID: 7
	};

	beforeEach(inject(function($rootScope, $controller, mockBooksResource, mockCentrisNotify, MockBookDlg) {
		scope = $rootScope.$new();
		scope.book = {};
		mockResource = mockBooksResource;
		mockCentris = mockCentrisNotify;
		mockBookDlg = MockBookDlg;
		spyOn(mockResource, "addBookToCourse").and.callThrough();
		spyOn(mockResource, "editBookInCourse").and.callThrough();
		spyOn(mockResource, "deleteBookInCourse").and.callThrough();
		spyOn(mockResource, "getBooksByCourseID").and.callThrough();
		spyOn(mockCentrisNotify, "success").and.callThrough();
		spyOn(mockCentrisNotify, "successWithUndo").and.callThrough();
		spyOn(mockCentrisNotify, "error").and.callThrough();
		spyOn(mockBookDlg, "show").and.callThrough();
		ctrl = $controller("BooksController", {
			$scope: scope,
			BooksResource: mockResource,
			CourseInstanceRoleService: mockRoleProvider,
			BookDlg: mockBookDlg,
			centrisNotify: mockCentris,
			$stateParams: mockStateParams
		});
	}));
	var book = {

		Book: {
			ISBN:        "",
			Title:       "This is the first book",
			Published:   "",
			Publisher:   "",
			Release:     "",
			Author:      "",
			Memo:        "This book is terrible, don't buy it",
			ISBN13:      "",
			Required:    false
		}
	};

	it("should define functions and variables", function() {
		// Variables:
		expect(scope.books).toBeDefined();
		expect(scope.courseInstanceID).toBeDefined();

		// Functions:Expected
		expect(mockResource.addBookToCourse).toBeDefined();
		expect(mockResource.editBookInCourse).toBeDefined();
		expect(mockResource.deleteBookInCourse).toBeDefined();
		expect(mockResource.getBooksByCourseID).toBeDefined();
		expect(mockCentris.success).toBeDefined();
		expect(mockCentris.successWithUndo).toBeDefined();
	});

	it("should load list of books from resource", function() {
		expect(mockResource.getBooksByCourseID).toHaveBeenCalled();
	});

	it("should call addBook when teacher wants to add a new book", function() {
		scope.addBook();
		expect(mockBookDlg.show).toHaveBeenCalled();
		expect(mockResource.addBookToCourse).toHaveBeenCalled();
		expect(mockCentris.success).toHaveBeenCalled();
	});

	it("should call editBook when teacher wants to edit an existing book", function() {
		scope.editBook(book);
		expect(mockBookDlg.show).toHaveBeenCalled();
		expect(mockResource.editBookInCourse).toHaveBeenCalled();
		expect(mockCentris.success).toHaveBeenCalled();
	});

	it("should be able to delete a book", function() {
		scope.deleteBook(book);
		expect(mockResource.deleteBookInCourse).toHaveBeenCalled();
		expect(mockCentris.successWithUndo).toHaveBeenCalled();
	});

	it("should not be able to add book", function() {
		mockResource.successAdd = false;
		scope.addBook();
		expect(mockBookDlg.show).toHaveBeenCalled();
		expect(mockResource.addBookToCourse).toHaveBeenCalled();
		expect(mockCentris.error).toHaveBeenCalled();
	});

	it("should not be able to edit a book", function() {
		mockResource.successEdit = false;
		scope.editBook(book);
		expect(mockBookDlg.show).toHaveBeenCalled();
		expect(mockResource.editBookInCourse).toHaveBeenCalled();
		expect(mockCentris.error).toHaveBeenCalled();
	});

	it("should not be able to delete a book", function() {
		mockResource.successDelete = false;
		scope.deleteBook(book);
		expect(mockResource.deleteBookInCourse).toHaveBeenCalled();
		expect(mockCentris.error).toHaveBeenCalled();
	});

	it("should not be able to getBooksByCourseID", function() {
		// TODO: this code doesn't actually test anything!
		// mockResource.successGetList = false;
		// expect(mockResource.getBooksByCourseID).toHaveBeenCalled();
	});
});