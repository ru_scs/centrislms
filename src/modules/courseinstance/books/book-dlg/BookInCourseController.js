"use strict";

/**
 * Controller for adding a book to a course.
 * If a book is found you can disable editing of the fields by changing
 * the '$scope.editDisabled' variable to true, currently not used but implemented
 * if we want to enable it in the future
 */
angular.module("booksApp").controller("BookInCourseController",
function ($scope, BooksResource, modalParam, centrisNotify) {
	$scope.ISBN            = "";
	$scope.spinner         = false;
	$scope.editDisabled    = false;
	$scope.maxYear         = new Date().getFullYear();
	$scope.editDisabled    = false;
	$scope.dialogTitle     = "bookincourse.AddingBook";
	$scope.isISBNDisabled  = false;
	$scope.isEditing       = false;
	$scope.missingTitle    = false;

	if (modalParam.book === undefined) {
		$scope.book            = {
			ISBN:        "",
			ISBN13:      "",
			Title:       "",
			Memo:        "",
			Published:   "",
			Publisher:   "",
			Release:     "",
			Author:      "",
			CourseMemo:  "",
			Required:    false
		};
	} else {
		$scope.book            = {
			ID:         modalParam.book.ID,
			ISBN:       modalParam.book.ISBN,
			ISBN13:     modalParam.book.ISBN13,
			Title:      modalParam.book.Title,
			Memo:       modalParam.book.Memo,
			Published:  modalParam.book.Published,
			Publisher:  modalParam.book.Publisher,
			Release:    modalParam.book.Release,
			Author:     modalParam.book.Author,
			CourseMemo: modalParam.book.CourseMemo,
			Required:   false
		};
		$scope.dialogTitle    = "bookincourse.EditingBook";
		$scope.isISBNDisabled = true;
		$scope.isEditing      = true;
	}

	$scope.$watch("book.Title", function (newValue, oldValue) {
		if (newValue.length > 0) {
			$scope.missingTitle = false;
		}
	});

	// When user submits the book-in-course-dialog this function gets called
	$scope.closeDialog = function () {
		if (!$scope.book.Title) {
			$scope.missingTitle = true;
			return;
		}

		if ($scope.book.Published === undefined) {
			$scope.book.Published = "";
		}
		$scope.$close($scope.book);
	};

	// When a user changes to another input this function gets called
	$scope.lookUp = function () {
		var isbn = $scope.book.ISBN;
		isbn = isbn.replace("-", "");
		isbn = isbn.replace(" ", "");

		if (isbn.length !== 10 && isbn.length !== 13) {
			return;
		}

		$scope.spinner = true;
		var promise = BooksResource.getBookByISBN(isbn);
		promise.then(
		function (res) {
			$scope.spinner = false;
			$scope.book.Title     = res.data.Title;
			$scope.book.Publisher = res.data.Publisher;
			$scope.book.Author    = res.data.Author;
			$scope.book.ISBN      = res.data.ISBN;
			$scope.book.ISBN13    = res.data.ISBN13;
		}, function (err) {
			$scope.spinner = false;
		});
	};
});
