"use strict";

describe("BookInCourseController", function() {

	beforeEach(module("booksApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockConfig"));
	beforeEach(module("ui.bootstrap"));
	beforeEach(module("mockCourseInstanceBooks"));

	var scope, controller, resource, mockModalParam;
	beforeEach(function() {
		resource = {
			succeeds: true,
			requestResult: {
				data: {
					Title: "MyBook",
					Publisher: "Höskuldur Ágústsson Publishing",
					Author: "Höskuldur Ágústsson",
					ISBN: "0123456789",
					ISBN13: "0123456789123"
				}
			},
			getBookByISBN: function() {
				return {
					then: function(fn, fnErr) {
						if (resource.succeeds) {
							fn(resource.requestResult);
						} else {
							fnErr();
						}
					}
				};
			}
		};
	});

	beforeEach(inject(function($rootScope, $controller, mockCentrisNotify) {
		scope = $rootScope.$new();
		scope.$close = function() {

		};

		spyOn(resource, "getBookByISBN").and.callThrough();
		spyOn(scope, "$close").and.callThrough();
		controller = $controller("BookInCourseController", {
			$scope: scope,
			modalParam: mockModalParam,
			BooksResource: resource,
			centrisNotify: mockCentrisNotify
		});
	}));

	it("should define all functions and variables", function() {
		// Variables
		expect(scope.closeDialog).toBeDefined();
		expect(scope.lookUp).toBeDefined();
		expect(scope.book).toBeDefined();
	});

	it("should not run $close if title is empty", function () {
		scope.book.Title = "";
		scope.closeDialog();
		expect(scope.$close).not.toHaveBeenCalled();
	});

	it("should run $close if title exists", function () {
		scope.book.Title = "Gamli Nói og ævintýri hans";
		scope.closeDialog();
		expect(scope.$close).toHaveBeenCalled();
	});

	it("should not run $close if Published is undefined", function () {
		scope.book.Title = "Gamli Nói og ævintýri hans";
		scope.book.Published = undefined;
		scope.closeDialog();
		expect(scope.book.Published).toBeDefined();
	});

	describe("when adding book", function() {
		var emptyMockModalParam = {
			book: undefined
		};
		beforeEach(inject(function($rootScope, $controller, mockCentrisNotify) {
			scope = $rootScope.$new();
			scope.$close = function() {

			};

			spyOn(scope, "$close").and.callThrough();
			controller = $controller("BookInCourseController", {
				$scope: scope,
				modalParam: emptyMockModalParam,
				BooksResource: resource,
				centrisNotify: mockCentrisNotify
			});
		}));

		it("should define all variables", function() {
			expect(scope.book.Title).toBe("");
		});
	});

	describe("when editing book", function() {
		mockModalParam = {
			book: {
				ISBN:        "0123456789",
				ISBN13:      "0123456789123",
				Title:       "HössaBók",
				Memo:        "Good Book",
				Published:   "26.09.1994",
				Publisher:   "Höskuldur Ágústsson Publishing",
				Release:     "",
				Author:      "Höskuldur Ágústsson",
				CourseMemo:  "",
				Required:    true
			}
		};

		it("should show the value of all variables", function() {
			expect(scope.book.ISBN).toBe(mockModalParam.book.ISBN);
		});

		it("should not allow user to delete book Title", function() {

		});
	});

	it("should not fill out book info if ISBN is invalid", function () {
		scope.book.ISBN = "23";
		scope.lookUp();
		expect(resource.getBookByISBN).not.toHaveBeenCalled();
	});

	it("should fill out book info if ISBN is ten characters", function () {
		scope.book.ISBN = "2234342432";
		scope.lookUp();
		expect(resource.getBookByISBN).toHaveBeenCalled();
		expect(scope.book.Title).toBe(resource.requestResult.data.Title);
	});

	it("should fill out book info if ISBN is thirteen characters", function () {
		scope.book.ISBN = "2234342432890";
		scope.lookUp();
		expect(resource.getBookByISBN).toHaveBeenCalled();
		expect(scope.book.Title).toBe(resource.requestResult.data.Title);
	});

	it("should not fill out book info if getBookByISBN returns false", function () {
		scope.book.ISBN = "2234342432890";
		resource.succeeds = false;
		var bookTitle = scope.book.Title;
		scope.lookUp();
		expect(resource.getBookByISBN).toHaveBeenCalled();
		expect(scope.book.Title).not.toBe(resource.requestResult.data.Title);
		expect(scope.book.Title).toBe(bookTitle);
	});

});