"use strict";

angular.module("mockCourseInstanceBookDlg", []).factory("MockBookDlg", function() {
	var mock = {
		show: function () {
			return {
				then: function(fn) {
					fn();
				}
			};
		}
	};
	return mock;
});