"use strict";

angular.module("booksApp").factory("BookDlg",
function BookDlg($uibModal) {
	return {
		show: function show(courseInstanceID, model) {
			var templateInstance = {
				controller:  "BookInCourseController",
				templateUrl: "courseinstance/books/book-dlg/book-dlg.tpl.html",
				resolve: {
					modalParam: function() {
						return {
							book: model,
							courseInstanceID: courseInstanceID
						};
					}
				}
			};

			var modalInstance = $uibModal.open(templateInstance);

			return modalInstance.result;
		}
	};
});
