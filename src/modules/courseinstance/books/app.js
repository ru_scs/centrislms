"use strict";

angular.module("booksApp", ["pascalprecht.translate", "ui.router"])
.config(function ($stateProvider) {
	$stateProvider.state("courseinstance.books", {
		url:         "/books",
		templateUrl: "courseinstance/books/index/index.html",
		controller:  "BooksController"
	});
});
