"use strict";

/**
 * AssignmentListController => Display list of assignments for specific course.
 */

angular.module("assignmentsApp").controller("AssignmentListController",
	function($scope, $stateParams, AssignmentResource, centrisNotify, CourseInstanceRoleService, $translate, $state) {

		// variables for csv file
		$scope.csvHeader = [];
		$scope.csvObject = [];

		// TODO: load/save the config for which columns to show!
		$scope.table = {
			columns: [
				{name: "assignments.Weight",          visible: true,  alias: "Weight"},
				{name: "assignments.PublishDate",     visible: true,  alias: "Published date"},
				{name: "assignments.DueDate",         visible: true,  alias: "Due date"},
				{name: "assignments.HandinStatus",    visible: true,  alias: "Handin status"},
				{name: "assignments.Category",       visible: true,  alias: "Categories"}
			]
		};
		// shows or hides columns
		$scope.toggleColumn = function toggleColumn(column) {
			column.visible = !column.visible;
		};
		// first line in csv file.
		$scope.makeCSVHeader = function makeCSVHeader() {
			var lang = $translate.use() === "is";
			$scope.csvHeader = [];
			if (lang) {
				$scope.csvHeader.push("Titill");
				$scope.csvHeader.push("Vægi");
				$scope.csvHeader.push("Lagt fyrir");
				$scope.csvHeader.push("Skiladagur");
				$scope.csvHeader.push("Yfirferðarstaða");
				$scope.csvHeader.push("Flokkar");
			} else {
				$scope.csvHeader.push("Title");
				$scope.csvHeader.push("Weight");
				$scope.csvHeader.push("Published date");
				$scope.csvHeader.push("Due date");
				$scope.csvHeader.push("Grading status");
				$scope.csvHeader.push("Categories");
			}

			$scope.getCSV();
		};

		// Gets percentage of graded handins for csv.
		var getHandinStatus = function getHandinStatus (c) {
			var status = "";
			if (c.HandinCount <= 0) {
				return "No handin";
			} else {
				return status = (c.GradedCount / c.HandinCount) * 100 + "%";
			}
		};
		// returns categories as string. if string has quotation marks -> replaces with dash for csv.
		var getCategoryList = function getCategoryList (c) {
			var categories = c.Categories.toString();
			if (categories.length === 0) {
				return "No categories";
			} else if (categories.includes("\"")) {
				return _.replace(categories,"\"","-");
			} else {
				return categories;
			}
		};
		// After assignments have been fetched we create
		// CSV file for the assignment list
		$scope.getCSV = function getCSVs() {
			var lang = $translate.use() === "is";
			$scope.csvObject = [];
			$scope.assignments.forEach(function forEachAssignment(a) {
				var tmpAssignment = {};
				if (lang) {
					tmpAssignment["Titill"]         = a.Title;
					tmpAssignment["vægi"]           = a.Weight;
					tmpAssignment["Lagt fyrir"]     = a.DatePublished;
					tmpAssignment["Skiladagur"]     = a.DateClosed;
					tmpAssignment["Yfirferðastaða"] = getHandinStatus(a);
					tmpAssignment["Flokkar"]        = getCategoryList(a);
				} else {
					tmpAssignment["Title"]          = a.Title;
					tmpAssignment["Weight"]         = a.Weight;
					tmpAssignment["Published date"] = a.DatePublished;
					tmpAssignment["Due date"]       = a.DateClosed;
					tmpAssignment["Grading status"] = getHandinStatus(a);
					tmpAssignment["Categories"]     = getCategoryList(a);

				}

				$scope.csvObject.push(tmpAssignment);
			});
		};

		// Intial variable for collecting assignments in.
		$scope.courseInstanceID = $stateParams.courseInstanceID;
		$scope.teacherOrAdmin = false;
		$scope.assignments = [];

		CourseInstanceRoleService.isTeacherInCourse($scope.courseInstanceID)
			.then(function(isTeacher) {
				$scope.teacherOrAdmin = isTeacher;
				// adds action for students to view/hide their grade.
				if (!$scope.teacherOrAdmin) {
					var obj = {name: "assignments.Grade", visible: true,  alias: "Grade"};
					$scope.table.columns.push(obj);
				}
			});

		$scope.getAllAssignmentsWeight = function(data) {
			var weight = 0;
			for (var i = 0; i < data.length; i++) {
				weight = (data[i].Weight ? data[i].Weight : 0) + weight;
			}
			return weight;
		};

		// Gets all assignments in this given course and semester.
		($scope.getAllAssignments = function() {
			$scope.fetchingData = true;
			AssignmentResource.getCourseInstanceAssignments($scope.courseInstanceID)
				.success(function(data) {
					$scope.fetchingData = false;
					// sorts Categories with case insensitive.
					for (var i = 0; i < data.length; i++) {
						if (data[i].Categories) {
							data[i].Categories.sort(function (a, b) {
								return a.toLowerCase().localeCompare(b.toLowerCase());
							});
						}
					}
					$scope.assignments = data;
					$scope.assignmentWeight = $scope.getAllAssignmentsWeight($scope.assignments);
				}).error(function(error, data) {
					$scope.fetchingData = false;
				});
		}).call();

		// Retry button on the error message.
		$scope.retry = function retry() {
			$scope.getAllAssignments();
		};

		$scope.publishGrades = function publishGrades(assignment) {
			AssignmentResource.publishAllGradesForAssignment($scope.courseInstanceID, assignment.ID)
				.success(function(data) {
					centrisNotify.success("assignments.Msg.PublishingGradesSucceeded");
				});
		};

		$scope.deleteAssignment = function deleteAssignment(assignment) {
			AssignmentResource.deleteAssignment($scope.courseInstanceID, assignment.ID).success(function(data) {
				var undoParam = {
					type: "assignment-delete",
					id: {
						assignment: assignment.ID,
						instance: $scope.courseInstanceID
					}
				};

				centrisNotify.successWithUndo("assignments.Msg.DeleteSuccessful", undoParam);

				var index = $scope.assignments.indexOf(assignment);
				if (index !== -1) {
					$scope.assignments.splice(index, 1);
				}
				$scope.assignmentWeight = $scope.getAllAssignmentsWeight($scope.assignments);
			});
		};

		// This event gets broadcast whenever an undo command
		// is issued. We check that it matches an operation we know how to undo.
		$scope.$on("centrisUndo", function undo(event, param) {
			if (param.type === "assignment-delete") {
				AssignmentResource.editAssignment(param.id.instance, param.id.assignment, undefined).success(function(data) {
					centrisNotify.success("assignments.Msg.AssignmentDeleteUndoSuccess");
					$scope.assignments.push(data);
					$scope.assignmentWeight = $scope.getAllAssignmentsWeight($scope.assignments);
				});
			}
		});

		// This event gets broadcast from the "New Assignment" controller
		// when (surprise, surprise!) a new assignment has been created
		$scope.$on("assignmentCreated", function onCreated(event, assignment) {
			$scope.assignments.push(assignment);
			$scope.assignmentWeight = $scope.getAllAssignmentsWeight($scope.assignments);
		});

		// Then there is another event which get broadcast when an assignment
		// has been edited. This does not mean that we should add/remove items
		// from our list, but it could mean a change in name/weight, start/end dates
		// etc.
		$scope.$on("assignmentEdited", function onEdited(event, assignment) {
			for (var i = 0; i < $scope.assignments.length; ++i) {
				var current = $scope.assignments[i];
				if (current.ID === assignment.ID) {
					current.Title = assignment.Title;
					current.Anonymity = assignment.Anonymity;
					current.DateClosed = assignment.DateClosed;
					current.DatePublished = assignment.DatePublished;
					current.Weight = assignment.Weight;
					current.Categories = assignment.Categories;
				}
			}

			$scope.assignmentWeight = $scope.getAllAssignmentsWeight($scope.assignments);
		});

		// Starting helper viewer
		// Defines the steps and calls the step id
		$scope.IntroStudentOptions = {
			steps: [{
				element: "#content",
				position: "right",
				intro: ""
			}, {
				element: "#assignments-student-helper-step2",
				position: "right",
				intro: ""
			}, {
				element: "#assignments-student-helper-step3",
				position: "right",
				intro: ""
			}, {
				element: "#courseStep4",
				position: "left",
				intro: ""
			}],
			showStepNumbers: false,
			exitOnOverlayClick: true,
			exitOnEsc: true,
		};

		$scope.IntroTeacherOptions = {
			steps: [{
				element: "#content",
				position: "right",
				intro: ""
			}, {
				element: "#assignments-student-helper-step2",
				position: "right",
				intro: ""
			},{
				element: "#assignments-teacher-helper-step3",
				position: "right",
				intro: ""
			}, {
				element: "#assignments-student-helper-step3",
				position: "right",
				intro: ""
			}, {
				element: "#assignments-teacher-helper-step4",
				position: "left",
				intro: ""
			}],
			showStepNumbers: false,
			exitOnOverlayClick: true,
			exitOnEsc: true,
		};

		// Calling the translate files for each step and putting them into an array
		var studentIntroKeys = [
			"assignments.Helper.Student.Step1",
			"assignments.Helper.Student.Step2",
			"assignments.Helper.Student.Step3",
			"assignments.Helper.Student.Step4"
		];

		var teacherIntroKeys = [
			"assignments.Helper.Teacher.Step1",
			"assignments.Helper.Teacher.Step2",
			"assignments.Helper.Teacher.Step3",
			"assignments.Helper.Teacher.Step4",
			"assignments.Helper.Teacher.Step5"
		];

		// Setting the translates as the text for each step
		$translate(teacherIntroKeys).then(function whenDoneTranslating(translations) {
			$scope.IntroTeacherOptions.steps[0].intro = translations[teacherIntroKeys[0]];
			$scope.IntroTeacherOptions.steps[1].intro = translations[teacherIntroKeys[1]];
			$scope.IntroTeacherOptions.steps[2].intro = translations[teacherIntroKeys[2]];
			$scope.IntroTeacherOptions.steps[3].intro = translations[teacherIntroKeys[3]];
			$scope.IntroTeacherOptions.steps[4].intro = translations[teacherIntroKeys[4]];
		});

		$translate(studentIntroKeys).then(function whenDoneTranslating(translations) {
			$scope.IntroStudentOptions.steps[0].intro = translations[studentIntroKeys[0]];
			$scope.IntroStudentOptions.steps[1].intro = translations[studentIntroKeys[1]];
			$scope.IntroStudentOptions.steps[2].intro = translations[studentIntroKeys[2]];
			$scope.IntroStudentOptions.steps[3].intro = translations[studentIntroKeys[3]];
		});

		// Calling the translate files for each button and putting them into an array
		var introButtons = [
			"AngularIntro.Next",
			"AngularIntro.Prev",
			"AngularIntro.Skip",
			"AngularIntro.Done"
		];

		// Setting the translates as the text for each button
		$translate(introButtons).then(function whenDoneTranslating(translations) {
			$scope.IntroStudentOptions.nextLabel = translations[introButtons[0]];
			$scope.IntroStudentOptions.prevLabel = translations[introButtons[1]];
			$scope.IntroStudentOptions.skipLabel = translations[introButtons[2]];
			$scope.IntroStudentOptions.doneLabel = translations[introButtons[3]];
		});

		$translate(introButtons).then(function whenDoneTranslating(translations) {
			$scope.IntroTeacherOptions.nextLabel = translations[introButtons[0]];
			$scope.IntroTeacherOptions.prevLabel = translations[introButtons[1]];
			$scope.IntroTeacherOptions.skipLabel = translations[introButtons[2]];
			$scope.IntroTeacherOptions.doneLabel = translations[introButtons[3]];
		});

		$scope.$on("onCentrisIntro", function(event, args) {
			// Only show this if the current route is for our page:
			if ($state.current.name === "courseinstance.assignments") {
				if (!$scope.teacherOrAdmin) {
					$scope.ShowMyAssignmentsStudentIntro();
				} else {
					$scope.ShowMyAssignmentsTeacherIntro();
				}
			}
		});
	});
