"use strict";

describe("AssignmentListController", function() {

	var ctrl, scope, state, stateParams, translate, resource, notify, roleService;

	beforeEach(module("courseInstanceShared"));
	beforeEach(module("sharedServices"));
	beforeEach(module("assignmentsApp"));

	beforeEach(inject(function($rootScope) {
		scope = $rootScope.$new();
	}));

	beforeEach(function() {
		translate = function mockTranslating(keys) {
			var translations = [];
			for (var i = 0; i < keys.length; i++) {
				translations[keys[i]] = keys[i];
			}
			return {
				then: function mockTranslatingThen(fn) {
					fn(translations);
				}
			};
		};
	});

	beforeEach(inject(function(mockAssignmentResource) {
		resource = mockAssignmentResource;
	}));

	beforeEach(inject(function(mockCentrisNotify) {
		notify = mockCentrisNotify;
	}));

	beforeEach(inject(function($injector) {
		roleService = $injector.get("MockCourseInstanceRoleService");
	}));

	describe("Initalize", function() {

		beforeEach(function() {
			stateParams = {
				courseInstanceID: 1337
			};
		});

		beforeEach(function() {
			resource.setCourseInstanceAssignmentsAnswers([{}]);
			resource.setCourseInstanceAssignmentsConditions([
				undefined
			]);
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentListController", {
				$scope: scope,
				$stateParams: stateParams,
				AssignmentResource: resource,
				centrisNotify: notify,
				CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
				$translate: translate,
				$state: state
			});
		}));

		it("should set courseInstanceID", function() {
			expect(scope.courseInstanceID).toEqual(1337);
		});

		it("should set teacherOrAdmin", function() {
			expect(scope.teacherOrAdmin).toEqual(false);
		});

		it("should set assignments", function() {
			expect(scope.assignments).toEqual([]);
		});

		it("should set have fetchingData true", function() {
			expect(scope.fetchingData).toEqual(true);
		});

	});

	describe("resource success", function() {

		var assignments = [{
			Weight: 3
		}, {
			Weight: 6
		}, {
			Weight: 0
		}];

		beforeEach(function() {
			stateParams = {
				courseInstanceID: 1337
			};
		});

		beforeEach(function() {
			resource.setCourseInstanceAssignmentsAnswers([assignments]);
			resource.setCourseInstanceAssignmentsConditions([
				true
			]);
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentListController", {
				$scope: scope,
				$stateParams: stateParams,
				AssignmentResource: resource,
				centrisNotify: notify,
				CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
				$translate: translate,
				$state: state
			});
		}));

		it("should set assignmentWeight", function() {
			expect(scope.assignmentWeight).toEqual(9);
		});

		it("should set fetchingData to false", function() {
			expect(scope.fetchingData).toEqual(false);
		});

		it("should set assignments", function() {
			expect(scope.assignments).toEqual(assignments);
		});

	});

	describe("resource failure", function() {

		var assignments = [];

		beforeEach(function() {
			stateParams = {
				courseInstanceID: 1337
			};
		});

		beforeEach(function() {
			resource.setCourseInstanceAssignmentsAnswers([assignments]);
			resource.setCourseInstanceAssignmentsConditions([
				false
			]);
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentListController", {
				$scope: scope,
				$stateParams: stateParams,
				AssignmentResource: resource,
				centrisNotify: notify,
				CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
				$translate: translate,
				$state: state
			});
		}));

		it("should set fetchingData to false", function() {
			expect(scope.fetchingData).toEqual(false);
		});

	});

	describe("getAllAssignmentsWeight", function() {

		beforeEach(function() {
			resource.setCourseInstanceAssignmentsAnswers([{}]);
			resource.setCourseInstanceAssignmentsConditions([
				undefined
			]);
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentListController", {
				$scope: scope,
				$stateParams: stateParams,
				AssignmentResource: resource,
				centrisNotify: notify,
				CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
				$translate: translate,
				$state: state
			});
		}));

		it("should return the weight of assignments", function() {
			var assignments = [{
				Weight: 10
			}, {
				Weight: 5
			}, {
				Weight: 15
			}];
			expect(scope.getAllAssignmentsWeight(assignments)).toEqual(30);
		});

		it("should assume undefined is zero", function() {
			var assignments = [{
				Weight: 10
			}, {
				Weight: undefined
			}, {
				Weight: 15
			}];
			expect(scope.getAllAssignmentsWeight(assignments)).toEqual(25);
		});

	});

	describe("retry", function() {

		beforeEach(function() {
			stateParams = {};
		});

		beforeEach(function() {
			resource.setCourseInstanceAssignmentsAnswers([{}]);
			resource.setCourseInstanceAssignmentsConditions([
				undefined
			]);
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentListController", {
				$scope: scope,
				$stateParams: stateParams,
				AssignmentResource: resource,
				centrisNotify: notify,
				CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
				$translate: translate,
				$state: state
			});
		}));

		it("should call getAllAssignments", function() {
			spyOn(scope, "getAllAssignments");
			scope.retry();
			expect(scope.getAllAssignments).toHaveBeenCalled();
		});

	});

	describe("publishGrades", function() {

		beforeEach(function() {
			stateParams = {
				courseInstanceID: 1337
			};
		});

		beforeEach(function() {
			resource.setCourseInstanceAssignmentsAnswers([{}]);
			resource.setCourseInstanceAssignmentsConditions([
				undefined
			]);
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentListController", {
				$scope: scope,
				$stateParams: stateParams,
				AssignmentResource: resource,
				centrisNotify: notify,
				CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
				$translate: translate,
				$state: state
			});
		}));

		it("should call AssignmentResource", function() {
			var assignment = {
				ID: 33
			};
			resource.setPublishAllGradesForAssignmentAnswers([{}]);
			resource.setPublishAllGradesForAssignmentConditions([
				undefined
			]);
			spyOn(resource, "publishAllGradesForAssignment").and.callThrough();
			scope.publishGrades(assignment);
			expect(resource.publishAllGradesForAssignment).toHaveBeenCalledWith(1337, 33);
		});

		it("should notify user on success", function() {
			var assignment = {
				ID: 33
			};
			resource.setPublishAllGradesForAssignmentAnswers([{}]);
			resource.setPublishAllGradesForAssignmentConditions([
				true
			]);
			spyOn(notify, "success");
			scope.publishGrades(assignment);
			expect(notify.success).toHaveBeenCalledWith("assignments.Msg.PublishingGradesSucceeded");
		});

	});

	describe("deleteAssignment", function() {

		beforeEach(function() {
			stateParams = {
				courseInstanceID: 1337
			};
		});

		beforeEach(function() {
			resource.setCourseInstanceAssignmentsAnswers([{}]);
			resource.setCourseInstanceAssignmentsConditions([
				undefined
			]);
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentListController", {
				$scope: scope,
				$stateParams: stateParams,
				AssignmentResource: resource,
				centrisNotify: notify,
				CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
				$translate: translate,
				$state: state
			});
		}));

		it("should call resource deleteAssignment", function() {
			var assignment = {
				ID: 33
			};
			resource.setDeleteAssignmentAnswers([{}]);
			resource.setDeleteAssignmentConditions([
				undefined
			]);
			spyOn(resource, "deleteAssignment").and.callThrough();
			scope.deleteAssignment(assignment);
			expect(resource.deleteAssignment).toHaveBeenCalledWith(1337, 33);
		});

		it("success should notify user", function() {
			var assignment = {
				ID: 33
			};
			var undoParam = {
				type: "assignment-delete",
				id: {
					assignment: 33,
					instance: 1337
				}
			};
			resource.setDeleteAssignmentAnswers([{}]);
			resource.setDeleteAssignmentConditions([
				true
			]);
			spyOn(notify, "successWithUndo");
			scope.deleteAssignment(assignment);
			expect(notify.successWithUndo).toHaveBeenCalledWith("assignments.Msg.DeleteSuccessful", undoParam);
		});

		it("success should remove assignment from assignments", function() {
			var assignment = {
				ID: 33,
				Weight: 8
			};
			scope.assignments = [{
				ID: 13,
				Weight: 13
			}, assignment, {
				ID: 47,
				Weight: 12
			}];
			resource.setDeleteAssignmentAnswers([{}]);
			resource.setDeleteAssignmentConditions([
				true
			]);
			scope.deleteAssignment(assignment);
			expect(scope.assignments).toEqual([{
				ID: 13,
				Weight: 13
			}, {
				ID: 47,
				Weight: 12
			}]);
		});

		it("success should recalculate weight", function() {
			var assignment = {
				ID: 33,
				Weight: 8
			};
			scope.assignments = [{
				ID: 13,
				Weight: 13
			}, assignment, {
				ID: 47,
				Weight: 12
			}];
			resource.setDeleteAssignmentAnswers([{}]);
			resource.setDeleteAssignmentConditions([
				true
			]);
			scope.deleteAssignment(assignment);
			expect(scope.assignmentWeight).toEqual(25);
		});

	});

	describe("onCentrisIntro", function() {

		beforeEach(function() {
			stateParams = {};
		});

		beforeEach(function() {
			resource.setCourseInstanceAssignmentsAnswers([{}]);
			resource.setCourseInstanceAssignmentsConditions([
				undefined
			]);
		});

		describe("student", function() {

			beforeEach(function() {
				scope.$on = function(name, fn) {
					scope.ShowMyAssignmentsStudentIntro = jasmine.createSpy();
					scope.ShowMyAssignmentsTeacherIntro = jasmine.createSpy();
					if (name === "onCentrisIntro") {
						fn();
					}
				};
			});

			describe("state current name === assignments", function() {

				beforeEach(function() {
					state = {
						current: {
							name: "courseinstance.assignments"
						}
					};
				});

				beforeEach(inject(function($controller) {
					ctrl = $controller("AssignmentListController", {
						$scope: scope,
						$stateParams: stateParams,
						AssignmentResource: resource,
						centrisNotify: notify,
						CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
						$translate: translate,
						$state: state
					});
				}));

				it("should call ShowMyAssignmentsStudentIntro", function() {
					expect(scope.ShowMyAssignmentsStudentIntro).toHaveBeenCalled();
				});

			});

			describe("state current name !== assignments", function() {

				beforeEach(function() {
					state = {
						current: {
							name: "not.courseinstance.assignments"
						}
					};
				});

				beforeEach(inject(function($controller) {
					ctrl = $controller("AssignmentListController", {
						$scope: scope,
						$stateParams: stateParams,
						AssignmentResource: resource,
						centrisNotify: notify,
						CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
						$translate: translate,
						$state: state
					});
				}));

				it("should not call an intro", function() {
					expect(scope.ShowMyAssignmentsStudentIntro).not.toHaveBeenCalled();
					expect(scope.ShowMyAssignmentsTeacherIntro).not.toHaveBeenCalled();
				});

			});

		});

		describe("teacher", function() {

			beforeEach(function() {
				scope.$on = function(name, fn) {
					scope.ShowMyAssignmentsStudentIntro = jasmine.createSpy();
					scope.ShowMyAssignmentsTeacherIntro = jasmine.createSpy();
					scope.teacherOrAdmin = true;
					if (name === "onCentrisIntro") {
						fn();
					}
				};
			});

			describe("state current name === assignments", function() {

				beforeEach(function() {
					state = {
						current: {
							name: "courseinstance.assignments"
						}
					};
				});

				beforeEach(inject(function($controller) {
					ctrl = $controller("AssignmentListController", {
						$scope: scope,
						$stateParams: stateParams,
						AssignmentResource: resource,
						centrisNotify: notify,
						CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(true),
						$translate: translate,
						$state: state
					});
				}));

				it("should call ShowMyAssignmentsTeacherIntro", function() {
					expect(scope.ShowMyAssignmentsTeacherIntro).toHaveBeenCalled();
				});

			});

			describe("state current name !== assignments", function() {

				beforeEach(function() {
					state = {
						current: {
							name: "not.courseinstance.assignments"
						}
					};
				});

				beforeEach(inject(function($controller) {
					ctrl = $controller("AssignmentListController", {
						$scope: scope,
						$stateParams: stateParams,
						AssignmentResource: resource,
						centrisNotify: notify,
						CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(true),
						$translate: translate,
						$state: state
					});
				}));

				it("should not call an intro", function() {
					expect(scope.ShowMyAssignmentsStudentIntro).not.toHaveBeenCalled();
					expect(scope.ShowMyAssignmentsTeacherIntro).not.toHaveBeenCalled();
				});

			});

		});

	});

	describe("assignmentCreated", function() {

		beforeEach(function() {
			stateParams = {};
		});

		beforeEach(function() {
			resource.setCourseInstanceAssignmentsAnswers([{}]);
			resource.setCourseInstanceAssignmentsConditions([
				undefined
			]);
		});

		var assignments;

		beforeEach(function() {
			assignments = [{
				Weight: 10
			}, {
				Weight: 12
			}, {
				Weight: 7
			}];
		});

		beforeEach(function() {
			scope.$on = function(name, fn) {
				if (name === "assignmentCreated") {
					scope.assignments = assignments;
					fn(undefined, {
						Weight: 10
					});
				}
			};
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentListController", {
				$scope: scope,
				$stateParams: stateParams,
				AssignmentResource: resource,
				centrisNotify: notify,
				CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
				$translate: translate,
				$state: state
			});
		}));

		it("should add assignment to assignments", function() {
			expect(scope.assignments).toEqual([{
				Weight: 10
			}, {
				Weight: 12
			}, {
				Weight: 7
			}, {
				Weight: 10
			}]);
		});

		it("should recalculate assignmentWeight", function() {
			expect(scope.assignmentWeight).toEqual(39);
		});

	});

	describe("centrisUndo", function() {

		beforeEach(function() {
			stateParams = {};
		});

		beforeEach(function() {
			resource.setCourseInstanceAssignmentsAnswers([{}]);
			resource.setCourseInstanceAssignmentsConditions([
				undefined
			]);
		});

		var assignments;

		beforeEach(function() {
			assignments = [{
				Weight: 10
			}, {
				Weight: 12
			}, {
				Weight: 7
			}];
		});

		beforeEach(function() {
			var assignment = {
				Weight: 30
			};
			resource.setEditAssignmentConditions([
				true
			]);
			resource.setEditAssignmentAnswers([
				assignment
			]);
		});

		beforeEach(function() {
			scope.$on = function(name, fn) {
				if (name === "centrisUndo") {
					spyOn(notify, "success");
					spyOn(resource, "editAssignment").and.callThrough();
					scope.assignments = assignments;
					fn(undefined, {
						type: "assignment-delete",
						id: {
							instance: 32,
							assignment: "assignment 2"
						}
					});
				}
			};
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentListController", {
				$scope: scope,
				$stateParams: stateParams,
				AssignmentResource: resource,
				centrisNotify: notify,
				CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
				$translate: translate,
				$state: state
			});
		}));

		it("should notify user on success", function() {
			expect(notify.success).toHaveBeenCalledWith("assignments.Msg.AssignmentDeleteUndoSuccess");
		});

		it("should add assignment to assignments", function() {
			expect(scope.assignments).toEqual([{
				Weight: 10
			}, {
				Weight: 12
			}, {
				Weight: 7
			}, {
				Weight: 30
			}]);
		});

		it("should recalculate weight of assignments", function() {
			expect(scope.assignmentWeight).toEqual(59);
		});

	});

	describe("centrisUndo", function() {

		beforeEach(function() {
			stateParams = {};
		});

		beforeEach(function() {
			resource.setCourseInstanceAssignmentsAnswers([{}]);
			resource.setCourseInstanceAssignmentsConditions([
				undefined
			]);
		});

		var assignments;

		beforeEach(function() {
			assignments = [{
				Weight: 10
			}, {
				Weight: 12
			}, {
				Weight: 7
			}];
		});

		beforeEach(function() {
			var assignment = {
				Weight: 30
			};
			resource.setEditAssignmentConditions([
				true
			]);
			resource.setEditAssignmentAnswers([
				assignment
			]);
		});

		beforeEach(function() {
			scope.$on = function(name, fn) {
				if (name === "centrisUndo") {
					spyOn(resource, "editAssignment").and.callThrough();
					scope.assignments = assignments;
					fn(undefined, {
						type: "assignment-other",
						id: {
							instance: 32,
							assignment: "assignment 2"
						}
					});
				}
			};
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentListController", {
				$scope: scope,
				$stateParams: stateParams,
				AssignmentResource: resource,
				centrisNotify: notify,
				CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
				$translate: translate,
				$state: state
			});
		}));

		it("should do nothing for other param types", function() {
			expect(resource.editAssignment).not.toHaveBeenCalled();
		});

	});

	describe("assignmentEdited", function() {

		beforeEach(function() {
			stateParams = {};
		});

		beforeEach(function() {
			resource.setCourseInstanceAssignmentsAnswers([{}]);
			resource.setCourseInstanceAssignmentsConditions([
				undefined
			]);
		});

		var assignments;

		beforeEach(function() {
			assignments = [{
				ID: 10,
				Weight: 5
			}, {
				ID: 12,
				Weight: 8
			}, {
				ID: 30,
				Title: "Old Title",
				Anonymity: "Old Anonymity",
				DateClosed: "Old Date Closed",
				DatePublished: "Old Date Published",
				Weight: 15
			}, {
				ID: 7,
				Weight: 6
			}];
		});

		beforeEach(function() {
			var editedassignment = {
				ID: 30,
				Title: "New Title",
				Anonymity: "New Anonymity",
				DateClosed: "New Date Closed",
				DatePublished: "New Date Published",
				Weight: 25
			};
			scope.$on = function(name, fn) {
				if (name === "assignmentEdited") {
					scope.assignments = assignments;
					fn(undefined, editedassignment);
				}
			};
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentListController", {
				$scope: scope,
				$stateParams: stateParams,
				AssignmentResource: resource,
				centrisNotify: notify,
				CourseInstanceRoleService: roleService.mockCourseInstanceRoleService(false),
				$translate: translate,
				$state: state
			});
		}));

		it("should update assignment in assignments", function() {
			expect(scope.assignments).toEqual([{
				ID: 10,
				Weight: 5
			}, {
				ID: 12,
				Weight: 8
			}, {
				ID: 30,
				Title: "New Title",
				Anonymity: "New Anonymity",
				DateClosed: "New Date Closed",
				DatePublished: "New Date Published",
				Weight: 25,
				Categories: undefined
			}, {
				ID: 7,
				Weight: 6
			}]);
		});

		it("should update assignmentWeight", function() {
			expect(scope.assignmentWeight).toEqual(44);
		});

	});

});
