"use strict";

/*
 * AssignmentStudentHandinController => Shows the student solution for the assignment
 * (if any), and allows the user/student to upload a solution.
 */
angular.module("assignmentsApp").controller("AssignmentStudentHandinController",
function AssignmentStudentHandinController($scope, $stateParams, $location, AssignmentResource,
	centrisNotify, Upload, FileUtils) {

	var courseID = $stateParams.courseInstanceID;
	// The handin object. We will try to load it from the API,
	// but we set the main properties as well here, just to be safe.
	$scope.solution = {
		StudentMemo:        "",
		Attachments:        []
	};

	// If we are passed deadline or passed deadline + additional late return days
	// then student should not be able to handin.
	$scope.passDeadline = true;
	// This will contain all the new files which the user adds
	// to the solution handin.
	$scope.NewFiles = [];
	// This will contain all files which the user wants to remove
	// from the solution handin.
	$scope.RemovedHandinFiles = [];

	$scope.$on("onAssignmentDetails", function (event, data) {
		$scope.assignment = data.assignment;
		// For how long is the late handin allowed
		var duration = $scope.assignment.LateHandinDuration;
		// 1=days, 2=hours, 3=minutes
		var type = getDurationType($scope.assignment.LateHandinDurationType);
		var now = moment(Date.now());
		// Make sure we are not pass deadline
		// Check if late handin is allowed
		if ($scope.assignment.AllowLateHandin === true) {
			// Get late handin date
			var lateHandin = moment($scope.assignment.DateClosed).add(duration,type);
			// If deadline time is reached we want user to submit a file
			if (now < lateHandin) {
				$scope.passDeadline = false;
			} else {
				$scope.passDeadline = true;
			}
		} else {
			// Late handin not allowed get actual return date and time
			var dateClosed = moment($scope.assignment.DateClosed);
			if (now < dateClosed) {
				$scope.passDeadline = false;
			} else {
				$scope.passDeadline = true;
			}
		}
	});

	// If the student has already handed in a solution, we load it here.
	$scope.fetchingData = true;
	AssignmentResource.getStudentHandin($scope.courseInstanceID, $scope.assignmentID)
	.success(function(data) {
		$scope.solution = data;
	})
	.error(function() {
		centrisNotify.error("assignments.Msg.ErrorLoadingSolution");
	})
	.finally(function() {
		$scope.fetchingData = false;
	});

	// Upload files, and post\edit the group solution.
	$scope.submitChanges = function submitChanges() {
		// Delete the selected files.
		// We do this separately to not over complicate the postSolution process.
		if ($scope.RemovedHandinFiles.length > 0) {
			AssignmentResource.removeHandinFile($scope.courseInstanceID, $scope.assignmentID, $scope.RemovedHandinFiles)
			.success(function(sData) {

			})
			.error(function(eData) {
				centrisNotify.error("assignments.Msg.ErrorRemovingFilesFromSolution");
			});
		}

		// Create/update the solution.
		$scope.solution.HandinDate = moment(Date.now());
		AssignmentResource.postSolution($scope.courseInstanceID, $scope.assignmentID, $scope.solution)
		.success(function (data) {
			centrisNotify.success("assignments.Msg.SolutionUploaded");
			$location.path("/courses/" + courseID + "/assignments");
		}).error(function (data) {
			centrisNotify.error("assignments.Msg.ErrorUploadingSolution");
			$location.path();
		});
	};

	$scope.onFileSelected = function onFileSelected($files, $event) {

		if ($scope.solution.Attachments.length + $files.length > $scope.assignment.AllowedNumberOfFiles) {
			centrisNotify.error("assignments.ValidationMessages.TooManyFilesSelected");
			return;
		}

		angular.forEach($files, function(file) {
			// Add the file to solution.Attachments if its extension is allowed.
			for (var i = 0; i < $scope.assignment.AllowedFileExtensions.length; ++i) {
				var str = new RegExp($scope.assignment.AllowedFileExtensions[i] + "$", "g");
				var res = file.name.match(str);
				if (res) {
					// Match found
					$scope.solution.Attachments.push({
						FriendlyFilename: file.name,
						Size:             FileUtils.formatBytes(file.size),
						IsNew:            true,
						$$hashKey:        file.$$hashKey
						// TODO: icon?
					});
					break;
				} else if (i === $scope.assignment.AllowedFileExtensions.length - 1) {
					centrisNotify.error("assignments.ValidationMessages.FileEndingNotAllowed");
				}
			}
		});
	};

	// Removes a submitted handinfile
	$scope.removeAttachment = function removeAttachment(file) {
		if (file.IsNew) {
			removeAttachmentFromList(file);
		} else {
			var undoParam = {
				type: "assignment-handin-delete",
				id: {
					attachmentID: file.ID,
					instance:     $scope.courseInstanceID
				}
			};
			$scope.RemovedHandinFiles.push(file);
			centrisNotify.successWithUndo("assignments.Msg.AttachmentRemoved", undoParam);
			removeAttachmentFromList(file);
		}
	};

	$scope.$on("centrisUndo", function undo(event, param) {
		if (param.type === "assignment-handin-delete") {
			// Return the file to the soltuion.Attachments,
			// and remove it from the RemovedHandinFiles.
			var i = $scope.RemovedHandinFiles.length;
			while (i--) {
				if ($scope.RemovedHandinFiles[i].ID === param.id.attachmentID) {
					$scope.solution.Attachments.push($scope.RemovedHandinFiles[i]);
					$scope.RemovedHandinFiles.splice(i, 1);
				}
			}
		}
	});

	// ------------------------------------------------------------------------
	// PRIVATE FUNCTIONS

	// Translates type to correct string value
	function getDurationType(type) {
		if (type === 1) {
			return "days";
		} else if (type === 2) {
			return "hours";
		} else if (type === 3) {
			return "minutes";
		}
	}

	// Removes a file from the submitted file list.
	function removeAttachmentFromList(data) {
		for (var i = 0; i < $scope.solution.Attachments.length; i++) {
			if ($scope.solution.Attachments[i].ID === data.ID) {
				$scope.solution.Attachments.splice(i, 1);
				break;
			}
		}

		// TODO: test this!!!
		for (i = 0; i < $scope.NewFiles.length; i++) {
			if ($scope.NewFiles[i].$$hashKey === data.$$hashKey) {
				$scope.NewFiles.splice(i, 1);
				break;
			}
		}
	}
	$scope.isViewerSupport = function isViewerSupport(fileName) {
		var str = $scope.getMode(fileName);
		if (str.length > 0) {
			return true;
		}
		return false;
	};
	// todo: the next two functions getExtension and getMode exits in
	// ace/src/common/AceFactory.js but I don't dare to add it AceFactory.js
	// to the common folder.
	$scope.getExtension = function getExtension(str) {
		var index = str.lastIndexOf(".") + 1;
		if (index < 1) {
			return "";
		}
		return str.substring(index);
	};
	$scope.getMode = function getMode(fileName) {
		if (fileName === undefined) {
			return "";
		}
		var extension = $scope.getExtension(fileName).toLowerCase();
		var mode;
		switch (extension) {
			case "cpp":
			case "c":
			case "h": {
				mode = "ace/mode/c_cpp";
				break;
			}
			case "js": {
				mode = "ace/mode/javascript";
				break;
			}
			case "java":
			case "class": {
				mode = "ace/mode/java";
				break;
			}
			case "bat": {
				mode = "ace/mode/batchfile";
				break;
			}
			case "cs": {
				mode = "ace/mode/csharp";
				break;
			}
			case "css": {
				mode = "ace/mode/css";
				break;
			}
			case "html": {
				mode = "ace/mode/html";
				break;
			}
			case "json": {
				mode = "ace/mode/json";
				break;
			}
			case "less": {
				mode = "ace/mode/less";
				break;
			}
			case "lisp": {
				mode = "ace/mode/lisp";
				break;
			}
			case "md": {
				mode = "ace/mode/markdown";
				break;
			}
			case "sql": {
				mode = "ace/mode/sql";
				break;
			}
			case "tex": {
				mode = "ace/mode/tex";
				break;
			}
			case "vb": {
				mode = "ace/mode/vbscript";
				break;
			}
			case "xml": {
				mode = "ace/mode/xml";
				break;
			}
			case "py": {
				mode = "ace/mode/python";
				break;
			}
			case "sh": {
				mode = "ace/mode/sh";
				break;
			}
			case "jsp": {
				mode = "ace/mode/jsp";
				break;
			}
			case "cshtml": {
				mode = "ace/mode/razor";
				break;
			}
			case "pl": {
				mode = "ace/mode/prolog";
				break;
			}
			case "pas": {
				mode = "ace/mode/pascal";
				break;
			}
			case "scss": {
				mode = "ace/mode/scss";
				break;
			}
			case "ss": {
				mode = "ace/mode/scheme";
				break;
			}
			default: {
				mode = "";
			}
		}
		return mode;
	};
});
