"use strict";

describe("Testing AssignmentStudentHandinController", function() {
	var ctrl, scope, mockStateParams, mockAssignmentResource,
			mockCentrisNotify, mockUpload;

	beforeEach(module("assignmentsApp", "sharedServices"));

	beforeEach(inject(function($rootScope, $injector) {
		scope = $rootScope.$new();
		mockAssignmentResource = $injector.get("mockAssignmentResource");
		mockCentrisNotify = $injector.get("mockCentrisNotify");
		mockStateParams = {
		};
	}));

	describe("testing the success functions", function() {
		beforeEach(function() {
			mockAssignmentResource.setFakeGettingHandinsAnswers([{
				Name: "Gunnar Gunnarsson"
			}]);
			mockAssignmentResource.setFakeGettingHandinsConditions([
				true
			]);
			mockAssignmentResource.setRemoveHandinFileAnswers([{
			}]);
			mockAssignmentResource.setRemoveHandinFileConditions([
				true
			]);
			mockAssignmentResource.setPostSolutionAnswers([{
			}]);
			mockAssignmentResource.setPostSolutionConditions([
				true
			]);
			mockCentrisNotify.error = jasmine.createSpy("error");
			mockCentrisNotify.success = jasmine.createSpy("success");
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentStudentHandinController", {
				$scope: scope,
				$stateParams: mockStateParams,
				AssignmentResource: mockAssignmentResource,
				centrisNotify: mockCentrisNotify,
				Upload: mockUpload
			});
		}));

		it("Should getStudentHandin with correct as scope.solution", function() {
			expect(scope.fetchingData).toBe(false);
			expect(scope.solution).toEqual({ Name: "Gunnar Gunnarsson" });
		});

		it("Should removeHandinFiles and postSolution", function() {
			scope.RemovedHandinFiles = [{
				Handin: "FakeHandin"
			}];
			scope.submitChanges();
			expect(mockCentrisNotify.success).toHaveBeenCalledWith("assignments.Msg.SolutionUploaded");
		});

		it("Should not removeHandinFiles and postSolution", function() {
			scope.RemovedHandinFiles = [];
			scope.submitChanges();
			expect(mockCentrisNotify.success).toHaveBeenCalledWith("assignments.Msg.SolutionUploaded");
		});

		it("should call onFileSelected and return an error message", function() {
			scope.assignment = {
				AllowedNumberOfFiles: 0
			};
			scope.solution = {
				Attachments: []
			};
			var files = [{}, {}];
			scope.onFileSelected(files);
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("assignments.ValidationMessages.TooManyFilesSelected");
		});

		it("should call onFileSelected and return an error message", function() {
			scope.assignment = {
				AllowedNumberOfFiles: 4,
				AllowedFileExtensions: [{}]
			};
			scope.solution = {
				Attachments: []
			};
			var files = [{
				name: "Hannibal"
			}, {
				name: "Senior"
			}];
			scope.onFileSelected(files);
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("assignments.ValidationMessages.FileEndingNotAllowed");
		});

		it("should call onFileSelected and return an error message", function() {
			scope.assignment = {
				AllowedNumberOfFiles: 4,
				AllowedFileExtensions: ["Hannibal"]
			};
			scope.solution = {
				Attachments: []
			};
			var files = [{
				name: "Hannibal"
			}, {
				name: "Senior"
			}];
			scope.onFileSelected(files);
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("assignments.ValidationMessages.FileEndingNotAllowed");
		});
	});

	describe("testing the error functions", function() {
		beforeEach(function() {
			mockAssignmentResource.setFakeGettingHandinsAnswers([{
				Name: "Gunnar Gunnarsson"
			}]);
			mockAssignmentResource.setFakeGettingHandinsConditions([
				false
			]);
			mockAssignmentResource.setRemoveHandinFileAnswers([{
			}]);
			mockAssignmentResource.setRemoveHandinFileConditions([
				false
			]);
			mockAssignmentResource.setPostSolutionAnswers([{
			}]);
			mockAssignmentResource.setPostSolutionConditions([
				false
			]);
			mockCentrisNotify.error = jasmine.createSpy("error");
			mockCentrisNotify.success = jasmine.createSpy("success");
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentStudentHandinController", {
				$scope: scope,
				$stateParams: mockStateParams,
				AssignmentResource: mockAssignmentResource,
				centrisNotify: mockCentrisNotify,
				Upload: mockUpload
			});
		}));

		it("Should fail getStudentHandin", function() {
			expect(scope.fetchingData).toBe(false);
			expect(scope.solution).toEqual({ StudentMemo: "",Attachments: [] });
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("assignments.Msg.ErrorLoadingSolution");
		});

		it("Should fail to removeHandinFiles and postSolution", function() {
			scope.RemovedHandinFiles = [{
				Handin: "FakeHandin"
			}];
			scope.submitChanges();
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("assignments.Msg.ErrorRemovingFilesFromSolution");
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("assignments.Msg.ErrorUploadingSolution");
		});
	});
});
