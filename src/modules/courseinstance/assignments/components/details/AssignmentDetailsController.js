"use strict";

/*
 * AssignmentDetailController => Shows details about a single assignment.
 * There will be subsections displayed, based on wether the user is a
 * teacher or a student.
 */
angular.module("assignmentsApp").controller("AssignmentDetailsController",
function AssignmentDetailsController($scope, $translate, $state, $stateParams, AssignmentResource,
	centrisNotify, CourseInstanceRoleService) {

	// Fetching IDs from url:
	$scope.courseInstanceID     = $stateParams.courseInstanceID;
	$scope.assignmentID         = $stateParams.assignmentID;
	// Variables for discussionboard:
	$scope.resid                = $stateParams.assignmentID;
	$scope.restype              = "assignment";

	$scope.isInvalid            = false;
	// Checks if Tabs are visable:
	$scope.checkGroup = false;
	$scope.checkStats = false;
	$scope.checkTeacherStats = false;
	$scope.checkDiscuss = false;

	// Load the assignment object:
	$scope.gettingAssignment = true;
	AssignmentResource.getAssignmentDetails($scope.courseInstanceID, $scope.assignmentID)
	.success(function(data) {
		if (data.MaxStudentsInGroup > 1 && (!$scope.isTeacher)) {
			$scope.checkGroup = true;
		}
		if (data.Statistics !== undefined && data.Statistics !== null) {
			$scope.checkStats = true;
		}
		if (data.Statistics !== null) {
			$scope.checkTeacherStats = true;
		}
		if (data.HasDiscussionBoard) {
			$scope.checkDiscuss = true;
		}
		// sorts Categories cas insensitive
		data.Categories.sort(function (a, b) {
			return a.toLowerCase().localeCompare(b.toLowerCase());
		});
		$scope.assignment = data;
		$scope.isInvalid = false;
		$scope.$broadcast("onAssignmentDetails", {
			assignment: $scope.assignment
		});
	})
	.error(function(data) {
		$scope.isInvalid = true;
		if (data === "INVALID_ASSIGNMENT_ID") {
			centrisNotify.error("assignments.AssignmentNotFound");
		}
	}).finally(function() {
		$scope.gettingAssignment = false;
	});

	// Course assignment details tabs
	$scope.tabs = {
		list: [
			{ route:"courseinstance.assignments.details.description", active: true  },
			{ route:"courseinstance.assignments.details.discussions", active: false },
			{ route:"courseinstance.assignments.details.group",       active: false },
			{ route:"courseinstance.assignments.details.handin",      active: false },
			{ route:"courseinstance.assignments.details.grade",       active: false },
			{ route:"courseinstance.assignments.details.grading",     active: false },
			{ route:"courseinstance.assignments.details.statistics",  active: false },
		],
		// Changes the current route
		go: function go(route) {
			$scope.$broadcast(route);
			$state.go(route);
		},
		active: function active(route) {
			return $state.is(route);
		}
	};

	// Listening after state change event
	$scope.$on("$stateChangeSuccess", function stateChangeSuccess() {
		if ($state.current.name !== "courseinstance.assignments.details") {
			$scope.checkGrade = false;
			$scope.tabs.list.forEach(function forEachTab(tab) {
				tab.active = $state.is(tab.route);
			});
		}
	});

	CourseInstanceRoleService.isTeacherInCourse($stateParams.courseInstanceID).then(function(isTeacher) {
		$scope.isTeacher = isTeacher;
	});
	// Helper menu.

	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	$scope.onCentrisIntro = function(event, args) {
		if (!$scope.isTeacher) {
			if ($state.current.name === "courseinstance.assignments.details.description") {
				if ($scope.checkGroup && $scope.checkStats && $scope.checkDiscuss) {
					$scope.ShowMyOptionsAllDesc();
				} else if ($scope.checkGroup && $scope.checkDiscuss) {
					$scope.ShowMyOptionsGrpDiscDesc();
				} else if ($scope.checkGroup && $scope.checkStats) {
					$scope.ShowMyOptionsGrpStatsDesc();
				} else if ($scope.checkStats && $scope.checkDiscuss) {
					$scope.ShowMyOptionsStatsDiscDesc();
				} else if ($scope.checkDiscuss) {
					$scope.ShowMyOptionsDiscDesc();
				} else if ($scope.checkGroup) {
					$scope.ShowMyOptionsGrpDesc();
				} else if ($scope.checkStats) {
					$scope.ShowMyOptionsStatsDesc();
				} else {
					$scope.ShowMyOptionsNewDesc();
				}
			} else if ($state.current.name === "courseinstance.assignments.details.discussions") {
				if ($scope.checkGroup && $scope.checkStats) {
					$scope.ShowMyOptionsAllDisc();
				} else if ($scope.checkGroup) {
					$scope.ShowMyOptionsGrpDisc();
				} else if ($scope.checkStats) {
					$scope.ShowMyOptionsStatsDisc();
				} else {
					$scope.ShowMyOptionsNewDisc();
				}
			} else if ($state.current.name === "courseinstance.assignments.details.group") {
				if ($scope.checkDiscuss && $scope.checkStats) {
					$scope.ShowMyOptionsAllGrp();
				} else if ($scope.checkStats) {
					$scope.ShowMyOptionsStatsGrp();
				} else if ($scope.checkDiscuss) {
					$scope.ShowMyOptionsDiscGrp();
				} else {
					$scope.ShowMyOptionsNewGrp();
				}
			} else if ($state.current.name === "courseinstance.assignments.details.handin") {
				if ($scope.checkGroup && $scope.checkStats && $scope.checkDiscuss) {
					$scope.ShowMyOptionsAllHandin();
				} else if ($scope.checkGroup && $scope.checkDiscuss) {
					$scope.ShowMyOptionsGrpDiscHandin();
				} else if ($scope.checkGroup && $scope.checkStats) {
					$scope.ShowMyOptionsGrpStatsHandin();
				} else if ($scope.checkStats && $scope.checkDiscuss) {
					$scope.ShowMyOptionsStatsDiscHandin();
				} else if ($scope.checkDiscuss) {
					$scope.ShowMyOptionsDiscHandin();
				} else if ($scope.checkGroup) {
					$scope.ShowMyOptionsGrpHandin();
				} else if ($scope.checkStats) {
					$scope.ShowMyOptionsStatsHandin();
				} else {
					$scope.ShowMyOptionsNewHandin();
				}
			} else if ($state.current.name === "courseinstance.assignments.details.grade") {
				if ($scope.checkGroup && $scope.checkDiscuss) {
					$scope.ShowMyOptionsAllGrade();
				} else if ($scope.checkGroup) {
					$scope.ShowMyOptionsGroupGrade();
				} else if ($scope.checkDiscuss) {
					$scope.ShowMyOptionsDiscGrade();
				} else {
					$scope.ShowMyOptionsNewGrade();
				}
			} else {
				if ($scope.checkGroup && $scope.checkDiscuss) {
					$scope.ShowMyOptionsAllStats();
				} else if ($scope.checkGroup) {
					$scope.ShowMyOptionsGrpStats();
				} else if ($scope.checkDiscuss) {
					$scope.ShowMyOptionsDiscStats();
				} else {
					$scope.ShowMyOptionsNewStats();
				}
			}
		} else {
			if ($state.current.name === "courseinstance.assignments.details.description") {
				if ($scope.checkTeacherStats && $scope.checkDiscuss) {
					$scope.ShowMyOptionsDescTeachAll();
				} else if ($scope.checkDiscuss) {
					$scope.ShowMyOptionsDescTeachDisc();
				} else if ($scope.checkTeacherStats) {
					$scope.ShowMyOptionsDescTeachStats();
				} else {
					$scope.ShowMyOptionsDescTeach();
				}
			} else if ($state.current.name === "courseinstance.assignments.details.grading") {
				if ($scope.checkTeacherStats && $scope.checkDiscuss) {
					$scope.ShowMyOptionsGradeTeachAll();
				} else if ($scope.checkDiscuss) {
					$scope.ShowMyOptionsGradeTeachDisc();
				} else if ($scope.checkTeacherStats) {
					$scope.ShowMyOptionsGradeTeachStats();
				} else {
					$scope.ShowMyOptionsGradeTeach();
				}
			} else if ($state.current.name === "courseinstance.assignments.details.statistics") {
				if ($scope.checkDiscuss) {
					$scope.ShowMyOptionsStatsTeachAll();
				} else {
					$scope.ShowMyOptionsStatsTeach();
				}
			} else if ($state.current.name === "courseinstance.assignments.details.discussions") {
				if ($scope.checkTeacherStats) {
					$scope.ShowMyOptionsDiscTeachAll();
				} else {
					$scope.ShowMyOptionsDiscTeach();
				}
			}
		}
	};
	// Description Every Tab covered for students
	$scope.IntroOptionsAllDesc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsAllDesc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsAllDesc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsAllDesc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsAllDesc.doneLabel = translations[introButtons[3]];
	});
	var introKeysAllDesc = [
		"assignments.Details_Helper.Description.Step1",
		"assignments.Details_Helper.Description.Step2",
		"assignments.Details_Helper.Description.Step3",
		"assignments.Details_Helper.Description.Step4",
		"assignments.Details_Helper.Description.Step5",
		"assignments.Details_Helper.Description.Step6"
	];

	$translate(introKeysAllDesc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysAllDesc.length; ++i) {
			$scope.IntroOptionsAllDesc.steps[i].intro = translations[introKeysAllDesc[i]];
		}
	});

	$scope.IntroOptionsGrpStatsDesc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsGrpStatsDesc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsGrpStatsDesc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsGrpStatsDesc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsGrpStatsDesc.doneLabel = translations[introButtons[3]];
	});
	var introKeysGrpStatsDesc = [
		"assignments.Details_Helper.Description.Step1",
		"assignments.Details_Helper.Description.Step3",
		"assignments.Details_Helper.Description.Step4",
		"assignments.Details_Helper.Description.Step5",
		"assignments.Details_Helper.Description.Step6"
	];

	$translate(introKeysGrpStatsDesc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGrpStatsDesc.length; ++i) {
			$scope.IntroOptionsGrpStatsDesc.steps[i].intro = translations[introKeysGrpStatsDesc[i]];
		}
	});

	$scope.IntroOptionsGrpDiscDesc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsGrpDiscDesc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsGrpDiscDesc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsGrpDiscDesc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsGrpDiscDesc.doneLabel = translations[introButtons[3]];
	});
	var introKeysGrpDiscDesc = [
		"assignments.Details_Helper.Description.Step1",
		"assignments.Details_Helper.Description.Step2",
		"assignments.Details_Helper.Description.Step3",
		"assignments.Details_Helper.Description.Step4"
	];

	$translate(introKeysGrpDiscDesc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGrpDiscDesc.length; ++i) {
			$scope.IntroOptionsGrpDiscDesc.steps[i].intro = translations[introKeysGrpDiscDesc[i]];
		}
	});

	$scope.IntroOptionsGrpDesc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsGrpDesc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsGrpDesc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsGrpDesc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsGrpDesc.doneLabel = translations[introButtons[3]];
	});
	var introKeysGrpDesc = [
		"assignments.Details_Helper.Description.Step1",
		"assignments.Details_Helper.Description.Step3",
		"assignments.Details_Helper.Description.Step4"
	];

	$translate(introKeysGrpDesc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGrpDesc.length; ++i) {
			$scope.IntroOptionsGrpDesc.steps[i].intro = translations[introKeysGrpDesc[i]];
		}
	});

	$scope.IntroOptionsStatsDiscDesc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsStatsDiscDesc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsStatsDiscDesc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsStatsDiscDesc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsStatsDiscDesc.doneLabel = translations[introButtons[3]];
	});
	var introKeysStatsDiscDesc = [
		"assignments.Details_Helper.Description.Step1",
		"assignments.Details_Helper.Description.Step2",
		"assignments.Details_Helper.Description.Step4",
		"assignments.Details_Helper.Description.Step5",
		"assignments.Details_Helper.Description.Step6"
	];

	$translate(introKeysStatsDiscDesc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysStatsDiscDesc.length; ++i) {
			$scope.IntroOptionsStatsDiscDesc.steps[i].intro = translations[introKeysStatsDiscDesc[i]];
		}
	});

	$scope.IntroOptionsDiscDesc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsDiscDesc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsDiscDesc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsDiscDesc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsDiscDesc.doneLabel = translations[introButtons[3]];
	});
	var introKeysDiscDesc = [
		"assignments.Details_Helper.Description.Step1",
		"assignments.Details_Helper.Description.Step2",
		"assignments.Details_Helper.Description.Step4"
	];

	$translate(introKeysDiscDesc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysDiscDesc.length; ++i) {
			$scope.IntroOptionsDiscDesc.steps[i].intro = translations[introKeysDiscDesc[i]];
		}
	});

	$scope.IntroOptionsStatsDesc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsStatsDesc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsStatsDesc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsStatsDesc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsStatsDesc.doneLabel = translations[introButtons[3]];
	});
	var introKeysStatsDesc = [
		"assignments.Details_Helper.Description.Step1",
		"assignments.Details_Helper.Description.Step4",
		"assignments.Details_Helper.Description.Step5",
		"assignments.Details_Helper.Description.Step6"
	];

	$translate(introKeysStatsDesc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysStatsDesc.length; ++i) {
			$scope.IntroOptionsStatsDesc.steps[i].intro = translations[introKeysStatsDesc[i]];
		}
	});

	$scope.IntroOptionsNewDesc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsNewDesc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsNewDesc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsNewDesc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsNewDesc.doneLabel = translations[introButtons[3]];
	});
	var introKeysStatsNewDesc = [
		"assignments.Details_Helper.Description.Step1",
		"assignments.Details_Helper.Description.Step4"
	];

	$translate(introKeysStatsNewDesc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysStatsNewDesc.length; ++i) {
			$scope.IntroOptionsNewDesc.steps[i].intro = translations[introKeysStatsNewDesc[i]];
		}
	});

	// Discuss every Tab covered for students.

	$scope.IntroOptionsAllDisc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsAllDisc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsAllDisc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsAllDisc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsAllDisc.doneLabel = translations[introButtons[3]];
	});
	var introKeysAllDisc = [
		"assignments.Details_Helper.Discussion.Step1",
		"assignments.Details_Helper.Discussion.Step2",
		"assignments.Details_Helper.Discussion.Step3",
		"assignments.Details_Helper.Discussion.Step4",
		"assignments.Details_Helper.Discussion.Step5",
		"assignments.Details_Helper.Discussion.Step6",
		"assignments.Details_Helper.Discussion.Step7"
	];

	$translate(introKeysAllDisc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysAllDisc.length; ++i) {
			$scope.IntroOptionsAllDisc.steps[i].intro = translations[introKeysAllDisc[i]];
		}
	});

	$scope.IntroOptionsGrpDisc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsGrpDisc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsGrpDisc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsGrpDisc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsGrpDisc.doneLabel = translations[introButtons[3]];
	});
	var introKeysGrpDisc = [
		"assignments.Details_Helper.Discussion.Step1",
		"assignments.Details_Helper.Discussion.Step2",
		"assignments.Details_Helper.Discussion.Step3",
		"assignments.Details_Helper.Discussion.Step4",
		"assignments.Details_Helper.Discussion.Step5"
	];

	$translate(introKeysGrpDisc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGrpDisc.length; ++i) {
			$scope.IntroOptionsGrpDisc.steps[i].intro = translations[introKeysGrpDisc[i]];
		}
	});

	$scope.IntroOptionsStatsDisc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsStatsDisc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsStatsDisc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsStatsDisc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsStatsDisc.doneLabel = translations[introButtons[3]];
	});
	var introKeysStatsDisc = [
		"assignments.Details_Helper.Discussion.Step1",
		"assignments.Details_Helper.Discussion.Step2",
		"assignments.Details_Helper.Discussion.Step3",
		"assignments.Details_Helper.Discussion.Step5",
		"assignments.Details_Helper.Discussion.Step6",
		"assignments.Details_Helper.Discussion.Step7",
	];

	$translate(introKeysStatsDisc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysStatsDisc.length; ++i) {
			$scope.IntroOptionsStatsDisc.steps[i].intro = translations[introKeysStatsDisc[i]];
		}
	});

	$scope.IntroOptionsNewDisc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsNewDisc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsNewDisc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsNewDisc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsNewDisc.doneLabel = translations[introButtons[3]];
	});
	var introKeysNewDisc = [
		"assignments.Details_Helper.Discussion.Step1",
		"assignments.Details_Helper.Discussion.Step2",
		"assignments.Details_Helper.Discussion.Step3",
		"assignments.Details_Helper.Discussion.Step5",
	];

	$translate(introKeysNewDisc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysNewDisc.length; ++i) {
			$scope.IntroOptionsNewDisc.steps[i].intro = translations[introKeysNewDisc[i]];
		}
	});

	// Group Every Tab covered for students.
	$scope.IntroOptionsAllGrp = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsAllGrp.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsAllGrp.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsAllGrp.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsAllGrp.doneLabel = translations[introButtons[3]];
	});
	var introKeysAllGrp = [
		"assignments.Details_Helper.Group.Step1",
		"assignments.Details_Helper.Group.Step2",
		"assignments.Details_Helper.Group.Step3",
		"assignments.Details_Helper.Group.Step4",
		"assignments.Details_Helper.Group.Step5",
		"assignments.Details_Helper.Group.Step6",
		"assignments.Details_Helper.Group.Step7"
	];

	$translate(introKeysAllGrp).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysAllGrp.length; ++i) {
			$scope.IntroOptionsAllGrp.steps[i].intro = translations[introKeysAllGrp[i]];
		}
	});

	$scope.IntroOptionsStatsGrp = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		},{
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsStatsGrp.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsStatsGrp.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsStatsGrp.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsStatsGrp.doneLabel = translations[introButtons[3]];
	});
	var introKeysStatsGrp = [
		"assignments.Details_Helper.Group.Step1",
		"assignments.Details_Helper.Group.Step2",
		"assignments.Details_Helper.Group.Step3",
		"assignments.Details_Helper.Group.Step5",
		"assignments.Details_Helper.Group.Step6",
		"assignments.Details_Helper.Group.Step7"
	];

	$translate(introKeysStatsGrp).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysStatsGrp.length; ++i) {
			$scope.IntroOptionsStatsGrp.steps[i].intro = translations[introKeysStatsGrp[i]];
		}
	});

	$scope.IntroOptionsDiscGrp = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsDiscGrp.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsDiscGrp.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsDiscGrp.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsDiscGrp.doneLabel = translations[introButtons[3]];
	});
	var introKeysDiscGrp = [
		"assignments.Details_Helper.Group.Step1",
		"assignments.Details_Helper.Group.Step2",
		"assignments.Details_Helper.Group.Step3",
		"assignments.Details_Helper.Group.Step4",
		"assignments.Details_Helper.Group.Step5"
	];

	$translate(introKeysDiscGrp).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysDiscGrp.length; ++i) {
			$scope.IntroOptionsDiscGrp.steps[i].intro = translations[introKeysDiscGrp[i]];
		}
	});

	$scope.IntroOptionsNewGrp = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-group",
			position: "right",
			intro: ""
		},  {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsNewGrp.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsNewGrp.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsNewGrp.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsNewGrp.doneLabel = translations[introButtons[3]];
	});
	var introKeysNewGrp = [
		"assignments.Details_Helper.Group.Step1",
		"assignments.Details_Helper.Group.Step2",
		"assignments.Details_Helper.Group.Step3",
		"assignments.Details_Helper.Group.Step5"
	];

	$translate(introKeysNewGrp).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysNewGrp.length; ++i) {
			$scope.IntroOptionsNewGrp.steps[i].intro = translations[introKeysNewGrp[i]];
		}
	});

	// Handin Every Tab covered for students.
	$scope.IntroOptionsAllHandin = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-file",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsAllHandin.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsAllHandin.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsAllHandin.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsAllHandin.doneLabel = translations[introButtons[3]];
	});
	var introKeysAllHandin = [
		"assignments.Details_Helper.Handin.Step1",
		"assignments.Details_Helper.Handin.Step2",
		"assignments.Details_Helper.Handin.Step3",
		"assignments.Details_Helper.Handin.Step4",
		"assignments.Details_Helper.Handin.Step5",
		"assignments.Details_Helper.Handin.Step6",
		"assignments.Details_Helper.Handin.Step7",
		"assignments.Details_Helper.Handin.Step8"
	];

	$translate(introKeysAllHandin).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysAllHandin.length; ++i) {
			$scope.IntroOptionsAllHandin.steps[i].intro = translations[introKeysAllHandin[i]];
		}
	});

	$scope.IntroOptionsGrpDiscHandin = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-file",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsGrpDiscHandin.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsGrpDiscHandin.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsGrpDiscHandin.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsGrpDiscHandin.doneLabel = translations[introButtons[3]];
	});
	var introKeysGrpDiscHandin = [
		"assignments.Details_Helper.Handin.Step1",
		"assignments.Details_Helper.Handin.Step2",
		"assignments.Details_Helper.Handin.Step3",
		"assignments.Details_Helper.Handin.Step4",
		"assignments.Details_Helper.Handin.Step5",
		"assignments.Details_Helper.Handin.Step6"
	];

	$translate(introKeysGrpDiscHandin).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGrpDiscHandin.length; ++i) {
			$scope.IntroOptionsGrpDiscHandin.steps[i].intro = translations[introKeysGrpDiscHandin[i]];
		}
	});

	$scope.IntroOptionsGrpStatsHandin = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-file",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsGrpStatsHandin.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsGrpStatsHandin.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsGrpStatsHandin.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsGrpStatsHandin.doneLabel = translations[introButtons[3]];
	});
	var introKeysGrpStatsHandin = [
		"assignments.Details_Helper.Handin.Step1",
		"assignments.Details_Helper.Handin.Step2",
		"assignments.Details_Helper.Handin.Step3",
		"assignments.Details_Helper.Handin.Step4",
		"assignments.Details_Helper.Handin.Step6",
		"assignments.Details_Helper.Handin.Step7",
		"assignments.Details_Helper.Handin.Step8"
	];

	$translate(introKeysGrpStatsHandin).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGrpStatsHandin.length; ++i) {
			$scope.IntroOptionsGrpStatsHandin.steps[i].intro = translations[introKeysGrpStatsHandin[i]];
		}
	});

	$scope.IntroOptionsStatsDiscHandin = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-file",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsStatsDiscHandin.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsStatsDiscHandin.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsStatsDiscHandin.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsStatsDiscHandin.doneLabel = translations[introButtons[3]];
	});
	var introKeysStatsDiscHandin = [
		"assignments.Details_Helper.Handin.Step1",
		"assignments.Details_Helper.Handin.Step2",
		"assignments.Details_Helper.Handin.Step3",
		"assignments.Details_Helper.Handin.Step4",
		"assignments.Details_Helper.Handin.Step5",
		"assignments.Details_Helper.Handin.Step7",
		"assignments.Details_Helper.Handin.Step8"
	];

	$translate(introKeysStatsDiscHandin).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysStatsDiscHandin.length; ++i) {
			$scope.IntroOptionsStatsDiscHandin.steps[i].intro = translations[introKeysStatsDiscHandin[i]];
		}
	});

	$scope.IntroOptionsStatsHandin = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-file",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsStatsHandin.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsStatsHandin.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsStatsHandin.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsStatsHandin.doneLabel = translations[introButtons[3]];
	});
	var introKeysStatsHandin = [
		"assignments.Details_Helper.Handin.Step1",
		"assignments.Details_Helper.Handin.Step2",
		"assignments.Details_Helper.Handin.Step3",
		"assignments.Details_Helper.Handin.Step4",
		"assignments.Details_Helper.Handin.Step7",
		"assignments.Details_Helper.Handin.Step8"
	];

	$translate(introKeysStatsHandin).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysStatsHandin.length; ++i) {
			$scope.IntroOptionsStatsHandin.steps[i].intro = translations[introKeysStatsHandin[i]];
		}
	});

	$scope.IntroOptionsGrpHandin = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-file",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsGrpHandin.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsGrpHandin.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsGrpHandin.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsGrpHandin.doneLabel = translations[introButtons[3]];
	});
	var introKeysGrpHandin = [
		"assignments.Details_Helper.Handin.Step1",
		"assignments.Details_Helper.Handin.Step2",
		"assignments.Details_Helper.Handin.Step3",
		"assignments.Details_Helper.Handin.Step4",
		"assignments.Details_Helper.Handin.Step6"
	];

	$translate(introKeysGrpHandin).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGrpHandin.length; ++i) {
			$scope.IntroOptionsGrpHandin.steps[i].intro = translations[introKeysGrpHandin[i]];
		}
	});

	$scope.IntroOptionsDiscHandin = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-file",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsDiscHandin.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsDiscHandin.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsDiscHandin.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsDiscHandin.doneLabel = translations[introButtons[3]];
	});
	var introKeysDiscHandin = [
		"assignments.Details_Helper.Handin.Step1",
		"assignments.Details_Helper.Handin.Step2",
		"assignments.Details_Helper.Handin.Step3",
		"assignments.Details_Helper.Handin.Step4",
		"assignments.Details_Helper.Handin.Step5"
	];

	$translate(introKeysDiscHandin).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysDiscHandin.length; ++i) {
			$scope.IntroOptionsDiscHandin.steps[i].intro = translations[introKeysDiscHandin[i]];
		}
	});

	$scope.IntroOptionsNewHandin = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-file",
			position: "right",
			intro: ""
		}, {
			element: "#details-add-comment",
			position: "right",
			intro: ""
		},  {
			element: "#details-desc",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsNewHandin.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsNewHandin.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsNewHandin.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsNewHandin.doneLabel = translations[introButtons[3]];
	});
	var introKeysStatsNewHandin = [
		"assignments.Details_Helper.Handin.Step1",
		"assignments.Details_Helper.Handin.Step2",
		"assignments.Details_Helper.Handin.Step3",
		"assignments.Details_Helper.Handin.Step4"
	];

	$translate(introKeysStatsNewHandin).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysStatsNewHandin.length; ++i) {
			$scope.IntroOptionsNewHandin.steps[i].intro = translations[introKeysStatsNewHandin[i]];
		}
	});

	// Grade Every Tab covered for students.
	$scope.IntroOptionsAllGrade = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsAllGrade.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsAllGrade.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsAllGrade.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsAllGrade.doneLabel = translations[introButtons[3]];
	});
	var introKeysAllGrade = [
		"assignments.Details_Helper.Grade.Step1",
		"assignments.Details_Helper.Grade.Step2",
		"assignments.Details_Helper.Grade.Step3",
		"assignments.Details_Helper.Grade.Step4",
		"assignments.Details_Helper.Grade.Step5",
		"assignments.Details_Helper.Grade.Step6"
	];

	$translate(introKeysAllGrade).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysAllGrade.length; ++i) {
			$scope.IntroOptionsAllGrade.steps[i].intro = translations[introKeysAllGrade[i]];
		}
	});

	$scope.IntroOptionsGrpGrade = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsGrpGrade.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsGrpGrade.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsGrpGrade.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsGrpGrade.doneLabel = translations[introButtons[3]];
	});
	var introKeysGrpGrade = [
		"assignments.Details_Helper.Grade.Step1",
		"assignments.Details_Helper.Grade.Step2",
		"assignments.Details_Helper.Grade.Step4",
		"assignments.Details_Helper.Grade.Step5",
		"assignments.Details_Helper.Grade.Step6"
	];

	$translate(introKeysGrpGrade).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGrpGrade.length; ++i) {
			$scope.IntroOptionsGrpGrade.steps[i].intro = translations[introKeysGrpGrade[i]];
		}
	});

	$scope.IntroOptionsDiscGrade = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsDiscGrade.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsDiscGrade.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsDiscGrade.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsDiscGrade.doneLabel = translations[introButtons[3]];
	});
	var introKeysDiscGrade = [
		"assignments.Details_Helper.Grade.Step1",
		"assignments.Details_Helper.Grade.Step2",
		"assignments.Details_Helper.Grade.Step3",
		"assignments.Details_Helper.Grade.Step5",
		"assignments.Details_Helper.Grade.Step6"
	];

	$translate(introKeysDiscGrade).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysDiscGrade.length; ++i) {
			$scope.IntroOptionsDiscGrade.steps[i].intro = translations[introKeysDiscGrade[i]];
		}
	});

	$scope.IntroOptionsNewGrade = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsNewGrade.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsNewGrade.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsNewGrade.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsNewGrade.doneLabel = translations[introButtons[3]];
	});
	var introKeysNewGrade = [
		"assignments.Details_Helper.Grade.Step1",
		"assignments.Details_Helper.Grade.Step2",
		"assignments.Details_Helper.Grade.Step5",
		"assignments.Details_Helper.Grade.Step6"
	];

	$translate(introKeysNewGrade).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysNewGrade.length; ++i) {
			$scope.IntroOptionsNewGrade.steps[i].intro = translations[introKeysNewGrade[i]];
		}
	});

	// Statistics Every Tab covered for students.
	$scope.IntroOptionsAllStats = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsAllStats.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsAllStats.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsAllStats.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsAllStats.doneLabel = translations[introButtons[3]];
	});
	var introKeysAllStats = [
		"assignments.Details_Helper.Stats.Step1",
		"assignments.Details_Helper.Stats.Step2",
		"assignments.Details_Helper.Stats.Step3",
		"assignments.Details_Helper.Stats.Step4",
		"assignments.Details_Helper.Stats.Step5",
		"assignments.Details_Helper.Stats.Step6"
	];

	$translate(introKeysAllStats).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysAllStats.length; ++i) {
			$scope.IntroOptionsAllStats.steps[i].intro = translations[introKeysAllStats[i]];
		}
	});

	$scope.IntroOptionsDiscStats = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsDiscStats.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsDiscStats.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsDiscStats.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsDiscStats.doneLabel = translations[introButtons[3]];
	});
	var introKeysDiscStats = [
		"assignments.Details_Helper.Stats.Step1",
		"assignments.Details_Helper.Stats.Step2",
		"assignments.Details_Helper.Stats.Step3",
		"assignments.Details_Helper.Stats.Step5",
		"assignments.Details_Helper.Stats.Step6"
	];

	$translate(introKeysDiscStats).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysDiscStats.length; ++i) {
			$scope.IntroOptionsDiscStats.steps[i].intro = translations[introKeysDiscStats[i]];
		}
	});

	$scope.IntroOptionsGrpStats = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-group",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsGrpStats.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsGrpStats.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsGrpStats.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsGrpStats.doneLabel = translations[introButtons[3]];
	});
	var introKeysGrpStats = [
		"assignments.Details_Helper.Stats.Step1",
		"assignments.Details_Helper.Stats.Step2",
		"assignments.Details_Helper.Stats.Step4",
		"assignments.Details_Helper.Stats.Step5",
		"assignments.Details_Helper.Stats.Step6"
	];

	$translate(introKeysGrpStats).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGrpStats.length; ++i) {
			$scope.IntroOptionsGrpStats.steps[i].intro = translations[introKeysGrpStats[i]];
		}
	});

	$scope.IntroOptionsNewStats = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-handin",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-student",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsNewStats.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsNewStats.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsNewStats.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsNewStats.doneLabel = translations[introButtons[3]];
	});
	var introKeysNewStats = [
		"assignments.Details_Helper.Stats.Step1",
		"assignments.Details_Helper.Stats.Step2",
		"assignments.Details_Helper.Stats.Step5",
		"assignments.Details_Helper.Stats.Step6"
	];

	$translate(introKeysNewStats).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysNewStats.length; ++i) {
			$scope.IntroOptionsNewStats.steps[i].intro = translations[introKeysNewStats[i]];
		}
	});

	// Teacher Help
	$scope.OptionsDescTeachAll = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-edit",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-teacher",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.OptionsDescTeachAll.nextLabel = translations[introButtons[0]];
		$scope.OptionsDescTeachAll.prevLabel = translations[introButtons[1]];
		$scope.OptionsDescTeachAll.skipLabel = translations[introButtons[2]];
		$scope.OptionsDescTeachAll.doneLabel = translations[introButtons[3]];
	});
	var introKeysDescTeachAll = [
		"assignments.Details_Helper.Teacher.Disc_cont",
		"assignments.Details_Helper.Teacher.Edit_button",
		"assignments.Details_Helper.Teacher.Disc_tab",
		"assignments.Details_Helper.Teacher.Grade_tab",
		"assignments.Details_Helper.Teacher.Stats_tab"
	];

	$translate(introKeysDescTeachAll).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysDescTeachAll.length; ++i) {
			$scope.OptionsDescTeachAll.steps[i].intro = translations[introKeysDescTeachAll[i]];
		}
	});

	$scope.IntroOptionsDescTeachDisc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-edit",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-teacher",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsDescTeachDisc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsDescTeachDisc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsDescTeachDisc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsDescTeachDisc.doneLabel = translations[introButtons[3]];
	});
	var introKeysDescTeachDisc = [
		"assignments.Details_Helper.Teacher.Disc_cont",
		"assignments.Details_Helper.Teacher.Edit_button",
		"assignments.Details_Helper.Teacher.Disc_tab",
		"assignments.Details_Helper.Teacher.Grade_tab"
	];

	$translate(introKeysDescTeachDisc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysDescTeachDisc.length; ++i) {
			$scope.IntroOptionsDescTeachDisc.steps[i].intro = translations[introKeysDescTeachDisc[i]];
		}
	});

	$scope.IntroOptionsDescTeachStats = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-edit",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-teacher",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsDescTeachStats.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsDescTeachStats.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsDescTeachStats.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsDescTeachStats.doneLabel = translations[introButtons[3]];
	});
	var introKeysDescTeachStats = [
		"assignments.Details_Helper.Teacher.Disc_cont",
		"assignments.Details_Helper.Teacher.Edit_button",
		"assignments.Details_Helper.Teacher.Grade_tab",
		"assignments.Details_Helper.Teacher.Stats_tab"
	];

	$translate(introKeysDescTeachStats).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysDescTeachStats.length; ++i) {
			$scope.IntroOptionsDescTeachStats.steps[i].intro = translations[introKeysDescTeachStats[i]];
		}
	});

	$scope.IntroOptionsDescTeach = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-edit",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-teacher",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsDescTeach.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsDescTeach.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsDescTeach.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsDescTeach.doneLabel = translations[introButtons[3]];
	});
	var introKeysDescTeach = [
		"assignments.Details_Helper.Teacher.Disc_cont",
		"assignments.Details_Helper.Teacher.Edit_button",
		"assignments.Details_Helper.Teacher.Grade_tab"
	];

	$translate(introKeysDescTeach).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysDescTeach.length; ++i) {
			$scope.IntroOptionsDescTeach.steps[i].intro = translations[introKeysDescTeach[i]];
		}
	});

	$scope.OptionsGradeTeachAll = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-edit",
			position: "right",
			intro: ""
		}, {
			element: "#dropdownMenu1",
			position: "right",
			intro: ""
		}, {
			element: "#insert-grade",
			position: "right",
			intro: ""
		}, {
			element: "#insert-comment",
			position: "right",
			intro: ""
		}, {
			element: "#arrow-right",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.OptionsGradeTeachAll.nextLabel = translations[introButtons[0]];
		$scope.OptionsGradeTeachAll.prevLabel = translations[introButtons[1]];
		$scope.OptionsGradeTeachAll.skipLabel = translations[introButtons[2]];
		$scope.OptionsGradeTeachAll.doneLabel = translations[introButtons[3]];
	});
	var introKeysGradeTeachAll = [
		"assignments.Details_Helper.Teacher.Grade_cont",
		"assignments.Details_Helper.Teacher.Edit_button",
		"assignments.Details_Helper.Teacher.Action_button",
		"assignments.Details_Helper.Teacher.Insert_grade",
		"assignments.Details_Helper.Teacher.Insert_comment",
		"assignments.Details_Helper.Teacher.Arrow_details",
		"assignments.Details_Helper.Teacher.Desc_tab",
		"assignments.Details_Helper.Teacher.Disc_tab",
		"assignments.Details_Helper.Teacher.Stats_tab"
	];

	$translate(introKeysGradeTeachAll).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGradeTeachAll.length; ++i) {
			$scope.OptionsGradeTeachAll.steps[i].intro = translations[introKeysGradeTeachAll[i]];
		}
	});

	$scope.IntroOptionsGradeTeachDisc = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-edit",
			position: "right",
			intro: ""
		}, {
			element: "#dropdownMenu1",
			position: "right",
			intro: ""
		}, {
			element: "#insert-grade",
			position: "right",
			intro: ""
		}, {
			element: "#insert-comment",
			position: "right",
			intro: ""
		}, {
			element: "#arrow-right",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsGradeTeachDisc.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsGradeTeachDisc.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsGradeTeachDisc.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsGradeTeachDisc.doneLabel = translations[introButtons[3]];
	});
	var introKeysGradeTeachDisc = [
		"assignments.Details_Helper.Teacher.Grade_cont",
		"assignments.Details_Helper.Teacher.Edit_button",
		"assignments.Details_Helper.Teacher.Action_button",
		"assignments.Details_Helper.Teacher.Insert_grade",
		"assignments.Details_Helper.Teacher.Insert_comment",
		"assignments.Details_Helper.Teacher.Arrow_details",
		"assignments.Details_Helper.Teacher.Desc_tab",
		"assignments.Details_Helper.Teacher.Disc_tab"
	];

	$translate(introKeysGradeTeachDisc).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGradeTeachDisc.length; ++i) {
			$scope.IntroOptionsGradeTeachDisc.steps[i].intro = translations[introKeysGradeTeachDisc[i]];
		}
	});

	$scope.IntroOptionsGradeTeachStats = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-edit",
			position: "right",
			intro: ""
		}, {
			element: "#dropdownMenu1",
			position: "right",
			intro: ""
		}, {
			element: "#insert-grade",
			position: "right",
			intro: ""
		}, {
			element: "#insert-comment",
			position: "right",
			intro: ""
		}, {
			element: "#arrow-right",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsGradeTeachStats.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsGradeTeachStats.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsGradeTeachStats.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsGradeTeachStats.doneLabel = translations[introButtons[3]];
	});
	var introKeysGradeTeachStats = [
		"assignments.Details_Helper.Teacher.Grade_cont",
		"assignments.Details_Helper.Teacher.Edit_button",
		"assignments.Details_Helper.Teacher.Action_button",
		"assignments.Details_Helper.Teacher.Insert_grade",
		"assignments.Details_Helper.Teacher.Insert_comment",
		"assignments.Details_Helper.Teacher.Arrow_details",
		"assignments.Details_Helper.Teacher.Desc_tab",
		"assignments.Details_Helper.Teacher.Stats_tab"
	];

	$translate(introKeysGradeTeachStats).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGradeTeachStats.length; ++i) {
			$scope.IntroOptionsGradeTeachStats.steps[i].intro = translations[introKeysGradeTeachStats[i]];
		}
	});

	$scope.IntroOptionsGradeTeach = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-edit",
			position: "right",
			intro: ""
		}, {
			element: "#dropdownMenu1",
			position: "right",
			intro: ""
		}, {
			element: "#insert-grade",
			position: "right",
			intro: ""
		}, {
			element: "#insert-comment",
			position: "right",
			intro: ""
		}, {
			element: "#arrow-right",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsGradeTeach.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsGradeTeach.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsGradeTeach.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsGradeTeach.doneLabel = translations[introButtons[3]];
	});
	var introKeysGradeTeach = [
		"assignments.Details_Helper.Teacher.Grade_cont",
		"assignments.Details_Helper.Teacher.Edit_button",
		"assignments.Details_Helper.Teacher.Action_button",
		"assignments.Details_Helper.Teacher.Insert_grade",
		"assignments.Details_Helper.Teacher.Insert_comment",
		"assignments.Details_Helper.Teacher.Arrow_details",
		"assignments.Details_Helper.Teacher.Desc_tab"
	];

	$translate(introKeysGradeTeach).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysGradeTeach.length; ++i) {
			$scope.IntroOptionsGradeTeach.steps[i].intro = translations[introKeysGradeTeach[i]];
		}
	});

	$scope.IntroOptionsStatsTeachAll = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-edit",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-teacher",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsStatsTeachAll.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsStatsTeachAll.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsStatsTeachAll.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsStatsTeachAll.doneLabel = translations[introButtons[3]];
	});
	var introKeysStatsTeachAll = [
		"assignments.Details_Helper.Teacher.Stats_cont",
		"assignments.Details_Helper.Teacher.Edit_button",
		"assignments.Details_Helper.Teacher.Desc_tab",
		"assignments.Details_Helper.Teacher.Disc_tab",
		"assignments.Details_Helper.Teacher.Grade_tab"
	];

	$translate(introKeysStatsTeachAll).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysStatsTeachAll.length; ++i) {
			$scope.IntroOptionsStatsTeachAll.steps[i].intro = translations[introKeysStatsTeachAll[i]];
		}
	});

	$scope.IntroOptionsStatsTeach = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-edit",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-teacher",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsStatsTeach.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsStatsTeach.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsStatsTeach.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsStatsTeach.doneLabel = translations[introButtons[3]];
	});
	var introKeysStatsTeach = [
		"assignments.Details_Helper.Teacher.Stats_cont",
		"assignments.Details_Helper.Teacher.Edit_button",
		"assignments.Details_Helper.Teacher.Desc_tab",
		"assignments.Details_Helper.Teacher.Grade_tab"
	];

	$translate(introKeysStatsTeach).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysStatsTeach.length; ++i) {
			$scope.IntroOptionsStatsTeach.steps[i].intro = translations[introKeysStatsTeach[i]];
		}
	});

	$scope.IntroOptionsDiscTeachAll = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-edit",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-teacher",
			position: "right",
			intro: ""
		}, {
			element: "#details-stats",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsDiscTeachAll.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsDiscTeachAll.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsDiscTeachAll.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsDiscTeachAll.doneLabel = translations[introButtons[3]];
	});
	var introKeysDiscTeachAll = [
		"assignments.Details_Helper.Teacher.Disc_cont",
		"assignments.Details_Helper.Teacher.Edit_button",
		"assignments.Details_Helper.Teacher.Disc_comment",
		"assignments.Details_Helper.Teacher.Desc_tab",
		"assignments.Details_Helper.Teacher.Grade_tab",
		"assignments.Details_Helper.Teacher.Stats_tab"
	];

	$translate(introKeysDiscTeachAll).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysDiscTeachAll.length; ++i) {
			$scope.IntroOptionsDiscTeachAll.steps[i].intro = translations[introKeysDiscTeachAll[i]];
		}
	});

	$scope.IntroOptionsDiscTeach = {
		steps: [{
			element: "#content",
			position: "right",
			intro: ""
		}, {
			element: "#details-edit",
			position: "right",
			intro: ""
		}, {
			element: "#details-discuss-comment",
			position: "right",
			intro: ""
		}, {
			element: "#details-desc",
			position: "right",
			intro: ""
		}, {
			element: "#details-grade-teacher",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsDiscTeach.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsDiscTeach.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsDiscTeach.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsDiscTeach.doneLabel = translations[introButtons[3]];
	});
	var introKeysDiscTeach = [
		"assignments.Details_Helper.Teacher.Disc_cont",
		"assignments.Details_Helper.Teacher.Edit_button",
		"assignments.Details_Helper.Teacher.Disc_comment",
		"assignments.Details_Helper.Teacher.Desc_tab",
		"assignments.Details_Helper.Teacher.Grade_tab"
	];

	$translate(introKeysDiscTeach).then(function whenDoneTranslating(translations) {
		for (var i = 0; i < introKeysDiscTeach.length; ++i) {
			$scope.IntroOptionsDiscTeach.steps[i].intro = translations[introKeysDiscTeach[i]];
		}
	});
	$scope.$on("onCentrisIntro", $scope.onCentrisIntro);
});
