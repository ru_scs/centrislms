"use strict";

describe("Testing AssignmentsListGradingController", function() {
	var ctrl, scope, mockAssignmentResource, mockCentrisNotify,
			resource, mockHandins, mockTranslate;

	mockHandins = [{
		SSN: "0101152569",
		Name: "Fake User",
		AssignmentSolutionID: 7,
		CourseGroupName: 1337,
		Rank: 1
	}, {
		SSN: "0101152369",
		Name: "Fake User2",
		AssignmentSolutionID: 73,
		CourseGroupName: 1367,
		Rank: 13
	}];

	beforeEach(module("assignmentsApp", "sharedServices"));

	describe("testing when getHandins is failed", function() {
		beforeEach(inject(function($rootScope, $injector) {
			scope = $rootScope.$new();
			mockCentrisNotify = $injector.get("mockCentrisNotify");
			mockAssignmentResource = $injector.get("mockAssignmentResource");
		}));

		beforeEach(function() {
			mockAssignmentResource.setFakeGettingHandinsAnswers([{
				Solutions: []
			}]);
			mockAssignmentResource.setFakeGettingHandinsConditions([
				false
			]);
			mockCentrisNotify.error = jasmine.createSpy("error");
			mockCentrisNotify.success = jasmine.createSpy("success");
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentsListGradingController", {
				$scope: scope,
				AssignmentResource: mockAssignmentResource,
				centrisNotify: mockCentrisNotify
			});
		}));

		it("should give error message and set gettingHandins to false", function() {
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("assignments.ErrorGettingStudents");
			expect(scope.gettingHandins).toBe(false);
		});
	});
	describe("testing when getHandins is succeeded", function() {
		beforeEach(inject(function($rootScope, $injector) {
			scope = $rootScope.$new();
			mockCentrisNotify = $injector.get("mockCentrisNotify");
			mockAssignmentResource = $injector.get("mockAssignmentResource");
		}));

		beforeEach(function() {
			mockAssignmentResource.setFakeGettingHandinsAnswers([{
				Solutions: []
			}]);
			mockAssignmentResource.setFakeGettingHandinsConditions([
				true
			]);
			mockCentrisNotify.error = jasmine.createSpy("error");
			mockCentrisNotify.success = jasmine.createSpy("success");
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentsListGradingController", {
				$scope: scope,
				AssignmentResource: mockAssignmentResource,
				centrisNotify: mockCentrisNotify
			});
		}));

		it("should not give error message and set gettingHandins to false", function() {
			expect(mockCentrisNotify.error).not.toHaveBeenCalled();
			expect(scope.gettingHandins).toBe(false);
		});

		describe("success functions for publishing", function() {
			beforeEach(function() {
				mockAssignmentResource.setPublishAllGradesForAssignmentAnswers([{}]);
				mockAssignmentResource.setPublishAllGradesForAssignmentConditions([
					true
				]);
			});

			beforeEach(inject(function($controller) {
				ctrl = $controller("AssignmentsListGradingController", {
					$scope: scope,
					AssignmentResource: mockAssignmentResource,
					centrisNotify: mockCentrisNotify
				});
			}));

			it("should give success message when publishing grades", function() {
				scope.assignment = {
					ID: 1337
				};
				scope.publishAllGrades();
				expect(mockCentrisNotify.success).toHaveBeenCalledWith("assignments.Msg.PublishingGradesSucceeded");
			});
		});

		describe("success functions for retracting grades", function() {
			beforeEach(function() {
				mockAssignmentResource.setRetractAllGradesForAssignmentAnswers([{}]);
				mockAssignmentResource.setRetractAllGradesForAssignmentConditions([
					true
				]);
			});

			beforeEach(inject(function($controller) {
				ctrl = $controller("AssignmentsListGradingController", {
					$scope: scope,
					AssignmentResource: mockAssignmentResource,
					centrisNotify: mockCentrisNotify
				});
			}));

			it("should give success message when retracting grades", function() {
				scope.assignment = {
					ID: 1337
				};
				scope.retractAllGrades();
				expect(mockCentrisNotify.success).toHaveBeenCalledWith("assignments.RetractingGradesSucceeded");
			});
		});

		describe("error functions for retracting grades", function() {
			beforeEach(function() {
				mockAssignmentResource.setRetractAllGradesForAssignmentAnswers([{}]);
				mockAssignmentResource.setRetractAllGradesForAssignmentConditions([
					false
				]);
			});

			beforeEach(inject(function($controller) {
				ctrl = $controller("AssignmentsListGradingController", {
					$scope: scope,
					AssignmentResource: mockAssignmentResource,
					centrisNotify: mockCentrisNotify
				});
			}));

			it("should give error message when retracting grades", function() {
				scope.assignment = {
					ID: 1337
				};
				scope.retractAllGrades();
				expect(mockCentrisNotify.error).toHaveBeenCalledWith("assignments.ErrorRetractingGrades",
						"assignments.ErrorRetractingGradesTitle");
			});
		});

		describe("error functions for publishing", function() {
			beforeEach(function() {
				mockAssignmentResource.setPublishAllGradesForAssignmentAnswers([{}]);
				mockAssignmentResource.setPublishAllGradesForAssignmentConditions([
					false
				]);
			});

			beforeEach(inject(function($controller) {
				ctrl = $controller("AssignmentsListGradingController", {
					$scope: scope,
					AssignmentResource: mockAssignmentResource,
					centrisNotify: mockCentrisNotify
				});
			}));

			it("should give success message when publishing grades", function() {
				scope.assignment = {
					ID: 1337
				};
				scope.publishAllGrades();
				expect(mockCentrisNotify.error).toHaveBeenCalledWith("assignments.ErrorPublishingGrades",
						"assignments.ErrorPublishingGradesTitle");
			});
		});

		describe("success functions for saving grades", function() {
			beforeEach(function() {
				mockAssignmentResource.setEditSolutionsAnswers([{}]);
				mockAssignmentResource.setEditSolutionsConditions([
					true
				]);
				mockAssignmentResource.setEditAssignmentAnswers([{}]);
				mockAssignmentResource.setEditAssignmentConditions([
					true
				]);
			});

			beforeEach(inject(function($controller) {
				ctrl = $controller("AssignmentsListGradingController", {
					$scope: scope,
					AssignmentResource: mockAssignmentResource,
					centrisNotify: mockCentrisNotify
				});
			}));

			it("should give success message when publishing grades", function() {
				scope.studentList = [{
					SSN: "0101573369",
					grade: 1337,
					tMemo: "Hello world"
				}];
				scope.saveGrades();
				expect(mockCentrisNotify.success).toHaveBeenCalledWith("assignments.SavingGradesSucceeded");
			});
		});

		describe("error functions for saving grades", function() {
			beforeEach(function() {
				mockAssignmentResource.setEditSolutionsAnswers([{}]);
				mockAssignmentResource.setEditSolutionsConditions([
					false
				]);
				mockAssignmentResource.setEditAssignmentAnswers([{}]);
				mockAssignmentResource.setEditAssignmentConditions([
					true
				]);
			});

			beforeEach(inject(function($controller) {
				ctrl = $controller("AssignmentsListGradingController", {
					$scope: scope,
					AssignmentResource: mockAssignmentResource,
					centrisNotify: mockCentrisNotify
				});
			}));

			it("should give success message when publishing grades", function() {
				scope.studentList = [{
					SSN: "0101573369",
					grade: 1337,
					tMemo: "Hello world"
				}];
				scope.saveGrades();
				expect(mockCentrisNotify.error).toHaveBeenCalledWith("assignments.ErrorSavingGrades",
						"assignments.ErrorSavingGradesTitle");
			});
		});
	});
});
