"use strict";

angular.module("assignmentsApp").controller("AssignmentsListGradingController",
function AssignmentsListGradingController($scope, AssignmentResource, centrisNotify) {
	$scope.filterText = "";
	$scope.gettingHandins = true;
	$scope.gettingAllSolutions = true;
	$scope.solution = null;
	$scope.table = {
		columns: [
			{name: "SSN", visible: true, alias: "SSN"},
			{name: "Email",visible: true, alias: "Email"},
			{name: "assignments.AssignmentGroup", visible: true, alias: "Assignment group"},
			{name: "Group", visible: false, alias: "Group"},
			{name: "assignments.DueDate", visible: true, alias: "Due Date"},
			{name: "assignments.Rank", visible: false, alias: "Rank"},
			{name: "assignments.Grade", visible: true, alias: "Grade"},
			{name: "assignments.Comments", visible: true, alias: "Comments"},
		],
		predicate: "assignments.Name"
	};

	$scope.toggleColumn = function toggleColumn(column) {
		column.visible = !column.visible;
	};

	AssignmentResource.getHandins($scope.courseInstanceID, $scope.assignmentID)
	.success(function(data) {
		$scope.allHandins = data.Solutions;
		$scope.handins    = data.Solutions;
		$scope.ZipUrl     = data.ZipUrl;
	}).error(function(data) {
		centrisNotify.error("assignments.ErrorGettingStudents");
	}).finally(function() {
		$scope.gettingHandins = false;
	});

	$scope.filterHandins = function(data) {
		$scope.handins = [];
		for (var i in $scope.allHandins) {
			if ($scope.allHandins[i].Name.toLowerCase().includes(data.toLowerCase())) {
				$scope.handins.push($scope.allHandins[i]);
			} else if ($scope.allHandins[i].Email.toLowerCase().includes(data.toLowerCase())) {
				$scope.handins.push($scope.allHandins[i]);
			} else if ($scope.allHandins[i].SSN.includes(data)) {
				$scope.handins.push($scope.allHandins[i]);
			} else if ($scope.allHandins[i].AssignmentSolutionID) {
				if ($scope.allHandins[i].AssignmentSolutionID.toString().includes(data)) {
					$scope.handins.push($scope.allHandins[i]);
				}
			}
		}
	};

	// This cannot be implemented properly before there are seperate teacher/student views
	$scope.publishAllGrades = function() {
		AssignmentResource.publishAllGradesForAssignment($scope.courseInstanceID, $scope.assignment.ID)
		.success(function (data) {
			centrisNotify.success("assignments.Msg.PublishingGradesSucceeded");
		})
		.error(function() {
			centrisNotify.error("assignments.ErrorPublishingGrades", "assignments.ErrorPublishingGradesTitle");
		});
	};

	// This cannot be implemented properly before there are seperate teacher/student views
	$scope.retractAllGrades = function() {
		AssignmentResource.retractAllGradesForAssignment($scope.courseInstanceID, $scope.assignment.ID)
		.success(function (data) {
			centrisNotify.success("assignments.RetractingGradesSucceeded");
		})
		.error(function() {
			centrisNotify.error("assignments.ErrorRetractingGrades", "assignments.ErrorRetractingGradesTitle");
		});
	};

	// Sends the updated grades list to the server.
	$scope.saveGrades = function saveGrades() {
		var solutions = [];

		for (var i = 0; i < $scope.studentList.length; i++) {
			var obj = {
				ssn:   $scope.studentList[i].SSN,
				grade: $scope.studentList[i].Grade,
				tMemo: $scope.studentList[i].TeacherMemo
			};
			solutions.push(obj);
		}

		// This will fail because there is not an API call ready.
		AssignmentResource.editSolutions($scope.courseInstanceID, $scope.assignmentID, solutions)
		.success(function() {
			centrisNotify.success("assignments.SavingGradesSucceeded");
		}).error(function() {
			centrisNotify.error("assignments.ErrorSavingGrades", "assignments.ErrorSavingGradesTitle");
		});
	};

});
