"use strict";

angular.module("assignmentsApp").factory("SolutionMemberDlg",
function SolutionMemberDlg($uibModal) {
	return {
		show: function show(studentList) {
			var modalInstance = $uibModal.open({
				templateUrl: "courseinstance/assignments/components/details/group-dlg/solution-member-dlg.html",
				controller:  "SolutionMemberDlgController",
				resolve:     {
					modalParam: function() {
						return {
							studentList: studentList
						};
					}
				}
			});
			return modalInstance.result;
		}
	};
});