"use strict";

describe("Testing SolutionMemberDlg", function () {
	var mockModal, dialog, mockStudentList;

	beforeEach(module("assignmentsApp", "sharedServices", "mockConfig"));
	beforeEach(inject(function($injector) {
		dialog = $injector.get("SolutionMemberDlg");
	}));

	it("can get an instance of TopicsNewDlg", function() {
		expect(dialog).toBeDefined();
	});

	it("TopicsNewDlg.show should be defined", function() {
		dialog.show(mockStudentList);
		expect(dialog.show).toBeDefined();
	});
});
