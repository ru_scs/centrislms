"use strict";

angular.module("assignmentsApp").controller("SolutionMemberDlgController",
function SolutionMemberDlgController($scope, modalParam) {
	$scope.groupMember = {
		selected: {}
	};

	$scope.validation = {
		noStudentSelected: false
	};

	$scope.studentsNotInGroup = modalParam.studentList;

	$scope.addMember = function addMember() {
		if ($scope.groupMember.selected.SSN) {
			$scope.$close($scope.groupMember.selected);
		} else {
			$scope.validation.noStudentSelected = true;
		}
	};
});
