"use strict";

describe("Testing SolutionMemberDlgController", function() {
	var ctrl, scope, mockModalParam;

	mockModalParam = {
		studentList: [{
			Name: "Sveinn"
		}, {
			Name: "Andri"
		}]
	};

	beforeEach(module("assignmentsApp"));
	beforeEach(inject(function($rootScope) {
		scope = $rootScope.$new();
		scope.$close = jasmine.createSpy("close");
	}));

	beforeEach(inject(function($controller) {
		ctrl = $controller("SolutionMemberDlgController", {
			$scope: scope,
			modalParam: mockModalParam
		});
	}));

	it("should set defaultFocus as true", function() {
		expect(scope.validation.noStudentSelected).toBe(false);
	});

	it("should call submodal close", function() {
		scope.groupMember.selected.SSN = "0101102369";
		scope.addMember();
		expect(scope.validation.noStudentSelected).toBe(false);
		expect(scope.$close).toHaveBeenCalledWith(scope.groupMember.selected);
	});

	it("should call submodal close", function() {
		scope.groupMember.selected.SSN = undefined;
		scope.addMember();
		expect(scope.$close).not.toHaveBeenCalledWith();
		expect(scope.validation.noStudentSelected).toBe(true);

	});
});
