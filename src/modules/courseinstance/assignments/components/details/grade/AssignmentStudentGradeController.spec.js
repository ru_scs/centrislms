"use strict";

describe("Testing AssignmentStudentGradeController", function() {
	var ctrl, scope, mockStateParams, mockAssignmentResource;

	beforeEach(module("assignmentsApp", "sharedServices"));

	describe("testing when getStudentHandin is succeeded", function() {
		beforeEach(inject(function($rootScope, $injector) {
			scope = $rootScope.$new();
			mockAssignmentResource = $injector.get("mockAssignmentResource");
		}));

		beforeEach(function() {
			mockAssignmentResource.setFakeGettingHandinsAnswers([{
				Rank: {
					From: "1",
					To: "2",
					Total: "150"
				},
				Grader: {
					Name: "Dabs",
					SSN: "0101015449",
					GradedHandinMilestones: []
				}
			}]);
			mockAssignmentResource.setFakeGettingHandinsConditions([
				true
			]);

		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentStudentGradeController", {
				$scope: scope,
				AssignmentResource: mockAssignmentResource
			});
		}));

		it("should call onFetchFail and set fetchingData to true", function() {
			expect(scope.fetchingData).toBe(false);
			expect(scope.grader.Name).toEqual("Dabs");
			expect(scope.rank).toEqual("1 - 2 / 150");
		});
	});

	describe("testing when getStudentHandin is failed", function() {
		beforeEach(inject(function($rootScope, $injector) {
			scope = $rootScope.$new();
			mockAssignmentResource = $injector.get("mockAssignmentResource");
		}));

		beforeEach(function() {
			mockAssignmentResource.setFakeGettingHandinsAnswers([{
				Rank: {
					From: "1",
					To: "2",
					Total: "150"
				},
				Grader: {
					Name: "Dabs",
					SSN: "0101015449",
					GradedHandinMilestones: []
				}
			}]);
			mockAssignmentResource.setFakeGettingHandinsConditions([
				false
			]);

		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentStudentGradeController", {
				$scope: scope,
				AssignmentResource: mockAssignmentResource
			});
		}));

		it("should call onFetchFail and set fetchingData to true", function() {
			expect(scope.fetchingData).toBe(true);
		});
	});

	describe("testing when rank is undefined/null", function() {
		beforeEach(inject(function($rootScope, $injector) {
			scope = $rootScope.$new();
			mockAssignmentResource = $injector.get("mockAssignmentResource");
		}));

		beforeEach(function() {
			mockAssignmentResource.setFakeGettingHandinsAnswers([{
				Grader: {
					Name: "Dabs",
					SSN: "0101015449",
					GradedHandinMilestones: []
				}
			}]);
			mockAssignmentResource.setFakeGettingHandinsConditions([
				true
			]);
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentStudentGradeController", {
				$scope: scope,
				AssignmentResource: mockAssignmentResource
			});
		}));

		it("should call onFetchSuccess and set rank to empty string", function() {
			expect(scope.fetchingData).toBe(false);
			expect(scope.grader.Name).toEqual("Dabs");
			expect(scope.rank).toEqual("");
		});
	});
});
