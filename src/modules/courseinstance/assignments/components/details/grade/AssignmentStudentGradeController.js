"use strict";

angular.module("assignmentsApp").controller("AssignmentStudentGradeController",
function AssignmentStudentGradeController($scope, $stateParams, AssignmentResource) {
	// UI in loading mode.
	$scope.fetchingData = true;

	// Load grading for current assignment.
	AssignmentResource.getStudentHandin($stateParams.courseInstanceID, $stateParams.assignmentID)
		.success(onFetchSuccess)
		.error(onFetchFail);

	/**
	 * Parse a rank into a string.
	 * Example: 1 - 20 / 30
	 * @param {{From: number, To: number, Total: number}} rank
	 * @returns {string}
	 */
	function parseRank(rank) {
		if (!rank) {
			return "";
		}

		return rank.From + " - " + rank.To + " / " + rank.Total;
	}

	/**
	 * Fired when hand in fetching is complete.
	 * @param solution
	 */
	function onFetchSuccess(solution) {
		$scope.solution = solution;
		$scope.rank = parseRank(solution.Rank);
		$scope.grader = solution.Grader;
		$scope.milestones = solution.GradedHandinMilestones;

		// UI is finished loading.
		$scope.fetchingData = false;
	}

	/**
	 * Fired when hand in fetching fails.
	 */
	function onFetchFail() {
		// TODO: implement.
	}
});
