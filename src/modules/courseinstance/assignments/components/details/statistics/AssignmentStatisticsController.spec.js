"use strict";

describe("Testing AssignmentStatisticsController", function() {
	var ctrl, scope;

	beforeEach(module("assignmentsApp"));

	beforeEach(inject(function($rootScope, $injector) {
		scope = $rootScope.$new();
	}));

	beforeEach(inject(function($controller) {
		ctrl = $controller("AssignmentStatisticsController", {
			$scope: scope
		});
	}));

	it("should call the scope.watch on statisticsData and do nothing with value as undefined", function() {
		scope.$apply();
		scope.$apply();
		expect(scope.chartParams).toBe(null);
	});
	// TODO: Needs to test somehow the apply function with something in value,
	// Does it need directive?
});
