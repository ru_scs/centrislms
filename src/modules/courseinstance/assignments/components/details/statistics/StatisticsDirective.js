"use strict";

angular.module("assignmentsApp").directive("assignmentStatistics",
function onlineexamsStatistics() {
	return {
		restrict: "E",
		scope: {
			statisticsData: "=apiData",
		},
		controller: "AssignmentStatisticsController",
		templateUrl: "courseinstance/assignments/components/details/statistics/statistics.html",
	};
});
