"use strict";

/*
* This controller takes care of displaying statistical information for an assignment.
*/
angular.module("assignmentsApp").controller("AssignmentStatisticsController",
function AssignmentStatisticsController($scope) {
	$scope.chartParams = null;

	$scope.$watch("statisticsData", function(value) {
		if (value !== undefined) {
			var assignment = value;
			var arrValues  = [];
			var arrLabels  = [];

			$scope.$on("create", function(event,data) {
				var studentGrade = value.StudentGrade;
				if (studentGrade) {
					var roundedStudentGrade = Math.round(studentGrade * 2) / 2;

					// Grades > 10 belong to the Grade = 10 column on the chart
					if (roundedStudentGrade > 10) {
						roundedStudentGrade = 10;
					}

					data.datasets[0].bars[roundedStudentGrade * 2].fillColor       = "rgba(100,255,100,0.2)";
					data.datasets[0].bars[roundedStudentGrade * 2].highlightFill   = "rgba(100,255,100,0.2)";
					data.datasets[0].bars[roundedStudentGrade * 2].highlightStroke = "rgba(100,255,100,1)";
					data.datasets[0].bars[roundedStudentGrade * 2].strokeColor     = "rgba(20,200,40,1)";
					data.update();
				}
			});

			var val = 0;
			for (var i = 0; i <= 20; ++i) {
				arrLabels[i] = val;
				arrValues[i] = 0;
				for (var j = 0; j < assignment.GradeCount.length; j++) {
					if (assignment.GradeCount[j].Grade === val) {
						arrValues[i] = assignment.GradeCount[j].Count;
					}
				}
				val += 0.5;
			}
			assignment.values = [arrValues];
			assignment.labels = arrLabels;
		}
	});
});
