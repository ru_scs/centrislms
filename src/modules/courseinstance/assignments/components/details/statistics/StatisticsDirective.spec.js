"use strict";

describe("Testing Statistic directive", function() {
	var scope, compile, element;

	beforeEach(module("assignmentsApp", "sharedServices", "mockConfig", "courseInstanceShared"));
	beforeEach(inject(function($rootScope, $compile) {
		element = angular.element(
				"<assignment-statistics></assignment-statistics>"
		);

		scope = $rootScope.$new();
		compile = $compile(element)(scope);
	}));

	it("should define the directive", function() {
		expect(compile).toBeDefined();
	});
});
