"use strict";

angular.module("assignmentsApp").directive("assignmentSolutionGroup",
function assignmentSolutionGroup() {
	return {
		restrict: "E",
		scope: {
			assignment: "="
		},
		templateUrl: "courseinstance/assignments/components/details/group/group.html",
		controller: "AssignmentSolutionGroupController"
	};
});