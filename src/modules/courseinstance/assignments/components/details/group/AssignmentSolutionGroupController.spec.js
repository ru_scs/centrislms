"use strict";

describe("Testing AssignmentSolutionGroupController", function() {
	var ctrl, scope, mockAssignmentResource, mockStateParams,
			mockCentrisNotify, mockUserService, mockSolutionMemberDlg;

	mockSolutionMemberDlg = {
		show: function() {
			return {
				then: function(fn) {
					var member = {};
					fn(member);
				}
			};
		}
	};

	beforeEach(module("assignmentsApp", "sharedServices"));

	beforeEach(inject(function($rootScope, $injector) {
		scope = $rootScope.$new();
		mockAssignmentResource = $injector.get("mockAssignmentResource");
		mockCentrisNotify = $injector.get("mockCentrisNotify");
		mockStateParams = {
			courseInstanceID: 0,
			assignmentID: 0
		};
		mockUserService = {
			getUserSSN: function() {
				return "0101015539";
			},
			getFullName: function() {
				return "Fake User";
			}
		};
	}));

	describe("testing the when success functions of for the resource calls", function() {
		beforeEach(function() {
			mockAssignmentResource.setCourseInstanceAssignmentsAnswers([{
				Students: [{
					SSN: "0101592929"
				}]
			}]);
			mockAssignmentResource.setCourseInstanceAssignmentsConditions([
				true
			]);
			mockAssignmentResource.setUserAssignmentGroupAnswers([{
				ID: 7,
				Members: []
			}]);
			mockAssignmentResource.setUserAssignmentGroupConditions([
				true
			]);
			mockCentrisNotify.error = jasmine.createSpy("error");
			mockCentrisNotify.success = jasmine.createSpy("success");
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentSolutionGroupController", {
				$scope: scope,
				$stateParams: mockStateParams,
				AssignmentResource: mockAssignmentResource,
				centrisNotify: mockCentrisNotify,
				UserService: mockUserService,
				SolutionMemberDlg: mockSolutionMemberDlg
			});
		}));

		it("should getStudentsNotInGroup and getGroupMembers and set fetchingdata to false", function() {
			expect(scope.fetchingData).toBe(false);
			expect(scope.studentsNotInGroup.length).not.toEqual(0);
		});

		it("should onAddGroupMember and show SolutionMemberDlg for groupid 1 and maximum members", function() {
			scope.groupID = 1;
			scope.groupMembersList = {
				length: 1
			};
			scope.assignment = {
				MaxStudentsInGroup: 1
			};
			scope.onAddGroupMember();
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("groups.Error_to_many_registered");
		});

		it("should onAddGroupMember and show SolutionMemberDlg for groupid 1 and not maximum members", function() {
			scope.groupID = 1;
			scope.groupMembersList = [{
				length: 1
			}];
			scope.assignment = {
				MaxStudentsInGroup: 3
			};
			var length = scope.groupMembersList.length;
			scope.onAddGroupMember();
			expect(scope.groupMembersList.length).toEqual(length + 1);
		});

		it("should removeUserFromGroup and give success message", function() {
			scope.groupID = 1;
			var user = {
				SSN: "0909092559"
			};
			scope.removeStudentFromGroup(user);
			expect(mockCentrisNotify.success).toHaveBeenCalledWith("groups.MemberSuccessfullyRemoved");
		});

		it("should removeUserFromGroup and give success message", function() {
			scope.groupID = 1;
			var user = {
				SSN: "0101015539"
			};
			scope.removeStudentFromGroup(user);
			expect(mockCentrisNotify.success).toHaveBeenCalledWith("groups.MemberSuccessfullyRemoved");
		});

		it("should try to removeUserFromGroup with groupid 0 and give error message", function() {
			scope.groupID = 0;
			var user = {
				SSN: "0909092559"
			};
			scope.removeStudentFromGroup(user);
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("groups.Error_not_registered_in_a_group");
		});
	});

	describe("testing the when error functions of for the resource calls", function() {
		beforeEach(function() {
			mockAssignmentResource.setCourseInstanceAssignmentsAnswers([{
				Students: [{
					SSN: "0101592929"
				}]
			}]);
			mockAssignmentResource.setCourseInstanceAssignmentsConditions([
				false
			]);
			mockAssignmentResource.setUserAssignmentGroupAnswers([{
				ID: 7,
				Members: []
			}]);
			mockAssignmentResource.setUserAssignmentGroupConditions([
				false
			]);
			mockCentrisNotify.error = jasmine.createSpy("error");
			mockCentrisNotify.success = jasmine.createSpy("success");
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentSolutionGroupController", {
				$scope: scope,
				$stateParams: mockStateParams,
				AssignmentResource: mockAssignmentResource,
				centrisNotify: mockCentrisNotify,
				UserService: mockUserService,
				SolutionMemberDlg: mockSolutionMemberDlg
			});
		}));

		it("should call error in getStudentsNotInGroup and set fetchingdata to false", function() {
			expect(scope.fetchingData).toBe(false);
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("groups.ErrorLoadingStudents");
		});

		it("should onAddGroupMember and show SolutionMemberDlg for groupid 1 and maximum members", function() {
			scope.groupID = 1;
			scope.groupMembersList = {
				length: 1
			};
			scope.assignment = {
				MaxStudentsInGroup: 3
			};
			scope.onAddGroupMember();
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("groups.Error_posting_user_to_group");
		});

		it("should removeUserFromGroup and give success message", function() {
			scope.groupID = 1;
			var user = {
				SSN: "0909092559"
			};
			scope.removeStudentFromGroup(user);
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("groups.Error_removing_user_from_group");
		});
	});

	describe("testing the when success functions of for the resource calls, when current student is notingroup",
			function() {
		beforeEach(function() {
			mockAssignmentResource.setCourseInstanceAssignmentsAnswers([{
				Students: [{
					SSN: "0101015539"
				}]
			}]);
			mockAssignmentResource.setCourseInstanceAssignmentsConditions([
				true
			]);
			mockAssignmentResource.setUserAssignmentGroupAnswers([{
				ID: 7,
				Members: []
			}]);
			mockAssignmentResource.setUserAssignmentGroupConditions([
				true
			]);
			mockCentrisNotify.error = jasmine.createSpy("error");
			mockCentrisNotify.success = jasmine.createSpy("success");
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentSolutionGroupController", {
				$scope: scope,
				$stateParams: mockStateParams,
				AssignmentResource: mockAssignmentResource,
				centrisNotify: mockCentrisNotify,
				UserService: mockUserService,
				SolutionMemberDlg: mockSolutionMemberDlg
			});
		}));

		it("should not call error function in getStudentsNotInGroup", function() {
			expect(scope.fetchingData).toBe(false);
			expect(mockCentrisNotify.error).not.toHaveBeenCalled();
		});
	});

	describe("testing the onAddGroupMember with groupID as 0, success functions for createnewgroup and postUserToGroup",
			function() {
		beforeEach(function() {
			mockAssignmentResource.setCourseInstanceAssignmentsAnswers([{
				Students: [{
					SSN: "0101015539"
				}],
				SSN: "0101015539"
			}]);
			mockAssignmentResource.setCourseInstanceAssignmentsConditions([
				true
			]);
			mockAssignmentResource.setUserAssignmentGroupAnswers([{
				ID: 7,
				Members: [{
					SSN: "0101015539"
				}],
				SSN: "0101015539"
			}]);
			mockAssignmentResource.setUserAssignmentGroupConditions([
				true
			]);
			mockCentrisNotify.error = jasmine.createSpy("error");
			mockCentrisNotify.success = jasmine.createSpy("success");
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentSolutionGroupController", {
				$scope: scope,
				$stateParams: mockStateParams,
				AssignmentResource: mockAssignmentResource,
				centrisNotify: mockCentrisNotify,
				UserService: mockUserService,
				SolutionMemberDlg: mockSolutionMemberDlg
			});
		}));

		it("should not call error function in getStudentsNotInGroup, and remove from studentsNotInGroup", function() {
			scope.groupID = 0;
			scope.studentsNotInGroup = [{
				SSN: "0101015539"
			}];
			var length = scope.groupMembersList.length;
			var length2 = scope.studentsNotInGroup.length;
			scope.onAddGroupMember();
			expect(mockCentrisNotify.success).toHaveBeenCalledWith("groups.New_member_successfully_added");
			expect(scope.groupMembersList.length).toEqual(length + 1);
			expect(scope.studentsNotInGroup.length).toEqual(length2 - 1);
		});
	});

	describe("testing the when success functions of for the resource calls, when current student is notingroup",
			function() {
		beforeEach(function() {
			mockAssignmentResource.setCourseInstanceAssignmentsAnswers([{
				Students: [{
					SSN: "0101015539"
				}]
			}]);
			mockAssignmentResource.setCourseInstanceAssignmentsConditions([
				true
			]);
			mockAssignmentResource.setUserAssignmentGroupAnswers([{
				ID: 7,
				Members: []
			}]);
			mockAssignmentResource.setUserAssignmentGroupConditions([
				true
			]);
			mockCentrisNotify.error = jasmine.createSpy("error");
			mockCentrisNotify.success = jasmine.createSpy("success");
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentSolutionGroupController", {
				$scope: scope,
				$stateParams: mockStateParams,
				AssignmentResource: mockAssignmentResource,
				centrisNotify: mockCentrisNotify,
				UserService: mockUserService,
				SolutionMemberDlg: mockSolutionMemberDlg
			});
		}));

		it("should not call error function in getStudentsNotInGroup", function() {
			expect(scope.fetchingData).toBe(false);
			expect(mockCentrisNotify.error).not.toHaveBeenCalled();
		});
	});

	describe("testing the onAddGroupMember with groupID as 0, error functions for createnewgroup", function() {
		beforeEach(function() {
			mockAssignmentResource.setCourseInstanceAssignmentsAnswers([{
				Students: [{
					SSN: "0101015539"
				}]
			}]);
			mockAssignmentResource.setCourseInstanceAssignmentsConditions([
				true
			]);
			mockAssignmentResource.setUserAssignmentGroupAnswers([{
				ID: 7,
				Members: []
			}]);
			mockAssignmentResource.setUserAssignmentGroupConditions([
				false
			]);
			mockCentrisNotify.error = jasmine.createSpy("error");
			mockCentrisNotify.success = jasmine.createSpy("success");
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentSolutionGroupController", {
				$scope: scope,
				$stateParams: mockStateParams,
				AssignmentResource: mockAssignmentResource,
				centrisNotify: mockCentrisNotify,
				UserService: mockUserService,
				SolutionMemberDlg: mockSolutionMemberDlg
			});
		}));

		it("should not call error function in getStudentsNotInGroup", function() {
			scope.groupID = 0;
			var length = scope.groupMembersList.length;
			scope.onAddGroupMember();
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("groups.Error_creating_new_group");
			expect(scope.groupMembersList.length).toEqual(length);
		});
	});

	describe("testing the onAddGroupMember with groupID as 0, error functions for postusertogroup", function() {
		beforeEach(function() {
			mockAssignmentResource.setCourseInstanceAssignmentsAnswers([{
				Students: [{
					SSN: "0101015539"
				}]
			}]);
			mockAssignmentResource.setCourseInstanceAssignmentsConditions([
				false
			]);
			mockAssignmentResource.setUserAssignmentGroupAnswers([{
				ID: 7,
				Members: []
			}]);
			mockAssignmentResource.setUserAssignmentGroupConditions([
				true
			]);
			mockCentrisNotify.error = jasmine.createSpy("error");
			mockCentrisNotify.success = jasmine.createSpy("success");
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentSolutionGroupController", {
				$scope: scope,
				$stateParams: mockStateParams,
				AssignmentResource: mockAssignmentResource,
				centrisNotify: mockCentrisNotify,
				UserService: mockUserService,
				SolutionMemberDlg: mockSolutionMemberDlg
			});
		}));

		it("should not call error function in getStudentsNotInGroup", function() {
			scope.groupID = 0;
			var length = scope.groupMembersList.length;
			scope.onAddGroupMember();
			expect(mockCentrisNotify.error).toHaveBeenCalledWith("groups.Error_posting_user_to_group");
			expect(scope.groupMembersList.length).toEqual(length);
		});
	});
});
