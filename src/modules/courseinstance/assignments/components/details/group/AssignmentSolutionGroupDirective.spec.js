"use strict";

describe("Testing AssignmentSolutionGroup directive", function() {
	var scope, compile, element;

	beforeEach(module("assignmentsApp", "sharedServices", "mockConfig", "courseInstanceShared"));
	beforeEach(inject(function($rootScope, $compile) {
		element = angular.element(
				"<assignment-solution-group></assignment-solution-group>"
		);

		scope = $rootScope.$new();
		compile = $compile(element)(scope);
	}));

	it("should define the directive", function() {
		expect(compile).toBeDefined();
	});
});
