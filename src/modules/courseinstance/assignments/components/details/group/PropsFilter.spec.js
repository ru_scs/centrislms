"use strict";

describe("Testing PropsFilter", function () {
	var filter, items, props, result;

	beforeEach(function () {
		module("assignmentsApp");

		inject(function ($filter) {
			filter = $filter;
		});
	});

	it("should call filter with no array and return same", function () {
		items = "Hello World";
		result = filter("propsFilter")(items, props);
		expect(result).toEqual("Hello World");
	});

	it("should call filter with a non match and return empty array", function () {
		items = [{
			Name: "Sigurður",
			SSN: "0101012559"
		}];
		props = {
			"Name": "Jón",
			"SSN": "0102012559"
		};
		result = filter("propsFilter")(items, props);
		expect(result).toEqual([]);
	});

	it("should call filter with a match and return items", function () {
		items = [{
			Name: "Sigurður",
			SSN: "0101012559"
		}];
		props = {
			"Name": "Jón",
			"SSN": "0101012559"
		};
		result = filter("propsFilter")(items, props);
		expect(result).toEqual(items);
	});
});
