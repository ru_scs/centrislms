"use strict";

/**
 * AssignmentStudentGroupController is responsible for allowing the current
 * user (a student) to manage his/her assignment group, i.e. add and remove members.
 */
angular.module("assignmentsApp").controller("AssignmentSolutionGroupController",
function AssignmentSolutionGroupController($scope, AssignmentResource, $stateParams,
	centrisNotify, UserService, SolutionMemberDlg) {
	$scope.groupMembersList = [];
	$scope.courseInstanceID = $stateParams.courseInstanceID;
	$scope.assignmentID     = $stateParams.assignmentID;
	// Students not in a group that appear in the select list
	$scope.studentsNotInGroup = [];
	// GroupID is 0 until a group is created
	$scope.groupID = 0;

	// Information for the current user
	$scope.currentUser = {
		SSN:  "",
		Name: ""
	};

	// UserService fetches the name and ssn for the current user
	$scope.currentUser.SSN  = UserService.getUserSSN();
	$scope.currentUser.Name = UserService.getFullName();

	$scope.fetchingData     = true;
	AssignmentResource.getStudentsNotInGroup($scope.courseInstanceID, $scope.assignmentID)
	.success(function successFetchingStudents(availableStudents) {
		$scope.fetchingData = false;
		for (var i = 0; i < availableStudents.Students.length; ++i) {
			if ($scope.currentUser.SSN !== availableStudents.Students[i].SSN) {
				$scope.studentsNotInGroup.push(availableStudents.Students[i]);
			}
		}
	}).error(function errorFetchingStudents(error) {
		$scope.fetchingData = false;
		centrisNotify.error("groups.ErrorLoadingStudents");
	});

	// Populate the html table for group members
	function getGroupMembers() {
		AssignmentResource.getUserAssignmentGroup($scope.courseInstanceID, $scope.assignmentID)
		.success(function(data) {
			$scope.groupID = data.ID;
			$scope.groupMembersList = data.Members;
		})
		.error(function(data) {
			centrisNotify.error("groups.Error_getting_user_group");
		});
	}

	getGroupMembers();

	$scope.onAddGroupMember = function onAddGroupMember() {
		SolutionMemberDlg.show($scope.studentsNotInGroup).then(function(member) {
			// If groupID is 0 we need to make 2 API calls,
			// first createNewGroup with the currentUser,
			// then add the selected user to the newly created group
			if ($scope.groupID === 0) {
				// $scope.fetchingData = true;
				// Create new group and then in the callback after the group is created
				// add the selected user to the group
				AssignmentResource.createNewGroup($scope.courseInstanceID, $scope.assignmentID)
				.success(function(cData) {
					$scope.groupID          = cData.ID;
					$scope.groupMembersList = cData.Members;

					// Add the selected user to the group
					AssignmentResource.postUserToGroup($scope.courseInstanceID, $scope.assignmentID, member.SSN, $scope.groupID)
					.success(function(uData) {
						$scope.groupMembersList.push(uData);
						removeStudentFromList($scope.studentsNotInGroup, uData);

						centrisNotify.success("groups.New_member_successfully_added");
					})
					.error(function(pError) {
						centrisNotify.error("groups.Error_posting_user_to_group");
					});
				})
				.error(function(cError) {
					centrisNotify.error("groups.Error_creating_new_group");
					// $scope.fetchingData = false;
				});
			} else {
				// Check if we've reached maximum group size
				if (($scope.groupMembersList.length + 1) > $scope.assignment.MaxStudentsInGroup) {
					centrisNotify.error("groups.Error_to_many_registered");
					return;
				}

				// Add user to existing group
				AssignmentResource.postUserToGroup($scope.courseInstanceID, $scope.assignmentID, member.SSN, $scope.groupID)
				.success(function(pData) {
					$scope.groupMembersList.push(pData);
					removeStudentFromList($scope.studentsNotInGroup, pData);
				}).error(function(pError) {
					centrisNotify.error("groups.Error_posting_user_to_group");
				});
			}
		});
	};

	$scope.removeStudentFromGroup = function removeStudentFromGroup(user) {
		// $scope.fetchingData = true;
		// If the student is not in a group he cannot remove himself
		// TODO: disable this option under that circumstances!!!
		if ($scope.groupID === 0) {
			centrisNotify.error("groups.Error_not_registered_in_a_group");
			return;
		}

		AssignmentResource.removeUserFromGroup($scope.courseInstanceID, $scope.assignmentID, user.SSN, $scope.groupID)
		.success(function(data) {
			getGroupMembers();
			// Add user back to available students, if he is not current user
			if (user.SSN !== $scope.currentUser.SSN) {
				$scope.studentsNotInGroup.push(user);
			}

			centrisNotify.success("groups.MemberSuccessfullyRemoved");
			// TODO: add undo support!

		})
		.error(function(data) {
			centrisNotify.error("groups.Error_removing_user_from_group");
			// $scope.fetchingData = false;
		});
	};

	function removeStudentFromList(studentList, data) {
		for (var i = 0; i < studentList.length; i++) {
			if (studentList[i].SSN === data.SSN) {
				studentList.splice(i, 1);
				break;
			}
		}
	}
});
