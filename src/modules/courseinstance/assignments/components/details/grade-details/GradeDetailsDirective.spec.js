"use strict";

describe("Testing GradeDetails directive", function() {
	var scope, compile, element;

	beforeEach(module("assignmentsApp", "sharedServices", "mockConfig"));
	beforeEach(inject(function($rootScope, $compile) {
		element = angular.element(
				"<grade-details></grade-details>"
		);

		scope = $rootScope.$new();
		compile = $compile(element)(scope);
	}));

	it("should define the directive", function() {
		expect(compile).toBeDefined();
	});
});
