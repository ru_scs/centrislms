"use strict";

angular.module("assignmentsApp").directive("gradeDetails",
function gradeDetails() {
	return {
		restrict: "E",
		scope: {
			grade: "@",
			grader: "=",
			teacherMemo: "@memo",
			rank: "@",
			penalty: "@",
		},
		templateUrl: "courseinstance/assignments/components/details/grade-details/grade-details.html",
	};
});
