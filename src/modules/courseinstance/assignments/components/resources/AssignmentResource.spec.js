"use strict";

describe("AssignmentResource", function () {

	var mockAssignmentResource, mock, mockTopicObject;

	beforeEach(module("assignmentsApp", "sharedServices", "courseInstanceShared"));

	beforeEach(module("assignmentsApp", function($provide) {
		mock = {};
		mock.get = jasmine.createSpy();
		mock.put = jasmine.createSpy();
		mock.post = jasmine.createSpy();
		mock.remove = jasmine.createSpy();
		$provide.value("CentrisResource", mock);
	}));

	beforeEach(inject(function(AssignmentResource) {
		mockAssignmentResource = AssignmentResource;
	}));

	it("should call a function to get all course instance assignments", function() {
		mockAssignmentResource.getCourseInstanceAssignments(1);
		expect(mock.get).toHaveBeenCalledWith("courses", ":id/assignments" , {id: 1});
	});

	it("should call a function to get assignment details", function() {
		mockAssignmentResource.getAssignmentDetails(2, 3);
		expect(mock.get).toHaveBeenCalledWith("courses", ":courseInstanceID/assignments/:assignmentID", {
				courseInstanceID: 2,
				assignmentID: 3
			});
	});

	it("should call a function to create an assignment", function() {
		var data = {
			data: "my test data is awesome"
		};
		mockAssignmentResource.createAssignment(4, data);
		expect(mock.post).toHaveBeenCalledWith("courses", ":courseInstanceID/assignments", {
				courseInstanceID: 4
			}, data);
	});

	it("should call a function to edit an assignment", function() {
		var data = {
			data: "my test data is awesome"
		};
		mockAssignmentResource.editAssignment(5, 6, data);
		expect(mock.put).toHaveBeenCalledWith("courses", ":courseInstanceID/assignments/:assignmentID", {
				courseInstanceID: 5,
				assignmentID: 6
			}, data);
	});

	it("should call a function to delete an assignment", function() {
		mockAssignmentResource.deleteAssignment(7, 8);
		expect(mock.remove).toHaveBeenCalledWith("courses", ":courseInstanceID/assignments/:assignmentID", {
				courseInstanceID: 7,
				assignmentID: 8
			});
	});

	it("should call a function to get all students in course", function() {
		mockAssignmentResource.getStudentsInCourse(9);
		expect(mock.get).toHaveBeenCalledWith("courses", ":courseInstanceID/students", {
				courseInstanceID: 9
			});
	});

	it("should call a function to get all groups in course", function() {
		mockAssignmentResource.getGroupsInCourse(10);
		expect(mock.get).toHaveBeenCalledWith("courses", ":courseInstanceID/groups", {
			courseInstanceID: 10
		});
	});

	it("should call a function to publish grades for assignment", function() {
		mockAssignmentResource.publishAllGradesForAssignment(11, 12);
		expect(mock.put).toHaveBeenCalledWith("courses", ":courseInstanceID/assignments/:assignmentID/handins/publish", {
				courseInstanceID: 11,
				assignmentID: 12
			});
	});

	it("should call a function to retract grades for an assignment", function() {
		mockAssignmentResource.retractAllGradesForAssignment(13, 14);
		expect(mock.put).toHaveBeenCalledWith("courses", ":courseInstanceID/assignments/:assignmentID/handins/retract", {
				courseInstanceID: 13,
				assignmentID: 14
			});
	});

	it("should call a function to get all handins for an assignment", function() {
		mockAssignmentResource.getHandins(14, 15);
		expect(mock.get).toHaveBeenCalledWith("courses", ":courseInstanceID/assignments/:assignmentID/handins", {
				courseInstanceID: 14,
				assignmentID: 15
			});
	});

	it("should call a function to get solutions for an assignment", function() {
		mockAssignmentResource.getSolutions(16, 17);
		expect(mock.get).toHaveBeenCalledWith("courses", ":courseInstanceID/assignments/:assignmentID/solutions", {
				courseInstanceID: 16,
				assignmentID: 17
			});
	});

	it("should call a function to edit a solutions for an assignment", function() {
		var data = {
			data: "my test data is awesome"
		};
		mockAssignmentResource.editSolutions(18, 19, data);
		expect(mock.put).toHaveBeenCalledWith("courses", ":courseInstanceID/assignments/:assignmentID/handins/editbulk", {
				courseInstanceID: 18,
				assignmentID: 19
			}, data);
	});

	it("should call a function to grade a single assignment handin", function() {
		var data = {
			data: "my test data is awesome"
		};
		mockAssignmentResource.gradeSingleAssignmentHandin(20, 21, 22, data);
		expect(mock.put).toHaveBeenCalled();
		// See below
		/*
		expect(mock.put).toHaveBeenCalledWith("courses", ":courseInstanceID/assignments/:assignmentID/handins/:solutionID", {
				courseInstanceID: 20,
				assignmentID: 21,
				solutionID: 22
			}, data);
			*/
	});

	it("should call a function to grade a collection of handins", function() {
		var data = {
			data: "my test data is awesome"
		};
		mockAssignmentResource.createAndGradeSingleAssignmentHandin(23, 24, "0408852859", data);
		expect(mock.post).toHaveBeenCalled();

		// Note: this "expect" has been disabled,
		// since the object now contains form data
		// and that complicates the assertion.
		/*
		expect(mock.post).toHaveBeenCalledWith("courses", ":id/assignments/:assignmentID/handins/:ssn", {
				id: 23,
				assignmentID: 24,
				ssn: "0408852859"
			}, data); */
	});

	it("should call a function to get students that do not belong to a group", function() {
		mockAssignmentResource.getStudentsNotInGroup(25, 26);
		expect(mock.get).toHaveBeenCalledWith("courses", ":id/assignments/:assignmentID/availablestudents", {
				id: 25,
				assignmentID: 26
			});
	});

	it("should call a function to create a new group", function() {
		mockAssignmentResource.createNewGroup(27, 28);
		expect(mock.post).toHaveBeenCalledWith("courses", ":id/assignments/:assignmentID/groups" , {
				id: 27,
				assignmentID: 28
			});
	});

	it("should call a function to get a group for an assignment", function() {
		mockAssignmentResource.getAssignmentGroup(29, 30, 31);
		expect(mock.get).toHaveBeenCalledWith("courses", ":id/assignments/:assignmentID/groups/:groupID", {
				id: 29,
				assignmentID: 30,
				groupID: 31
			});
	});

	it("should call a function to get groups for an assignment", function() {
		mockAssignmentResource.getUserAssignmentGroup(32, 33);
		expect(mock.get).toHaveBeenCalledWith("courses", ":id/assignments/:assignmentID/groups" , {
				id: 32,
				assignmentID: 33
			});
	});

	it("should call a function to get the handin information for current user", function() {
		mockAssignmentResource.getStudentHandin(34, 35);
		expect(mock.get).toHaveBeenCalledWith("courses", ":id/assignments/:assignmentID/handin", {
				id: 34,
				assignmentID: 35
			});
	});

	it("should call a function to add user to a group for an assignment", function() {
		mockAssignmentResource.postUserToGroup(36, 37, "0407853469", 38);
		expect(mock.post).toHaveBeenCalledWith("courses", ":id/assignments/:assignmentID/groups/:groupID", {
				id: 36,
				assignmentID: 37,
				groupID: 38
			}, {
				SSN: "0407853469"
			});
	});

	it("should call a function to post a solution with no new files", function() {
		mockAssignmentResource.postSolution(39, 40, {StudentMemo: "memo", Members: ["0408852869"]});
		expect(mock.post).toHaveBeenCalledWith("courses", ":courseInstanceID/assignments/:assignmentID/handin", {
				courseInstanceID: 39,
				assignmentID: 40
			}, new FormData(), true);
	});

	it("should call a function to post a solution with some files", function() {
		mockAssignmentResource.postSolution(39, 40, {StudentMemo: "memo", Members: ["0408852869"], NewFiles: ["zip", "pdf"]});
		expect(mock.post).toHaveBeenCalledWith("courses", ":courseInstanceID/assignments/:assignmentID/handin", {
				courseInstanceID: 39,
				assignmentID: 40
			}, new FormData(), true);
	});

	it("should call a function to remove a handin file from an assignment", function() {
		mockAssignmentResource.removeHandinFile(41, 42, [{ID: "zip"}, {ID:"pdf"}]);
		expect(mock.put).toHaveBeenCalledWith("courses", ":id/assignments/:assignmentID/attachments", {
				id: 41,
				assignmentID: 42
			}, {
				Files: ["zip", "pdf"]
			});
	});

	it("should call a function to remove a user from a group", function() {
		mockAssignmentResource.removeUserFromGroup(43, 44, "0507904289", 45);
		expect(mock.remove).toHaveBeenCalledWith("courses", ":id/assignments/:assignmentID/groups/:groupID/:ssn", {
				id: 43,
				assignmentID: 44,
				ssn: "0507904289",
				groupID: 45
			});
	});

	it("should call a function to get a mooshak contest", function() {
		mockAssignmentResource.getMooshakContests();
		expect(mock.get).toHaveBeenCalledWith("courses", "1/assignments/plugins/0", {});
	});

	it("should call a function to get a mooshak problem", function() {
		var contest = {
			data: "awesome test data"
		};
		mockAssignmentResource.getMooshakProblems(contest);
		expect(mock.post).toHaveBeenCalledWith("courses", "1/assignments/plugins/0/problems", {}, {
			AssignmentName: contest
		});
	});

	it("should call a function to get the mooshak grade for a group", function() {
		var contest = {
			data: "awesome test data"
		};
		var problem = "problem 1";
		var ssnlist = ["1207941234", "1307941234"];
		mockAssignmentResource.getMooshakProblemsGroupGrade(contest, problem, ssnlist);
		expect(mock.post).toHaveBeenCalledWith("courses", "2/assignments//plugins/0/problems/submission", {}, {
				AssignmentName: contest,
				ProblemName: problem,
				UserSSNList: ssnlist
			});
	});

	it("should call a function to get categories for assignments", function() {
		mockAssignmentResource.createAssignmentCategory(45);
		expect(mock.get).toHaveBeenCalledWith("courses", ":courseInstanceID/assignmentcategories", { courseInstanceID: 45 });
	});
});
