"use strict";

/*
 * AssignmentResource handles all interaction with the backend in the assignments app.
 */
angular.module("assignmentsApp").factory("AssignmentResource",
function AssignmentResource(CentrisResource) {
	var service = {

		// CRUD methods
		getCourseInstanceAssignments: function getCourseInstanceAssignments(courseInstanceID) {
			return CentrisResource.get("courses", ":id/assignments" , {id: courseInstanceID} );
		},

		getAssignmentDetails: function getAssignmentDetails(courseInstanceID, assignmentID) {
			var param = {
				courseInstanceID: courseInstanceID,
				assignmentID:     assignmentID
			};
			return CentrisResource.get("courses", ":courseInstanceID/assignments/:assignmentID", param);
		},

		createAssignment: function createAssignment(courseInstanceID, data) {
			var param = {
				courseInstanceID: courseInstanceID
			};
			return CentrisResource.post("courses", ":courseInstanceID/assignments", param, data);
		},

		editAssignment: function editAssignment(courseInstanceID, assignmentID, data) {
			var param = {
				courseInstanceID: courseInstanceID,
				assignmentID:     assignmentID
			};
			return CentrisResource.put("courses", ":courseInstanceID/assignments/:assignmentID", param, data);
		},

		deleteAssignment: function deleteAssignment(courseInstanceID, assignmentID) {
			var param = {
				courseInstanceID: courseInstanceID,
				assignmentID:     assignmentID
			};

			return CentrisResource.remove("courses", ":courseInstanceID/assignments/:assignmentID", param);
		},

		createAssignmentCategory: function createAssignmentCategory(courseInstanceID) {
			var param = {
				courseInstanceID: courseInstanceID,
			};

			return CentrisResource.get("courses", ":courseInstanceID/assignmentcategories", param);
		},

		// Non-assignment related methods

		// May not be needed....
		getStudentsInCourse: function getStudentsInCourse(courseInstanceID) {
			var param = {
				courseInstanceID: courseInstanceID
			};
			return CentrisResource.get("courses", ":courseInstanceID/students", param);
		},

		// Not currently used, but should be.
		getGroupsInCourse: function getGroupsInCourse(courseInstanceID) {
			return CentrisResource.get("courses", ":courseInstanceID/groups", { courseInstanceID: courseInstanceID });
		},

		// Handins section

		// Teacher methods:

		// Publishes all grades in a given assignment.
		publishAllGradesForAssignment: function publishAllGradesForAssignment(courseInstanceID, assignmentID) {
			var param = {
				courseInstanceID: courseInstanceID,
				assignmentID:     assignmentID
			};
			return CentrisResource.put("courses", ":courseInstanceID/assignments/:assignmentID/handins/publish", param);
		},

		// Unpublishes all grades in a given assignment
		retractAllGradesForAssignment: function retractAllGradesForAssignment(courseInstanceID, assignmentID) {
			var param = {
				courseInstanceID: courseInstanceID,
				assignmentID:     assignmentID
			};
			return CentrisResource.put("courses", ":courseInstanceID/assignments/:assignmentID/handins/retract", param );
		},

		// Returns a list of all student handins
		getHandins: function getHandins(courseInstanceID, assignmentID) {
			var param = {
				courseInstanceID: courseInstanceID,
				assignmentID:     assignmentID
			};
			return CentrisResource.get("courses", ":courseInstanceID/assignments/:assignmentID/handins", param);
		},

		getSolutions: function getSolutions(courseInstanceID, assignmentID) {
			var param = {
				courseInstanceID: courseInstanceID,
				assignmentID:     assignmentID
			};
			return CentrisResource.get("courses", ":courseInstanceID/assignments/:assignmentID/solutions", param);
		},

		// TODO!!! missing from API!!!
		editSolutions: function editSolutions(courseInstanceID, assignmentID, data) {
			var param = {
				courseInstanceID: courseInstanceID,
				assignmentID:     assignmentID
			};
			return CentrisResource.put("courses", ":courseInstanceID/assignments/:assignmentID/handins/editbulk", param, data);
		},

		// This method grades a single handin
		gradeSingleAssignmentHandin: function gradeSingleAssignmentHandin(courseInstanceID,
			assignmentID, solutionID, data) {

			var fd = new FormData();

			// TODO: Published
			var model = {
				SolutionID:        data. ID,
				Grade:             data.Grade,
				LateHandinPenalty: data.LateHandinPenalty,
				TeacherMemo:       data.TeacherMemo,
				Published:         false,
				HandinMilestones:  data.HandinMilestones
			};

			// Create the model object which will be a part of the request:
			fd.append("model", JSON.stringify(model));
			if (data.NewFiles !== undefined && data.NewFiles.length > 0) {
				for (var i = 0 ; i < data.NewFiles.length ; i++) {
					fd.append("File" + i, data.NewFiles[i]);
				}
			}

			var param = {
				courseInstanceID: courseInstanceID,
				assignmentID:     assignmentID,
				solutionID:       solutionID
			};

			return CentrisResource.put("courses", ":courseInstanceID/assignments/:assignmentID/handins/:solutionID",
				param, fd, true);
		},

		// This method grades a collection of handins
		createAndGradeSingleAssignmentHandin: function createAndGradeSingleAssignmentHandin(courseInstanceID,
			assignmentID, ssn, data) {

			var fd = new FormData();

			// TODO: Published
			var model = {
				SolutionID:        data. ID,
				Grade:             data.Grade,
				LateHandinPenalty: data.LateHandinPenalty,
				TeacherMemo:       data.TeacherMemo,
				Published:         false,
				HandinMilestones:  data.HandinMilestones
			};

			// Create the model object which will be a part of the request:
			fd.append("model", JSON.stringify(model));
			if (data.NewFiles !== undefined && data.NewFiles.length > 0) {
				for (var i = 0 ; i < data.NewFiles.length ; i++) {
					fd.append("File" + i, data.NewFiles[i]);
				}
			}

			var param = {
				id:           courseInstanceID,
				assignmentID: assignmentID,
				ssn:          ssn
			};

			return CentrisResource.post("courses", ":id/assignments/:assignmentID/handins/:ssn",
				param, fd, true);
		},

		// Student methods - note that the teacher may use those as well:

		getStudentsNotInGroup: function getStudentsNotInGroup(courseInstanceID, assignmentID) {
			var param = {
				id:           courseInstanceID,
				assignmentID: assignmentID
			};
			return CentrisResource.get("courses", ":id/assignments/:assignmentID/availablestudents", param);
		},

		createNewGroup: function createNewGroup(courseInstanceID, assignmentID) {
			var param = {
				id:           courseInstanceID,
				assignmentID: assignmentID
			};
			return CentrisResource.post("courses", ":id/assignments/:assignmentID/groups" , param);
		},

		getAssignmentGroup: function getAssignmentGroup(courseInstanceID, assignmentID, groupID) {
			var param = {
				id:           courseInstanceID,
				assignmentID: assignmentID,
				groupID:      groupID
			};
			return CentrisResource.get("courses", ":id/assignments/:assignmentID/groups/:groupID" , param );
		},

		getUserAssignmentGroup: function getUserAssignmentGoup(courseInstanceID, assignmentID) {
			var param = {
				id:           courseInstanceID,
				assignmentID: assignmentID
			};
			return CentrisResource.get("courses", ":id/assignments/:assignmentID/groups" , param);
		},

		// Returns the handin information for the current user
		getStudentHandin: function getStudentHandin(courseInstanceID, assignmentID) {
			var param = {
				id:           courseInstanceID,
				assignmentID: assignmentID
			};
			return CentrisResource.get("courses", ":id/assignments/:assignmentID/handin", param);
		},

		postUserToGroup: function postUserToGroup(courseInstanceID, assignmentID, ssn, groupID) {
			var model = {
				SSN: ssn
			};

			var param = {
				id:           courseInstanceID,
				assignmentID: assignmentID,
				groupID:      groupID
			};

			return CentrisResource.post("courses", ":id/assignments/:assignmentID/groups/:groupID", param, model);
		},

		postSolution: function postAssignment (courseInstanceID, assignmentID, handinObj) {
			var fd = new FormData();
			// Copy the student memo property:
			var model = {
				StudentMemo: handinObj.StudentMemo,
				GroupMembers: []
			};

			// Copy all members:
			angular.forEach(handinObj.Members, function(obj) {
				model.GroupMembers.push(obj.SSN);
			});

			// Create the model object which will be a part of the request:
			fd.append("model", JSON.stringify(model));
			if (handinObj.NewFiles !== undefined && handinObj.NewFiles.length > 0) {
				for (var i = 0 ; i < handinObj.NewFiles.length ; i++) {
					fd.append("File" + i, handinObj.NewFiles[i]);
				}
			}

			var param = {
				courseInstanceID: courseInstanceID,
				assignmentID:     assignmentID
			};

			return CentrisResource.post("courses", ":courseInstanceID/assignments/:assignmentID/handin",
				param, fd, true);
		},

		removeHandinFile: function removeHandinFile(courseInstanceID, assignmentID, removedHandinFiles) {
			var model = {
				Files: []
			};

			angular.forEach(removedHandinFiles, function(obj) {
				model.Files.push(obj.ID);
			});

			var param = {
				id:           courseInstanceID,
				assignmentID: assignmentID
			};

			return CentrisResource.put("courses", ":id/assignments/:assignmentID/attachments", param, model);
		},

		removeUserFromGroup: function removeUserFromGroup(courseInstanceID, assignmentID, ssn, groupID) {
			var param = {
				id:           courseInstanceID,
				assignmentID: assignmentID,
				ssn:          ssn,
				groupID:      groupID
			};
			return CentrisResource.remove("courses", ":id/assignments/:assignmentID/groups/:groupID/:ssn", param);
		},

		getMooshakContests: function getMooshakContests() {
			return CentrisResource.get("courses", "1/assignments/plugins/0" , { } );
		},

		getMooshakProblems: function getMooshakProblems(contest) {
			var model = {
				AssignmentName: contest
			};
			return CentrisResource.post("courses", "1/assignments/plugins/0/problems" , { }, model );
		},

		// UserSSNList is a comma seperated list of Socal sequrity number of group memebers
		getMooshakProblemsGroupGrade: function getMooshakProblemsGroupGrade(contest, problem, UserSSNList) {
			var param = {
				AssignmentName: contest,
				ProblemName: problem,
				UserSSNList:UserSSNList
			};
			return CentrisResource.post("courses", "2/assignments//plugins/0/problems/submission", { } , param );
		}
	};

	return service;
});
