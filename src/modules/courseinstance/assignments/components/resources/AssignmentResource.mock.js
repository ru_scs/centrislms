"use strict";

angular.module("assignmentsApp").factory("mockAssignmentResource", function() {
	var courseInstanceAssignmentsIndex = 0;
	var fakeCourseInstanceAssignmentsAnswers;
	var fakeCourseInstanceAssignmentsConditions;

	var assignmentDetailsIndex = 0;
	var fakeAssignmentDetailsAnswers;
	var fakeAssignmentDetailsConditions;

	var editAssignmentIndex = 0;
	var fakeEditAssignmentAnswers;
	var fakeEditAssignmentConditions;

	var deleteAssignmentIndex = 0;
	var fakeDeleteAssignmentAnswers;
	var fakeDeleteAssignmentConditions;

	var publishAllGradesForAssignmentIndex = 0;
	var fakePublishAllGradesForAssignmentAnswers;
	var fakePublishAllGradesForAssignmentConditions;

	var groupsInCourseIndex = 0;
	var fakeGroupsInCourseAnswers;
	var fakeGroupsInCourseConditions;

	var gettingHandinsIndex = 0;
	var fakeGettingHandinsAnswers;
	var fakeGettingHandinsConditions;

	var retractAllGradesForAssignmentIndex = 0;
	var fakeRetractAllGradesForAssignmentAnswers;
	var fakeRetractAllGradesForAssignmentConditions;

	var editSolutionsIndex = 0;
	var fakeEditSolutionsAnswers;
	var fakeEditSolutionsConditions;

	var userAssignmentGroupIndex = 0;
	var fakeUserAssignmentGroupAnswers;
	var fakeUserAssignmentGroupConditions;

	var removeHandinFileIndex = 0;
	var fakeRemoveHandinFileAnswers;
	var fakeRemoveHandinFileConditions;

	var postSolutionIndex = 0;
	var fakePostSolutionAnswers;
	var fakePostSolutionConditions;

	var categoriesAssignmentIndex = 0;
	var fakeCategoryAnswers = [];
	var fakeCategoriesCondition = true;

	function MockPromise(condition, answer) {
		return {
			success: function(fn) {
				if (condition === undefined) {
					return {
						error: function(fn) {
							return;
						}
					};
				}
				if (condition) {
					fn(answer);
				}
				return {
					error: function(fn) {
						if (!condition) {
							fn(answer);
						}
						return {
							finally: function(fn) {
								fn();
							}
						};
					},
					finally: function(fn) {
						fn();
					}
				};
			}
		};
	}

	return {
		setCourseInstanceAssignmentsAnswers: function setCourseInstanceAssignmentsAnswers(answers) {
			fakeCourseInstanceAssignmentsAnswers = answers;
		},
		setCourseInstanceAssignmentsConditions: function setCourseInstanceAssignmentsConditions(conditions) {
			fakeCourseInstanceAssignmentsConditions = conditions;
		},
		getCourseInstanceAssignments: function getCourseInstanceAssignments(courseInstanceID) {
			var condition = fakeCourseInstanceAssignmentsConditions[courseInstanceAssignmentsIndex];
			var answer = fakeCourseInstanceAssignmentsAnswers[courseInstanceAssignmentsIndex];
			courseInstanceAssignmentsIndex++;
			return new MockPromise(condition, answer);
		},
		setAssignmentDetailsAnswers: function setAssignmentDetailsAnswers(answers) {
			fakeAssignmentDetailsAnswers = answers;
		},
		setAssignmentDetailsConditions: function setAssignmentDetailsConditions(conditions) {
			fakeAssignmentDetailsConditions = conditions;
		},
		getAssignmentDetails: function getAssignmentDetails(courseInstanceID, assignmentID) {
			var condition = fakeAssignmentDetailsConditions[assignmentDetailsIndex];
			var answer = fakeAssignmentDetailsAnswers[assignmentDetailsIndex];
			assignmentDetailsIndex++;
			return new MockPromise(condition, answer);
		},
		createAssignment: function createAssignment(courseInstanceID, data) {

		},
		setEditAssignmentAnswers: function setEditAssignmentAnswers(answers) {
			fakeEditAssignmentAnswers = answers;
		},
		setEditAssignmentConditions: function setEditAssignmentConditions(conditions) {
			fakeEditAssignmentConditions = conditions;
		},
		editAssignment: function editAssignment(courseInstanceID, assignmentID, data) {
			var condition = fakeEditAssignmentConditions[editAssignmentIndex];
			var answer = fakeEditAssignmentAnswers[editAssignmentIndex];
			editAssignmentIndex++;
			return new MockPromise(condition, answer);
		},
		setDeleteAssignmentAnswers: function setDeleteAssignmentAnswers(answers) {
			fakeDeleteAssignmentAnswers = answers;
		},
		setDeleteAssignmentConditions: function setDeleteAssignmentConditions(conditions) {
			fakeDeleteAssignmentConditions = conditions;
		},
		deleteAssignment: function deleteAssignment(courseInstanceID, assignmentID) {
			var condition = fakeDeleteAssignmentConditions[deleteAssignmentIndex];
			var answer = fakeDeleteAssignmentAnswers[deleteAssignmentIndex];
			deleteAssignmentIndex++;
			return new MockPromise(condition, answer);
		},
		getStudentsInCourse: function getStudentsInCourse(courseInstanceID) {

		},
		setGroupsInCourseAnswers: function setGroupsInCourseAnswers(answers) {
			fakeGroupsInCourseAnswers = answers;
		},
		setGroupsInCourseConditions: function setGroupsInCourseConditions(conditions) {
			fakeGroupsInCourseConditions = conditions;
		},
		getGroupsInCourse: function getGroupsInCourse(courseInstanceID) {
			var condition = fakeGroupsInCourseConditions[groupsInCourseIndex];
			var answer = fakeGroupsInCourseAnswers[groupsInCourseIndex];
			groupsInCourseIndex++;
			return new MockPromise(condition, answer);
		},
		setPublishAllGradesForAssignmentAnswers: function setPublishAllGradesForAssignmentAnswers(answers) {
			fakePublishAllGradesForAssignmentAnswers = answers;
		},
		setPublishAllGradesForAssignmentConditions: function setPublishAllGradesForAssignmentConditions(conditions) {
			fakePublishAllGradesForAssignmentConditions = conditions;
		},
		publishAllGradesForAssignment: function publishAllGradesForAssignment(courseInstanceID, assignmentID) {
			var condition = fakePublishAllGradesForAssignmentConditions[publishAllGradesForAssignmentIndex];
			var answer = fakePublishAllGradesForAssignmentAnswers[publishAllGradesForAssignmentIndex];
			publishAllGradesForAssignmentIndex++;
			return new MockPromise(condition, answer);
		},
		setRetractAllGradesForAssignmentAnswers: function setRetractAllGradesForAssignmentAnswers(answers) {
			fakeRetractAllGradesForAssignmentAnswers = answers;
		},
		setRetractAllGradesForAssignmentConditions: function setRetractAllGradesForAssignmentConditions(conditions) {
			fakeRetractAllGradesForAssignmentConditions = conditions;
		},
		retractAllGradesForAssignment: function retractAllGradesForAssignment(courseInstanceID, assignmentID) {
			var condition = fakeRetractAllGradesForAssignmentConditions[retractAllGradesForAssignmentIndex];
			var answer = fakeRetractAllGradesForAssignmentAnswers[retractAllGradesForAssignmentIndex];
			return new MockPromise(condition, answer);
		},
		setFakeGettingHandinsAnswers: function setFakeGettingHandinsAnswers(answers) {
			fakeGettingHandinsAnswers = answers;
		},
		setFakeGettingHandinsConditions: function setFakeGettingHandinsConditions(conditions) {
			fakeGettingHandinsConditions = conditions;
		},
		getHandins: function getHandins(courseInstanceID, assignmentID) {
			var condition = fakeGettingHandinsConditions[gettingHandinsIndex];
			var answer = fakeGettingHandinsAnswers[gettingHandinsIndex];
			return new MockPromise(condition, answer);
		},
		getSolutions: function getSolutions(courseInstanceID, assignmentID) {
			var condition = fakeCourseInstanceAssignmentsConditions[courseInstanceID];
			var answer = fakeCourseInstanceAssignmentsAnswers[assignmentID];
			return new MockPromise(condition, answer);
		},
		setEditSolutionsAnswers: function setEditSolutionsAnswers(answers) {
			fakeEditSolutionsAnswers = answers;
		},
		setEditSolutionsConditions: function setEditSolutionsConditions(conditions) {
			fakeEditSolutionsConditions = conditions;
		},
		editSolutions: function editSolutions(courseInstanceID, assignmentID, data) {
			var condition = fakeEditSolutionsConditions[editSolutionsIndex];
			var answer = fakeEditAssignmentAnswers[editSolutionsIndex];
			return new MockPromise(condition, answer);
		},
		gradeSingleAssignmentHandin: function gradeSingleAssignmentHandin(courseInstanceID, assignmentID, solutionID, data) {

		},
		createAndGradeSingleAssignmentHandin: function createAndGradeSingleAssignmentHandin(courseInstanceID,
			assignmentID, ssn, data) {

		},
		getStudentsNotInGroup: function getStudentsNotInGroup(courseInstanceID, assignmentID) {
			var condition = fakeCourseInstanceAssignmentsConditions[courseInstanceID];
			var answer = fakeCourseInstanceAssignmentsAnswers[assignmentID];
			return new MockPromise(condition, answer);
		},
		createNewGroup: function createNewGroup(courseInstanceID, assignmentID) {
			var condition = fakeUserAssignmentGroupConditions[courseInstanceID];
			var answer = fakeUserAssignmentGroupAnswers[assignmentID];
			return new MockPromise(condition, answer);
		},
		getAssignmentGroup: function getAssignmentGroup(courseInstanceID, assignmentID, groupID) {

		},
		setUserAssignmentGroupConditions: function setUserAssignmentGroupConditions(conditions) {
			fakeUserAssignmentGroupConditions = conditions;
		},
		setUserAssignmentGroupAnswers: function setUserAssignmentGroupAnswers(answers) {
			fakeUserAssignmentGroupAnswers = answers;
		},
		getUserAssignmentGroup: function getUserAssignmentGoup(courseInstanceID, assignmentID) {
			var condition = fakeUserAssignmentGroupConditions[userAssignmentGroupIndex];
			var answer = fakeUserAssignmentGroupAnswers[userAssignmentGroupIndex];
			return new MockPromise(condition, answer);
		},
		getStudentHandin: function getStudentHandin(courseInstanceID, assignmentID) {
			var condition = fakeGettingHandinsConditions[gettingHandinsIndex];
			var answer = fakeGettingHandinsAnswers[gettingHandinsIndex];
			return new MockPromise(condition, answer);
		},
		postUserToGroup: function postUserToGroup(courseInstanceID, assignmentID, ssn, groupID) {
			var condition = fakeCourseInstanceAssignmentsConditions[courseInstanceID];
			var answer = fakeCourseInstanceAssignmentsAnswers[assignmentID];
			return new MockPromise(condition, answer);
		},
		setPostSolutionAnswers: function setPostSolutionAnswers(answers) {
			fakePostSolutionAnswers = answers;
		},
		setPostSolutionConditions: function setPostSolutionConditions(conditions) {
			fakePostSolutionConditions = conditions;
		},
		postSolution: function postAssignment(courseInstanceID, assignmentID, handinObj) {
			var condition = fakePostSolutionConditions[postSolutionIndex];
			var answer = fakePostSolutionAnswers[postSolutionIndex];
			return new MockPromise(condition, answer);
		},
		setRemoveHandinFileAnswers: function setRemoveHandinFileAnswers(answers) {
			fakeRemoveHandinFileAnswers = answers;
		},
		setRemoveHandinFileConditions: function setRemoveHandinFileConditions(conditions) {
			fakeRemoveHandinFileConditions = conditions;
		},
		removeHandinFile: function removeHandinFile(courseInstanceID, assignmentID, removedHandinFiles) {
			var condition = fakeRemoveHandinFileConditions[removeHandinFileIndex];
			var answer = fakeRemoveHandinFileAnswers[removeHandinFileIndex];
			return new MockPromise(condition, answer);
		},
		removeUserFromGroup: function removeUserFromGroup(courseInstanceID, assignmentID, ssn, groupID) {
			var condition = fakeCourseInstanceAssignmentsConditions[courseInstanceID];
			var answer = fakeCourseInstanceAssignmentsAnswers[assignmentID];
			return new MockPromise(condition, answer);
		},
		getMooshakContests: function getMooshakContests() {

		},
		getMooshakProblems: function getMooshakProblems(contest) {

		},
		getMooshakProblemsGroupGrade: function getMooshakProblemsGroupGrade(contest, problem, UserSSNList) {

		},
		createAssignmentCategory: function getCategoriesAssignment(courseInstanceId) {
			return new MockPromise(fakeCategoriesCondition, fakeCategoryAnswers);
		}
	};
});
