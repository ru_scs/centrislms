"use strict";

/*
 * AssignmentGradingController takes care of displaying a grading form
 * for a single handin from a given student or a group of students.
 * It also allows the teacher to manage grades, group members and
 * handin attachments, even if the student never handed in anything
 * (in case handins were submitted via email or some other means).
 *
 * This module can get called in two ways:
 * - by passing in a handinID - in that case it is highly likely that
 *   the student has handed in the assignment and that the handinID
 *   is valid, even though that is not guaranteed! (the user may try
 *   to modify the url parameters).
 * - by passing in a student ssn. In this case, it is by no means
 *   guaranteed that a handin exists.
 */
angular.module("assignmentsApp").controller("AssignmentGradingController",
function ($scope, $stateParams, $state, AssignmentResource, centrisNotify,
	pluginFactory, SolutionMemberDlg, FileUtils) {
	// Global variables for the looping movement when user is going prev\next on assignments.
	var handinId = 0;
	var handins;
	var milestoneId = 0;

	// Init scope variables:
	$scope.mileStoneSolution                  = false;
	$scope.noHandins                          = false;
	$scope.assignmentID                       = $stateParams.assignmentID;
	$scope.courseInstanceID                   = $stateParams.courseInstanceID;
	$scope.hasChangedSolution                 = false;
	$scope.totalWeight                        = 0;
	$scope.totalPoints                        = 0;

	// Contains all milestones for the assignment
	$scope.assignmentMilestones               = null;
	// The milestone to be graded
	$scope.selectedStone                      = null;

	// Students available to be added to a group
	$scope.studentsNotInGroup = [];

	// Late handin info
	$scope.lateHandin                         = false;
	// The calculated penalty
	$scope.lateHandinPenalty                  = 0;
	// The already given penalty
	$scope.previousPenalty                    = 0;
	// The chosen penalty, teacher might want to choose the penalty for some solutions.
	// When grading solutions that have already been graded and given penalty, this variable
	// initialises as zero.
	$scope.chosenPenalty                      = 0;

	$scope.lateHandinDurationType             = 0;
	$scope.assignmentDateClosed               = null;
	$scope.assignmentLateHandinGradeDecrease  = 0;

	// This will contain all the new files which the teacher adds
	// to the solution handin.
	$scope.NewFiles = [];
	// This will contain all files which the user wants to remove
	// from the solution handin.
	$scope.RemovedHandinFiles = [];

	// The ID of the handin, 'undefined' if the student has not handed in.
	$scope.handinID							  = $stateParams.handinID;

	$scope.checkIfLateHandin = function checkIfLateHandin() {

		var handinDate  = new Date($scope.studentAssignment.HandinDate);
		var closeDate   = new Date($scope.assignmentDateClosed);

		$scope.lateHandin = false;
		if (handinDate > closeDate) {
			$scope.lateHandin = true;
			// Time since close date in milliseconds
			var timeDiff = Math.abs(handinDate.getTime() - closeDate.getTime());

			// Check if late handin, 1=Days, 2=Hours, 3=Minutes
			if ($scope.lateHandinDurationType === 1) {
				var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
				$scope.lateHandinPenalty = $scope.assignmentLateHandinGradeDecrease * diffDays;
			} else if ($scope.lateHandinDurationType === 2) {
				var diffHours = Math.ceil(timeDiff / (1000 * 3600));
				$scope.lateHandinPenalty = $scope.assignmentLateHandinGradeDecrease * diffHours;
			} else if ($scope.lateHandinDurationType === 3) {
				var diffMinutes = Math.ceil(timeDiff / 1000 / 60);
				$scope.lateHandinPenalty = $scope.assignmentLateHandinGradeDecrease * diffMinutes;
			}

			// Change the number to a negative
			$scope.lateHandinPenalty *= -1;

			if ($scope.studentAssignment.LateHandinPenalty === null) {
				$scope.chosenPenalty      = $scope.lateHandinPenalty;
			} else {
				// The solution has been graded before, so we set the chosen penalty to zero,
				// so we wont penalise it twice.
				$scope.previousPenalty   = $scope.studentAssignment.LateHandinPenalty;
				$scope.chosenPenalty     = 0;
			}
		}
	};

	// If some milestones have already been graded, we display the grades
	$scope.setMilestoneGrades = function setMilestoneGrades() {

		// If a milestone was graded in this session,
		// GradedHandinMilestones will not contain the grade.
		if ($scope.studentAssignment.HandinMilestones !== undefined) {
			// Maybe a better way to do this ?
			for (var n in $scope.studentAssignment.HandinMilestones) {
				for (var m in $scope.assignmentMilestones) {
					if ($scope.studentAssignment.HandinMilestones[n].MilestoneID === $scope.assignmentMilestones[m].ID) {
						$scope.assignmentMilestones[m].Grade = $scope.studentAssignment.HandinMilestones[n].Grade;
					}
				}
			}
		} else {
			// Add the grade to the milestones. Maybe a better way to do this ?
			for (var k in $scope.studentAssignment.GradedHandinMilestones) {
				for (var j in $scope.assignmentMilestones) {
					if ($scope.studentAssignment.GradedHandinMilestones[k].MilestoneID === $scope.assignmentMilestones[j].ID) {
						$scope.assignmentMilestones[j].Grade = $scope.studentAssignment.GradedHandinMilestones[k].Grade;
					}
				}
			}
		}
	};

	// Getting single assignment from the id fetched with $stateParams.
	// We need this in order to display the name of the assignment in the header,
	// plus various other things such as determining late handin config for
	// the assignment, and more.
	AssignmentResource.getAssignmentDetails($scope.courseInstanceID, $scope.assignmentID)
	.success(function(data) {
		$scope.assignment = angular.copy(data);
		if (data.Milestones.length > 0) {
			$scope.assignmentMilestones = data.Milestones;
		}

		$scope.lateHandinDurationType            = $scope.assignment.LateHandinDurationType;
		$scope.assignmentDateClosed              = $scope.assignment.DateClosed;
		$scope.assignmentLateHandinGradeDecrease = $scope.assignment.LateHandinGradeDecrease;

		$scope.mileStoneSolution = data.Milestones.length > 0;

		for (var i = 0; i < $scope.assignment.Milestones.length; i++) {
			$scope.totalWeight += $scope.assignment.Milestones[i].Weight;
		}
	})
	.error(function(data, error) {
		centrisNotify.error("assignments.ErrorLoadingAssignment");
	});

	$scope.gettingAssignmentGroups = true;
	// Fetching the student handins for this assignment
	AssignmentResource.getSolutions($scope.courseInstanceID, $scope.assignmentID)
	.success(function(data) {
		var i;
		if (data.length > 0) {
			handins = data;
			if ($scope.handinID !== undefined ) {
				// Get the selected solution
				for (i = 0; i < handins.length; ++i) {
					if (handins[i].ID.toString() === $scope.handinID) {
						$scope.studentAssignment = data[i];
						$scope.checkIfLateHandin();

						// Check if milestones have been graded.
						if ($scope.studentAssignment.GradedHandinMilestones.length > 0) {
							$scope.setMilestoneGrades();
						}
					}
				}
			} else {
				// The student has not handed in a solution, so we find his empty template
				for (i = 0; i < handins.length; ++i) {
					if (handins[i].Members) {
						if (handins[i].Members[0].SSN === $stateParams.ssn) {
							$scope.studentAssignment = data[i];
							handinId = i;
						}
					}
				}
			}

			$scope.setHasStudentComment();
		}
		// No handins is not an error condition. Even though the method
		// which returns handins does also return objects for those students
		// which haven't handed in anything, it won't do that if the assignment
		// is anonymous. In that case, we might possibly get back an empty set,
		// if no students have handed in the assignment.

		$scope.gettingAssignmentGroups = false;
	}).error(function(data) {
		$scope.gettingAssignmentGroups = false;
		if (data === "INVALID_ASSIGNMENT_ID") {
			centrisNotify.error("assignments.AssignmentNotFound");
		} else {
			// TODO: ensure is in translate table!
			centrisNotify.error("assignments.FailedLoadingAssignment");
		}
	});

	$scope.setHasStudentComment = function setHasStudentComment() {
		if ($scope.studentAssignment) {
			$scope.hasStudentComment = $scope.studentAssignment.StudentMemo !== null &&
			$scope.studentAssignment.StudentMemo !== "" &&
			$scope.studentAssignment.StudentMemo !== " ";
			// TODO: Check if the string is all white space.
		} else {
			$scope.hasStudentComment = false;
		}
	};

	AssignmentResource.getStudentsNotInGroup($scope.courseInstanceID, $scope.assignmentID)
	.success(function successFetchingStudents(availableStudents) {
		for (var i = 0; i < availableStudents.Students.length; ++i) {
			$scope.studentsNotInGroup.push(availableStudents.Students[i]);
		}
	}).error(function errorFetchingStudents(error) {
		centrisNotify.error("groups.ErrorLoadingStudents");
	});

	// Prev\Next function user passes in +1\-1 to move between assignments, if you are at the end\start of
	// the assignment array it loops back to the starting\end position by calling loopArray.
	$scope.setAssignment = function setAssignment(val) {

		if ($scope.hasChangedSolution) {
			$scope.submitChanges($scope.studentAssignment);
			$scope.hasChangedSolution = false;
		}
		handinId                 = loopArray(val, handins, handinId);
		$scope.studentAssignment = handins[handinId];
		$scope.changeRoute($scope.studentAssignment);
		$scope.setMilestone(0);
		$scope.setHasStudentComment();

		// Set late handin scope variables
		$scope.checkIfLateHandin();

		$scope.RemovedHandinFiles = [];

		if ($scope.assignment.Milestones !== null) {

			$scope.assignmentMilestones = angular.copy($scope.assignment.Milestones);

			// Make sure user stays on same milestone tab
			var currMilestone = $scope.assignmentMilestones[milestoneId];
			if (currMilestone) {
				$scope.selectedStone        = currMilestone.active = true;

				// Check if milestones have been graded.
				if ($scope.studentAssignment.GradedHandinMilestones.length > 0 ||
					$scope.studentAssignment.HandinMilestones !== undefined) {
					$scope.setMilestoneGrades();
				}
			}
		}
	};

	$scope.changeRoute = function changeRoute(studentAssignment) {
		var gradingUrl = "courseinstance.assignments.";
		if (studentAssignment.ID !== 0) {
			$scope.handinID = studentAssignment.ID;
			$state.go(gradingUrl + "grading", {assignmentID: $scope.assignmentID, handinID: $scope.handinID}, {notify: false});
		}else {
			$scope.handinID = undefined;
			var ssn = studentAssignment.Members[0].SSN;
			$state.go(gradingUrl + "studentgrading", {assignmentID: $scope.assignmentID, ssn: ssn}, {notify: false});
		}
	};

	$scope.setMilestone = function setMilestone(val) {
		if (val === 0) {
			if ($scope.assignmentMilestones && $scope.assignmentMilestones.length > 0) {
				$scope.selectedStone      = $scope.assignmentMilestones[0];
			}
		} else {
			milestoneId               = loopArray(val, $scope.assignmentMilestones, milestoneId);
			$scope.assignmentMilestones[milestoneId].active = true;
			$scope.selectedStone      = $scope.assignmentMilestones[milestoneId];
		}

		$scope.milestoneFocus = true;
	};

	// When a milestone tab is clicked, we must kepp track of the index.
	$scope.setMilestoneIndex = function setMilestoneIndex(index) {
		milestoneId          = index;
		$scope.selectedStone = $scope.assignmentMilestones[milestoneId];
	};

	function indexOfMilestone(stoneId, stones) {
		for (var i = 0; i < stones.length; i++) {
			if (stoneId === stones[i].ID) {
				return i;
			}
		}
	}

	// Makes the next/prev button for assignments\milestones
	// loop back to the end/start, depending on the position.
	function loopArray(action, item, fixPos) {
		action = fixPos + action;
		if (action >= item.length) {
			return 0;
		}

		if (action === -1) {
			return item.length - 1;
		}
		return action;
	}

	// Check if the assignment has milestones, and if it has milestones show them
	// and set the first one as default selected.
	function areMilestones(item) {
		if (item.Milestones) {
			if (item.Milestones.length > 0) {
				$scope.setMilestone(milestoneId);
				return true;
			}
		}

		return false;
	}

	// NOT USED, but if we choose to grade milestones with 1-10 instead of points,
	// this function will calculate the total grade.
	$scope.updateGradeTotal = function updateGradeTotal() {
		var sum = 0;
		var grade;
		$scope.assignmentMilestones.forEach(function(item, index, array) {
			grade = 0;
			if (item.Grade !== undefined) {
				grade = item.Grade;
			}
			sum += ( item.Weight / 100 ) * grade;
		});

		$scope.studentAssignment.Grade = parseFloat(sum.toFixed(2));
	};

	// Updates the assignment total grade. To get the right grade,
	// you need to first subtract the grade if given for current milestone.
	$scope.updateGrade = function updateGrade(stone, grade) {

		if ($scope.selectedStone.Grade > stone.Weight) {
			$scope.selectedStone.Grade = stone.Weight;
		}

		$scope.totalPoints = 0;
		for (var i = 0; i < $scope.assignmentMilestones.length; i++) {
			if ($scope.assignmentMilestones[i].Grade !== undefined &&
				!isNaN($scope.assignmentMilestones[i].Grade) &&
				$scope.assignmentMilestones[i].Grade !== "" &&
				$scope.assignmentMilestones[i].Grade !== null) {
				$scope.totalPoints += parseInt($scope.assignmentMilestones[i].Grade);
			}
		}

		$scope.studentAssignment.Grade = parseFloat(($scope.totalPoints / 10).toFixed(2));

		// TODO: Maybe add support for bonuses.
	};

	// Saves the grade for the "current" solution
	$scope.submitChanges = function submitChanges(item) {

		// Add late handin penalty to the solution
		if ($scope.lateHandin) {
			item.LateHandinPenalty = $scope.chosenPenalty;
			item.Grade            += $scope.chosenPenalty;
			$scope.previousPenalty = $scope.chosenPenalty;
		}

		var handinMilestonesModel = [];
		if ($scope.mileStoneSolution) {
			for (var i in $scope.assignmentMilestones) {
				if ($scope.assignmentMilestones[i].Grade !== undefined &&
					!isNaN($scope.assignmentMilestones[i].Grade) &&
					$scope.assignmentMilestones[i].Grade !== "") {
					var stone = {
						MilestoneID:          $scope.assignmentMilestones[i].ID,
						AssignmentID:         $scope.assignmentMilestones[i].AssignmentID,
						AssignmentSolutionID: $scope.studentAssignment.ID,
						Grade:                $scope.assignmentMilestones[i].Grade
					};
					handinMilestonesModel.push(stone);
				}
				item.HandinMilestones = handinMilestonesModel;
			}
		}

		if ($scope.studentAssignment.ID === 0) {
			// Student did not hand in a solution, so we create it
			// and grade it
			item.Graded = true;
			AssignmentResource.createAndGradeSingleAssignmentHandin($scope.courseInstanceID,
				$scope.assignmentID, $scope.studentAssignment.Members[0].SSN, item)
			.success(function(data) {
				$scope.studentAssignment.Attachments = data.Attachments;
				$scope.hasChangedSolution = false;
				// Now he has a AssignmentSolutionID
				handins[handinId].ID = data.ID;
				centrisNotify.success("assignments.Msg.CouldGradeSolution");
			}).error(function(data) {
				$scope.hasChangedSolution = true;
				centrisNotify.error("assignments.Msg.CouldNotGradeSolution");
			});

		} else {
			// Delete the selected files.
			// We do this separately to not over complicate the postSolution process.
			if ($scope.RemovedHandinFiles.length > 0) {
				AssignmentResource.removeHandinFile($scope.courseInstanceID,
					$scope.assignmentID, $scope.RemovedHandinFiles)
				.success(function(sData) {
				})
				.error(function(eData) {
					centrisNotify.error("assignments.Msg.ErrorRemovingFilesFromSolution");
				});
			}

			item.Graded = true;
			AssignmentResource.gradeSingleAssignmentHandin($scope.courseInstanceID,
				$scope.assignmentID, $scope.studentAssignment.ID, item)
			.success(function(data) {
				$scope.studentAssignment.Attachments = data.Attachments;
				$scope.hasChangedSolution = false;
				centrisNotify.success("assignments.Msg.CouldGradeSolution");
			}).error(function(data) {
				$scope.hasChangedSolution = true;
				centrisNotify.error("assignments.Msg.CouldNotGradeSolution");
			});
		}
	};

	$scope.markChanged = function markChanged() {
		$scope.hasChangedSolution = true;
	};

	// KEYBOARD SHORTCUTS:
	// When in the grade for milestone text box:
	// - Press Shift+Tab to go back to the previous milestone.
	// - Press Shift+Enter to go to the next group.
	//
	// When in the comment for milestone text box:
	// - Press Tab to go to the next milestone.
	// - Press Shift+Enter to go to the next group.
	$scope.checkIfTabWasPressed = function checkIfTabWasPressed(event) {
		if (event.keyCode === 9 && !event.shiftKey) {
			event.preventDefault();
			$scope.setMilestone(+1);

			$scope.milestoneFocus = true;
		}

		if (event.keyCode === 13 && event.shiftKey) {
			$scope.setAssignment(+1);
			$scope.milestoneFocus = true;
		}
	};

	$scope.checkIfTabWasPressedBackwards = function checkIfTabWasPressedBackwards(event) {
		if (event.keyCode === 9 && event.shiftKey) {
			event.preventDefault();
			$scope.setMilestone(-1);
			$scope.milestoneFocus = true;
		}

		if (event.keyCode === 13 && event.shiftKey) {
			$scope.setAssignment(+1);
			$scope.milestoneFocus = true;
		}
	};
	// The functions below are also used in AssignmentStudentHandinController. Create a factory?
	$scope.onFileSelected = function onFileSelected($files, $event) {

		angular.forEach($files, function(file) {
			// Add the file to solution.Attachments if its extension is allowed.
			for (var i = 0; i < $scope.assignment.AllowedFileExtensions.length; ++i) {
				var str = new RegExp($scope.assignment.AllowedFileExtensions[i] + "$", "g");
				var res = file.name.match(str);
				if (res !== null) {
					// Match found
					$scope.studentAssignment.Attachments.push({
						FriendlyFilename: file.name,
						Size:             FileUtils.formatBytes(file.size),
						IsNew:            true,
						$$hashKey:        file.$$hashKey
						// TODO: icon?
					});
					break;
				} else if (i === $scope.assignment.AllowedFileExtensions.length - 1) {
					centrisNotify.error("assignments.ValidationMessages.FileEndingNotAllowed");
				}
			}
		});
	};

	// Removes a submitted handinfile
	$scope.removeAttachment = function removeAttachment(file) {
		if (file.IsNew) {
			removeAttachmentFromList(file);
		} else {
			var undoParam = {
				type: "assignment-handin-delete",
				id: {
					attachmentID: file.ID,
					instance:     $scope.courseInstanceID
				}
			};
			$scope.RemovedHandinFiles.push(file);
			centrisNotify.successWithUndo("assignments.Msg.AttachmentRemoved", undoParam);
			removeAttachmentFromList(file);
		}
	};

	$scope.$on("centrisUndo", function undo(event, param) {
		if (param.type === "assignment-handin-delete") {
			// Return the file to the soltuion.Attachments,
			// and remove it from the RemovedHandinFiles.
			var i = $scope.RemovedHandinFiles.length;
			while (i--) {
				if ($scope.RemovedHandinFiles[i].ID === param.id.attachmentID) {
					$scope.studentAssignment.Attachments.push($scope.RemovedHandinFiles[i]);
					$scope.RemovedHandinFiles.splice(i, 1);
				}
			}
		}
	});

	// Removes a file from the submitted file list.
	function removeAttachmentFromList(data) {
		for (var i = 0; i < $scope.studentAssignment.Attachments.length; i++) {
			if ($scope.studentAssignment.Attachments[i].ID === data.ID) {
				$scope.studentAssignment.Attachments.splice(i, 1);
				break;
			}
		}

		// TODO: test this!!!
		for (i = 0; i < $scope.NewFiles.length; i++) {
			if ($scope.NewFiles[i].$$hashKey === data.$$hashKey) {
				$scope.NewFiles.splice(i, 1);
				break;
			}
		}
	}

	// Makes a object from a given json string this object is retuned by the
	// getMooshakProblemsGroupGrade success function. if the string is invalid
	// then this function will return null but if the string contains a valid
	// object then that object will be returned.
	$scope.ojbectifySubmission = function ojbectifySubmission(jsonSubmissionString) {
		if (jsonSubmissionString === undefined) {
			return null;
		}
		var obj = JSON.parse(jsonSubmissionString);
		if (obj === undefined) {
			return null;
		}
		var keys = Object.keys(obj);
		if (keys.length < 1) {
			return null;
		}
		var userName = keys[0];
		if (obj[userName].grade === undefined) {
			return null;
		}
		var grade = parseFloat(obj[userName].grade);
		if (isNaN(grade)) {
			return null;
		}
		// overwrite grade of type string with grade of type string.
		obj[userName].grade = grade;
		return obj;
	};
	// Fetches a milestone grade from Mooshak and set it as the group grade.
	$scope.getMoosahkMileStoneGrade = function getMoosahkMileStoneGrade(mileStone) {
		var groupSsn = "";
		$scope.studentAssignment.Members.forEach(function(item, index, array) {
			if (index > 0) {
				groupSsn += ",";
			}
			groupSsn += item.SSN;
		});
		AssignmentResource.getMooshakProblemsGroupGrade(
			$scope.selectedStone.Contest,
			$scope.selectedStone.Problem,
			groupSsn)
			.success(function(data) {
				var submission = $scope.ojbectifySubmission(data);
				$scope.codeLink = "";
				if (submission) {
					// We got a valid submission object according to ojbectifySubmission( function
					var key = Object.keys(submission)[0];
					$scope.codeLink = "/ace/?contest=" +
									$scope.selectedStone.Contest +
									"&problem="        + $scope.selectedStone.Problem +
									"&groupssn="       + groupSsn +
									"&courseinstance=" + $scope.courseInstanceID;
					mileStone.Grade = submission[key].grade;
					$scope.updateGradeTotal();
				} else {
					console.log("Submission object is invalid");
				}
			}).error(function(data) {
				centrisNotify.error("assignments.Msg.ErrorGetMooshakProblemsGroupGrade");
			});
	};
	// checks if the pluginData contains a key "MooshakPlugin"
	$scope.isMooshakPlugin = function isMooshakPlugin(pluginData) {
		var isPlugin = pluginFactory.keyExists("MooshakPlugin", pluginData);
		if (isPlugin === true) {
			var obj = pluginFactory.getPluginData($scope.selectedStone.PluginData, "MooshakPlugin");
			if (obj !== undefined) {
				$scope.selectedStone.Contest = obj.Contest;
				$scope.selectedStone.Problem = obj.Problem;
			} else {
				$scope.selectedStone.Contest = "";
				$scope.selectedStone.Problem = "";
			}
			return true;
		}
		return false;
	};
	$scope.isViewerSupport = function isViewerSupport(fileName) {
		var str = $scope.getMode(fileName);
		if (str.length > 0) {
			return true;
		}
		return false;
	};
	// todo: the next two functions getExtension and getMode exits in
	// ace/src/common/AceFactory.js but I don't dare to add it AceFactory.js
	// to the common folder.
	$scope.getExtension = function getExtension(str) {
		var index = str.lastIndexOf(".") + 1;
		if (index < 1) {
			return "";
		}
		return str.substring(index);
	};
	$scope.getMode = function getMode(fileName) {
		if (fileName === undefined) {
			return "";
		}
		var extension = $scope.getExtension(fileName).toLowerCase();
		var mode;
		switch (extension) {
			case "cpp":
			case "c":
			case "h": {
				mode = "ace/mode/c_cpp";
				break;
			}
			case "js": {
				mode = "ace/mode/javascript";
				break;
			}
			case "java":
			case "class": {
				mode = "ace/mode/java";
				break;
			}
			case "bat": {
				mode = "ace/mode/batchfile";
				break;
			}
			case "cs": {
				mode = "ace/mode/csharp";
				break;
			}
			case "css": {
				mode = "ace/mode/css";
				break;
			}
			case "html": {
				mode = "ace/mode/html";
				break;
			}
			case "json": {
				mode = "ace/mode/json";
				break;
			}
			case "less": {
				mode = "ace/mode/less";
				break;
			}
			case "lisp": {
				mode = "ace/mode/lisp";
				break;
			}
			case "md": {
				mode = "ace/mode/markdown";
				break;
			}
			case "sql": {
				mode = "ace/mode/sql";
				break;
			}
			case "tex": {
				mode = "ace/mode/tex";
				break;
			}
			case "vb": {
				mode = "ace/mode/vbscript";
				break;
			}
			case "xml": {
				mode = "ace/mode/xml";
				break;
			}
			case "py": {
				mode = "ace/mode/python";
				break;
			}
			case "sh": {
				mode = "ace/mode/sh";
				break;
			}
			case "jsp": {
				mode = "ace/mode/jsp";
				break;
			}
			case "cshtml": {
				mode = "ace/mode/razor";
				break;
			}
			case "pl": {
				mode = "ace/mode/prolog";
				break;
			}
			case "pas": {
				mode = "ace/mode/pascal";
				break;
			}
			case "scss": {
				mode = "ace/mode/scss";
				break;
			}
			case "ss": {
				mode = "ace/mode/scheme";
				break;
			}
			default: {
				mode = "";
			}
		}
		return mode;
	};
	$scope.onAddGroupMember = function onAddGroupMember() {
		SolutionMemberDlg.show($scope.studentsNotInGroup).then(function(member) {
			AssignmentResource.postUserToGroup($scope.courseInstanceID, $scope.assignmentID, member.SSN,
				$scope.studentAssignment.ID)
			.success(function(pData) {
				$scope.studentAssignment.Members.push(member);
				removeStudentFromList($scope.studentsNotInGroup, member);
				// Make appropriate changes to the handin list
				for (var i = 0; i < handins.length; ++i) {
					if (handins[i].ID.toString() !== $scope.handinID) {
						// Remove the empty solution from the new member
						if (handins[i].Members[0].SSN === member.SSN) {
							handins.splice(i, 1);
						}
					}
				}

				centrisNotify.success("groups.New_member_successfully_added");

			}).error(function(pError) {
				centrisNotify.error("groups.Error_posting_user_to_group");
			});
		});
	};

	$scope.removeStudentFromGroup = function removeStudentFromGroup(user) {
		// Teacher is not allowed to remove the last student from the solution.
		// This check is also performed in the API and the teacher should not be
		// afforded this option in the UI.
		if ($scope.studentAssignment.Members.length === 1) {
			centrisNotify.error("groups.Error_removing_last_user_from_group");
			return;
		}

		AssignmentResource.removeUserFromGroup($scope.courseInstanceID,
			$scope.assignmentID, user.SSN, $scope.studentAssignment.ID)
		.success(function(data) {
			// Add empty solution for the removed student in the handin list
			var emptySolution = {
				Attachments: [],
				Grade: null,
				GradedHandinMilestones: [],
				HandinDate: null,
				ID: 0,
				LateHandinPenalty: null,
				Members: [],
				Rank: null,
				StudentMemo: null,
				TeacherMemo: null
			};
			emptySolution.Members.push(user);
			handins.push(emptySolution);

			// Find and remove the student from the studentAssignment.
			removeStudentFromList($scope.studentAssignment.Members, user);
			// Student is now availible to be added to a group
			$scope.studentsNotInGroup.push(user);

			centrisNotify.success("groups.MemberSuccessfullyRemoved");
			// TODO: add undo support!
		})
		.error(function(data) {
			centrisNotify.error("groups.Error_removing_user_from_group");
		});
	};
	function removeStudentFromList(studentList, data) {
		for (var i = 0; i < studentList.length; i++) {
			if (studentList[i].SSN === data.SSN) {
				studentList.splice(i, 1);
				break;
			}
		}
	}
});
