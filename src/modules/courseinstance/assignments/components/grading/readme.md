# Assignment grading

This folder contains files which are used while grading a single assignment.

Note: there is another folder called "grading" in assignment details. That folder
deals with the grading of a list of handins, while this focuses on a single assignment.