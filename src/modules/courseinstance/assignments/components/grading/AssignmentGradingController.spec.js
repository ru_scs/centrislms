"use strict";

describe(" Testing AssignmentGradingController", function() {
	var ctrl, scope, rootScope, mockStateParams, mockCentrisNotify,
	mockAssignmentsResource, mockPluginFactory;

	beforeEach(module("assignmentsApp", "sharedServices", "mockConfig"));

	beforeEach(inject(function($rootScope, $injector) {
		rootScope = $rootScope;
		scope = $rootScope.$new();
		mockCentrisNotify = $injector.get("mockCentrisNotify");
		mockAssignmentsResource = $injector.get("mockAssignmentResource");
		mockPluginFactory = $injector.get("mockPluginFactory");
		mockStateParams = {
			assignmentID: 1,
			courseInstanceID: 2,
			handinID: 3
		};
	}));

	beforeEach(function() {
		mockAssignmentsResource.setCourseInstanceAssignmentsAnswers([{}]);
		mockAssignmentsResource.setCourseInstanceAssignmentsConditions([
			undefined
		]);
		mockAssignmentsResource.setAssignmentDetailsAnswers([{}]);
		mockAssignmentsResource.setAssignmentDetailsConditions([
			undefined
		]);
	});

	describe("using empty assignment resource but adding necessary data", function() {

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentGradingController", {
				$scope: scope,
				$stateParams: mockStateParams,
				pluginFactory: mockPluginFactory,
				AssignmentResource: mockAssignmentsResource,
				centrisNotify: mockCentrisNotify
			});
		}));

		beforeEach(function() {
			scope.lateHandinDurationType = 1;
			scope.studentAssignment = {};
		});

		it("should initalize the stateParams correctly", function() {
			expect(scope.assignmentID).toEqual(mockStateParams.assignmentID);
			expect(scope.courseInstanceID).toEqual(mockStateParams.courseInstanceID);
			expect(scope.handinID).toEqual(mockStateParams.handinID);
		});

		describe("testing the check late handin for an assignment", function() {

			it("should do nothing if handin is not allowed", function() {
				scope.lateHandinDurationType = undefined;
				scope.checkIfLateHandin();
				expect(scope.lateHandinDurationType).not.toEqual(1);
			});

			it("should not add any penalties if submission is not late", function() {
				scope.studentAssignment.HandinDate = "2016-04-03T12:39:52.707";
				scope.assignmentDateClosed = "2016-04-04T23:59:00.000";
				scope.checkIfLateHandin();
				expect(scope.lateHandinPenalty).toBe(0);
				expect(scope.lateHandinDurationType).toEqual(1);
			});

			it("should check how late the submission was if handin was late and add penalty", function() {
				scope.studentAssignment.HandinDate = "2016-04-03T12:39:52.707";
				scope.studentAssignment.LateHandinPenalty = null;
				scope.assignmentLateHandinGradeDecrease = -1;
				scope.assignmentDateClosed = "2016-04-02T23:59:00.000";
				scope.checkIfLateHandin();
				expect(scope.lateHandinPenalty).toBe(1);
				expect(scope.lateHandinDurationType).toEqual(1);
			});

			it("should check how late the submission was if handin was late and add penalty", function() {
				scope.studentAssignment.HandinDate = "2016-04-03T12:39:52.707";
				scope.studentAssignment.LateHandinPenalty = 2;
				scope.assignmentDateClosed = "2016-04-02T23:59:00.000";
				scope.checkIfLateHandin();
				expect(scope.lateHandinPenalty).toBe(0);
				expect(scope.previousPenalty).toEqual(2);
			});
		});

		describe("testing the set milestones function", function() {

			it("should do not do anything if there are no milestones", function() {
				scope.setMilestoneGrades();
				expect(scope.studentAssignment.HandinMilestones).toBe(undefined);
			});

			it("should add already graded milestones to the scope, if they were graded in this session", function() {
				scope.studentAssignment.HandinMilestones = [{
					MilestoneID: 0,
					Grade: 7
				}, {
					MilestoneID: 1,
					Grade: 8
				}];
				scope.assignmentMilestones = [{
					ID: 0
				}, {
					ID: 1
				}];
				scope.setMilestoneGrades();
				expect(scope.assignmentMilestones).toEqual([{
					ID: 0,
					Grade: 7
				}, {
					ID: 1,
					Grade: 8
				}]);
			});

			it("should add the grade to Graded Handin Milestones", function() {
				scope.studentAssignment.GradedHandinMilestones = [{
					MilestoneID: 0,
					Grade: 9
				}, {
					MilestoneID: 1,
					Grade: 10
				}];
				scope.assignmentMilestones = [{
					ID: 0
				}, {
					ID: 1
				}];
				scope.setMilestoneGrades();
				expect(scope.assignmentMilestones).toEqual([{
					ID: 0,
					Grade: 9
				}, {
					ID: 1,
					Grade: 10
				}]);
			});
		});
	});
});
