'use strict';

describe('Testing pluginFactory', function () {
	var pluginFactory, mockCentris;

	beforeEach(module('assignmentsApp', 'sharedServices'));

	beforeEach(module('assignmentsApp', function($provide) {
		mockCentris = {};
		mockCentris.error = jasmine.createSpy();
		$provide.value('centrisNotify', mockCentris);
	}));

	beforeEach(inject(function(_pluginFactory_) {
		pluginFactory = _pluginFactory_;
	}));

	it('should create a mooshak plugin object', function() {
		var obj = pluginFactory.makeObject('Assigment 2', 'Solve NP-complete');
		expect(obj).toEqual({
			MooshakPlugin: {
				name:        'Assigment 2',
				description: 'Solve NP-complete'
			}
		});
	});

	describe('testing the addOrEditPluginData function', function () {

		it('should return error for invalid input but return the input unchanged', function() {
			var list = pluginFactory.addOrEditPluginData('', {});
			expect(mockCentris.error).toHaveBeenCalled();
			expect(list).toEqual('');
		});

		it('should edit name property of a plugin object', function() {
			var list = pluginFactory.addOrEditPluginData(
					'{"MooshakPlugin":{"name":"Assigment 2","description":"Solve NP-complete"}}',
					pluginFactory.makeObject('Assigment 1', 'Solve NP-complete'));
			expect(list).toEqual('{"MooshakPlugin":{"name":"Assigment 1","description":"Solve NP-complete"}}');
		});

		it('should add name property of a plugin object if not present', function() {
			var list = pluginFactory.addOrEditPluginData(
					'{"MooshakPlugin":{"description":"Solve NP-complete"}}',
					pluginFactory.makeObject('Assigment 1', 'Solve NP-complete'));
			expect(list).toEqual('{"MooshakPlugin":{"name":"Assigment 1","description":"Solve NP-complete"}}');
		});
	});

	describe('testing the deletePluginData function', function () {

		it('should return the input unchanged if key does not exist', function() {
			var list = pluginFactory.deletePluginData('', {});
			expect(list).toEqual('');
			list = pluginFactory.deletePluginData(
					'{"MooshakPlugin":{"name":"Assigment 5","description":"Create an unbeatable PacMan game"}}',
					{wrongProperty: 'Assigment 8'});
			expect(list).toEqual('{"MooshakPlugin":{"name":"Assigment 5","description":"Create an unbeatable PacMan game"}}');
		});

		it('should delete data from plugin if property exists', function() {
			var list = pluginFactory.deletePluginData(
					'{"MooshakPlugin":{"name":"Assigment 5","description":"Create an unbeatable PacMan game"}}',
					pluginFactory.makeObject('', ''));
			expect(list).toEqual('{}');
		});
	});

	describe('testing the getPluginData function', function () {

		it('should return undefined if pluginKey data does not exist', function() {
			var obj = pluginFactory.getPluginData('', 'name');
			expect(obj).toEqual(undefined);
			obj = pluginFactory.getPluginData(
					'{"MooshakPlugin":{"name":"Assigment 5","description":"Create an unbeatable PacMan game"}}',
					'wrongProperty');
			expect(obj).toEqual(undefined);
		});

		it('should return the data from property if exists', function() {
			var obj = pluginFactory.getPluginData(
					'{"MooshakPlugin":{"name":"Assigment 5","description":"Create an unbeatable PacMan game"}}',
					'MooshakPlugin');
			expect(obj).toEqual({name: 'Assigment 5', description: 'Create an unbeatable PacMan game'});
		});
	});

	describe('testing the keyExists function', function () {

		it('should return false if key does not exist or list is empty', function() {
			var exists = pluginFactory.keyExists('', '{"MooshakPlugin":{"name":"Assigment 7",' +
			'"description":"10 page essay about geese"}}');
			expect(exists).toBe(false);
			exists = pluginFactory.keyExists('MooshakPlugin', '');
			expect(exists).toBe(false);
			exists = pluginFactory.keyExists('MooshakPlugin', undefined);
			expect(exists).toBe(false);
		});

		it('should return true if key exists', function() {
			var exists = pluginFactory.keyExists('MooshakPlugin', '{"MooshakPlugin":{"name":"Assigment 7",' +
			'"description":"10 page essay about geese"}}');
			expect(exists).toBe(true);
		});
	});
});
