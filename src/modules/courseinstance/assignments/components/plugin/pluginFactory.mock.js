"use strict";

angular.module("assignmentsApp").factory("mockPluginFactory",
function() {

	return {
		makeObject: function makeObject(name, description) {
			return {
				MooshakPlugin: {
					name:        name,
					description: description
				}
			};
		},
		addOrEditPluginData: function addOrEditPluginData(pluginList, dataFromPlugin) {
			return "";
		},
		deletePluginData: function deletePluginData(pluginList, dataFromPlugin) {
			return "";
		},
		getPluginData: function getPluginData(pluginList, dataFromPlugin) {
			return "";
		},
		keyExists: function keyExists(pluginList, dataFromPlugin) {
			return false;
		}
	};

});
