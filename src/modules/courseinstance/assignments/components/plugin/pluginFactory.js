"use strict";

// pluginFactory is a library with helper functions for plugins.
angular.module("assignmentsApp").factory("pluginFactory",
function(centrisNotify) {

	return {
		// Create a simple plugin object
		makeObject: function makeObject(name, description) {
			return {
				MooshakPlugin: {
					name:        name,
					description: description
				}
			};
		},

		// Add plugin data to the plugin list of this assignment.
		// The function returns the modified pluginList object.
		// Example:
		// $scope.model.assignmentwizard.PluginData = pluginFactory.addOrEditPluginData(
		//   $scope.model.assignmentwizard.PluginData,
		//   $scope.makeObjectMooshak()
		// );
		addOrEditPluginData: function addOrEditPluginData(pluginList, dataFromPlugin) {
			var out;
			// Get the name of the plugin object
			var pluginKey = Object.keys(dataFromPlugin)[0];
			// Update the object which is saved as a styring
			var obj;
			try {
				obj            = JSON.parse(pluginList);
				obj[pluginKey] = dataFromPlugin[pluginKey];
				out            = JSON.stringify( obj );
			} catch (e) {
				centrisNotify.error("assignments.Msg.ErrorNotJSONString");
				// Error, but returning an unchanged object
				return pluginList;
			}
			return out;
		},

		// Delete data from pluginList, and pluginList is a stringified PluginData object
		// the function returns the modified pluginList object
		// on error the function returns the same pluginList un modified
		deletePluginData: function deletePluginData(pluginList, dataFromPlugin) {
			var out;
			// get the name of the plugin object
			var pluginKey = Object.keys(dataFromPlugin)[0];
			// update the object which is saved as a styring
			var obj;
			try {
				obj = JSON.parse(pluginList);
				delete obj[pluginKey];
				out = JSON.stringify( obj );
			} catch (e) {
				// Key does not exist so returning an unchanged object.
				return pluginList;
			}
			return out;
		},

		// Gets data from a stringified plugin object (PluginData) and returns it.
		// if pluginKey data does not exist the function returns undefinded
		getPluginData: function getPluginData(pluginList, pluginKey) {
			var obj;
			try {
				obj = JSON.parse(pluginList);
			} catch (e) {
				return undefined;
			}
			return obj[pluginKey];
		},

		// Checks if pluginList contains the key specified int pluginKey
		// returns true if the key was found regardless if it contains data or not
		// returns false if the pluginList is null or if the pluginKey was not found
		// or if pluginList contains a invalid JSON object.
		keyExists: function keyExists(pluginKey, pluginList) {
			var obj;
			if (pluginList === undefined || pluginList === null) {
				return false;
			}

			try {
				obj = JSON.parse(pluginList);
			} catch (e) {
				return false;
			}
			return obj[pluginKey] !== undefined;
		}
	};
});
