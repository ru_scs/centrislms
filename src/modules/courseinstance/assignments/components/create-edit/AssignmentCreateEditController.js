"use strict";

/* This controller handles both creating a new assignment,
 * and editing an existing assignment.
 *
 * NOTE: Took quite some time to find the resource the other used to
 * make the wizard in the /course so here is the resource:
 * https://github.com/simpulton/angularjs-wizard
 */
angular.module("assignmentsApp").controller("AssignmentCreateEditController",
function($rootScope, $scope, $state, $stateParams, AssignmentResource, dateTimeService,
	centrisNotify, pluginFactory) {
	// We are currently using ng-include for the tab pages,
	// and each ng-include creates a separate child scope.
	// This means that objects directly accessible through the scope object
	// are NOT reflected both ways, so we need to create a separate
	// model object on the scope and ensure all variables are a
	// member of that object. To quote Misko Hevery: "there should be a dot there somewhere".
	$scope.model = {
		isInEditMode: false, // Are we creating an assignment or editing an existing one?
		newMilestone: {},
		assessments:  [],
		assessment:   {
			selected: undefined
		},
		allowedFileExtensions: {},
		selectedGroups: {},
		categories: [],
		// TODO: should probably not be hardcoded!
		groupData: {
			allGroups: "true",
			groups: [{
				GroupName: "HÃ³pur 1",
				ID: 1
			}, {
				GroupName: "HÃ³pur 2",
				ID: 2
			}]
		}
	};

	$scope.toggleGroup = function toggleGroup(group) {
		var idx = $scope.model.assignmentwizard.GroupIDs.indexOf(group.ID);

		// Is currently selected
		if (idx > -1) {
			// Remove the selection:
			$scope.model.assignmentwizard.GroupIDs.splice(idx, 1);
		} else {
			// Add to selection:
			$scope.model.assignmentwizard.GroupIDs.push(group.ID);
		}
	};

	// A list of most of the file extensions which have been used
	// over the years.
	$scope.model.fileExtensions = [
		"7z","accdb","avi","bpr","bz2","cc","class","cpp","cs","cxx","doc","docx","dot","dovx","dst","dvi","dwf",
		"dwg","dwgx","dwt","dxf","ear","ees","egg","exe","exl","gif","gz","gzip","h","htm","html","iam","idw","ipn",
		"ipt","jar","java","jpg","kbb","m","mcd","mdb","mpp","msd","odt","pdf","pl","png","ppm","pps","ppt","pptx",
		"ps","pub","py","R","rar","rb","rfa","rpt","rtf","rvt","s8","scm","sml","smu","sol","sql","stu","sws","tar",
		"targz","tex","tgz","tif","txt","uml","vi","vsd","xls","xlsm","xlsx","xlx","xml","zargo","zip","zm2","zuml"];
	// Perhaps we should use a more compact list of the most popular items?
	/*
		$scope.fileExtensions = ["doc", "docx", "xls", "xlsx", "zip", "rar", "pdf"];
	*/

	$scope.nameChanged = false;
	$scope.onChangeAssignmentTitle = function onChangeAssignmentTitle() {
		$scope.nameChanged = true;
	};

	// Called whenever there is a change in the assessment dropdown:
	$scope.onChangeAssessment = function onChangeAssessment(assessment) {

		// We always update the id of the assessment:
		$scope.model.assignmentwizard.AssessmentGroupID = assessment.ID;

		// However, we only change the title of the assignment if the
		// user hasn't edited it already and we are not in edit mode:
		if (!$scope.nameChanged && !$scope.model.isInEditMode) {
			$scope.model.assignmentwizard.Title = assessment.Name;
		}
	};

	$scope.isValidForm = function isValidForm() {
		$scope.model.hasError = false;

		// There must be a title.
		if (!$scope.model.assignmentwizard.Title) {
			$scope.model.hasError = true;
			$scope.wiza.assignmentName.$invalid = true;

			return false;
		}

		// If the assignment is not meant for all groups,
		// but rather for a subset of groups, we ensure
		// that some groups are chosen!
		if ($scope.model.groupData.allGroups === "false") {
			if ($scope.model.assignmentwizard.GroupIDs.length === 0) {
				$scope.model.hasError = true;
				$scope.model.selectedGroups.$invalid = true;
				return false;
			}
		}

		// If students are supposed to hand in some files,
		// we do want to know what types of files are allowed!
		if ($scope.model.assignmentwizard.AllowedNumberOfFiles > 0) {
			if ($scope.model.assignmentwizard.AllowedFileExtensions.length === 0) {
				$scope.model.hasError = true;
				$scope.model.allowedFileExtensions.$invalid = true;
				return false;
			}
		}
		return true;
	};

	// Creating assigment logic, sending all the data to the API.
	$scope.createAssignment = function() {
		var data = angular.copy($scope.model.assignmentwizard);

		if (!$scope.isValidForm()) {
			return;
		}

		// If the assignment is meant for all groups,
		// we don't send any group IDs
		if ($scope.model.groupData.allGroups === "true") {
			$scope.model.assignmentwizard.GroupIDs = [];
		}

		// Merge and date together into one date.
		var mergeBegin  = dateTimeService.mergeDateAndTime(data.DatePublished, data.DatePublishedTime);
		var mergeEnd    = dateTimeService.mergeDateAndTime(data.DateClosed, data.DateClosedTime);

		// Changes to data to adjust to DTO for the WEBAPI:
		data.DatePublished  = dateTimeService.momentToISO(mergeBegin);
		data.DateClosed     = dateTimeService.momentToISO(mergeEnd);
		AssignmentResource.createAssignment($scope.courseInstanceID, data).success(function(data) {
			centrisNotify.success("assignments.Msg.SuccessCreatingAssignment");
			$rootScope.$broadcast("assignmentCreated", data);
			$state.go("courseinstance.assignments");
		}).error(function(data) {
			centrisNotify.error("assignments.Msg.ErrorCreatingAssignment");
		});
	};

	// TODO: combine createAssignment and editAssignment as much as possible!!!
	$scope.editAssignment = function () {
		var data = angular.copy($scope.model.assignmentwizard);

		if (!$scope.isValidForm()) {
			return;
		}

		// Merge and date together into one date.
		var mergeBegin  = dateTimeService.mergeDateAndTime(data.DatePublished, data.DatePublishedTime);
		var mergeEnd    = dateTimeService.mergeDateAndTime(data.DateClosed, data.DateClosedTime);

		data.DatePublished  = dateTimeService.momentToISO(mergeBegin);
		data.DateClosed     = dateTimeService.momentToISO(mergeEnd);

		// If the assignment is meant for all groups,
		// we don't send any group IDs
		if ($scope.model.groupData.allGroups === "true") {
			$scope.model.assignmentwizard.GroupIDs = [];
		}

		AssignmentResource.editAssignment($scope.courseInstanceID, assignmentID, data)
		.success(function (assignment) {
			centrisNotify.success("assignments.Msg.SuccessEditingAssignment");
			$rootScope.$broadcast("assignmentEdited", assignment);
			$state.go("courseinstance.assignments");
		})
		.error(function() {
			centrisNotify.error("assignments.Msg.ErrorEditingAssignment");
		});
	};

	// New Milestone logic.
	$scope.createNewMilestone = function createNewMilestone() {
		$scope.model.assignmentwizard.Milestones.push($scope.model.newMilestone);
		$scope.model.newMilestone = {};
		$scope.summarizeMilestoneWeight();
	};

	$scope.removeMilestone = function removeMilestone(stone) {
		var pos = $scope.model.assignmentwizard.Milestones.indexOf(stone);
		if (pos !== -1) {
			$scope.model.assignmentwizard.Milestones.splice(pos, 1);
			$scope.summarizeMilestoneWeight();
		}
	};

	$scope.editMilestone = function editMilestone(stone) {
		var pos = $scope.model.assignmentwizard.Milestones.indexOf(stone);
		$scope.model.newMilestone = $scope.model.assignmentwizard.Milestones[pos];
		if (pos !== -1) {
			$scope.model.assignmentwizard.Milestones.splice(pos, 1);
			$scope.summarizeMilestoneWeight();
		}
	};

	$scope.summarizeMilestoneWeight = function summarizeMilestoneWeight() {
		var sum = 0.00;
		$scope.model.assignmentwizard.Milestones.forEach(function(item, index, array) {
			// Create the array for the select box
			sum += item.Weight;
		});
		$scope.model.weightSum = sum.toFixed(2);
	};

	// Drop validate, here we can examine if we want to cancel the drop or not
	// but in our case we will always allow drop.
	$scope.dropValidateHandler = function dropValidateHandler($drop, $event, $data, index) {
		return true;
	};

	// Drop start, let's save where we are dropping the element to.
	$scope.onDrop = function onDrop($event, $data, array, index) {
		$scope.model.assignmentwizard.dropTo = index;
	};

	// Drop successful so let's change the milestone array.
	$scope.dropSuccessHandler = function dropSuccessHandler($event, index, array) {
		var newIndex = $scope.model.assignmentwizard.dropTo;
		var oldIndex = index;
		var data = array[oldIndex];
		array.splice(oldIndex, 1);
		array.splice(newIndex, 0, data);
	};

	// Dragging and dropping milestones failed
	$scope.dropFailureHandler = function dropFailureHandler($event, index, array) {
		centrisNotify.error("assignments.Msg.ErrorDragdropMilestone");
	};

	// Available plugins, add new plugins here.
	$scope.getPlugins = function getPlugins() {
		return [{
			Name: "none",
			Index: 0
		}, {
			Name: "TurnItIn",
			Index: 1
		}, {
			Name: "Mooshak",
			Index: 2
		}];
	};

	$scope.getMooshakContests = function getMooshakContests() {
		AssignmentResource.getMooshakContests()
		.success(function(data) {
			data.Names.forEach(function(item, index, array) {
				// Create the array for the select box
				$scope.model.assignmentwizard.MooshakContests.push( {Name: item, Index: index});
			});

			$scope.model.assignmentwizard.MooshakContest = $scope.model.assignmentwizard.MooshakContests[0];
			$scope.model.assignmentwizard.MooshakContestSelected = $scope.model.assignmentwizard.MooshakContest;
		}).error(function(data) {

			if (data !== undefined) {
				centrisNotify.errorWithParam("assignments.Msg.ErrorMooshakGetMooshakContests");
			} else {
				centrisNotify.errorWithParam("assignments.Msg.ErrorMooshakGetMooshakContestsData", data.Message);
			}
		});
	};

	// Gets names of all problems in a given mooshak contest
	// and creates milestones in the assignment for each problem.
	// The information comes from the centris API.
	// The parameter 'contest' is a string representing the name of the mooshak contest.
	$scope.getMooshakProblems = function getMooshakProblems(contest) {
		AssignmentResource.getMooshakProblems(contest)
			.success(function(data) {
				$scope.mooshakProblemsToMilestones(contest, data.Names);
				centrisNotify.success("assignments.Msg.SuccessMooshakProblemsToMilestones");
				$scope.summarizeMilestoneWeight();
				$scope.resetPlugins();
				// todo: select tab3
				// tabs[0].active;
			}).error(function(data) {
				if (data !== undefined) {
					centrisNotify.errorWithParam("assignments.Msg.ErrorMooshakProblemsToMilestones", contest);
				} else {
					centrisNotify.errorWithParam("assignments.Msg.ErrorMooshakProblemsToMilestonesData", {
						contest:contest,
						msg:data.message
					});
				}
			});
	};

	$scope.PluginMooshakInit = function PluginMooshakInit() {
		$scope.PluginMooshakCleanup();
		$scope.getMooshakContests();
	};

	// Clean the scope by removing all variables used by the Mooshak plugin
	// That is Destroy the Mooskak plugin
	$scope.PluginMooshakCleanup = function PluginMooshakCleanup() {
		$scope.model.assignmentwizard.MooshakContests = [];
		$scope.model.assignmentwizard.MooshakContest = null; // What is the best way to clean this?
	};

	// A new plugin has been selected, we will initialize the select plugin
	$scope.selChangePlugin = function selChangePlugin(item) {
		$scope.model.assignmentwizard.PluginSelected = item;
		switch (item.Index) {
			case 2: { // Mooshak
				$scope.PluginMooshakInit();
				break;
			}
		}
	};

	// A new contest has been selected, we will save the selected contest
	$scope.selChangeMooshakContest = function selChangeMooshakContest(item) {
		$scope.model.assignmentwizard.MooshakContestSelected = item;
	};

	// Creates milestones from a string array of problemNames
	// and adds them to the assignment
	$scope.mooshakProblemsToMilestones = function mooshakProblemsToMilestones(contest, problemNames) {
		var stones = [];
		// Divide milestones evenly
		var weight = Math.round( 100 / problemNames.length );
		// If weight was rounded we fix it here to make weights sum to 100
		var firstWeight = 100 - ( weight * (problemNames.length - 1) );
		problemNames.forEach(function(item, index, array) {
			// Create a milestone object
			var obj = {
				Name:        item,
				Weight:      weight,
				Description: "description " + (index + 1),
				PluginData:  JSON.stringify({
					MooshakPlugin: {
						Contest: contest,
						Problem: item
					}
				})
			};
			stones.push( obj );
		});

		if (stones.length > 0) {
			$scope.model.assignmentwizard.PluginData = pluginFactory.addOrEditPluginData(
				$scope.model.assignmentwizard.PluginData,
				$scope.makePluginObjectMooshak()
				);
			stones[0].Weight = firstWeight;
		} else {
			$scope.model.assignmentwizard.PluginData = pluginFactory.deletePluginData(
				$scope.model.assignmentwizard.PluginData,
				"MooshakPlugin"
			);
		}

		stones.forEach(function(stone, index, array) {
			$scope.model.assignmentwizard.Milestones.push(stone);
		});
	};

	// Get all problems from the selected mooshak contest and move them into milestones.
	$scope.getProblemsFromSelectedMooshakContests = function getProblemsFromSelectedMooshakContests() {
		$scope.getMooshakProblems( $scope.model.assignmentwizard.MooshakContestSelected.Name );
	};

	// Every plugin should implement this method
	$scope.makePluginObjectMooshak = function makePluginObjectMooshak() {
		return pluginFactory.makeObject("Mooshak Plugin",
			"A plugin to connect Centris to Mooshak.  Allowing teachers to create and review " +
				"assignments created on Mooshak.");
	};

	// Select the plugin "none" and hide controls on plugin tab
	$scope.resetPlugins = function resetPlugins() {
		if ($scope.model.assignmentwizard === undefined) {
			return;
		}

		$scope.model.assignmentwizard.Plugins = $scope.getPlugins();
		$scope.model.assignmentwizard.Plugin = $scope.model.assignmentwizard.Plugins[0];

		if ($scope.model.assignmentwizard.PluginSelected !== undefined) {
			$scope.model.assignmentwizard.PluginSelected.Index = 0;
		}
	};

	$scope.setEditMode = function() {
		$scope.model.isInEditMode = true;
		$scope.model.assignmentwizard = {};
		$scope.model.assignmentwizard.Categories = [];
		// Load the given assignment:
		AssignmentResource.getAssignmentDetails($scope.courseInstanceID, assignmentID)
		.success(function (data) {
			$scope.model.assignmentwizard = data;
			if ($scope.model.assignmentwizard.Categories) {
				$scope.model.assignmentwizard.Categories.sort();
			}
			// Convert the two date properties to moment objects:
			var pubDateM   = moment(data.DatePublished);
			var closeDateM = moment(data.DateClosed);

			$scope.model.assignmentwizard.DatePublished     = pubDateM.format(dateTimeService.dateFormat());
			$scope.model.assignmentwizard.DatePublishedTime = pubDateM.format("HH:mm");
			$scope.model.assignmentwizard.DateClosed        = closeDateM.format(dateTimeService.dateFormat());
			$scope.model.assignmentwizard.DateClosedTime    = closeDateM.format("HH:mm");

			// Ensure the correct option is selected if the assignment
			// is meant for a particular set of groups:
			if (data.GroupIds.length > 0) {
				$scope.model.groupData.allGroups = "false";
			}

			$scope.summarizeMilestoneWeight();

		}).error(function() {
			centrisNotify.error("assignments.Msg.ErrorLoadingAssignment");
		});
	};

	$scope.setCreateMode = function() {
		$scope.model.isInEditMode = false;

		var startTime = moment().format("HH:MM");
		var endTime   = "23:59";

		// Set model values to reasonable defaults:
		$scope.model.assignmentwizard = {
			AssessmentGroupID:       "",
			DatePublished:           moment().format(dateTimeService.dateFormat()),
			DateClosed:              moment().add(1, "days").endOf("day").format(dateTimeService.dateFormat()),
			DatePublishedTime:       startTime,
			DateClosedTime:          endTime,
			GroupIDs:                [],
			MaxStudentsInGroup:      1,
			AllowedNumberOfFiles:    1,
			AllowedFileExtensions:   ["pdf"],
			Categories:              [],
			Milestones:              [],
			LateHandinDurationType:  1,
			LateHandinDuration:      1,
			LateHandinGradeDecrease: 1,
			PluginData:              JSON.stringify({})
		};

	};

	// Fetch IDs from URL:
	$scope.courseInstanceID = $stateParams.courseInstanceID;
	var assignmentID        = $stateParams.assignmentID;

	if (assignmentID) {
		$scope.setEditMode();
	} else {
		$scope.setCreateMode();
	}
	AssignmentResource.createAssignmentCategory($scope.courseInstanceID)
		.success(function(data) {
			$scope.model.courseCategories = data;
		}).error(function(err) {
			centrisNotify.errorWithParam("assignments.Msg.ErrorNoCategory");
		});
	AssignmentResource.getGroupsInCourse($scope.courseInstanceID).success(function(data) {
		$scope.model.groupData.groups = data;
	});

	// Globally run within this controller
	$scope.resetPlugins();
});
