"use strict";

describe("AssignmentCreateEditController", function() {

	var ctrl, rootscope, scope, state, stateParams, resource, dateTime, notify, plugin;

	beforeEach(module("courseInstanceShared"));
	beforeEach(module("sharedServices"));
	beforeEach(module("assignmentsApp"));

	beforeEach(function() {
		stateParams = {
			courseInstanceID: 1337
		};
	});

	beforeEach(inject(function($rootScope) {
		scope = $rootScope.$new();
		rootscope = $rootScope;
	}));

	beforeEach(inject(function(mockAssignmentResource) {
		resource = mockAssignmentResource;
	}));

	beforeEach(inject(function(mockCentrisNotify) {
		notify = mockCentrisNotify;
	}));

	beforeEach(inject(function(mockPluginFactory) {
		plugin = mockPluginFactory;
	}));

	beforeEach(inject(function(dateTimeService) {
		dateTime = dateTimeService;
	}));

	beforeEach(function() {
		resource.setGroupsInCourseAnswers([{}]);
		resource.setGroupsInCourseConditions([
			undefined
		]);
	});

	describe("initalize", function() {

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentCreateEditController", {
				$rootScope: rootscope,
				$scope: scope,
				$stateParams: stateParams,
				dateTimeService: dateTime,
				AssignmentResource: resource,
				centrisNotify: notify,
				pluginFactory: plugin,
				$state: state
			});
		}));

		it("should set courseInstanceID", function() {
			expect(scope.courseInstanceID).toEqual(1337);
		});

		it("should set model", function() {
			expect(scope.model).toBeDefined();
		});

		it("should set model fileExtensions", function() {
			expect(scope.model.fileExtensions).toBeDefined();
		});

		it("should set nameChanged to false", function() {
			expect(scope.nameChanged).toEqual(false);
		});

	});

	describe("toggleGroup", function() {

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentCreateEditController", {
				$rootScope: rootscope,
				$scope: scope,
				$stateParams: stateParams,
				dateTimeService: dateTime,
				AssignmentResource: resource,
				centrisNotify: notify,
				pluginFactory: plugin,
				$state: state
			});
		}));

		it("should remove group if selected", function() {
			scope.model = {
				assignmentwizard: {
					GroupIDs: [1, 2, 3, 4]
				}
			};
			var group = {
				ID: 3
			};
			scope.toggleGroup(group);
			expect(scope.model.assignmentwizard.GroupIDs).toEqual([1, 2, 4]);
		});

		it("should add group if not selected", function() {
			scope.model = {
				assignmentwizard: {
					GroupIDs: [1, 2, 4]
				}
			};
			var group = {
				ID: 3
			};
			scope.toggleGroup(group);
			expect(scope.model.assignmentwizard.GroupIDs).toEqual([1, 2, 4, 3]);
		});

	});

	describe("inEditMode", function() {

		beforeEach(function() {
			stateParams.assignmentID = 10;
		});

		beforeEach(function() {
			resource.setAssignmentDetailsAnswers([{}]);
			resource.setAssignmentDetailsConditions([undefined]);
			spyOn(resource, "getAssignmentDetails").and.callThrough();
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentCreateEditController", {
				$rootScope: rootscope,
				$scope: scope,
				$stateParams: stateParams,
				dateTimeService: dateTime,
				AssignmentResource: resource,
				centrisNotify: notify,
				pluginFactory: plugin,
				$state: state
			});
		}));

		it("should set isInEditMode to true", function() {
			expect(scope.model.isInEditMode).toEqual(true);
		});

		it("should get assignment from assignment resource", function() {
			expect(resource.getAssignmentDetails).toHaveBeenCalledWith(1337, 10);
		});

	});

	describe("resource success", function() {

		beforeEach(function() {
			stateParams.assignmentID = 10;
		});

		beforeEach(function() {
			resource.setAssignmentDetailsAnswers([{
				DatePublished: new Date(1994, 6, 12, 16, 0, 0),
				DateClosed: new Date(1994, 6, 15, 23, 59, 0),
				GroupIds: [{}, {}, {}],
				Milestones: [{
					Weight: 5
				}, {
					Weight: 10
				}, {
					Weight: 8
				}]
			}]);
			resource.setAssignmentDetailsConditions([true]);
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentCreateEditController", {
				$rootScope: rootscope,
				$scope: scope,
				$stateParams: stateParams,
				dateTimeService: dateTime,
				AssignmentResource: resource,
				centrisNotify: notify,
				pluginFactory: plugin,
				$state: state
			});
		}));

		it("should update weightSum of assignments", function() {
			expect(scope.model.weightSum).toEqual("23.00");
		});

		it("should set DatePublished", function() {
			expect(scope.model.assignmentwizard.DatePublished).toEqual("12. July 1994");
		});

		it("should set DatePublishedTime", function() {
			expect(scope.model.assignmentwizard.DatePublishedTime).toEqual("16:00");
		});

		it("should set DateClosed", function() {
			expect(scope.model.assignmentwizard.DateClosed).toEqual("15. July 1994");
		});

		it("should set DateClosedTime", function() {
			expect(scope.model.assignmentwizard.DateClosedTime).toEqual("23:59");
		});

		it("should set allGroups to false", function() {
			expect(scope.model.groupData.allGroups).toEqual("false");
		});

	});

	describe("resource success", function() {

		beforeEach(function() {
			stateParams.assignmentID = 10;
		});

		beforeEach(function() {
			resource.setAssignmentDetailsAnswers([{
				DatePublished: new Date(1994, 6, 12, 16, 0, 0),
				DateClosed: new Date(1994, 6, 15, 23, 59, 0),
				GroupIds: [],
				Milestones: [{
					Weight: 5
				}, {
					Weight: 10
				}, {
					Weight: 8
				}]
			}]);
			resource.setAssignmentDetailsConditions([true]);
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentCreateEditController", {
				$rootScope: rootscope,
				$scope: scope,
				$stateParams: stateParams,
				dateTimeService: dateTime,
				AssignmentResource: resource,
				centrisNotify: notify,
				pluginFactory: plugin,
				$state: state
			});
		}));

		it("should not modify scope model groupData allGroups", function() {
			expect(scope.model.groupData.allGroups).toEqual("true");
		});

	});

	describe("resource failure", function() {

		beforeEach(function() {
			stateParams.assignmentID = 10;
		});

		beforeEach(function() {
			resource.setAssignmentDetailsAnswers([{}]);
			resource.setAssignmentDetailsConditions([false]);
		});

		beforeEach(inject(function($controller) {
			spyOn(notify, "error");
			ctrl = $controller("AssignmentCreateEditController", {
				$rootScope: rootscope,
				$scope: scope,
				$stateParams: stateParams,
				dateTimeService: dateTime,
				AssignmentResource: resource,
				centrisNotify: notify,
				pluginFactory: plugin,
				$state: state
			});
		}));

		it("should notify user of error", function() {
			expect(notify.error).toHaveBeenCalledWith("assignments.Msg.ErrorLoadingAssignment");
		});

	});

	describe("scope", function() {

		beforeEach(function() {
			resource.setAssignmentDetailsAnswers([{}]);
			resource.setAssignmentDetailsConditions([undefined]);
		});

		beforeEach(inject(function($controller) {
			ctrl = $controller("AssignmentCreateEditController", {
				$rootScope: rootscope,
				$scope: scope,
				$stateParams: stateParams,
				dateTimeService: dateTime,
				AssignmentResource: resource,
				centrisNotify: notify,
				pluginFactory: plugin,
				$state: state
			});
		}));

		describe("onChangeAssignmentTitle", function() {

			it("should set nameChanged to true", function() {
				scope.onChangeAssignmentTitle();
				expect(scope.nameChanged).toEqual(true);
			});

		});

		describe("onChangeAssessment", function() {

			it("should set assignmentwizard AssessmentGroupID", function() {
				var assessment = {
					ID: 146
				};
				scope.onChangeAssessment(assessment);
				expect(scope.model.assignmentwizard.AssessmentGroupID).toEqual(146);
			});

			it("should change name if name hasn't been changed", function() {
				scope.nameChanged = false;
				var assessment = {
					ID: 146,
					Name: "Assessment title"
				};
				scope.onChangeAssessment(assessment);
				expect(scope.model.assignmentwizard.Title).toEqual("Assessment title");
			});

			it("should not change name if name hasn been changed", function() {
				scope.nameChanged = true;
				scope.model.assignmentwizard.Title = "Original Assessment Title";
				var assessment = {
					ID: 146,
					Name: "Assessment title"
				};
				scope.onChangeAssessment(assessment);
				expect(scope.model.assignmentwizard.Title).toEqual("Original Assessment Title");
			});

		});

		describe("isValidForm", function() {

			describe("No title", function() {

				beforeEach(function() {
					scope.model.assignmentwizard.Title = undefined;
					scope.wiza = {
						assignmentName: {

						}
					};
				});

				it("should return false", function() {
					expect(scope.isValidForm()).toEqual(false);
				});

				it("should set set model hasError", function() {
					scope.isValidForm();
					expect(scope.model.hasError).toEqual(true);
				});

				it("should set set wiza assignment name invalid", function() {
					scope.isValidForm();
					expect(scope.wiza.assignmentName.$invalid).toEqual(true);
				});

			});

			describe("Not all groups", function() {

				beforeEach(function() {
					scope.model.assignmentwizard.Title = "Assignment Title";
					scope.model.groupData.allGroups = "false";
				});

				it("should return false", function() {
					expect(scope.isValidForm()).toEqual(false);
				});

				it("should be valid if there are groups selected", function() {
					scope.model.assignmentwizard.GroupIDs = [{}, {}];
					scope.isValidForm();
					expect(scope.model.hasError).toEqual(false);
				});

			});

			describe("Handin Files True", function() {

				beforeEach(function() {
					scope.model.assignmentwizard.Title = "Assignment Title";
					scope.model.groupData.allGroups = "true";
					scope.model.assignmentwizard.AllowedNumberOfFiles = 1;
				});

				it("should return false if allowedfileextensions are not selected", function() {
					scope.model.assignmentwizard.AllowedFileExtensions = [];
					expect(scope.isValidForm()).toEqual(false);
				});

				it("should set model hasError if there are no allowedfileextensions selected", function() {
					scope.model.assignmentwizard.AllowedFileExtensions = [];
					scope.isValidForm();
					expect(scope.model.hasError).toEqual(true);
				});

				it("should set model allowedFileExtensions invalid if there are no allowedfileextensions selected",
					function() {
						scope.model.assignmentwizard.AllowedFileExtensions = [];
						scope.isValidForm();
						expect(scope.model.allowedFileExtensions.$invalid).toEqual(true);
					});

				it("should not set model hasError if allowedFileExtensions are selected", function() {
					scope.model.assignmentwizard.AllowedFileExtensions = ["zip"];
					scope.isValidForm();
					expect(scope.model.hasError).toEqual(false);
				});

			});

			describe("Valid Form", function() {

				beforeEach(function() {
					scope.model.assignmentwizard.Title = "Assignment Title";
					scope.model.groupData.allGroups = "true";
					scope.model.assignmentwizard.AllowedNumberOfFiles = 0;
				});

				it("should return true", function() {
					expect(scope.isValidForm()).toEqual(true);
				});

			});

		});

		describe("PluginMooshakInit", function() {

			beforeEach(function() {
				spyOn(scope, "PluginMooshakCleanup");
				spyOn(scope, "getMooshakContests");
			});

			it("should call plugin mooshak cleanup", function() {
				scope.PluginMooshakInit();
				expect(scope.PluginMooshakCleanup).toHaveBeenCalled();
			});

			it("should call getMooshakContests", function() {
				scope.PluginMooshakInit();
				expect(scope.getMooshakContests).toHaveBeenCalled();
			});

		});

		describe("PluginMooshakCleanup", function() {

			beforeEach(function() {
				scope.model = {
					assignmentwizard: {

					}
				};
			});

			it("should clean mooshakcontests", function() {
				scope.PluginMooshakCleanup();
				expect(scope.model.assignmentwizard.MooshakContests).toEqual([]);
			});

			it("should clean mooshakcontest", function() {
				scope.PluginMooshakCleanup();
				expect(scope.model.assignmentwizard.MooshakContest).toEqual(null);
			});

		});

		describe("createNewMilestone", function() {

			var milestone;
			var newmilestone;

			beforeEach(function() {
				milestone = {};
				newmilestone = {};
				scope.model = {
					assignmentwizard: {
						Milestones: [milestone]
					}
				};
				spyOn(scope, "summarizeMilestoneWeight");
				scope.model.newMilestone = newmilestone;
			});

			it("should add milestone to milestones", function() {
				scope.createNewMilestone();
				expect(scope.model.assignmentwizard.Milestones).toEqual([
					milestone,
					newmilestone
				]);
			});

			it("should call summarizeMilestoneWeight", function() {
				scope.createNewMilestone();
				expect(scope.summarizeMilestoneWeight).toHaveBeenCalled();
			});

		});

		describe("selChangePlugin", function() {

			var item;

			beforeEach(function() {
				item = {
					Index: 1
				};
				scope.model = {
					assignmentwizard: {
						PluginSelected: null
					}
				};
				spyOn(scope, "PluginMooshakInit");
			});

			it("should set PluginSelected to item", function() {
				scope.selChangePlugin(item);
				expect(scope.model.assignmentwizard.PluginSelected).toBe(item);
			});

			it("should not call PluginMooshakInit if index is not 2", function() {
				scope.selChangePlugin(item);
				expect(scope.PluginMooshakInit).not.toHaveBeenCalled();
			});

			it("should call PluginMooshakInit if index is 2", function() {
				item = {
					Index: 2
				};
				scope.selChangePlugin(item);
				expect(scope.PluginMooshakInit).toHaveBeenCalled();
			});

		});

	});

});
