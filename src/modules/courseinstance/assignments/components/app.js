"use strict";

angular.module("assignmentsApp", [
	"ui.bootstrap",
	"ui.utils",
	"ui.router",
	"ui",
	"sharedServices",
	"ui.directives",
	"ngFileUpload",
	"ui.select",
	"ui.bootstrap.tooltip",
	"ngSanitize",
	"courseInstanceShared"]).config(function ($stateProvider) {
	$stateProvider.state("courseinstance.assignments", {
		url:         "/assignments",
		templateUrl: "courseinstance/assignments/components/index/index.tpl.html",
		controller:  "AssignmentListController"
	});
	$stateProvider.state("courseinstance.assignments.new", {
		url:         "/new",
		templateUrl: "courseinstance/assignments/components/create-edit/assignment-create.tpl.html",
		controller:  "AssignmentCreateEditController"
	});
	$stateProvider.state("courseinstance.assignments.edit", {
		url:         "/:assignmentID/edit",
		templateUrl: "courseinstance/assignments/components/create-edit/assignment-edit.tpl.html",
		controller:  "AssignmentCreateEditController"
	});
	$stateProvider.state("courseinstance.assignments.grading", {
		url:         "/:assignmentID/grading/:handinID",
		templateUrl: "courseinstance/assignments/components/grading/grading.tpl.html",
		controller:  "AssignmentGradingController"
	});
	$stateProvider.state("courseinstance.assignments.studentgrading", {
		url:         "/:assignmentID/grading/student/:ssn",
		templateUrl: "courseinstance/assignments/components/grading/grading.tpl.html",
		controller:  "AssignmentGradingController"
	});
	$stateProvider.state("courseinstance.assignments.details", {
		url:         "/:assignmentID",
		templateUrl: "courseinstance/assignments/components/details/assignment-details.html",
		controller:  "AssignmentDetailsController"
	});
	$stateProvider.state("courseinstance.assignments.details.description", {
		url:         "/description"
	});
	$stateProvider.state("courseinstance.assignments.details.discussions", {
		url:         "/discussions"
	});
	$stateProvider.state("courseinstance.assignments.details.group", {
		url:         "/group"
	});
	$stateProvider.state("courseinstance.assignments.details.handin", {
		url:         "/handin"
	});
	$stateProvider.state("courseinstance.assignments.details.grade", {
		url:         "/grade"
	});
	$stateProvider.state("courseinstance.assignments.details.grading", {
		url:         "/gradinglist"
	});
	$stateProvider.state("courseinstance.assignments.details.statistics", {
		url:         "/statistics"
	});
});
