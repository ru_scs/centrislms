"use strict";

angular.module("evaluationsApp", [
	"pascalprecht.translate",
	"ui.router",
	"sharedServices"
]).config(function ($stateProvider) {
	$stateProvider.state("evaluations", {
		url:         "/evaluations",
		templateUrl: "evaluations/components/index/index.html",
		controller:  "EvaluationsController"
	});
	$stateProvider.state("studentEval",{
		url:         "/evaluations/:courseInstanceId/:evalId",
		templateUrl: "evaluations/components/studenteval/student-eval.html",
		controller:  "StudentEvalController"
	});
	$stateProvider.state("teacherEval",{
		url:         "/evaluationresults/:evalId",
		templateUrl: "evaluations/components/teachereval/teacher-eval.html",
		controller:  "TeacherEvalController"
	});
});
