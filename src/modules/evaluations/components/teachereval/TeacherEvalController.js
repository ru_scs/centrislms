"use strict";

angular.module("evaluationsApp").controller("TeacherEvalController",
function TeacherEvalController($scope, $filter, $sce, $state, $translate, TeacherEvalResources,
	centrisNotify, $stateParams, $location) {
	var evalId = $stateParams.evalId;
	var teacherList;
	$scope.TeachList = teacherList;

	TeacherEvalResources.getMyEvaluation(evalId)
		.then(function(response) {
			TeacherEvalResources.getTeachers(response.data.Course.ID)
			.then(function(response) {
				$scope.TeachList = response.data;
			}, function(response) {
				centrisNotify.error("eval.Error.getTeacherInfo");
			});
			$scope.evaluation = response.data;
		}, function(response) {
			centrisNotify.error("eval.Error.GetEvaluation");
		});

	$scope.calculateAverage = function (question) {
		if (question.length !== 0) {
			var i = 0;
			var count = 0;
			var totalGrade = 0;
			for (i = 0; i < question.length; ++i) {
				totalGrade += question[i].Count * question[i].Weight;
				count += question[i].Count;
			}
			var result = rounding(totalGrade / count, 2);
			if (isNaN(result)) {
				return 0;
			}
			return result;
		}
		return 0;
	};

	$scope.numberOfAnswers = function (question) {
		if (question.length !== 0) {
			var i = 0;
			var count = 0;
			for (i = 0; i < question.length; ++i) {
				count+= question[i].Count;
			}
			return count;
		}
		return 0;
	};

	var rounding = function(number, decimals) {
		var _round = Math.round;
		if (arguments.length === 1) {
			return _round(number);
		}

		var multiplier = Math.pow(10, decimals);
		return _round(number * multiplier) / multiplier;
	};

	$scope.getTeacherName = function (teacherSSN) {
		if (teacherSSN !== undefined && $scope.TeachList !== undefined) {
			var name = "";
			var i = 0;
			for (i = 0; i < $scope.TeachList.length; ++i) {
				if ($scope.TeachList[i].SSN === teacherSSN) {
					name = $scope.TeachList[i].FullName;
				}
			}
			return name;
		}
	};
	// Help Desk
	$scope.$on("onCentrisIntro", function(event, args) {
		if ($state.current.name === "teacherEval") {
			$scope.ShowTeacherEvalIntro();
		}
	});
	$scope.IntroOptions = {
		steps: [{
			element: "teachereval",
			position: "left",
			intro: ""
		}, {
			element: ".txtQ",
			position: "right",
			intro: ""
		}, {
			element: ".multiQuestion",
			position: "right",
			intro: ""
		}, {
			element: ".tablerows",
			position: "right",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};

	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];
	var introKeys = [
		"AngTeacherEvalutation.QuestionCat",
		"AngTeacherEvalutation.TextInfo",
		"AngTeacherEvalutation.MultiInfo",
		"AngTeacherEvalutation.AverageInfo",
	];
	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
		$scope.IntroOptions.steps[1].intro = translations[introKeys[1]];
		$scope.IntroOptions.steps[2].intro = translations[introKeys[2]];
		$scope.IntroOptions.steps[3].intro = translations[introKeys[3]];
	});
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.goBack = function() {
		$location.path("/evaluations");
	};
});
