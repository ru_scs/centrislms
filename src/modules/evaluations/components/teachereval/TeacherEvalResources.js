"use strict";

angular.module("evaluationsApp").factory("TeacherEvalResources",
function TeacherEvalResources(CentrisResource) {
	return {
		getMyEvaluation: function getMyEvaluation(id) {
			var param = { id: id };
			return CentrisResource.get("evaluations", "teacherevaluation/:id", param);
		},
		getTeachers: function(courseId) {
			var params = { courseInstanceID: courseId};
			return CentrisResource.get("courses", ":courseInstanceID/teachers", params);
		},
		getMockEvaluationResults: function getMockEvaluationResults() {
			var results = [{
				QuestionID: 123,
				QuestionType: 0,
				Question: "Ertu almennt ánægð(ur) eða óánægð(ur) með námskeiðið/kennsluna það sem af er?",
				QuestionEN: "Generally, are you happy or unhappy with the course/teaching approach so far?",
				Answers: {
					val0: 14,
					val1: 29,
					val2: 22,
					val3: 2,
					val4: 7
				}
			}, {
				QuestionID: 124,
				QuestionType: 1,
				Question: "Annað",
				QuestionEN: "Any comments",
				Answers: {
					val0: "Tala hærra",
					val1: "Þægilegur hraði á yfirferð, hef ekkert út á neitt að setja",
					val2: "How about some lectures in English??",
					val3: "Hvað er þetta in praxís sem kennarinn er alltaf að segja?",
					val4: "Fleiri glærur plz",
					val5: "Pirrandi þegar kennarinn er alltaf að ruglast og segir úbbs",
					val6: "Tímarnir byrja of snemma, hvað um 12?",
					val7: "Fleiri pizza hádegi"
				}
			}, {
				QuestionID: 125,
				QuestionType: 0,
				Question: "Er gaman?",
				QuestionEN: "Are you having fun?",
				Answers: {
					val0: 14,
					val1: 29,
					val2: 22,
					val3: 2,
					val4: 7
				}
			}, {
				QuestionID: 126,
				QuestionType: 0,
				Question: "Er kennarinn skemmtilegur?",
				QuestionEN: "Is the teacher fun?",
				Answers: {
					val0: 29,
					val1: 18,
					val2: 8,
					val3: 2,
					val4: 3
				}
			}, {
				QuestionID: 124,
				QuestionType: 1,
				Question: "Skemmtilegur dagur með kennaranum",
				QuestionEN: "Describe a fun day with the teacher",
				Answers: {
					val0: "Talking about css and favorite hex colors",
					val1: "Kannski skoða twitter og hafa gaman",
					val2: "Ræða heimsmálin",
					val3: "Bara kannski forrita saman",
					val4: "Kíkja á barinn jeee"
				}
			}];

			return results;
		}
	};
});
