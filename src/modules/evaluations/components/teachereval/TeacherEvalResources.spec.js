"use strict";

describe("TeacherEvalResources", function() {

	var factory, centris;

	beforeEach(module("evaluationsApp", "sharedServices"));
	beforeEach(module("evaluationsApp", function($provide) {
		centris = {};
		centris.get = jasmine.createSpy();
		$provide.value("CentrisResource", centris);
	}));

	beforeEach(inject(function(TeacherEvalResources) {
		factory = TeacherEvalResources;
		spyOn(factory, "getMyEvaluation").and.callThrough();
	}));

	it("getMyEvaluation for defined", function() {
		factory.getMyEvaluation(20162);
		expect(centris.get).toHaveBeenCalledWith("evaluations", "teacherevaluation/:id", {
			id: 20162
		});
	});

});
