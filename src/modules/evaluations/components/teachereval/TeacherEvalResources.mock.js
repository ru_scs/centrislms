"use strict";

angular.module("mockTeacherEvalApp", []).factory("mockTeacherEvalResource",
	function AllEvalutationResource() {
		var index = 0;
		var fakeanswers;
		var fakeconditions;
		var fakeQuestions;
		return {
			getMyEvaluation: function(evalID) {
				return {
					then: function (fn, fnErr) {
						fn({
							data: {
								Course: {
									ID: 1
								}
							}
						});
					}
				};
			},
			getTeachers: function() {
				return {
					then: function(fn, fnErr) {
						fn({
							data: []
						});
					}
				};
			},
			setAnswers: function setAnswers(answers) {
				fakeanswers = answers;
			},
			setQuestions: function setQuestions(questions) {
				fakeQuestions = questions;
			},
			appendAnswers: function appendAnswers(answers) {
				fakeanswers = fakeanswers.concat(answers);
			},
			appendQuestions: function appendQuestions(questions) {
				fakeQuestions = fakeQuestions.concat(questions);
			},
			getEval: function getEval() {
				var question = fakeQuestions[index];
				var answer = fakeanswers[index];
				index++;
				return {
					success: function(fn) {
						if (question === undefined) {
							return {
								error: function(fn) {
									return;
								}
							};
						}
						if (question) {
							fn(answer);
						}
						return {
							error: function(fn) {
								if (!question) {
									fn(answer);
								}
							}
						};
					}
				};
			}
		};
	});