"use strict";

describe("TeacherEvalController", function() {

	var scope, ctrl, filter, state, translate, resource, notify;

	beforeEach(module("evaluationsApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockTeacherEvalApp"));

	beforeEach(inject(function($filter) {
		filter = $filter;
	}));

	beforeEach(inject(function($rootScope) {
		scope = $rootScope.$new();
	}));

	beforeEach(function() {
		state = {
			current: {
				name: "current_state"
			}
		};
	});

	beforeEach(inject(function(mockTeacherEvalResource) {
		resource = mockTeacherEvalResource;
	}));

	beforeEach(inject(function(mockCentrisNotify) {
		notify = mockCentrisNotify;
	}));

	beforeEach(inject(function(mockTranslate) {
		translate = mockTranslate.mockTranslate;
	}));

	describe("Initalize", function() {
		beforeEach(function () {
			resource.setAnswers({});
			resource.setQuestions([
				undefined
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			ctrl = $controller("TeacherEvalController", {
				$scope: scope,
				$state: state,
				$translate: translate,
				TeacherEvalResources: resource,
				cenrisNotify: notify
			});
		}));

		it("should initalize eval", function () {
			expect(scope.evaluation).toBeDefined();
		});
	});

});
