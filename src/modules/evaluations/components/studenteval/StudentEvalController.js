"use strict";

/**
 * This controller takes care of displaying a single evaluation form
 * which a student can answer.
 */
angular.module("evaluationsApp").controller("StudentEvalController",
function StudentEvalController($scope, $state, StudentEvalResources, centrisNotify,
 $stateParams, $location, $translate) {
	var evalId                 = $stateParams.evalId;
	var courseInstanceId       = $stateParams.courseInstanceId;
	$scope.isTeachersCollapsed = true;
	$scope.isCourseCollapsed   = false;

	// Load the given evaluation:
	$scope.loadingData = 1;
	StudentEvalResources.getEvalutationById(courseInstanceId, evalId).success(function(data) {
		$scope.evaluation  = data;
		$scope.loadingData--;

		// When both the evaluation and the list of teachers
		// has been fetched, we can build our list of reply
		// objects:
		if ($scope.loadingData === 0) {
			buildReturnObjects();
		}
	}).error(function() {
		$scope.loadingData--;
		centrisNotify.error("studentEval.Messages.ErrorLoadingEvaluation");
	});

	// Also, load the list of course teachers:
	$scope.loadingData++;
	StudentEvalResources.getTeachersForCourse(courseInstanceId).success(function(data, status, headers, config) {
		$scope.courseTeachers = data;
		$scope.loadingData--;

		// If there is only a single teacher, we keep him expanded in the UI:
		if ($scope.courseTeachers.length === 1) {
			$scope.isTeachersCollapsed = false;
		}

		if ($scope.loadingData === 0) {
			buildReturnObjects();
		}
	}).error(function(data, status, headers, config) {
		centrisNotify.error("studentEval.Messages.ErrorLoadingTeachers");
		$scope.loadingData--;
	});

	// This function builds the array of objects we will send to the server
	// when the student submits the evaluation.
	function buildReturnObjects() {
		$scope.courseResults = [];
		var i = 0;
		var q;
		var courseQ;

		for (i = 0; i < $scope.evaluation.CourseQuestions.length; ++i) {
			q = $scope.evaluation.CourseQuestions[i];
			courseQ = {
				QuestionID: q.ID,
				TeacherSSN: "",
				Value:      "" // Empty, since the student hasn't answered anything!
			};

			$scope.courseResults.push(courseQ);
		}

		$scope.teacherResults = {};
		for (i = 0; i < $scope.courseTeachers.length; ++i) {
			var teacher = $scope.courseTeachers[i];
			var arr = [];
			for (var j = 0; j < $scope.evaluation.TeacherQuestions.length; ++j) {
				q = $scope.evaluation.TeacherQuestions[j];
				courseQ = {
					QuestionID: q.ID,
					TeacherSSN: teacher.SSN,
					Value:      "" // Empty, since the student hasn't answered anything!
				};

				arr.push(courseQ);
			}
			$scope.teacherResults[teacher.SSN] = arr;
		}
	}

	$scope.cancelEvaluation = function() {
		$location.path("/evaluations");
	};

	$scope.saveEvaluation = function() {
		// The API expects a flat array, but we need another structure
		// for the UI, therefore we need to convert before we save:
		var model = [];
		var i     = 0;

		for (i = 0; i < $scope.courseResults.length; ++i) {
			model.push($scope.courseResults[i]);
		}

		for (i = 0; i < $scope.courseTeachers.length; ++i) {
			var arr = $scope.teacherResults[$scope.courseTeachers[i].SSN];
			for (var j = 0; j < arr.length; ++j) {
				var ans = arr[j];
				// Only submit those answers where the student
				// actually submitted anything:
				if (ans.Value) {
					model.push(ans);
				}
			}
		}

		StudentEvalResources.addEvaluationFromStudent(courseInstanceId, evalId, model)
		.success(function(data) {
			centrisNotify.success("studentEval.Messages.SuccessSavingEvaluation");
			// It is plausible that the student would want to
			// go back to the overview page, for instance if (s)he
			// has more evaluations to complete.
			$location.path("/evaluations");
		}).error(function() {
			centrisNotify.success("studentEval.Messages.ErrorSavingEvaluation");
		});
	};
	// Helpdesk
	$scope.$on("onCentrisIntro", function(event, args) {
		if ($state.current.name === "studentEval") {
			$scope.ShowStudentEvalIntro();
		}
	});
	$scope.IntroOptions = {
		steps: [{
			element: "#textInput",
			position: "left",
			intro: ""
		}, {
			element: "#singleInput",
			position: "left",
			intro: ""
		}, {
			element: "#multipleInput",
			position: "left",
			intro: ""
		}, {
			element: "#saveButton",
			position: "left",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];
	var introKeys = [
		"AngStudentEvalutation.TextInput",
		"AngStudentEvalutation.SingleInput",
		"AngStudentEvalutation.MultipleInput",
		"AngStudentEvalutation.SaveButton",
	];
	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
		$scope.IntroOptions.steps[1].intro = translations[introKeys[1]];
		$scope.IntroOptions.steps[2].intro = translations[introKeys[2]];
		$scope.IntroOptions.steps[3].intro = translations[introKeys[3]];
	});
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});
});
