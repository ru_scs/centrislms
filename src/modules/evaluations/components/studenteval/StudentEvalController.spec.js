"use strict";

describe("For student to answer an evaluation", function() {

	var scope, ctrl, filter, state, translate, resource, notify;

	beforeEach(module("evaluationsApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockAllAssignmentsApp"));

	beforeEach(inject(function($filter) {
		filter = $filter;
	}));

	beforeEach(function() {
		state = {
			current: {
				name: "current_state"
			}
		};
	});

});
