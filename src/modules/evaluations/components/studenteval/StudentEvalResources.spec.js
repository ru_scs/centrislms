"use strict";

describe("StudentEvalResources", function() {

	var factory, centris;

	beforeEach(module("evaluationsApp", "sharedServices"));
	beforeEach(module("evaluationsApp", function($provide) {
		centris = {};
		centris.get = jasmine.createSpy();
		centris.post = jasmine.createSpy();
		$provide.value("CentrisResource", centris);
	}));

	beforeEach(inject(function(StudentEvalResources) {
		factory = StudentEvalResources;
		spyOn(factory, "getEvalutationById").and.callThrough();
		spyOn(factory, "getTeachersForCourse").and.callThrough();
		spyOn(factory, "addEvaluationFromStudent").and.callThrough();
	}));

	it("getEvalutationById for defined", function() {
		factory.getEvalutationById(5,111);
		expect(centris.get).toHaveBeenCalledWith("courses", ":courseInstanceID/evaluations/:evaluationId", {
			courseInstanceID: 5,
			evaluationId: 111
		});
	});

	it("getTeachersForCourse for defined", function() {
		factory.getTeachersForCourse(5);
		expect(centris.get).toHaveBeenCalledWith("courses", ":courseInstanceId/teachers", {
			courseInstanceId: 5,
		});
	});

	it("addEvaluationFromStudent for defined", function() {
		factory.addEvaluationFromStudent(5,111,"model");
		expect(centris.post).toHaveBeenCalledWith("courses", ":courseInstanceID/evaluations/:evaluationId", {
			courseInstanceID: 5,
			evaluationId: 111
		}, "model");
	});

});
