"use strict";

angular.module("evaluationsApp").factory("StudentEvalResources",
function StudentEvalResources(CentrisResource) {
	return {
		getEvalutationById: function getEvalutationById(courseInstanceId, id) {
			var param = {
				courseInstanceID: courseInstanceId,
				evaluationId: id
			};
			return CentrisResource.get("courses", ":courseInstanceID/evaluations/:evaluationId", param);
		},

		getTeachersForCourse: function getTeachersForCourse(courseInstanceId) {
			var param = {
				courseInstanceId: courseInstanceId
			};
			return CentrisResource.get("courses", ":courseInstanceId/teachers", param);
		},

		addEvaluationFromStudent: function addEvaluationFromStudent(courseInstanceId, id, model) {
			var param = {
				courseInstanceID: courseInstanceId,
				evaluationId: id
			};
			return CentrisResource.post("courses", ":courseInstanceID/evaluations/:evaluationId", param, model);
		}
	};
});
