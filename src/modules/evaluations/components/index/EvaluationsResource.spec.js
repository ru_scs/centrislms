"use strict";

describe("EvaluationResource", function() {

	var factory, centris;

	beforeEach(module("evaluationsApp", "sharedServices"));
	beforeEach(module("evaluationsApp", function($provide) {
		centris = {};
		centris.get = jasmine.createSpy();
		$provide.value("CentrisResource", centris);
	}));

	beforeEach(inject(function(EvaluationResource) {
		factory = EvaluationResource;
		spyOn(factory, "getCourses").and.callThrough();
		spyOn(factory, "getMyEvaluations").and.callThrough();
		spyOn(factory, "getCurrentSemester").and.callThrough();
	}));

	it("getCourses for defined", function() {
		factory.getCourses();
		expect(centris.get).toHaveBeenCalledWith("my", "courses");
	});

	it("getMyEvaluations for defined", function() {
		factory.getMyEvaluations("20132");
		expect(centris.get).toHaveBeenCalledWith("my", "evaluations/?semester=:semester", {
			semester: "20132",
		});
	});

	it("getCurrentSemester for defined", function() {
		factory.getCurrentSemester();
		expect(centris.get).toHaveBeenCalledWith("semesters/current");
	});

});
