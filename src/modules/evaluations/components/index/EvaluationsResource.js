"use strict";

angular.module("evaluationsApp").factory("EvaluationResource",
function EvaluationResource(CentrisResource) {
	return {
		getCourses: function getCourses() {
			return CentrisResource.get("my", "courses");
		},
		getMyEvaluations: function getMyEvaluations(semester) {
			if (semester) {
				return CentrisResource.get("my", "evaluations/?semester=:semester", {semester: semester});
			} else {
				return CentrisResource.get("my", "evaluations");
			}
		},
		getCurrentSemester: function getCurrentSemester() {
			return CentrisResource.get("semesters/current");
		}
	};
});
