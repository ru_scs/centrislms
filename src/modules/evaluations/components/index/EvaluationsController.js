"use strict";

/**
 * This controller takes care of displaying all open evaluations which
 * the current user should answer as a student, and a link to all
 * evaluations results which apply to the current user as a teacher.
 */
angular.module("evaluationsApp").controller("EvaluationsController",
function EvaluationsController($scope, $state, EvaluationResource,
	centrisNotify, $location, $translate) {

	var semester = EvaluationResource.getCurrentSemester();
	if (semester) {
		$scope.semester = semester.ID;
	}

	$scope.filterSemester = function filterSemester(semester) {
		$location.search("semester", $scope.semester);
		getEvaluations($scope.semester);
	};

	var defaultSemester = $location.search().semester;

	if (defaultSemester === undefined) {
		$scope.semester = defaultSemester;
	}

	$scope.loadingData = true;
	EvaluationResource.getMyEvaluations($scope.semester).success(function(data, status, headers, config) {
		$scope.loadingData = false;
		$scope.studeval    = data.StudentEvaluationList;
		$scope.teacheval   = data.TeacherEvaluationList;
		console.log($scope.teacheval);
	}).error(function(data, status, headers, config) {
		$scope.loadingData = false;
		centrisNotify.error("evaluation.Messages.ErrorReceivingEvaluations");
	});

	var getEvaluations = function getEvaluations(semester) {
		$scope.loadingData = true;
		EvaluationResource.getMyEvaluations(semester).success(function(data, status, headers, config) {
			$scope.loadingData = false;
			$scope.studeval    = data.StudentEvaluationList;
			$scope.teacheval   = data.TeacherEvaluationList;
		}).error(function(data, status, headers, config) {
			$scope.loadingData = false;
			centrisNotify.error("evaluation.Messages.ErrorReceivingEvaluations");
		});
	};

	// This event is fired everytime the URL (including the query string)
	// changes. In order to ensure the back button works correctly, we must
	// reload our list accordingly.
	$scope.$on("$locationChangeSuccess", function(event, newUrl, oldUrl) {
		var defaultSemester = $location.search().semester;
		if (defaultSemester === undefined) {
			$scope.semester = defaultSemester;
		}
		getEvaluations($scope.semester);
	});
	// Help Desk
	$scope.$on("onCentrisIntro", function(event, args) {
		if ($state.current.name === "evaluations") {
			if ($scope.studeval.length === 0) {
				$scope.ShowTeacherEvalIntro();
			} else {
				$scope.ShowStudentEvalIntro();
			}
		}
	});
	$scope.IntroOptionsStudent = {
		steps: [{
			element: "#courseColumn2",
			position: "left",
			intro: ""
		}, {
			element: "#evalColumn2",
			position: "left",
			intro: ""
		}, {
			element: "#endedColumn2",
			position: "left",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};
	$scope.IntroOptionsTeacher = {
		steps: [
		{
			element: "#semesterDropdown",
			position: "left",
			intro: ""
		}, {
			element: "#courseColumn",
			position: "left",
			intro: ""
		}, {
			element: "#evalColumn",
			position: "left",
			intro: ""
		}, {
			element: "#endedColumn",
			position: "left",
			intro: ""
		}],
		showStepNumbers: false,
		exitOnOverlayClick: true,
		exitOnEsc: true,
	};

	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];
	var introKeys = [
		"AngEvalutation.Dropdown",
		"AngEvalutation.NameOfCourse",
		"AngEvalutation.NameOfEval",
		"AngEvalutation.DateofEval",
	];
	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsTeacher.steps[0].intro = translations[introKeys[0]];
		$scope.IntroOptionsTeacher.steps[1].intro = translations[introKeys[1]];
		$scope.IntroOptionsTeacher.steps[2].intro = translations[introKeys[2]];
		$scope.IntroOptionsTeacher.steps[3].intro = translations[introKeys[3]];
		$scope.IntroOptionsStudent.steps[0].intro = translations[introKeys[1]];
		$scope.IntroOptionsStudent.steps[1].intro = translations[introKeys[2]];
		$scope.IntroOptionsStudent.steps[2].intro = translations[introKeys[3]];
	});
	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptionsTeacher.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsTeacher.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsTeacher.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsTeacher.doneLabel = translations[introButtons[3]];
		$scope.IntroOptionsStudent.nextLabel = translations[introButtons[0]];
		$scope.IntroOptionsStudent.prevLabel = translations[introButtons[1]];
		$scope.IntroOptionsStudent.skipLabel = translations[introButtons[2]];
		$scope.IntroOptionsStudent.doneLabel = translations[introButtons[3]];
	});
});
