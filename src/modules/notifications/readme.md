# Notifications module

This module handles displaying all notifications for a user, as well as handling
the notifications menu at the top of the website.