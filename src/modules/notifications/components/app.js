"use strict";

angular.module("notificationApp", ["pascalprecht.translate", "ui.router", "SignalR"])
.config(function ($stateProvider) {
	$stateProvider.state("notifications", {
		url:         "/notifications",
		templateUrl: "notifications/components/list/all-notifications.html",
		controller:  "NotificationController"
	});
});