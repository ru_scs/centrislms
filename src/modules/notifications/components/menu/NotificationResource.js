"use strict";

angular.module("notificationApp").factory("NotificationResource",
function NotificationResource(CentrisResource) {
	return {
		getNotifications: function (page, size) {

			var query = "";
			if (page) {
				query = "?pageNumber=" + page;
				if (size) {
					query += "&pageSize=" + size;
				}
			}
			return CentrisResource.get(
				"my",
				"notifications" + query
			);
		},
		putReadNotification: function (notificationID) {
			return CentrisResource.put(
				"notifications",
				":notificationid",
				{ notificationid: notificationID }
			);
		},
		postNotification: function (type, subtype, operation, group, id, item) {
			var model = {
				NotificationGroup: {
					Group: group,
					Id: id
				},
				Body: {
					Type: type,
					SubType: subtype,
					Operation: operation,
					Body: {
						NotificationGroup: group,
						Item: item
					}
				}
			};

			return CentrisResource.post("/notifications", "", {}, model);
		}
	};
});
