"use strict";

// Notification controller that takes care of notifications.
// Now it only makes a get request when user logs in and shows all notifications in the
// notification drop down list
angular.module("notificationApp").controller("NotificationController",
function ($scope, NotificationResource, AuthenticationService, ENV, $templateCache) {

	$scope.notifications = [];
	$scope.count         = 0;

	// Variables for pagination:
	$scope.itemsPerPage = 20;
	$scope.currentPage  = 1;
	$scope.totalItems   = 0;
	$scope.pageCount    = 1;
	$scope.pageChanged  = function pageChanged() {
		$scope.getAllNotifications();
	};

	function notifySound() {
		// If there are multiple notifications coming in at the same time,
		// we only play a sound when the last one arrives (i.e. when at
		// least one second has passed...)
		// _.debounce(function () {
		// var playNotify = document.getElementById("notificationAudio");
		// playNotify.play();
		// }, 1000);
	}

	function connectToSignalR () {
		var sigr = $.hubConnection(ENV.signalREndpoint, { useDefaultPath: false });

		// sigr.logging = true;
		sigr.error(function (error) {
			console.log("API.SignalR -- " + error);
		});

		var notificationHub = sigr.createHubProxy("notificationHub");

		notificationHub.on("pushNotification", function (notification) {
			console.log("Received a notification from signalr server!!!!!");

			// Since this event happens outside of the Angular world,
			// we need to tell Angular all about it!
			$scope.$apply(function() {
				// Note: we assume this new notification should
				// come first, being the latest one!
				$scope.notifications.unshift(notification);
				$scope.count++;
				notifySound();
			});
		});

		sigr.start(function () {
			// This is currently the best way we"ve found which guarantees that
			// the SignalR server knows who the user is. Experiments have been
			// made using cookies, to no avail, and apparently it is not
			// trivial to attach HTTP header values to the communication.
			// However, it would be preferable if some other method could be found.
			notificationHub.invoke("authenticate", AuthenticationService.getToken());
		});
	}

	// Get all notifications for user logged in
	$scope.getAllNotifications = function getAllNotifications() {
		NotificationResource.getNotifications($scope.currentPage,
			$scope.itemsPerPage).success(function (data) {
			$scope.count = 0;
			// Note: data is an evelope, data.Data gives us the actual list
			$scope.notifications = data.Data;
			for (var i = 0; i < data.Data.length; i++) {
				if (!data.Data[i].Read) {
					$scope.count++;
				}
			}

			$scope.totalItems = data.Paging.TotalRecords;
			$scope.pageCount  = data.Paging.PageCount;
		}).error(function() {
			// TODO: toastr messages?
		});
	};

	// Call function on load to load all notifications,
	// but only if we know who the user is!
	if (AuthenticationService.isLoggedIn()) {
		$scope.getAllNotifications();
		connectToSignalR();
	}

	$scope.$on("loginSuccess", function() {
		$scope.getAllNotifications();
		connectToSignalR();
	});

	// When user click a notification it will become read
	$scope.putReadNotification = function putReadNotification(notification) {
		if (!notification.Read) {
			NotificationResource.putReadNotification(notification.ID).success(function (data) {
				notification.Read = true;
				$scope.count -= 1;
			}).error(function () {
				console.log("Error reading notification");
			});
		}
	};

	// Mark all notification user has as read
	$scope.markAllAsRead = function() {
		for (var i = 0; i < $scope.notifications.length; i++) {
			$scope.putReadNotification($scope.notifications[i]);
		}
	};

	// Return a template for given notification.
	// On creating new template the name HAS to match the type of the notification!
	$scope.getNotificationView = function getNotificationView(notification) {
		// Our strategy is as follows:
		// try to get a matching template from the cache. If it doesn"t exist,
		// an error should be thrown, we catch it, and fall back to a
		// default template in that case.
		try {
			var filePath = "notifications/components/templates/" + notification.Type + ".html";
			var result = $templateCache.get(filePath);
			return filePath;
		} catch (err) {
			// An error probably means that the template was not found in the
			// template cache. In that case, we use a default template.
			return "notifications/components/templates/Default.html";
		}
	};

	$scope.getDiscussionLink = function getDiscussionLink(n) {
		switch (n.Body.ResourceType) {
			case "topic": {
				return "#/courses/" + n.Body.CourseInstanceID + "/topics/" + n.Body.ResourceID;
			}
			case "assignment": {
				return "#/courses/" + n.Body.CourseInstanceID + "/assignments/" + n.Body.ResourceID;
			}
			case "assignmentgroup": {
				// TODO: this is not correct!
				return "#/courses/" + n.Body.CourseInstanceID + "/assignments/" + n.Body.ResourceID;
			}
			case "announcement": {
				return "#/courses/" + n.Body.CourseInstanceID + "/announcements/" + n.Body.ResourceID;
			}
			default: {
				// If other board types will be created,
				// they should be added here!
				return "";
			}
		}
	};
});
