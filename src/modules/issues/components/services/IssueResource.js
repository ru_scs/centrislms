"use strict";

angular.module("issuesApp").factory("IssuesResource",
function IssuesResource($http, ENV) {
	var url = ENV.issuesapiEndpoint;

	return {
		getIssues: function (params) {
			return $http.get(url + "issues/", {params: params});
		},
		getIssueById: function (id) {
			return $http.get(url + "issues/" + id);
		},
		updateIssue: function (id, issue) {
			return $http.put(url + "issues/" + id, issue);
		},
		postMessageToIssue: function (id, msg) {
			return $http.post(url + "issues/" + id + "/messages", msg);
		},
		postIssue: function (issue) {
			return $http.post(url + "issues/", issue);
		},
		getTypes: function () {
			return $http.get(url + "types");
		}
	};
});
