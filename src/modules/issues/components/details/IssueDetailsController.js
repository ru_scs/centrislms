"use strict";

angular.module("issuesApp").controller("IssueDetailsController",
function ($scope, $location, $stateParams, $translate, IssuesResource, UserService) {
	$scope.lang = $translate.use(); // en or is
	$scope.states = ["rejected", "accepted"];
	$scope.loading = true;

	// Validation rules for the message body.
	// @type {{minlen: number, maxlen: number}}
	$scope.rules = {
		minlen: 5,
		maxlen: 200
	};

	// Different panel style for each issue state.
	$scope.stateClass = {
		accepted:  "panel-success",
		rejected:  "panel-danger",
		inprocess: "panel-primary",
		received:  "panel-primary",
		created:   "panel-default"
	};

	$scope.user = {
		name: UserService.getFullName(),
		ssn:  UserService.getUserSSN()
	};

	$scope.issue = {};
	$scope.newMessage = "";

	var issueID = $stateParams.issueID;

	// Load single issue:
	IssuesResource.getIssueById(issueID).success(function (issue) {
		$scope.issue = issue;

		// Get the name of the type:
		IssuesResource.getTypes().success(function (types) {
			$scope.loading = false;

			var typeObj = types.filter(function (x) {
				return x["name"] === issue.typeName;
			})[0];

			if (typeObj) {
				if ($scope.lang === "en") {
					issue.typeName = typeObj.descriptionEN;
				} else if ($scope.lang === "is") {
					issue.typeName = typeObj.descriptionIS;
				}
			}
		}).
		error(function (err) {
			console.log("err:", err);
		});
	})
	.error(function (err) {
		console.log("err:", err);
	});

	// To list of Issues
	$scope.back = function(path) {
		$location.path(path);
	};

	$scope.postMessage = function () {
		var msg = {
			authorSSN:  $scope.user.ssn,
			authorName: $scope.user.name,
			message:    $scope.newMessage
		};

		IssuesResource.postMessageToIssue(issueID, msg).success(function (res) {
			// Add msg to message list:
			msg.createDate = new Date();
			$scope.issue.messages.push(msg);
			$scope.newMessage = "";
		}).
		error(function (err) {
			console.log(err);
		});
	};
});
