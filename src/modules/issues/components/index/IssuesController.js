"use strict";

angular.module("issuesApp").controller("IssuesController",
function IssuesController($scope, $location, $uibModal, $translate, IssuesResource, UserService) {
	$scope.lang = $translate.use(); // en or is
	$scope.issues = [];
	$scope.loading = true;

	var params = {
		ssn: UserService.getUserSSN()
	};

	// Load issue list
	IssuesResource.getIssues(params).success(function (issues) {
		$scope.issues = issues;

		// Get the name of the type.
		IssuesResource.getTypes().success(function (types) {
			$scope.loading = false;
			issues.forEach(function (i) {
				var typeObj = types.filter(function (x) {
					return x["name"] === i.typeName;
				})[0];

				if (typeObj) {
					if ($scope.lang === "en") {
						i.typeName = typeObj.descriptionEN;
					} else if ($scope.lang === "is") {
						i.typeName = typeObj.descriptionIS;
					}
				}
			});
		}).error(function (err) {
			$location.path("/issues");
		});
	}).error(function (data) {
		$location.path("/issues");
	});

	// Redirect to the clicked issue.
	$scope.viewIssue = function (id) {
		$location.path("/issues/" + id);
	};

	// Modal
	$scope.open = function () {
		$uibModal.open({
			templateUrl: "issues/components/submit/submit.html",
			controller: "SubmitController"
		});
	};
});
