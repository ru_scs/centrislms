"use strict";

angular.module("issuesApp", ["pascalprecht.translate", "ui.router", "ui.bootstrap"])
	.config(function ($stateProvider) {
		$stateProvider
		.state("issues", {
			url:         "/issues",
			templateUrl: "issues/components/index/issues.html",
			controller:  "IssuesController"
		}).
		state("issue", {
			url:         "/issues/:issueID",
			templateUrl: "issues/components/details/issue.tpl.html",
			controller:  "IssueDetailsController"
		});
	});
