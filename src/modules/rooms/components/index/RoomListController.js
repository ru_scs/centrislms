"use strict";

angular.module("roomsApp").controller("RoomListController",
function ($scope, RoomResource, $state, $translate) {
	$scope.fetchingData = true;
	RoomResource.getAllRooms().success(function (allRooms) {
		$scope.fetchingData = false;
		$scope.allRooms = allRooms;
	}).error(function() {
		// Notify!
		$scope.fetchingData = false;
	});
	// INTRO otions
	$scope.IntroOptions = {
		steps: [{
			element: "#aboutroomlist",
			position: "left",
			intro: ""
		}],
		showStepNumbers:    false,
		exitOnOverlayClick: true,
		exitOnEsc:          true,
	};

	var introKeys = [
		"AngIntroRoomList.AboutRoomList"
	];

	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
	});

	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.$on("onCentrisIntro", function(event, args) {
		// Only show this if the current route is for our page:
		if ($state.current.name === "rooms") {
			$scope.ShowRoomListIntro();
		}
	});
});
