"use strict";

/**
 * -----------------------------------------------------
 * Got this from SMS, same file (Shared in the future ?)
 * -----------------------------------------------------
 *  RoomResource
 *  Handles requests to server
 */
angular.module("roomsApp").factory("RoomResource",
function RoomResource(CentrisResource, centrisNotify) {
	return {
		/**
		 * Get all rooms
		 * @param none
		 * @returns Promise object
		 */
		getAllRooms: function() {
			return CentrisResource.get("rooms");
		},
		/**
		 * Get information about room with roomId
		 * @param roomId
		 * @returns Promise object
		 */
		getRoom: function(roomId) {
			return CentrisResource.get("rooms", ":roomId", {roomId: roomId});
		},

		getRoomTypes: function () {
			return CentrisResource.get("rooms", "types");
		},

		/**
		 * Get a list of rooms which satisfy a given search condition,
		 * i.e. have a minimum number of seats
		 * @param dateRange An array of dates which the room(s) must
		 *                  be available on.
		 * @returns Promise object
		 */
		getRooms: function(dateRange, seatCount) {
			if (seatCount === 0 && dateRange.length === 0) {
				return CentrisResource.get("rooms", "", {});
			} else {
				return CentrisResource.get("rooms", "", {ranges: dateRange, minseatcount: seatCount});
			}
		},

		/**
		 * Get bookings on room with roomId and dateRange
		 * @param roomId
		 * @param formattedDateRange
		 * @returns Promise object
		 */
		getBookings: function(roomId, startDate, endDate) {
			var firstDate  = startDate.format("YYYY-MM-DDTHH:mm:ss");
			var secondDate = endDate.format("YYYY-MM-DDTHH:mm:ss");
			var range      = firstDate + "," + secondDate;

			return CentrisResource.get("rooms", ":roomId/bookings", {roomId: roomId, ranges: range});
		},

		updateRoom: function (roomDetails) {
			CentrisResource.put("rooms", ":roomId", { roomId: roomDetails.ID }, roomDetails )
			.success(function (room) {
				centrisNotify.successWithParam("rooms.Msg.RoomUpdated", room.Name);
			}).error(function (data) {
				centrisNotify.error("rooms.Msg.RoomNotUpdated");
			});
		}
	};
});
