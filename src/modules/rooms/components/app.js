"use strict";

angular.module("roomsApp", [
	"pascalprecht.translate",
	"ui.router",
	"sharedServices"
]).config(function ($stateProvider) {
	$stateProvider
	.state("rooms", {
		url:         "/rooms",
		templateUrl: "rooms/components/index/index.html",
		controller:  "RoomListController"
	})
	.state("roomdetails", {
		url:         "/rooms/:roomId",
		templateUrl: "rooms/components/details/room-detail.tpl.html",
		controller:  "RoomDetailController"
	})
	.state("roomdetails.map", {
		url:         "/map"
	})
	.state("roomdetails.info", {
		url:         "/info"
	});
});
