"use strict";

angular.module("roomsApp").controller("RoomDetailController",
function ($scope, RoomResource, $stateParams, $state, centrisBookingDlg, formatEventsForCalendar) {

	$scope.roomId         = $stateParams.roomId;
	$scope.firstDayOfWeek = moment().startOf("week");
	$scope.lastDayOfWeek  = moment().endOf("week");
	$scope.events         = [];

	// Get room from server
	RoomResource.getRoom($scope.roomId).success(function (room) {
		$scope.room = room;
	});

	$scope.onViewChanged = function onViewChanged(start, end) {
		$scope.firstDayOfWeek = moment(start);
		$scope.lastDayOfWeek  = moment(end);
		loadSchedule();
	};

	var loadSchedule = function loadSchedule() {
		var promise = RoomResource.getBookings($scope.roomId, $scope.firstDayOfWeek, $scope.lastDayOfWeek);
		promise.success(function(events) {
			$scope.events = formatEventsForCalendar.formatEvents(events);
			// Notify the calendar about the new events:
			$scope.$broadcast("eventsChanged", $scope.events);
		});
	};

	loadSchedule();

	// Listening after state change event
	$scope.$on("$stateChangeSuccess", function stateChangeSuccess() {
		$scope.tabs.list.forEach(function forEachTab(tab) {
			tab.active = $state.is(tab.route);
		});
		$scope.isCalendarTabVisible = $scope.tabs.list[0].active;
	});

	// Tab section
	// Note that the following can be found in other controllers
	// which have tabs. We need to find out a way to remove this
	// duplication.
	$scope.tabs = {
		list: [
			{ route: "roomdetails",      active: true  },
			{ route: "roomdetails.map",  active: false },
			{ route: "roomdetails.info", active: false }],

		go: function go(route) {
			$state.go(route);
		},

		active: function active(route) {
			return $state.is(route);
		}
	};
});
