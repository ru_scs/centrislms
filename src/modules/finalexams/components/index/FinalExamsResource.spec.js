'use strict';

describe('FinalExamsResource', function() {

	var factory, centris;

	beforeEach(module('finalexamsApp', 'sharedServices'));
	beforeEach(module('finalexamsApp', function($provide) {
		centris = {};
		centris.get = jasmine.createSpy();
		$provide.value('CentrisResource', centris);
	}));

	beforeEach(inject(function(FinalExamsResource) {
		factory = FinalExamsResource;
		spyOn(factory, 'getExams').and.callThrough();
	}));

	it('getExams should get current semester', function() {
		factory.getExams();
		expect(centris.get).toHaveBeenCalledWith('semesters', ':semesterID/finalexamschedule', {semesterID: 'current'});
	});

});
