"use strict";

angular.module("mockFinalexamsApp", []).factory("mockFinalExamsResource",
	function AllAssignmentsResource() {
		var index = 0;
		var fakeanswers;
		var fakeconditions;
		return {
			setAnswers: function setAnswers(answers) {
				fakeanswers = answers;
			},
			setConditions: function setConditions(conditions) {
				fakeconditions = conditions;
			},
			appendAnswers: function appendAnswers(answers) {
				fakeanswers = fakeanswers.concat(answers);
			},
			appendConditions: function appendConditions(conditions) {
				fakeconditions = fakeconditions.concat(conditions);
			},
			getExams: function getExams() {
				var condition = fakeconditions[index];
				var answer = fakeanswers[index];
				index++;
				return {
					success: function(fn) {
						if (condition === undefined) {
							return {
								error: function(fn) {
									return;
								}
							};
						}
						if (condition) {
							fn(answer);
						}
						return {
							error: function(fn) {
								if (!condition) {
									fn(answer);
								}
							}
						};
					}
				};
			}
		};
	});
