"use strict";
angular.module("finalexamsApp").controller("FinalExamsController",
	function FinalExamsController($scope, $state, $translate, FinalExamsResource, centrisNotify) {

		$scope.exams = {
			userExams: [],
			allExams: []
		};
		$scope.activeTab = "";

		$scope.time = Date.now();

		$scope.daysUntilExam = function daysUntilExam(exam) {
			var examtime = new Date(exam.DateScheduled).getTime();
			return Math.floor((examtime - $scope.time) / (24 * 3600 * 1000));
		};

		// Creates "from - to" string for exam time
		$scope.getExamTime = function getExamTime(exam) {
			var startTime = moment(exam.DateScheduled).format("HH:mm");
			var endTime = moment(exam.DateScheduled).add(exam.Length, "minutes").format("HH:mm");
			exam.ExamTime = startTime + " - " + endTime;
		};

		// Separates the room with most exam seats from the roomlist so it can
		// be displayed by default
		$scope.getMainRoom = function getMainRoom(exam) {
			if (exam.Rooms.length === 0) {
				return;
			} else if (exam.Rooms.length === 1) {
				exam.MainRoom = exam.Rooms[0];
				exam.Rooms = [];
				return;
			} else {
				var mainRoom = exam.Rooms[0];
				var index = 0;
				for (var i = 1; i < exam.Rooms.length; i++) {
					if (mainRoom.SeatsUsed < exam.Rooms[i].SeatsUsed) {
						mainRoom = exam.Rooms[i];
						index = i;
					}
				}
				exam.Rooms.splice(index, 1);
				exam.MainRoom = mainRoom;
			}
		};

		// Used for displaying loading message
		$scope.fetchData = function() {
			$scope.loading = true;
			FinalExamsResource.getExams().success(function(exams) {
				$scope.exams.userExams = exams.UserExams;
				$scope.exams.allExams = exams.AllExams;

				$scope.loading = false;

				// Set the start and end times
				angular.forEach($scope.exams.userExams, function(exam) {
					$scope.getExamTime(exam);
					$scope.getMainRoom(exam);
					exam.showAllRooms = false;
				});
				angular.forEach($scope.exams.allExams, function(exam) {
					$scope.getExamTime(exam);
					$scope.getMainRoom(exam);
					exam.showAllRooms = false;
				});
			}).error(function(exams) {
				centrisNotify.error("finalexams.Messages.Server_Error");
				$scope.loading = false;
			});
		};

		$scope.fetchData();

		// Tab section
		// Note that the following can be found in other controllers
		// which have tabs. We need to find out a way to remove this
		// duplication.
		$scope.go = function(route) {
			$state.go(route);
		};

		$scope.active = function(route) {
			return $state.is(route);
		};

		$scope.tabs = [{
			route: "finalexams.userexams",
			active: true,
			tag: "userExams"
		}, {
			route: "finalexams.allexams",
			active: false,
			tag: "allExams"
		}];

		$scope.onStateChange = function() {
			$scope.tabs.forEach(function(tab) {
				tab.active = $scope.active(tab.route);
				if (tab.active) {
					$scope.activeTab = tab.tag;
				}
			});
		};

		$scope.$on("$stateChangeSuccess", $scope.onStateChange);

		// Starting helper viewer
		// Defines the steps and calls the step id
		$scope.MyUserIntroOptions = {
			steps: [{
				element: "#content",
				position: "right",
				intro: ""
			}, {
				element: "#finalexamsuser-helper-step2",
				position: "right",
				intro: ""
			}, {
				element: "#finalexamsuser-helper-step3",
				position: "right",
				intro: ""
			}],
			showStepNumbers: false,
			exitOnOverlayClick: true,
			exitOnEsc: true,
		};

		$scope.MyAllIntroOptions = {
			steps: [{
				element: "#content",
				position: "right",
				intro: ""
			}, {
				element: "#finalexamsuser-helper-step2",
				position: "right",
				intro: ""
			}, {
				element: "#finalexamsall-helper-step3",
				position: "right",
				intro: ""
			}],
			showStepNumbers: false,
			exitOnOverlayClick: true,
			exitOnEsc: true,
		};

		// Calling the translate files for each step and putting them into an array
		var MyExamAllHelper = [
			"MyExamAllHelper.Step1",
			"MyExamAllHelper.Step2",
			"MyExamAllHelper.Step3",
			"MyExamAllHelper.Step4"
		];

		var MyExamUserHelper = [
			"MyExamUserHelper.Step1",
			"MyExamUserHelper.Step2",
			"MyExamUserHelper.Step3",
			"MyExamUserHelper.Step4"
		];

		// Setting the translates as the text for each step
		$translate(MyExamAllHelper).then(function whenDoneTranslating(translations) {
			$scope.MyAllIntroOptions.steps[0].intro = translations[MyExamAllHelper[0]];
			$scope.MyAllIntroOptions.steps[1].intro = translations[MyExamAllHelper[1]];
			$scope.MyAllIntroOptions.steps[2].intro = translations[MyExamAllHelper[2]];
		});

		$translate(MyExamUserHelper).then(function whenDoneTranslating(translations) {
			$scope.MyUserIntroOptions.steps[0].intro = translations[MyExamUserHelper[0]];
			$scope.MyUserIntroOptions.steps[1].intro = translations[MyExamUserHelper[1]];
			$scope.MyUserIntroOptions.steps[2].intro = translations[MyExamUserHelper[2]];
		});

		// Calling the translate files for each button and putting them into an array
		var introButtons = [
			"AngularIntro.Next",
			"AngularIntro.Prev",
			"AngularIntro.Skip",
			"AngularIntro.Done"
		];

		// Setting the translates as the text for each button
		$translate(introButtons).then(function whenDoneTranslating(translations) {
			$scope.MyUserIntroOptions.nextLabel = translations[introButtons[0]];
			$scope.MyUserIntroOptions.prevLabel = translations[introButtons[1]];
			$scope.MyUserIntroOptions.skipLabel = translations[introButtons[2]];
			$scope.MyUserIntroOptions.doneLabel = translations[introButtons[3]];
		});
		$translate(introButtons).then(function whenDoneTranslating(translations) {
			$scope.MyAllIntroOptions.nextLabel = translations[introButtons[0]];
			$scope.MyAllIntroOptions.prevLabel = translations[introButtons[1]];
			$scope.MyAllIntroOptions.skipLabel = translations[introButtons[2]];
			$scope.MyAllIntroOptions.doneLabel = translations[introButtons[3]];
		});

		$scope.onCentrisIntro = function(event, args) {
			// Only show this if the current route is for our page:
			if ($state.current.name === "finalexams.userexams") {
				$scope.ShowMyUserExamsIntro();
			}
			if ($state.current.name === "finalexams.allexams") {
				$scope.ShowMyAllExamsIntro();
			}

		};

		$scope.$on("onCentrisIntro", $scope.onCentrisIntro);

	});
