"use strict";
angular.module("finalexamsApp")
.factory("FinalExamsResource", function (CentrisResource) {

	return {
		getExams: function getExams() {
			return CentrisResource.get("semesters", ":semesterID/finalexamschedule", {semesterID: "current"});
		}
	};
});
