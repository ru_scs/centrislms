'use strict';

describe('FinalExamsController', function() {

	var ctrl, scope, state, translate, resource, notify;

	beforeEach(module('finalexamsApp'));
	beforeEach(module('sharedServices'));
	beforeEach(module('mockFinalexamsApp'));

	beforeEach(inject(function($rootScope) {
		scope = $rootScope.$new();
	}));

	beforeEach(function() {
		state = {

		};
	});

	beforeEach(function() {
		translate = function mockTranslating(keys) {
			var translations = [];
			for (var i = 0; i < keys.length; i++) {
				translations[keys[i]] = keys[i];
			}
			return {
				then: function mockTranslatingThen(fn) {
					fn(translations);
				}
			};
		};
	});

	beforeEach(inject(function(mockFinalExamsResource) {
		resource = mockFinalExamsResource;
	}));

	beforeEach(inject(function(mockCentrisNotify) {
		notify = mockCentrisNotify;
	}));

	describe('Initalize', function() {

		beforeEach(function() {
			resource.setAnswers({});
			resource.setConditions([
				undefined
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			ctrl = $controller('FinalExamsController', {
				$scope: scope,
				$state: state,
				$translate: translate,
				FinalExamsResource: resource,
				centrisNotify: notify
			});
		}));

		it('should initalize exams', function() {
			expect(scope.exams.userExams).toBeDefined();
			expect(scope.exams.allExams).toBeDefined();
		});

		it('should set loading', function() {
			expect(scope.loading).toEqual(true);
		});

	});

	describe('GetExamTime', function() {

		beforeEach(function() {
			resource.setAnswers({});
			resource.setConditions([
				undefined
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			ctrl = $controller('FinalExamsController', {
				$scope: scope,
				$state: state,
				$translate: translate,
				FinalExamsResource: resource,
				centrisNotify: notify
			});
		}));

		it('should set ExamTime', function() {
			var exam = {
				DateScheduled: '2016-04-01T09:00:00',
				Length: 180
			};
			scope.getExamTime(exam);
			expect(exam.ExamTime).toEqual('09:00 - 12:00');
		});

	});

	describe('GetMainRoom', function() {

		beforeEach(function() {
			resource.setAnswers({});
			resource.setConditions([
				undefined
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			ctrl = $controller('FinalExamsController', {
				$scope: scope,
				$state: state,
				$translate: translate,
				FinalExamsResource: resource,
				centrisNotify: notify
			});
		}));

		it('should make no changes for 0 rooms', function() {
			var exams = {
				Rooms: []
			};
			scope.getMainRoom(exams);
			expect(exams.MainRoom).not.toBeDefined();
		});

		it('should work for a single item', function() {
			var exams = {
				Rooms: [{
					name: 'room'
				}]
			};
			scope.getMainRoom(exams);
			expect(exams.MainRoom.name).toEqual('room');
			expect(exams.Rooms).toEqual([]);
		});

		it('should pick the room with the highest SeatsUsed', function() {
			var exams = {
				Rooms: [{
					SeatsUsed: 3
				}, {
					SeatsUsed: 5
				}, {
					name: 'RoomWith10',
					SeatsUsed: 10
				}, {
					SeatsUsed: 1
				}]
			};
			scope.getMainRoom(exams);
			expect(exams.MainRoom.name).toEqual('RoomWith10');
			expect(exams.Rooms).toEqual([{
				SeatsUsed: 3
			}, {
				SeatsUsed: 5
			}, {
				SeatsUsed: 1
			}]);
		});

	});

	describe('Resources success', function() {

		beforeEach(function() {
			resource.setAnswers([{}]);
			resource.setConditions([
				undefined
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			ctrl = $controller('FinalExamsController', {
				$scope: scope,
				$state: state,
				$translate: translate,
				FinalExamsResource: resource,
				centrisNotify: notify
			});
		}));

		beforeEach(function() {
			resource.appendAnswers([{
				UserExams: [{
					exam: 'user1'
				}, {
					exam: 'user2'
				}, {
					exam: 'user3'
				}],
				AllExams: [{
					exam: 'all1'
				}, {
					exam: 'all2'
				}, {
					exam: 'all3'
				}]
			}]);
			resource.appendConditions([
				true
			]);
		});

		beforeEach(function() {
			spyOn(scope, 'getExamTime');
			spyOn(scope, 'getMainRoom');
		});

		it('should set loading to false', function() {
			scope.fetchData();
			expect(scope.loading).toEqual(false);
		});

		it('should call getExamTime for all UserExams', function() {
			scope.fetchData();
			expect(scope.getExamTime).toHaveBeenCalledWith({
				exam: 'user1',
				showAllRooms: false
			});
			expect(scope.getExamTime).toHaveBeenCalledWith({
				exam: 'user2',
				showAllRooms: false
			});
			expect(scope.getExamTime).toHaveBeenCalledWith({
				exam: 'user3',
				showAllRooms: false
			});
		});

		it('should call getExamTime for all AllExams', function() {
			scope.fetchData();
			expect(scope.getExamTime).toHaveBeenCalledWith({
				exam: 'all1',
				showAllRooms: false
			});
			expect(scope.getExamTime).toHaveBeenCalledWith({
				exam: 'all2',
				showAllRooms: false
			});
			expect(scope.getExamTime).toHaveBeenCalledWith({
				exam: 'all3',
				showAllRooms: false
			});
		});

		it('should call getMainRoom for all UserExams', function() {
			scope.fetchData();
			expect(scope.getMainRoom).toHaveBeenCalledWith({
				exam: 'user1',
				showAllRooms: false
			});
			expect(scope.getMainRoom).toHaveBeenCalledWith({
				exam: 'user2',
				showAllRooms: false
			});
			expect(scope.getMainRoom).toHaveBeenCalledWith({
				exam: 'user3',
				showAllRooms: false
			});
		});

		it('should call getExamTime for all AllExams', function() {
			scope.fetchData();
			expect(scope.getMainRoom).toHaveBeenCalledWith({
				exam: 'all1',
				showAllRooms: false
			});
			expect(scope.getMainRoom).toHaveBeenCalledWith({
				exam: 'all2',
				showAllRooms: false
			});
			expect(scope.getMainRoom).toHaveBeenCalledWith({
				exam: 'all3',
				showAllRooms: false
			});
		});

	});

	describe('Resources failure', function() {

		beforeEach(function() {
			resource.setAnswers([{}]);
			resource.setConditions([
				false
			]);
		});

		beforeEach(function() {
			spyOn(notify, 'error');
		});

		beforeEach(inject(function($rootScope, $controller) {
			ctrl = $controller('FinalExamsController', {
				$scope: scope,
				$state: state,
				$translate: translate,
				FinalExamsResource: resource,
				centrisNotify: notify
			});
		}));

		it('should call centrisNotify error', function() {
			expect(notify.error).toHaveBeenCalledWith('finalexams.Messages.Server_Error');
		});

		it('should set loading to false', function() {
			expect(scope.loading).toEqual(false);
		});

	});

	describe('scope go', function() {

		beforeEach(function() {
			resource.setAnswers({});
			resource.setConditions([
				undefined
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			ctrl = $controller('FinalExamsController', {
				$scope: scope,
				$state: state,
				$translate: translate,
				FinalExamsResource: resource,
				centrisNotify: notify
			});
		}));

		beforeEach(function() {
			state.go = jasmine.createSpy();
		});

		it('should call state go', function() {
			scope.go('value');
			expect(state.go).toHaveBeenCalledWith('value');
		});

	});

	describe('scope active', function() {

		beforeEach(function() {
			resource.setAnswers({});
			resource.setConditions([
				undefined
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			ctrl = $controller('FinalExamsController', {
				$scope: scope,
				$state: state,
				$translate: translate,
				FinalExamsResource: resource,
				centrisNotify: notify
			});
		}));

		beforeEach(function() {
			state.is = jasmine.createSpy();
		});

		it('should call state is', function() {
			scope.active('value');
			expect(state.is).toHaveBeenCalledWith('value');
		});

	});

	describe('scope on stateChangeSuccess', function() {

		beforeEach(function() {
			resource.setAnswers({});
			resource.setConditions([
				undefined
			]);
		});

		beforeEach(function() {
			scope.$on = function(name, fn) {
				if (name === '$stateChangeSuccess') {
					fn();
				}
			};
			state.is = function(route) {
				return route === 'finalexams.userexams';
			};
		});

		beforeEach(inject(function($rootScope, $controller) {
			ctrl = $controller('FinalExamsController', {
				$scope: scope,
				$state: state,
				$translate: translate,
				FinalExamsResource: resource,
				centrisNotify: notify
			});
		}));

		it('should set initalize activeTab', function() {
			expect(scope.activeTab).toBeDefined();
		});

		it('should set other tabs to inactive', function() {
			scope.tabs = [{
				route: 'finalexams.allexams1',
				active: true,
				tag: '1'
			}, {
				route: 'finalexams.userexams',
				active: true,
				tag: '2'
			}, {
				route: 'finalexams.allexams2',
				active: true,
				tag: '3'
			}];
			scope.onStateChange();
			expect(scope.tabs[0].active).toEqual(false);
			expect(scope.tabs[1].active).toEqual(true);
			expect(scope.tabs[2].active).toEqual(false);
		});

	});

	describe('onCentrisIntro', function() {

		beforeEach(function() {
			resource.setAnswers({});
			resource.setConditions([
				undefined
			]);
		});

		beforeEach(function() {
			scope.$on = jasmine.createSpy();
			scope.ShowMyUserExamsIntro = jasmine.createSpy();
			scope.ShowMyAllExamsIntro = jasmine.createSpy();
		});

		beforeEach(inject(function($rootScope, $controller) {
			ctrl = $controller('FinalExamsController', {
				$scope: scope,
				$state: state,
				$translate: translate,
				FinalExamsResource: resource,
				centrisNotify: notify
			});
		}));

		it('should should call ShowMyUserExamsIntro for userexams', function() {
			state.current = {
				name: 'finalexams.userexams'
			};
			scope.onCentrisIntro();
			expect(scope.$on).toHaveBeenCalled();
			expect(scope.ShowMyUserExamsIntro).toHaveBeenCalled();
		});

		it('should should call ShowMyAllExamsIntro for allexams', function() {
			state.current = {
				name: 'finalexams.allexams'
			};
			scope.onCentrisIntro();
			expect(scope.$on).toHaveBeenCalled();
			expect(scope.ShowMyAllExamsIntro).toHaveBeenCalled();
		});

		it('should should call neither otherwise', function() {
			state.current = {
				name: 'finalexams.unvalid'
			};
			scope.onCentrisIntro();
			expect(scope.$on).toHaveBeenCalled();
			expect(scope.ShowMyAllExamsIntro).not.toHaveBeenCalled();
			expect(scope.ShowMyUserExamsIntro).not.toHaveBeenCalled();
		});

	});

	describe('daysUntilExam', function() {

		beforeEach(function() {
			resource.setAnswers({});
			resource.setConditions([
				undefined
			]);
		});

		beforeEach(inject(function($rootScope, $controller) {
			ctrl = $controller('FinalExamsController', {
				$scope: scope,
				$state: state,
				$translate: translate,
				FinalExamsResource: resource,
				centrisNotify: notify
			});
		}));

		var msInADay = 24 * 3600 * 1000;

		it('should returned days until exam', function() {
			var exam = {
				DateScheduled: new Date(2014, 4, 13, 9, 0, 0)
			};
			scope.time = new Date(2014, 4, 12, 9, 0, 0);
			expect(scope.daysUntilExam(exam)).toEqual(1);
		});

		it('should floor days until exam', function() {
			var exam = {
				DateScheduled: new Date(2014, 4, 13, 9, 0, 0)
			};
			scope.time = new Date(2014, 4, 12, 9, 0, 1);
			expect(scope.daysUntilExam(exam)).toEqual(0);
		});

		it('should return a negative number for past exams', function() {
			var exam = {
				DateScheduled: new Date(2014, 4, 13, 9, 0, 0)
			};
			scope.time = new Date(2014, 4, 13, 9, 0, 1);
			expect(scope.daysUntilExam(exam)).toEqual(-1);
		});

	});

});
