"use strict";

angular.module("finalexamsApp", ["ui.router"])
.config(function ($stateProvider) {
	$stateProvider
	.state("finalexams", {
		url:         "/finalexams",
		templateUrl: "finalexams/components/index/index.html",
		controller:  "FinalExamsController"
	})
	.state("finalexams.userexams", {
		url: "/userexams"
	})
	.state("finalexams.allexams", {
		url: "/allexams"
	});
});
