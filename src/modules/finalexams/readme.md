# FinalExams module

This module takes care of displaying the list of final exams at any given time.
It should display in a prominent matter the final exams a user would be
most interested in, both as a student and as a teacher.