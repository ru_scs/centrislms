"use strict";

angular.module("personsApp", [
	"pascalprecht.translate",
	"ui.router",
	"sharedServices"
]).config(function ($stateProvider) {
	$stateProvider
	.state("persons", {
		url:         "/persons",
		templateUrl: "persons/components/index/index.html",
		controller:  "PersonsController"
	})
	.state("persondetails", {
		url:          "/persons/:ssn",
		templateUrl:  "persons/components/details/person-details.html",
		controller:   "PersonDetailsController"
	});
});
