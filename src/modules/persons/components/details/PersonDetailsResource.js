"use strict";

angular.module("personsApp").factory("PersonDetailsResource",
function (CentrisResource) {
	return {
		// GET api/v1/persons/{ssn}/details
		getDetails: function (personSSN) {
			var urlParams = {
				ssn: personSSN
			};
			return CentrisResource.get("persons", ":ssn/details", urlParams);
		}
	};
});