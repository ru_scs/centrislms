"use strict";

describe("PersonDetailsResource", function() {

	var factory, centris;

	beforeEach(module("personsApp", "sharedServices"));
	beforeEach(module("personsApp", function($provide) {
		centris = {};
		centris.get = jasmine.createSpy();
		$provide.value("CentrisResource", centris);
	}));

	beforeEach(inject(function(PersonDetailsResource) {
		factory = PersonDetailsResource;
		spyOn(factory, "getDetails").and.callThrough();
	}));

	it ("should exits", function() {
		expect(factory).toBeDefined();
		expect(factory).not.toBe(null);
	});

	it("should call get Details in personsApp", function() {
		factory.getDetails();
		expect(factory.getDetails).toHaveBeenCalled();
		expect(centris.get).toHaveBeenCalled();
	});

	it("should exits Centrist", function() {
		expect(centris).toBeDefined();
	});

});