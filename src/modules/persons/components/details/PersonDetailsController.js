"use strict";

/**
 * This controller will fetch deteils of a person for /#/persons/SSN view
 */
angular.module("personsApp").controller("PersonDetailsController",
function PersonDetailsController($scope, $stateParams, PersonDetailsResource, centrisNotify) {
	$scope.ssn    = $stateParams.ssn;
	$scope.person = null;

	$scope.loadingData = true;
	PersonDetailsResource.getDetails($scope.ssn)
	.success(function (data) {
		$scope.loadingData = false;
		$scope.person = data;
	})
	.error(function (data, status, headers, config) {
		$scope.loadingData = false;
		if (status === 404 && data === "INVALID_SSN") {
			centrisNotify.error("persons.Invalid_SSN");
			$scope.person = "not_found";
		} else {
			centrisNotify.error("persons.Error_GET_Info");
			$scope.person = "not_found";
		}
	});
});