"use strict";

angular.module("personScheduleApp", ["ui.router"])
.config(function ($stateProvider) {
	$stateProvider.state("schedule", {
		url:         "/schedule",
		templateUrl: "schedule/components/index/index.html",
		controller:  "PersonScheduleController"
	});
});
