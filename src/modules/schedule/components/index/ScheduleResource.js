"use strict";

angular.module("personScheduleApp").factory("ScheduleResource",
function (CentrisResource, centrisNotify) {
	return {
		// Get schedule for logged in user by date range
		getBookings: function(startDate, endDate) {
			var firstDate  = startDate.format("YYYY-MM-DDTHH:mm:ss");
			var secondDate = endDate.format("YYYY-MM-DDTHH:mm:ss");
			var range      = firstDate + "," + secondDate;

			return CentrisResource.get("my", "schedule", {range: range});
		}
	};
});