"use strict";

angular.module("mockPersonScheduleApp", []).factory("mockScheduleResource",
	function () {
		var fakeBookings;
		fakeBookings = [{
				startTime: "2016-11-06T12:37:04.7360412+00:00",
				secondTime: "2016-12-06T12:37:04.7360412+00:00",
				Description: " Description"
			},{
				startTime: "2016-10-06T12:37:04.7360412+00:00",
				secondTime: "2016-11-06T12:37:04.7360412+00:00",
				Description: " Description"
			},{
				startTime: "2016-09-06T12:37:04.7360412+00:00",
				secondTime: "2016-10-06T12:37:04.7360412+00:00",
				Description: " Description"
			}];

		var mock = {
			successGetBookings: false,
			getBookingsResult: fakeBookings,
			getBookings: function getBookings(firstDate, secondDate) {
				return {
					success: function(fn) {
						if (mock.successGetBookings === true) {
							fn(mock.getBookingsResult);
						}
						return {
							error: function(fn) {
								if (mock.successGetBookings === false) {
									fn();
								}
							}
						};
					}
				};
			}
		};
		return mock;
	});

