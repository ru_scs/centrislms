"use strict";

angular.module("personScheduleApp").controller("PersonScheduleController",
function PersonScheduleController($scope, ScheduleResource, formatEventsForCalendar, $state, $translate,
	centrisNotify) {
	$scope.firstDayOfWeek = moment().startOf("week");
	$scope.lastDayOfWeek  = moment().endOf("week");
	$scope.events         = [];

	$scope.onViewChanged = function onViewChanged(start, end) {
		$scope.firstDayOfWeek = moment(start);
		$scope.lastDayOfWeek  = moment(end);
		loadSchedule();
	};

	var loadSchedule = function loadSchedule() {
		var promise = ScheduleResource.getBookings($scope.firstDayOfWeek, $scope.lastDayOfWeek);
		promise.success(function(events) {
			$scope.events = formatEventsForCalendar.formatEvents(events);
			// Notify the calendar about the new events:
			$scope.$broadcast("eventsChanged", $scope.events);
		}).error(function(error) {
			$scope.events = [];
			centrisNotify.error("PersonSchedule.LoadScheduleError");
		});
	};

	loadSchedule();
	// INTRO otions
	$scope.IntroOptions = {
		steps: [{
			element: "#abouttheschedule",
			position: "left",
			intro: ""
		}, {
			element: "#schoolcalendar",
			position: "left",
			intro: ""
		}],
		showStepNumbers:    false,
		exitOnOverlayClick: true,
		exitOnEsc:          true,
	};

	var introKeys = [
		"AngIntroSchedule.AboutTheSchedule",
		"AngIntroSchedule.SchoolCalendar"
	];

	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
		$scope.IntroOptions.steps[1].intro = translations[introKeys[1]];
	});

	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.$on("onCentrisIntro", function(event, args) {
		// Only show this if the current route is for our page:
		if ($state.current.name === "schedule") {
			$scope.ShowScheduleIntro();
		}
	});
});
