"use strict";

describe("Person Schedule", function() {

	var scope, resource, translate, ctrl, notify;
	var state = {
		current: {}
	};

	beforeEach(module("personScheduleApp"));
	beforeEach(module("sharedServices"));
	beforeEach(module("mockPersonScheduleApp"));

	beforeEach(inject(function(mockScheduleResource) {
		resource = mockScheduleResource;
	}));

	beforeEach(inject(function(mockTranslate) {
		translate = mockTranslate.mockTranslate;
	}));

	beforeEach(inject(function(mockCentrisNotify) {
		notify = mockCentrisNotify;
	}));

	describe("initalize resource success with bookings", function() {
		beforeEach(function() {
			resource.successGetBookings = true;
		});

		beforeEach(inject(function($rootScope, $controller, formatEventsForCalendar) {
			scope = $rootScope.$new();
			ctrl  = $controller("PersonScheduleController", {
				$scope: scope,
				$state: state,
				$translate: translate,
				ScheduleResource: resource,
				centrisNotify: notify,
				formatEventsForCalendar: formatEventsForCalendar
			});
		}));

		it("Should get bookings", function() {
			var start = "2016-11-06";
			var end   = "2016-12-06";
			scope.onViewChanged(start, end);
			expect(scope.events.length).toEqual(3);

		});
	});

	describe("initalize resource success with no bookings", function() {
		beforeEach(function() {
			resource.successGetBookings = true;
			resource.getBookingsResult = [];
		});

		beforeEach(inject(function($rootScope, $controller, formatEventsForCalendar) {
			scope = $rootScope.$new();
			ctrl  = $controller("PersonScheduleController", {
				$scope: scope,
				$state: state,
				$translate: translate,
				ScheduleResource: resource,
				centrisNotify: notify,
				formatEventsForCalendar: formatEventsForCalendar
			});
		}));

		it("Should get 0 bookings", function() {
			var start = "2016-11-06";
			var end   = "2016-12-06";
			scope.onViewChanged(start,end);
			expect(scope.events).toEqual([]);
			scope.$apply();
		});
	});

	describe("initalize resource error", function() {
		beforeEach(function() {
			resource.successGetBookings = false;
		});

		beforeEach(inject(function($rootScope, $controller, formatEventsForCalendar) {
			scope = $rootScope.$new();
			ctrl = $controller("PersonScheduleController", {
				$scope: scope,
				$state: state,
				$translate: translate,
				ScheduleResource: resource,
				centrisNotify: notify,
				formatEventsForCalendar: formatEventsForCalendar
			});
		}));

		it("Should get error", function() {
			var start = "2016-11-06";
			var end   = "2016-12-06";
			spyOn(notify, "error").and.callThrough();
			scope.onViewChanged(start,end);
			expect(notify.error).toHaveBeenCalled();
		});
	});

	describe("onCentrisIntro", function() {
		beforeEach(function() {
			resource.successGetBookings = true;
		});

		beforeEach(inject(function($rootScope, $controller, formatEventsForCalendar) {
			scope = $rootScope.$new();
			scope.ShowScheduleIntro = jasmine.createSpy().and.callThrough();
			state.current.name = "schedule";
			scope.$on = function MockScopeOn(name, fn) {
				fn();
			};
			ctrl = $controller("PersonScheduleController", {
				$scope: scope,
				$state: state,
				$translate: translate,
				ScheduleResource: resource,
				centrisNotify: notify,
				formatEventsForCalendar: formatEventsForCalendar
			});
		}));

		it("Should been called", function() {
			expect(scope.ShowScheduleIntro).toHaveBeenCalled();
		});
	});
});