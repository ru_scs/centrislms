'use strict';

describe('ScheduleResource', function() {

	var factory, centris, notify;

	beforeEach(module('personScheduleApp', 'sharedServices'));
	beforeEach(module('personScheduleApp', function($provide) {
		centris = {};
		centris.get = jasmine.createSpy();
		$provide.value('CentrisResource', centris);
		notify = {};
		notify.get = jasmine.createSpy();
		$provide.value('centrisNotify', notify);
	}));

	beforeEach(inject(function(ScheduleResource) {
		factory = ScheduleResource;
		spyOn(factory, 'getBookings').and.callThrough();
	}));

	it('Should getBookings', function() {
		var time = '2016-11-06T00:00:00';
		var time2 = '2016-12-06T00:00:00';
		var start = moment(time);
		var end   = moment(time2);
		factory.getBookings(start, end);
		expect(centris.get).toHaveBeenCalled();
	});
});