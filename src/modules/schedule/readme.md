# Schedule App

This module takes care of displaying a "global" schedule, i.e. the entire
schedule of a person. It may contain items from various courses, clubs,
events etc.