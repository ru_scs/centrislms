"use strict";

// DOCUMENTATION BELOW THE CODE

angular.module("discussionsApp").factory("DiscussionsResource",
function DiscussionsResource(CentrisResource) {
	return {
		getBoard: function (resourceType, resourceID) {
			return CentrisResource.get("discussions", "board/:resourceType/:resourceID", {
					resourceType: resourceType,
					resourceID: resourceID })
				.error(function (data, status, headers, config) {
					console.log("Error getBoard " + resourceType + " " + resourceID + "!");
				});
		},

		postThread: function (resourceType, resourceID, newThread) {
			var fd = new FormData();
			fd.append("Title", newThread.Title);
			fd.append("Text", newThread.Text);
			fd.append("IsAnonymous", newThread.IsAnonymous);

			if (newThread.Files.length > 0) {
				for (var i = 0 ; i < newThread.Files.length ; i++) {
					fd.append("File" + i, newThread.Files[i]);
				}
			}

			return CentrisResource.post("discussions", "boards/:resourceType/:resourceID/threads", {
					resourceType: resourceType,
					resourceID: resourceID }, fd, true)
				.error(function (data, status, headers, config) {
					console.log("Error postThread " + resourceType + " " + resourceID + "!");
				});
		},

		editThread: function (threadID, updatedThread) {
			return CentrisResource.put("discussions", "threads/:threadID", {
					threadID: threadID }, updatedThread).error(function (data, status, headers, config) {
				console.log("Error editThread " + threadID +
					"!\n\nThread info\nTitle: " + updatedThread.Title +
					"\nText: " + updatedThread.Text +
					"\nAnonymous? " + updatedThread.IsAnonymous);
			});
		},

		deleteThread: function (threadID) {
			return CentrisResource.remove("discussions", "threads/:threadID", { threadID: threadID }, {})
				.error(function (data, status, headers, config) {
					console.log("Error deleteThread " + threadID + "!");
				});
		},

		postReply: function (resourceType, resourceID, threadID, newReply) {
			var fd = new FormData();
			fd.append("Text", newReply.Text);
			fd.append("IsAnonymous", newReply.IsAnonymous);

			if (newReply.Files.length > 0) {
				for (var i = 0 ; i < newReply.Files.length ; i++) {
					fd.append("File" + i, newReply.Files[i]);
				}
			}

			return CentrisResource.post("discussions", "threads/:threadID/replies", {
					threadID: threadID,
					resourceType: resourceType,
					resourceID: resourceID }, fd, true)
				.error(function (data, status, headers, config) {
					console.log("Error postReply " + threadID + "!");
				});
		},

		editReply: function (replyID, updatedReply) {
			return CentrisResource.put("discussions", "replies/:replyID", { replyID: replyID }, updatedReply)
				.error(function (data, status, headers, config) {
					console.log("Error editReply " + replyID +
						"!\n\nThread info\nText: " + updatedReply.Text + "\nAnonymous? " + updatedReply.IsAnonymous);
				});
		},

		deleteReply: function (replyID) {
			return CentrisResource.remove("discussions", "replies/:replyID", { replyID: replyID }, {})
				.error(function (data, status, headers, config) {
					console.log("Error deleteReply " + replyID + "!");
				});
		},

		subscribe: function (type, contentID) {
			return CentrisResource.post("discussions", "subscriptions/:type/:contentID", {
					type: type,
					contentID: contentID }, {})
				.error(function (data, status, headers, config) {
					console.log("Error subscribe " + type + " " + contentID + "!");
				});
		},

		unsubscribe: function (type, contentID) {
			return CentrisResource.remove("discussions", "subscriptions/:type/:contentID", {
					type: type,
					contentID: contentID }, {})
				.error(function (data, status, headers, config) {
					console.log("Error unsubscribe " + type + " " + contentID + "!");
				});
		},

		like: function (type, messageID) {
			return CentrisResource.post("discussions", "likes/:messageType/:messageID", {
					messageType: type,
					messageID: messageID }, {})
				.error(function (data, status, headers, config) {
					console.log("Error like " + type + " " + messageID + "!");
				});
		},

		unlike: function (type, messageID) {
			return CentrisResource.remove("discussions", "likes/:messageType/:messageID", {
					messageType: type,
					messageID: messageID }, {})
				.error(function (data, status, headers, config) {
					console.log("Error unlike " + type + " " + messageID + "!");
				});
		}
	};
});

/**
 * @name discussionsApp.DiscussionsResource
 * @description
 * A factory that wraps some common IO operations for the discussions system
 *
 * getBoard(resourceType : string, resourceID : int) : returns promise (data: board object
 * Takes the type and ID of the resource as arguments and fetches
 * the discussion board data from the web server.
 * Returns a promise that contains a board object as callback data
 *
 * The board object has these members:
 * - Threads: An array of ThreadWithData objects
 * - IsInstructor: true if the calling user is an instructor for this board
 * - IsSubscribed: true if the calling user is subscribed to this board
 * - Name: The nickname for the board
 *
 * ThreadWithData (data in short) is as such:
 * - data[i].Thread contains thread data:
 *   ID (int), Title (string), Text (string), DatePosted (datetime),
 *   Author (object with Name and SSN), IsAnonymous (boolean)
 * - data[i].Replies contains an array of ReplyWithData objects:
 *   - Replies[i].Reply:
 *     Text (string), DatePosted (datetime), Author (object with Name and SSN),
 *     IsAonymous (boolean)
 *   - Replies[i].Likes:
 *     Count (int), InstructorCount (int), Liked (boolean)
 *   - Replies[i].Attachments is an array of objects with URL and Filename values
 * - data[i].Likes is a single object:
 *   Count (int), InstructorCount (int), Liked (boolean)
 * - data[i].Attachments is an array of objects with URL and Filename
 *
 * postThread(resourceType : string, resourceID : int, theThread : obj) : returns promise (data: Thread)
 * Takes type and ID of a resource as well as formData.newThread and sends it
 * to the API for storage.
 * Returns promise with the new Thread in callback data, used to append to the thread list
 *
 * editThread(threadID : int, updatedThread : obj) : returns promise (data: Thread object)
 * Takes a thread ID and Thread view model object (Title, Text and IsAnonymous) containing
 * the updated data for the thread and sends it to the web server for submission
 * Returns a promise with the updated Thread object that should replace the old one
 *
 * deleteThread(threadID : int) : returns promise (no data)
 * Takes a thread ID and sends a delete request to the web server to delete the thread
 * Returns a promise but no data
 *
 * postReply(threadID : int, theReply : obj) : returns promise (data: Reply array)
 * Takes thread ID and formData.newReply (see DiscussionsForms.js) and sends
 * it to the API for storage
 * Returns promise with an array of Reply in callback data, used for updating
 * the reply list of the thread
 *
 * editReply(replyID : int, updatedReply : obj) : returns promise (data: Reply object)
 * Takes a reply ID and Reply view model object (Text and IsAnonymous) containing
 * the updated data for the reply and sends it to the web server for submission
 * Returns a promise with the updated Reply object that should replace the old one
 *
 * deleteReply(replyID : int) : returns promise (no data)
 * Takes a reply ID and sends a delete request to the web server to delete the reply
 * Returns a promise but no data
 *
 * subscribe(type : string, contentID : int) : returns promise (no data)
 * Takes a content ID (currently only thread or discussion board) and subscribes
 * the current user to it
 * Returns a promise but no data for callback
 *
 * unsubscribe(type : string, contentID : int) : returns promise (no data)
 * Has the same mechanics as above except it undoes the subscription
 * of the current user
 * Returns a promise but no data for callback
 *
 * like(type : string, messageID : int) : returns promise (data: Likes)
 * Takes type ("replies" for reply or "threads" for thread) and message ID (the
 * ID of the reply or thread) as arguments and applies the current
 * user"s like for the content
 * Returns a promise where the data object is an updated Likes object
 *
 * unlike(type : string, messageID : int) : returns promise (data: Likes)
 * Has the same arguments as like, except this function will
 * undo the current user"s like for the content
 * Returns a promise where the data object is an updated Likes object for the content
 *
 */
