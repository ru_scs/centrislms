"use strict";

angular.module("discussionsApp").factory("DiscussionsResourceMock",
function ($q, $timeout, $rootScope) {
	// SETTINGS -- modify for testing
	var username = "dabs";
	var isInstructor = true;

	// Helper functions to make HTTP promises
	function qToHttp (deferred) {
		deferred.promise.success = function (fn) {
			deferred.promise.then (fn, null);

			return deferred.promise;
		};

		deferred.promise.error = function (fn) {
			deferred.promise.then (null, fn);

			return deferred.promise;
		};

		return deferred;
	}

	function httpPromiseFromValue (val, reject) {
		var def = $q.defer();

		if (reject) {
			def.reject (val);
		} else {
			def.resolve (val);
		}

		qToHttp (def);

		return def.promise;
	}

	function getReply (replyID) {
		for (var i = 0 ; i < database.Threads.length ; i++) {
			for (var k = 0 ; k < database.Threads[i].Replies.length ; k++) {
				if (database.Threads[i].Replies[k].Reply.ID === replyID) {
					return database.Threads[i].Replies[k];
				}
			}
		}

		return undefined;
	}

	function getThread (threadID) {
		for (var i = 0 ; i < database.Threads.length ; i++) {
			if (database.Threads[i].Thread.ID === threadID) {
				return database.Threads[i];
			}
		}

		return undefined;
	}

	function getLikes (type, messageID) {
		var content;

		if (type === "threads") {
			content = getThread (messageID);
			return content.Likes;
		} else if (type === "replies") {
			content = getReply (messageID);
			return content.Likes;
		}

		return undefined;
	}

	// To give each new reply or thread a unique ID, these are incremented after each posting
	var currReplyID = 3;
	var currThreadID = 3;

	// Database setup
	var threads = [{
				Thread: {
					ID: 2,
					Title: "Thread 2",
					Text: "Next thread after that",
					DatePosted: "2015-03-17T13:09:55.443",
					Author: {
						Name: "bjorna11",
						SSN: "bjorna11"
					},
					IsAnonymous: false
				},
				IsSubscribed: false,
				Likes: {
					Count: 0,
					InstructorCount: 0,
					Liked: false
				},
				Attachments: [],
				Replies: [{
						Reply: {
							ID: 1,
							Text: "First reply thread 2",
							DatePosted: "2015-03-17T13:10:05.537",
							Author: {
								Name: "bjorna11",
								SSN: "bjorna11"
							},
							IsAnonymous: false
						},
						Likes: {
							Count: 0,
							InstructorCount: 0,
							Liked: false
						},
						Attachments: [],
						IsAuthor: username === "bjorna11"
					}, {
						Reply: {
							ID: 2,
							Text: "Replying to my own thread",
							DatePosted: "2015-03-20T14:48:05.65",
							Author: {
								Name: "bjorna11",
								SSN: "bjorna11"
							},
							IsAnonymous: false
						},
						Likes: {
							Count: 0,
							InstructorCount: 0,
							Liked: false
						},
						Attachments: [],
						IsAuthor: username === "bjorna11"
					}
				],
				"IsAuthor": username === "bjorna11"
			}, {
				Thread: {
					ID: 1,
					Title: "Thread 1",
					Text: "First thread",
					DatePosted: "2015-03-17T13:09:33.423",
					Author: {
						Name: "bjorna11",
						SSN: "bjorna11"
					},
					IsAnonymous: false
				},
				IsSubscribed: true,
				Likes: {
					Count: 0,
					InstructorCount: 0,
					Liked: false
				},
				Attachments: [],
				Replies: [],
				IsAuthor: username === "bjorna11"
			}
		];
	var database = {
		ID: 1,
		IsSubscribed: false,
		IsInstructor: isInstructor,
		Threads: threads };

	return {
		getBoard: function (resourceType, resourceID) {
			return httpPromiseFromValue (database);
		},

		postThread: function (resourceType, resourceID, newThread) {
			var data = {
				Thread: {
					ID: currThreadID,
					Title: newThread.Title,
					Text: newThread.Text,
					DatePosted: new Date(),
					Author: {
						Name: username,
						SSN: username
					},
					IsAnonymous: newThread.IsAnonymous
				},
				IsSubscribed: true,
				IsAuthor: true,
				Likes: {
					Count: 0,
					InstructorCount: 0,
					Liked: false
				},
				Attachments: [],
				Replies: []
			};

			for (var i = 0 ; i < newThread.Files.length ; i++) {
				data.Attachments.push({ FriendlyFilename: newThread.Files[i].name });
			}

			currThreadID++;

			return httpPromiseFromValue (data);
		},

		editThread: function (threadID, updatedThread) {
			var thread = getThread (threadID);

			if (thread) {
				thread.Thread.Title = updatedThread.Title;
				thread.Thread.Text = updatedThread.Text;
				thread.Thread.IsAnonymous = updatedThread.IsAnonymous;

				return httpPromiseFromValue (thread.Thread);
			}

			return httpPromiseFromValue ({});
		},

		deleteThread: function (threadID) {
			return httpPromiseFromValue({});
		},

		postReply: function (resourceType, resourceID, threadID, newReply) {
			var data;

			var r = {
				Reply: {
					ID: currReplyID,
					Text: newReply.Text,
					DatePosted: new Date(),
					IsAnonymous: newReply.IsAnonymous,
					Author: {
						Name: username,
						SSN: username
					}
				},
				IsAuthor: true,
				Attachments: [],
				Likes: {
					Count: 0,
					InstructorCount: 0,
					Liked: false
				}
			};

			for (var a = 0 ; a < newReply.Files.length ; a++) {
				r.Attachments.push({ FriendlyFilename: newReply.Files[a].name });
			}

			currReplyID++;

			for (var i = 0 ; i < database.Threads.length ; i++) {
				if (database.Threads[i].Thread.ID === threadID) {
					data = database.Threads[i].Replies;
					data.push (r);
					break;
				}
			}

			return httpPromiseFromValue (data);
		},

		editReply: function (replyID, updatedReply) {
			var reply = getReply (replyID);

			if (reply) {
				reply.Reply.Text = updatedReply.Text;
				reply.Reply.IsAnonymous = updatedReply.IsAnonymous;

				return httpPromiseFromValue (reply.Reply);
			}

			return httpPromiseFromValue ({});
		},

		deleteReply: function (replyID) {
			return httpPromiseFromValue({});
		},

		subscribe: function (type, contentID) {
			return httpPromiseFromValue ({});
		},

		unsubscribe: function (type, contentID) {
			return httpPromiseFromValue ({});
		},

		like: function (type, messageID) {
			var data = getLikes (type, messageID);

			if (data) {
				if (data.Liked === true) {
					return httpPromiseFromValue (data);
				} else {
					if (isInstructor) {
						data.InstructorCount++;
					} else {
						data.Count++;
					}
					data.Liked = true;
				}
			} else {
				data = { Count: 1, InstructorCount: 0, Liked: true };
			}

			return httpPromiseFromValue (data);
		},

		unlike: function (type, messageID) {
			var data = getLikes (type, messageID);

			if (data) {
				if (data.Liked === false) {
					return httpPromiseFromValue (data);
				} else {
					if (isInstructor) {
						data.InstructorCount--;
					} else {
						data.Count--;
					}
					data.Liked = false;
				}
			} else {
				data = { Count: 1, InstructorCount: 0, Liked: true };
			}

			return httpPromiseFromValue (data);
		}
	};
});
