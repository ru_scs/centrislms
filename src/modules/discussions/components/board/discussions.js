"use strict";

angular.module("discussionsApp").directive("discussionsBoard",
function () {
	return {
		restrict: "E",
		scope: {
			resourceType: "=resourcetype",
			resourceID:   "=resourceid",

			threadID:     "=threadid",
			replyID:      "=replyid"
		},
		templateUrl: "discussions/components/board/board.html",
		controller:  "BoardController"
	};
});
