"use strict";

angular.module("discussionsApp").controller("BoardController",
function BoardController($scope, UserService, DiscussionsResource, DiscussionsForms,
	$location, $anchorScroll, $rootScope, $timeout) {
	// Switch between DiscussionsResource and DiscussionsResourceMock as desired (repeat the change in the injector above)
	var dsvc = DiscussionsResource;
	$scope.loadingBoard = true;

	if (!$scope.resourceID || !$scope.resourceType) {
		return;
	}

	// Initialize form data container in the discussion board
	$scope.formData = DiscussionsForms.initializeFormData();

	// Initialize local state
	$scope.showNext = false;
	$scope.showPrev = false;
	$scope.ti = 0; // Current index in the thread array

	$scope.focusReply     = false;
	$scope.focusThread    = false;
	$scope.focusNewReply  = false;
	$scope.focusNewThread = false;
	$scope.currentSSN     = UserService.getUserSSN();

	$scope.newReplyPostButton   = "discussions.Post";
	$scope.newThreadPostButton  = "discussions.Post";
	$scope.editReplyPostButton  = "discussions.Post";
	$scope.editThreadPostButton = "discussions.Post";

	$scope.deletingReply  = -1;
	$scope.deletingThread = -1;

	$scope.showEditReplyFormId = -1;
	$scope.forms = {
		newThread:  0,
		newReply:   0,
		editThread: 0,
		editReply:  0,
		replyID:    0
	};

	// Map over threads to make structure the same, simplifies markup.
	function unifyThreads(thread) {
		thread.Replies = thread.Replies.map(function(reply) {
			reply.Thread = reply.Reply;
			reply.isReply = true;
			return reply;
		});

		return thread;
	}

	// Return a board identifying object.
	function getBoardForData(data) {
		return {
			ID: data.ID,
			IsSubscribed: data.IsSubscribed,
			IsInstructor: data.IsInstructor
		};
	}

	// Fetch all the data at once for this discussion board
	dsvc.getBoard($scope.resourceType, $scope.resourceID).success(function (data) {
		if (Object.prototype.toString.call(data.Threads) === "[object Array]") {
			$scope.threads      = data.Threads.map(unifyThreads);
			$scope.board        = getBoardForData(data);
			$scope.loadingBoard = false;
		}
	});

	$scope.switchToThread = function switchToThread(index) {
		if (index >= 0 && index < $scope.threads.length) {
			$scope.ti = index;
		}

		updateNavArrows();
	};

	$scope.nextThread = function nextThread() {
		if ($scope.ti > 0) {
			$scope.ti--;
			DiscussionsForms.resetNewReply($scope.formData);
			DiscussionsForms.resetNewThread($scope.formData);
		}

		updateNavArrows();
	};

	$scope.prevThread = function prevThread() {
		if ($scope.ti < ($scope.threads.length - 1)) {
			$scope.ti++;
			DiscussionsForms.resetNewReply($scope.formData);
			DiscussionsForms.resetNewThread($scope.formData);
		}

		updateNavArrows();
	};

	$scope.openReplyForm = function openReplyForm(comment) {
		$scope.forms.newReply = -1;
		$scope.forms.replyID  = comment.Thread.ID;
		$scope.focusNewThread = true;
	};

	$scope.postReply = function postReply(ti) {
		if (DiscussionsForms.validateNewReply($scope.formData) === true) {
			var thread = $scope.threads[ti].Thread;

			$scope.newReplyPostButton = "discussions.PostActive";

			dsvc.postReply($scope.resourceType, $scope.resourceID, thread.ID, $scope.formData.newReply)
				.success(function (data) {
					DiscussionsForms.resetNewReply($scope.formData);
					$scope.forms.newReply = 0;
					$scope.newReplyPostButton = "discussions.Post";
					var tempThread = {
						Replies: data
					};

					$scope.threads[ti].Replies = unifyThreads(tempThread).Replies;
					$scope.threads[ti].IsSubscribed = true;
				}).error(function (data) {
				$scope.newReplyPostButton = "discussions.Post";
			});
		}
	};

	$scope.editReply = function (reply) {
		if (DiscussionsForms.validateUpdatedReply($scope.formData, reply.Reply) === true) {
			$scope.editReplyPostButton = "discussions.PostActive";

			dsvc.editReply(reply.Reply.ID, $scope.formData.updatedReply)
				.success(function (data) {
					DiscussionsForms.resetUpdatedReply($scope.formData);
					$scope.editReplyPostButton = "discussions.Post";

					reply.Reply = data;

					$scope.forms.editReply = 0;
					$scope.showEditReplyFormId = -1;
				}).error(function (data) {
				$scope.editReplyPostButton = "discussions.Post";
			});
		}
	};

	$scope.deleteReply = function (tIndex, rIndex) {
		$scope.deletingReply = rIndex;

		dsvc.deleteReply($scope.threads[tIndex].Replies[rIndex].Reply.ID)
			.success(function (data) {
				$scope.threads[tIndex].Replies.splice(rIndex, 1);

				$scope.deletingReply = -1;
			}).error(function (data) {
			$scope.deletingReply = -1;
		});
	};

	$scope.switchEditReplyForm = function (reply, index) {
		$scope.formData.updatedReply.Text = reply.Text;
		$scope.formData.updatedReply.IsAnonymous = reply.IsAnonymous;

		$scope.showEditReplyFormId = index;
		$scope.forms.editReply = 1;
	};

	$scope.cancelReplyEdit = function () {
		$scope.showEditReplyFormId = -1;
		$scope.forms.editReply = 0;
	};

	$scope.postThread = function () {
		if (DiscussionsForms.validateNewThread($scope.formData) === true) {
			$scope.newThreadPostButton = "discussions.PostActive";
			dsvc.postThread($scope.resourceType, $scope.resourceID, $scope.formData.newThread)
				.success(function (data) {
					DiscussionsForms.resetAll($scope.formData);
					$scope.newThreadPostButton = "discussions.Post";
					$scope.forms.newThread = 0;
					$scope.threads.unshift(data);
					$scope.ti = 0;

					updateNavArrows();
				}).error(function (data) {
				$scope.newThreadPostButton = "discussions.Post";
			});
		}
	};

	$scope.editThread = function (thread) {
		if (DiscussionsForms.validateUpdatedThread($scope.formData, thread.Thread) === true) {
			$scope.editThreadPostButton = "discussions.PostActive";
			dsvc.editThread(thread.Thread.ID, $scope.formData.updatedThread)
				.success(function (data) {
					thread.Thread = data;

					$scope.forms.editThread = 0;

					$scope.editThreadPostButton = "discussions.Post";
				}).error(function (data) {
				$scope.editThreadPostButton = "discussions.Post";
			});
		}
	};

	$scope.deleteThread = function (tIndex) {
		$scope.deletingThread = tIndex;

		dsvc.deleteThread($scope.threads[tIndex].Thread.ID)
			.success(function (data) {
				$scope.threads.splice(tIndex, 1);

				if ($scope.ti > ($scope.threads.length - 1)) {
					$scope.ti--;
				}

				$scope.deletingThread = -1;
				updateNavArrows();
			}).error(function (data) {
				$scope.deletingThread = -1;
			});
	};

	$scope.switchEditThreadForm = function (thread) {
		$scope.formData.updatedThread.Title = thread.Title;
		$scope.formData.updatedThread.Text = thread.Text;
		$scope.formData.updatedThread.IsAnonymous = thread.IsAnonymous;

		$scope.forms.editThread = 1;
	};

	$scope.cancelThread = function () {
		DiscussionsForms.resetNewThread($scope.formData);
		$scope.forms.newThread = 0;
	};

	$scope.cancelReply = function () {
		DiscussionsForms.resetNewReply($scope.formData);

		$scope.forms.newReply = 0;
	};

	$scope.subscribeToBoard = function () {
		dsvc.subscribe("discussions", $scope.board.ID)
			.success(function (data) {
				$scope.board.IsSubscribed = true;
			});
	};

	$scope.unsubscribeFromBoard = function () {
		dsvc.unsubscribe("discussions", $scope.board.ID)
			.success(function (data) {
				$scope.board.IsSubscribed = false;
			});
	};

	$scope.subscribeToThread = function (ti) {
		dsvc.subscribe("threads", $scope.threads[ti].Thread.ID)
			.success(function (data) {
				$scope.threads[ti].IsSubscribed = true;
			});
	};

	$scope.unsubscribeFromThread = function (ti) {
		dsvc.unsubscribe("threads", $scope.threads[ti].Thread.ID)
			.success(function (data) {
				$scope.threads[ti].IsSubscribed = false;
			});
	};

	$scope.like = function(comment, isReply) {
		if (comment.IsAuthor) {
			return;
		}

		var type = isReply ? "replies" : "threads";
		var commentID = isReply ? comment.Reply.ID : comment.Thread.ID;
		var action = comment.Likes.Liked ? "unlike" : "like";

		dsvc[action](type, commentID).success(function (data) {
			comment.Likes = data;
		});
	};

	$scope.downloadAttachment = function (attachmentID) {
		dsvc.downloadAttachment(attachmentID, $scope.resourceType, $scope.resourceID);
	};

	$scope.goToThread = function () {
		$location.hash("top");
		$anchorScroll();
	};

	$scope.switchForms = function (i) {
		$scope.forms[i] *= -1;
	};

	$scope.showSub = function (i) {
		if ($scope.threads[i].IsSubscribed === true) {
			return true;
		} else {
			return false;
		}
	};

	// Call this after every action that adds to/deletes from the threads array
	// or changes the thread index var (ti)
	function updateNavArrows () {
		$scope.forms.newThread  = 0;
		$scope.forms.editThread = 0;
		$scope.forms.newReply   = 0;
		$scope.forms.editReply  = 0;

		$scope.showPrev = false;
		$scope.showNext = false;

		if ($scope.threads.length < 2) {
			return;
		}

		if ($scope.ti < ($scope.threads.length - 1)) {
			$scope.showPrev = true;
		}

		if ($scope.ti > 0) {
			$scope.showNext = true;
		}
	}

	// Return the index of the thread with the specified ID, returns -1 if not found
	function findThreadIndex(threadID) {
		for (var i = 0; i < $scope.threads.length; i++) {
			if ($scope.threads[i].Thread.ID === threadID) {
				return i;
			}
		}
		return -1;
	}

	// Route to the correct place in the board based on threadID and replyID
	function routeToMessage () {
		if ($scope.threadID) {
			var threadIndex = findThreadIndex(parseInt($scope.threadID));

			if (threadIndex > -1) {
				$scope.ti = threadIndex;

				if ($scope.replyID) {
					$timeout(function () {
						$location.hash("reply" + $scope.replyID);
						$anchorScroll();
					}, 250);
				}
			}
		}
	}
});
