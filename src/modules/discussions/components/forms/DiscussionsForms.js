"use strict";

// DOCUMENTATION BELOW

angular.module("discussionsApp").factory("DiscussionsForms",
function () {
	var _resetNewThread = function (formData) {
		formData.newThread.Title       = "";
		formData.newThread.Text        = "";
		formData.newThread.IsAnonymous = false;
		formData.newThread.Files       = [];
	};

	var _resetUpdatedThread = function (formData) {
		formData.updatedThread.Title       = "";
		formData.updatedThread.Text        = "";
		formData.updatedThread.IsAnonymous = false;
	};

	var _resetNewReply = function (formData) {
		formData.newReply.Text        = "";
		formData.newReply.IsAnonymous = false;
		formData.newReply.Files       = [];
	};

	var _resetUpdatedReply = function (formData) {
		formData.updatedReply.Text        = "";
		formData.updatedReply.IsAnonymous = false;
	};

	// Check whether any non-whitespace characters are in the string
	function validateWhitespaces (str) {
		var i, c;

		for (i = 0; i < str.length; i++) {
			c = str.charAt(i);
			if (c !== " " && c !== "\n" && c !== "\t" && c !== "\r") {
				return true;
			}
		}

		return false;
	}

	return {
		initializeFormData: function () {
			var formData = {};

			formData.newThread = {};
			_resetNewThread(formData);

			formData.updatedThread = {};
			_resetUpdatedThread(formData);

			formData.newReply = {};
			_resetNewReply(formData);

			formData.updatedReply = {};
			_resetUpdatedReply(formData);

			return formData;
		},

		resetAll: function (formData) {
			_resetNewThread(formData);
			_resetUpdatedThread(formData);
			_resetNewReply(formData);
			_resetUpdatedReply(formData);
		},

		resetNewThread: _resetNewThread,

		resetUpdatedThread: _resetUpdatedThread,

		resetNewReply: _resetNewReply,

		resetUpdatedReply: _resetUpdatedReply,

		validateNewThread: function (formData) {
			if (formData.newThread.Text === "") {
				return false;
			}
			if (validateWhitespaces(formData.newThread.Text) === false) {
				return false;
			}

			return true;
		},

		validateUpdatedThread: function (formData, thread) {
			if (formData.updatedThread.Text === "") {
				return false;
			}
			if (validateWhitespaces(formData.updatedThread.Text) === false) {
				return false;
			}
			if (formData.updatedThread.Title === thread.Title && formData.updatedThread.Text === thread.Text &&
				formData.updatedThread.IsAnonymous === thread.IsAnonymous) {
				return false;
			}

			return true;
		},

		validateNewReply: function (formData) {
			if (formData.newReply.Text === "") {
				return false;
			}
			if (validateWhitespaces(formData.newReply.Text) === false) {
				return false;
			}

			return true;
		},

		validateUpdatedReply: function (formData, reply) {
			if (formData.updatedReply.Text === "") {
				return false;
			}
			if (validateWhitespaces(formData.updatedReply.Text) === false) {
				return false;
			}

			if (formData.updatedReply.Text === reply.Text &&
				formData.updatedReply.IsAnonymous === reply.IsAnonymous) {
				return false;
			}

			return true;
		}
	};
});

/**
 * @name discussionsApp.DiscussionsForms
 * @description
 * A small factory that wraps some common functions
 * for general form handling in the discussions system
 *
 * initializeFormData takes no arguments and returns an initialized object
 * that contains all the form data entries needed.
 * - newReply -- the form for new replies, has the following members:
 *   Text (string), IsAnonymous (boolean)
 * - updatedReply -- same as newReply
 * - newThread -- the form for new threads, has the following members:
 *   Title (string), Text (string), IsAnonymous (boolean)
 * - updatedThread -- same as newThread
 * Refer to the above members when doing data-binding in markup
 *
 * resetAll takes the form data object as argument and resets everything to defaults
 *
 * resetNewReply takes the form data object as argument and resets the new reply form
 *
 * resetUpdatedReply takes the form data object as argument and resets the updated reply form
 *
 * resetNewThread takes the form data object as argument and resets the new thread form
 *
 * resetUpdatedThread takes the form data object as argument and resets the updated thread form
 *
 * validateNewThread and
 * validateNewReply takes the form data object as argument and checks that it is valid
 * for the specific data being submitted
 *
 * validateUpdatedThread and validateUpdatedReply work the same as
 * the above except they accept the thread or reply object to verify that any changes
 * were actually made
 *
 */
