"use strict";

angular.module("discussionsApp").directive("commentAttachments", function() {
	return {
		scope:       {
			attachments: "=data"
		},
		restrict:    "E",
		templateUrl: "discussions/components/comments/attachments/index.html"
	};
});
