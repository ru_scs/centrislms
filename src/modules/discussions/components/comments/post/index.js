"use strict";

angular.module("discussionsApp").directive("commentPost", function() {
	return {
		restrict:    "E",
		templateUrl: "discussions/components/comments/post/index.html",
		transclude:  true
	};
});
