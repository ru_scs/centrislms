"use strict";

angular.module("discussionsApp").directive("commentHeader", function() {
	return {
		restrict:    "E",
		templateUrl: "discussions/components/comments/header/index.html"
	};
});
