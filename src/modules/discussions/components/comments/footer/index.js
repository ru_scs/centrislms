"use strict";

angular.module("discussionsApp").directive("commentFooter", function() {
	return {
		restrict:    "E",
		templateUrl: "discussions/components/comments/footer/index.html",
		transclude:  true
	};
});
