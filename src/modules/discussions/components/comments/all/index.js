"use strict";

angular.module("discussionsApp").directive("comments", function() {
	return {
		scope:       {
			resourceType: "=resourcetype",
			resourceID:   "=resourceid",
			threadID:     "=threadid",
			replyID:      "=replyid"
		},
		restrict:    "E",
		templateUrl: "discussions/components/comments/all/index.html",
		controller:  "BoardController"
	};
});
