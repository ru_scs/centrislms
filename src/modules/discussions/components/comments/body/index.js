"use strict";

angular.module("discussionsApp").directive("commentBody", function() {
	return {
		restrict:    "E",
		templateUrl: "discussions/components/comments/body/index.html"
	};
});
