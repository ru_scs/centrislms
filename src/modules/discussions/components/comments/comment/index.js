"use strict";

angular.module("discussionsApp").directive("comment", function() {
	return {
		scope:       {
			ssn: "="
		},
		restrict:    "E",
		templateUrl: "discussions/components/comments/comment/index.html",
		transclude:  true
	};
});
