"use strict";

angular.module("discussionsApp").directive("commentLikes", function() {
	return {
		restrict:    "E",
		templateUrl: "discussions/components/comments/likes/index.html"
	};
});
