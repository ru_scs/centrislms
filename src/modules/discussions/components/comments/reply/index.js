"use strict";

angular.module("discussionsApp").directive("commentReply", function() {
	return {
		restrict:    "E",
		templateUrl: "discussions/components/comments/reply/index.html",
		transclude:  true
	};
});
