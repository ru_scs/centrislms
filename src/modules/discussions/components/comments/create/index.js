"use strict";

angular.module("discussionsApp").directive("commentCreate",
function commentCreate() {
	return {
		restrict:    "E",
		templateUrl: "discussions/components/comments/create/index.html",
		transclude:  true
	};
});
