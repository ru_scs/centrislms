"use strict";

angular.module("discussionsApp").controller("BoardExampleController",
function($scope, $stateParams) {
	$scope.resid = $stateParams.resourceID;
	$scope.restype = $stateParams.resourceType;

	$scope.tid = $stateParams.threadID;
	$scope.rid = $stateParams.replyID;
});
