"use strict";

angular.module("coursesApp").factory("CoursesResource",
function (CentrisResource) {
	return {
		getCourses: function getCourses(departmentID, semester) {

			var filterString = "?expand=teachers";
			var filterObj    = {};

			if (departmentID) {
				filterString += "&department=:departmentID";
				filterObj.departmentID = departmentID;
			}

			if (semester) {
				filterString += "&semester=:semester";
				filterObj.semester = semester;
			}

			return CentrisResource.get("courses", filterString, filterObj);
		},
		getCourseRoleStatus: function getCourseRoleStatus(courseInstanceID) {
			return CentrisResource.get("courses", ":id/role", {id: courseInstanceID});
		},
		getCurrentSemester: function getCurrentSemester() {
			return CentrisResource.get("semesters/current");
		}
	};
});
