"use strict";

/**
 * CourseListController takes care of displaying a list of all courses
 * on a given semester (i.e. by default on the current semester) and in
 * a given department (by default, in the department of the user).
 */
angular.module("coursesApp").controller("CourseListController",
function CourseListController($scope, $translate, $location, CoursesResource, centrisNotify, UserService, $state) {

	$scope.department     = 0;
	$scope.searchKeywords = "";
	$scope.semester       = "";

	var department = UserService.getUserDepartment();
	if (department) {
		$scope.department     = department.ID;
	}
	var semester = CoursesResource.getCurrentSemester();
	if (semester) {
		$scope.semester = semester.ID;
	}
	var getCourses = function getCourses(department, semester) {
		$scope.loadingData = true;
		CoursesResource.getCourses(department, semester).success(function(data, status, headers, config) {
			$scope.loadingData = false;
			$scope.courses     = data;

			// Ensure each course has this property, used to determine
			// toggling of teachers list:
			angular.forEach($scope.courses, function(course) {
				course.showAllTeachers = false;
				// Sort by the first teacher:
				if (course.Teachers.length > 0) {
					course.mainTeacher = course.Teachers[0].FullName;
				}
			});
		}).error(function(data, status, headers, config) {
			$scope.loadingData = false;
			centrisNotify.error("allcourses.Messages.FailedLoadCourses");
		});
	};

	$scope.filterDepartment = function filterDepartment(department) {
		if (department !== undefined) {
			$scope.currentDepartment = department.ID;
			$location.search("department", $scope.currentDepartment);
		}

		getCourses(department.ID, $scope.semester);
	};

	$scope.filterSemester = function filterSemester(semester) {
		$location.search("semester", $scope.semester);
		getCourses($scope.department, $scope.semester);
	};

	// TODO: load/save the config for which columns to show!
	$scope.courseColumns = [
		{ visible: true,  name: "allcourses.Table_Col_Teachers" },
		{ visible: true,  name: "Language" },
		{ visible: true,  name: "allcourses.CourseSlot" },
		{ visible: false, name: "allcourses.Starts" },
		{ visible: false, name: "allcourses.Table_Col_Registered" },
		{ visible: false, name: "allcourses.Table_Col_Max_Students" },
		{ visible: false, name: "allcourses.Table_Col_Credits" }
	];

	$scope.toggleColumn = function toggleColumn(column) {
		column.visible = !column.visible;
	};

	var getTeachersInCourseAsString = function getTeachersInCourseAsString(c) {
		var str = "";
		var first = true;
		for (var i = 0; i < c.Teachers.length; ++i) {
			if (!first) {
				str += " - ";
			}
			first = false;
			str += c.Teachers[i].FullName;
		}
		return str;
	};

	var getSlotType = function getSlotType(course, icelandic) {
		// If icelandic === 1 then icelandic, else English
		if (icelandic) {
			switch (course.Slot) {
				case 1: {
					return "12 vikna";
				}
				case 2: {
					return "3ja vikna";
				}
				case 3: {
					return "15 vikna";
				}
				case 4: {
					return "6 vikna";
				}
				default: {
					return "Annað";
				}
			}
		} else {
			switch (course.Slot) {
				case 1: {
					return "12 weeks";
				}
				case 2: {
					return "3 weeks";
				}
				case 3: {
					return "15 weeks";
				}
				case 4: {
					return "6 weeks";
				}
				default: {
					return "Other";
				}
			}
		}
	};

	$scope.makeCSVHeader = function makeCSVHeader() {
		var lang = $translate.use() === "is";
		$scope.csvHeader = [];
		if (lang) {
			$scope.csvHeader.push("Auðkenni");
			$scope.csvHeader.push("Nafn");
			$scope.csvHeader.push("Kennarar");
			$scope.csvHeader.push("Tungumál");
			$scope.csvHeader.push("Tímabil");
			$scope.csvHeader.push("Fjöldi nemenda");
			$scope.csvHeader.push("Hámarksfjöldi");
			$scope.csvHeader.push("Einingar");
		} else {
			$scope.csvHeader.push("Course ID");
			$scope.csvHeader.push("Name");
			$scope.csvHeader.push("Teachers");
			$scope.csvHeader.push("Language");
			$scope.csvHeader.push("Slot");
			$scope.csvHeader.push("Number of students");
			$scope.csvHeader.push("Max students");
			$scope.csvHeader.push("Credits");
		}

		$scope.csvObject = [];
		$scope.courses.forEach(function forEachCourse(c) {
			var tmpStudent = {};

			if (lang) {
				tmpStudent["Auðkenni"]       = c.CourseID;
				tmpStudent["Nafn"]           = c.Name;
				tmpStudent["Kennarar"]       = getTeachersInCourseAsString(c);
				tmpStudent["Tungumál"]       = c.LanguageCode;
				tmpStudent["Tímabil"]        = getSlotType(c.Slot, true);
				tmpStudent["Fjöldi nemenda"] = c.RegisteredStudents;
				tmpStudent["Hámarksfjöldi"]  = c.MaxStudents;
				tmpStudent["Einingar"]       = c.Credits;
			} else {
				tmpStudent["Course ID"]          = c.CourseID;
				tmpStudent["Name"]               = c.Name;
				tmpStudent["Teachers"]           = getTeachersInCourseAsString(c);
				tmpStudent["Language"]           = c.LanguageCode;
				tmpStudent["Slot"]               = getSlotType(c.Slot, false);
				tmpStudent["Number of students"] = c.RegisteredStudents;
				tmpStudent["Max students"]       = c.MaxStudents;
				tmpStudent["Credits"]            = c.Credits;
			}

			$scope.csvObject.push(tmpStudent);
		});
	};

	// This function takes care of initializing the filtering,
	// either based on querystring values, or based on other defaults.
	var init = function init() {
		var queryString         = $location.search();
		var defaultDepartmentID = queryString.department;
		var defaultSemester     = queryString.semester;

		if (defaultSemester === undefined) {
			$scope.semester = defaultSemester;
		}

		if (defaultDepartmentID) {
			$scope.department = defaultDepartmentID;
		}
		getCourses($scope.department, $scope.semester);
	};

	// init();

	// This event is fired everytime the URL (including the query string)
	// changes. In order to ensure the back button works correctly, we must
	// reload our list accordingly.
	$scope.$on("$locationChangeSuccess", function(event, newUrl, oldUrl) {
		init();
	});
	// INTRO otions
	$scope.IntroOptions = {
		steps: [{
			element: "#aboutthelist",
			position: "left",
			intro: ""
		}, {
			element: "#searchbutton",
			position: "left",
			intro: ""
		}, {
			element: "#choosedepartment",
			position: "left",
			intro: ""
		}, {
			element: "#choosesemester",
			position: "left",
			intro: ""
		}, {
			element: "#optionsbutton",
			position: "left",
			intro: ""
		}],
		showStepNumbers:    false,
		exitOnOverlayClick: true,
		exitOnEsc:          true,
	};

	var introKeys = [
		"AngIntroAllCourses.AboutTheList",
		"AngIntroAllCourses.SearchButton",
		"AngIntroAllCourses.ChooseDepartment",
		"AngIntroAllCourses.ChooseSemester",
		"AngIntroAllCourses.OptionsButton"
	];

	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
		$scope.IntroOptions.steps[1].intro = translations[introKeys[1]];
		$scope.IntroOptions.steps[2].intro = translations[introKeys[2]];
		$scope.IntroOptions.steps[3].intro = translations[introKeys[3]];
		$scope.IntroOptions.steps[4].intro = translations[introKeys[4]];
	});

	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.$on("onCentrisIntro", function(event, args) {
		// Only show this if the current route is for our page:
		if ($state.current.name === "courselist") {
			$scope.ShowAllCoursesIntro();
		}
	});
});
