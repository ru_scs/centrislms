"use strict";

angular.module("coursesApp", [
	"pascalprecht.translate",
	"ui.router",
	"sharedServices"
]).config(function ($stateProvider) {
	$stateProvider.state("courselist", {
		url:         "/courses",
		templateUrl: "courses/components/index/index.html",
		controller:  "CourseListController"
	});
});
