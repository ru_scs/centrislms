"use strict";

angular.module("registrationApp").factory("CourseRegistrationFailureDlg",
function CourseRegistrationFailureDlg($uibModal) {
	return {
		show: function show(failures) {
			var templateInstance = {
				controller:  "CourseRegistrationFailureController",
				templateUrl: "registration/components/courseregistrationfailuredlg/CourseRegistrationFailureDlg.tpl.html",
				resolve: {
					inputParam: function() {
						return {
							failures: failures
						};
					}
				}
			};
			var modalInstance = $uibModal.open(templateInstance);
			return modalInstance.result;
		}
	};
});

angular.module("registrationApp").controller("CourseRegistrationFailureController",
function CourseRegistrationFailureController($scope, inputParam) {
	$scope.failures = inputParam.failures;
});