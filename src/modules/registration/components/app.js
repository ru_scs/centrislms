"use strict";

angular.module("registrationApp", ["pascalprecht.translate", "ui.router", "sharedServices"])
.config(function ($stateProvider) {
	$stateProvider.state("registration/", {
		url: "/registration",
		templateUrl: "registration/components/index/index.html",
		controller: "StudentCourseRegistrationController"
	});
	$stateProvider.state("graduation/", {
		url: "/registration/congratulations",
		templateUrl: "registration/components/graduationregistration/index.html",
		controller: "GraduationController"
	});
});