"use strict";

angular.module("registrationApp").controller("CourseRegistrationExemptionController",
function CourseRegistrationExemptionController($scope, inputParam) {
	$scope.model = {
		Message: ""
	};
	$scope.errors = inputParam.errors;
});
