"use strict";

angular.module("registrationApp").factory("CourseRegistrationExemptionDlg",
function CourseRegistrationExemptionDlg($uibModal) {
	return {
		show: function show(errors) {
			var modalInstance = $uibModal.open({
				controller:  "CourseRegistrationExemptionController",
				templateUrl: "registration/components/courseregistrationexemptiondlg/CourseRegistrationExemptionDlg.tpl.html",
				resolve: {
					inputParam: function() {
						return {
							errors: errors
						};
					}
				}
			});
			return modalInstance.result;
		}
	};
});