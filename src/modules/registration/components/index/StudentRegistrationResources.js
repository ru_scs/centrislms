"use strict";

angular.module("registrationApp").factory("StudentRegistrationResource",
function StudentRegistrationResource(CentrisResource) {
	return {
		getCoursesAvailableForRegistration: function getCoursesAvailableForRegistration(departmentID,regID) {
			// TODO: GET REGISTRATION ID, WHEN USER HAS MORE THAN ONE MAJOR ACTIVE
			var data = {
				departmentID: departmentID
			};
			return CentrisResource.get("my", "registration?departmentid=:departmentID", data);
		},

		getRegistrationPeriod: function getRegistrationPeriod() {
			// var mockData = {
			// 	Previous: {
			// 		Current:null,
			// 		Next:null,
			// 		Previous: {
			// 				Semester: {
			// 						ID: "20143",
			// 						Name: "Haustönn 2014",
			// 						NameEN: "Fall 2014",
			// 						DateBegin: "2015-01-01T00:00:00",
			// 						DateFinish: "2015-05-31T23:59:59",
			// 						Current: false
			// 				},
			// 				Start: "2014-05-02T08:00:00",
			// 				End: "2014-05-21T08:00:00"
			// 		}
			// 	},
			// 	Current: {
			// 		Current: {
			// 				Semester: {
			// 						ID: "20152",
			// 						Name: "Sumarönn 2015",
			// 						NameEN: "Summer 2015",
			// 						DateBegin: "2015-01-01T00:00:00",
			// 						DateFinish: "2015-05-31T23:59:59",
			// 						Current: false
			// 				},
			// 				Start: "2015-05-02T08:00:00",
			// 				End: "2015-05-21T08:00:00"
			// 		},
			// 		Next: null,
			// 		Previous:  null
			// 	},
			// 	Next: {
			// 		Current: null,
			// 		Next: {
			// 				Semester: {
			// 						ID: "20153",
			// 						Name: "Haustönn 2015",
			// 						NameEN: "Fall 2015",
			// 						DateBegin: "2015-01-01T00:00:00",
			// 						DateFinish: "2015-05-31T23:59:59",
			// 						Current: false
			// 				},
			// 				Start: "2015-06-02T08:00:00",
			// 				End: "2015-06-21T08:00:00"
			// 		},
			// 		Previous:  null
			// 	}
			// };
			// return {
			// 	success: function(fn) {
			// 		fn(mockData.Current);
			// 	}
			// 	//success: function(mockData.Next)
			// 	//success: function(mockData.Previous)
			// };
			return CentrisResource.get("semesters", "registration");
		},

		postStudentToCourse: function postStudentToCourse(courseList, regID) {
			var data = {
				CourseInstanceIDs: []
			};
			angular.forEach(courseList, function(course) {
				if (course.Type === "graduate") {
					// TODO: post request
				} else {
					data.CourseInstanceIDs.push(course.CourseInstanceID);
				}
			});
			return CentrisResource.post("my", "registration", null, data);
		},

		canGraduate: function canGraduate (registrationID) {
			var param = {
				registrationID: registrationID
			};

			return CentrisResource.get("my", "registration/graduation/check/?registrationID=:registrationID", param);
		}
	};
});
