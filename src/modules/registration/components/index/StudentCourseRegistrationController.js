"use strict";

angular.module("registrationApp").controller("StudentCourseRegistrationController",
function StudentCourseRegistrationController($scope, $translate, $location,
	StudentRegistrationResource, CourseRegistrationExemptionDlg,
	CourseRegistrationFailureDlg, centrisNotify, dateTimeService,
	IssuesResource, UserService,$state) {

	// A list of all courses available for enrollment, for a given department (and a given semester)
	$scope.courseList = [];

	// Boolean which comes true if student is graduating
	$scope.isGraduating = false;

	// A list of the courses which the student has selected, and plans to
	// enroll in the next semester.
	$scope.selectedCourses        = [];
	$scope.selectedCoursesFetched = false;

	// A list of courses which cannot be enrolled into, unless the student
	// is also enrolled into another class at the same time.
	$scope.concurrentClasses = [];

	// TODO: When login, change to logged in student departmentID
	$scope.departmentID = 1;

	// The semester the registration is for
	$scope.semester = {};

	// The reason why course is not available,
	$scope.toolTipText = "";

	// Text to display when registration is not open
	$scope.registrationMessage = "";

	// Registration period
	$scope.regStart = {};
	$scope.regEnd = {};

	// Is registration currently open
	$scope.regOpen = false;

	// Display correct message based on if the data is still loading or
	// if there is no data
	$scope.loading = true;

	// Holds information about if a student can graduate
	$scope.graduate = {};

	// In sync with API.Models/Curriculum/Prerequisites/PrerequisiteFailureReason.cs
	$scope.errorCodes = [
		// TODO: add error code for graduation
		{errorCode: 1, error: "CourseRegistration.Error.None"},
		{errorCode: 2, error: "CourseRegistration.Error.InsufficientCredits"},
		{errorCode: 3, error: "CourseRegistration.Error.InsufficientAverageGradeForCredits"},
		{errorCode: 4, error: "CourseRegistration.Error.CourseNotEnrolledIn"},
		{errorCode: 5, error: "CourseRegistration.Error.CourseNotCompleted"},
		{errorCode: 6, error: "CourseRegistration.Error.CourseNotConcurrent"},
		{errorCode: 7, error: "CourseRegistration.Error.AnotherCourseHasBeenCompleted"},
		{errorCode: 101, error: "CourseRegistration.Error.NotEnoughETCS"},
		{errorCode: 102, error: "CourseRegistration.Error.NotCompletedMandatory"}
	];

	$scope.table = [
		{name: "Registration.Table.Name",          visible: true,    alias: "Name"},
		{name: "Registration.Table.CourseID",      visible: true,    alias: "Course template"},
		{name: "Registration.Table.Credits",       visible: true,    alias: "Credits"},
		{name: "Registration.Table.Icon",          visible: true,    alias: "Icon"},
		{name: "Registration.Table.Semester",      visible: true,    alias: "Semester"}
	];

	$scope.initGetRegistrationPeriod = function initGetRegistrationPeriod() {
		StudentRegistrationResource.getRegistrationPeriod()
		.success(function(data) {
			if (data.Current) {
				$scope.registrationMessage = "CourseRegistration.Msg.RegistrationOpen";
				$scope.regStart = data.Current.Start;
				$scope.regEnd = data.Current.End;
				$scope.regOpen = true;
				$scope.semester = data.Current.Semester;
			} else if (data.Next) {
				$scope.registrationMessage = "CourseRegistration.Msg.RegistrationNotYetOpen";
				$scope.regStart = data.Next.Start;
				$scope.regEnd = data.Next.End;
				$scope.semester = data.Next.Semester;
			} else if (data.Previous) {
				$scope.registrationMessage = "CourseRegistration.Msg.RegistrationElapsed";
				$scope.regStart = data.Previous.Start;
				$scope.regEnd = data.Previous.End;
				$scope.semester = data.Previous.Semester;
			}
			$scope.regStart = moment($scope.regStart).format("D. MMMM YYYY, h:mm");
			$scope.regEnd = moment($scope.regEnd).format("D. MMMM YYYY, h:mm");
		})
		.error(function (error) {
			centrisNotify.error(error);
		});
	};

	// Call this function at startup (and we probably don't need it afterwards...)
	$scope.initGetRegistrationPeriod();

	$scope.init = function() {
		// empty courseList and set loading to true
		$scope.courseList = [];
		$scope.loading = true;
		$scope.graduate = {};
		$scope.gradObj = {};

		// Check if student can graduate
		StudentRegistrationResource.canGraduate($scope.registrationID)
		.success(function (data) {
			$scope.graduate = data;
			$scope.graduate.FailureReasons = "Cant";
			console.log($scope.graduate.FailureReasons);
			if ($scope.graduate.CanGraduate) {
				//
				// $scope.gradObj = {
				// 	Name: "Útskrift " + $scope.semester.Name,
				// 	NameEn: "Graduation " + $scope.semester.NameEN,
				// 	Semester: 6,
				// 	Type: "graduate",
				// 	Failures: $scope.graduate.FailureReasons
				// };
			}

			// TODO: translate!
			$scope.gradObj = {
				Name: "Útskrift " + $scope.semester.Name,
				NameEn: "Graduation " + $scope.semester.NameEN,
				Semester: 6,
				Type: "graduate",
				Failures: $scope.graduate.FailureReasons
			};
		}).error(function (error) {
			centrisNotify.error(error);
		});

		// Do not fetch available courses because registration is not open.
		if (!$scope.regOpen) {
			return;
		}

		// Get the courses that are available by department
		StudentRegistrationResource.getCoursesAvailableForRegistration($scope.departmentID, $scope.registrationID)
		.success(function (data) {
			$scope.courseList = data.AvailableCourses;

			// Use the data from the server only once, if the student
			// switches departments from the dropdown then we want to
			// keep the changes the student made
			if (!$scope.selectedCoursesFetched) {
				$scope.selectedCourses = data.EnrolledCourses;
				$scope.selectedCoursesFetched = true;
			}

			for (var i = 0; i < $scope.courseList.length; i++) {
				// If the user changes the department we need to keep track of what courses
				// (s)he has picked and make sure it's marked as selected
				// so (s)he cannot enlist in a course twice.
				for (var j = 0; j < $scope.selectedCourses.length; j++) {
					if ($scope.selectedCourses[j].CourseInstanceID === $scope.courseList[i].CourseInstanceID) {
						$scope.courseList[i].IsSelected = true;
					}
				}
				$scope.courseList[i].Failures = undefined;
				// Course has no failure, i.e. there is nothing that prevents
				// the student from enrolling into the given course.
				if ($scope.courseList[i].Failures.length <= 0) {
					continue;
				}

				// Finding index of failure with errorcode 6(concurrentCourse). Chould be more than one failure.
				var indexOfConCurrentFailure = $scope.courseList[i].Failures.map(function(e) {
					return e.ReasonCode;
				}).indexOf($scope.errorCodes[5].errorCode);

				// If found, we store the CourseID of the course which must be enrolled in at the same time
				// (see below how it is used).
				if (indexOfConCurrentFailure > -1) {
					var obj = {
						FailureClass:    $scope.courseList[i].CourseID,
						ConcurrentClass: $scope.courseList[i].Failures[indexOfConCurrentFailure].Reason,
						indexOfCourse:   i,
						indexOfFailure:  indexOfConCurrentFailure,
						ReasonCode:      $scope.errorCodes[5].errorCode
					};
					$scope.concurrentClasses.push(obj);
				}
			}

			// Loading finished
			$scope.loading = false;
			// If student can graduate, then it should be the first item
			if ($scope.graduate.CanGraduate) {
				$scope.courseList.unshift($scope.gradObj);
			} else {
				$scope.courseList.push($scope.gradObj);
			}
		}).error(function (data, status, headers, config) {
			centrisNotify.error(data);
		});
	};

	$scope.RegistrationExemption = function RegistrationExemption(course) {
		// we need the reason why the user can not be registered.
		// errorCodeToText finds out why and then call startModal function.
		// This is necessary because we need to translate the errorcodes and that requires
		// a callback function.
		$scope.errorCodeToText(course.Failures, true, course);
	};

	// Note: this is an internal function!
	var startModal = function startModal(errors, course) {
		CourseRegistrationExemptionDlg.show(errors)
		.then(function (data) {
			var department = UserService.getUserDepartment();
			var departmentID = 0;
			if (department) {
				departmentID = department.ID;
			}

			var issue = {
				issuerSSN:   UserService.getUserSSN(),
				issuerName:  UserService.getFullName(),
				description: data.Message,
				typeName:    "applyCourseNoPrerequisites",
				department:  departmentID,
				data: {
					courseInstanceID: course.CourseInstanceID,
					name: course.Name,
					nameEN: course.NameEN
				}
			};

			IssuesResource.postIssue(issue).success(function() {
				centrisNotify.success("CourseRegistration.ExceptionRequestSent");
			}).error(function() {
				centrisNotify.success("CourseRegistration.ExceptionRequestNotSent");
			});
		});
	};

	// Called when a course is checked/unchecked by the user.
	// We must process this separately, both to store the course
	// in our "basket", as well as to keep track of the courses
	// which cannot be enrolled into unless being enrolled in another
	// course simultaneously.
	$scope.onCheckCourse = function onCheckCourse(course) {
		if (course.IsSelected) {
			$scope.selectedCourses.push(course);
		} else {
			$scope.removeSelectedCourse(course);
		}

		// Checks if the students must take this class concurrently with some other class.
		concurrentRegistration(course);
	};

	// Removes course from selectedCourse
	$scope.removeSelectedCourse = function removeSelectedCourse(course) {
		var index = $scope.selectedCourses.map(function(e) {
			return e.CourseInstanceID;
		}).indexOf(course.CourseInstanceID);

		if (index > -1) {
			$scope.selectedCourses.splice(index, 1);
		}
		course.IsSelected = false;
		concurrentRegistration(course);
	};

	// Note: this is an internal function!
	// It checks if the course A that was (un)checked has any courses B that the user
	// need to take simultaneously with the course A.
	// If so, we need to remove that failure from the course B and make him avalible for the user.
	// If the User was unmarking the course A we need to add the failure back and remove the course B
	// from the selectedCourse list if he was alredy selected.
	var concurrentRegistration = function concurrentRegistration(course) {
		var concurrentIndex = $scope.concurrentClasses.map(function(e) {
			return e.ConcurrentClass;
		}).indexOf(course.CourseID);

		// If no course found in concurrentClasses
		if (concurrentIndex <= -1) {
			return;
		}

		var indexOfCourse = $scope.concurrentClasses[concurrentIndex].indexOfCourse;
		var indexOfFailure = $scope.concurrentClasses[concurrentIndex].indexOfFailure;

		// Removes failure from course.
		if (course.IsSelected) {
			$scope.courseList[indexOfCourse].Failures.splice(indexOfFailure,1);
		} else {
			var index = $scope.selectedCourses.indexOf($scope.courseList[indexOfCourse]);
			// remove class with failure from selected course
			if (index > -1) {
				$scope.selectedCourses.splice(index, 1);
				$scope.courseList[indexOfCourse].IsSelected = false;
			}

			var obj = {
				Reason:     $scope.concurrentClasses[concurrentIndex].ConcurrentClass,
				ReasonCode: $scope.concurrentClasses[concurrentIndex].ReasonCode
			};

			// adding failure back to course.
			$scope.courseList[indexOfCourse].Failures.push(obj);
		}
	};

	// Check if student chose the graduate course.
	$scope.isGraduating = function isGraduating() {
		for (var i = 0; i < $scope.selectedCourses.length; i++) {
			// Check if any of the selected courses is graduation
			if ($scope.selectedCourses[i].Type === "graduate") {
				submitGraduation();
			}
		}
		// If student isn't graduating then the courses will be submitted to the database
		submit();
	};

	var submit = function submit() {
		StudentRegistrationResource.postStudentToCourse($scope.selectedCourses, $scope.registrationID)
		.success(function (data) {
			if (data.Failed.length === 0) {
				centrisNotify.success("CourseRegistration.NotifyRegistrationSuccess");
				// $location.path("/profile/studies");
			} else {
				CourseRegistrationFailureDlg.show(data.Failed);
				angular.forEach(data.Succeeded, function(course) {
					course.CourseInstanceID = course.ID;
					$scope.removeSelectedCourse(course);
				});
			}
		})
		.error(function (error) {
			centrisNotify.error(error);
		});
	};

	// If student choice the graduate course check if he can graduate
	var submitGraduation = function submitGraduation() {
		// if student can graduate redirect to the congratulations page
		if ($scope.graduate.CanGraduate) {
			// $location.path("/registration/congratulations");
			// TODO: Logic to let the database know that the student is graduating.
		}
		// This is here just for testing purposes
		$location.path("/registration/congratulations");
	};

	// Change the toolTipText to correct failure text for given course.
	// The course registrationExemption modal and toolTip uses this function
	// and the parameter isModal is boolean
	$scope.errorCodeToText = function errorCodeToText(failures, isModal, course) {
		$scope.toolTipText = "";
		// We need to store the errors in array
		// one course could have more than one error
		var errors = [];

		for (var i = 0; i < failures.length; i++) {
			var index = $scope.errorCodes.map(function(e) {
				return e.errorCode;
			}).indexOf(failures[i].ReasonCode);
			errors.push($scope.errorCodes[index].error);
		}

		// Translate all errorCodes for given course
		$translate(errors).then(function whenDoneTranslating(trans) {
			var ErrorsCode = [];
			for (var i = 0; i < errors.length; i++) {
				if (trans.hasOwnProperty(errors[i])) {
					$scope.toolTipText += trans[errors[i]] + failures[i].Reason + "  ";
					// If the modal is calling this function we push the reasons why the user can not
					// be registered in the course to array which we then send as a parameter when lunching the modal.
					if (isModal) {
						ErrorsCode.push(trans[errors[i]] + failures[i].Reason);
					}
				}
			}

			if (isModal) {
				// Call the startModal function to launch the modal.
				// see why we do that in RegistrationExemption function. hint: callback
				startModal(ErrorsCode, course);
			}
		});
	};

	// Filters the courses by department
	$scope.filterDepartment = function filterDepartment(department) {
		// If no department object is passed in, the default department id is used
		if (department !== undefined) {
			$scope.departmentID = department.ID;
			$scope.init();
		}
	};
	// INTRO otions
	$scope.IntroOptions = {
		steps: [{
			element: "#aboutregistration",
			position: "left",
			intro: ""
		}, {
			element: "#registrationopenorclosed",
			position: "left",
			intro: ""
		}, {
			element: "#choosedepartment",
			position: "right",
			intro: ""
		}, {
			element: "#listofcourses",
			position: "left",
			intro: ""
		}, {
			element: "#iconinregistration",
			position: "left",
			intro: ""
		}, {
			element: "#subscribeorunsubscribe",
			position: "left",
			intro: ""
		}, {
			element: "#confirmregistration",
			position: "left",
			intro: ""
		}],
		showStepNumbers:    false,
		exitOnOverlayClick: true,
		exitOnEsc:          true,
	};

	var introKeys = [
		"AngIntroRegistration.AboutRegistration",
		"AngIntroRegistration.RegistrationOpenOrClosed",
		"AngIntroRegistration.ChooseDepartment",
		"AngIntroRegistration.ListOfCourses",
		"AngIntroRegistration.IconInRegistration",
		"AngIntroRegistration.SubscribeOrUnsubscribe",
		"AngIntroRegistration.ConfirmRegistration"
	];

	$translate(introKeys).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.steps[0].intro = translations[introKeys[0]];
		$scope.IntroOptions.steps[1].intro = translations[introKeys[1]];
		$scope.IntroOptions.steps[2].intro = translations[introKeys[2]];
		$scope.IntroOptions.steps[3].intro = translations[introKeys[3]];
		$scope.IntroOptions.steps[4].intro = translations[introKeys[4]];
		$scope.IntroOptions.steps[5].intro = translations[introKeys[5]];
		$scope.IntroOptions.steps[6].intro = translations[introKeys[6]];
	});

	var introButtons = [
		"AngularIntro.Next",
		"AngularIntro.Prev",
		"AngularIntro.Skip",
		"AngularIntro.Done"
	];

	$translate(introButtons).then(function whenDoneTranslating(translations) {
		$scope.IntroOptions.nextLabel = translations[introButtons[0]];
		$scope.IntroOptions.prevLabel = translations[introButtons[1]];
		$scope.IntroOptions.skipLabel = translations[introButtons[2]];
		$scope.IntroOptions.doneLabel = translations[introButtons[3]];
	});

	$scope.$on("onCentrisIntro", function(event, args) {
		// Only show this if the current route is for our page:
		if ($state.current.name === "registration/") {
			$scope.ShowRegistrationIntro();
		}
	});
});
