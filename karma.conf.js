"use strict";
// Karma configuration
// Generated on Fri May 02 2014 15:35:51 GMT+0000 (GMT)

module.exports = function(config) {
	config.set({

		// base path that will be used to resolve all patterns (eg. files, exclude)
		basePath: '',

		// frameworks to use
		// available frameworks: https://npmjs.org/browse/keyword/karma-adapter
		frameworks: ['jasmine'],

		// list of files / patterns to load in the browser
		files: [
			// First, we specify the files which other files depend upon
			// (i.e. jQuery, Angular, Moment etc.)
			'src/vendor/jquery/jquery.js',
			'src/vendor/lodash/lodash.js',
			'src/vendor/moment/moment.js',
			'src/vendor/angular/angular.js',
			// From here on, files should be in alphabetical order.
			// If some file A depends on file B to be included first, then
			// it should be in the dependency section above!
			'src/vendor/angular-animate/angular-animate.js',
			'src/vendor/angular-bootstrap/ui-bootstrap-tpls.js',
			'src/vendor/angular-bootstrap3-datepicker/dist/ng-bs3-datepicker.js',
			'src/vendor/angular-elastic/elastic.js',
			'src/vendor/angular-fcsa-number/src/fcsaNumber.js',
			'src/vendor/angular-intro.js/src/angular-intro.js',
			'src/vendor/angular-local-storage/angular-local-storage.js',
			'src/vendor/angular-mocks/angular-mocks.js',
			'src/vendor/angular-native-dragdrop/draganddrop.js',
			'src/vendor/angular-responsive-tables/release/angular-responsive-tables.min.js',
			'src/vendor/angular-sanitize/angular-sanitize.js',
			'src/vendor/angular-toastr/dist/angular-toastr.js',
			'src/vendor/angular-translate/angular-translate.js',
			'src/vendor/angular-translate-storage-cookie/angular-translate-storage-cookie.js',
			'src/vendor/angular-ui-router/release/angular-ui-router.js',
			'src/vendor/angular-ui-utils/ui-utils.js',
			'src/vendor/angular-ui/build/angular-ui.js',
			'src/vendor/angular-ui-select/dist/select.js',
			'src/vendor/Chart.js/Chart.js',
			'src/vendor/angular-chart.js/angular-chart.js',
			'src/vendor/elastic-js/dist/elastic-angular-client.js',
			'src/vendor/markdown-editpreview-ng/lib/codemwnci/markdown-editpreview-ng.js',
			'src/vendor/ng-csv/build/ng-csv.min.js',
			'src/vendor/textAngular/dist/textAngular-sanitize.min.js',
			'src/vendor/textAngular/dist/textAngular-rangy.min.js',
			'src/vendor/textAngular/dist/textAngular.min.js',
			'src/vendor/angular-youtube-mb/src/angular-youtube-embed.js',
			'src/vendor/ng-videosharing-embed/build/ng-videosharing-embed.min.js',
			'src/vendor/ng-file-upload/ng-file-upload.min.js',

			// Finally, our local source files:
			'src/app.js',
			'mockConstants.js',
			'src/common/**/*.js',
			'src/modules/**/app.js',
			'src/modules/**/*.+(html|js)',
			'src/index.html'
		],

		// list of files to exclude
		exclude: [
			'src/vendor/angular**/*.min.js'
		],

		// preprocess matching files before serving them to the browser
		// available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
		preprocessors: {
			'src/modules/**/*.html': 'html2js',
			'src/index.html': 'html2js',
			'src/app.js': 'coverage',
			'src/modules/**/*.js': 'coverage',
			'src/common/**/*.js': 'coverage'
		},

		// test results reporter to use
		// possible values: 'dots', 'progress'
		// available reporters: https://npmjs.org/browse/keyword/karma-reporter
		reporters: ['progress', 'coverage'],

		usePolling: true,

		// web server port
		port: 9999,

		// enable / disable colors in the output (reporters and logs)
		colors: true,

		// level of logging
		// possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
		logLevel: config.LOG_INFO,

		// enable / disable watching file and executing tests whenever any file changes
		autoWatch: true,

		// start these browsers
		// available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
		browsers: ['PhantomJS'],

		// Continuous Integration mode
		// if true, Karma captures browsers, runs the tests and exits
		singleRun: false,

		coverageReporter: {
			type : 'html',
			dir : 'coverage/'
		}
	});
};
