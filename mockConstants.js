/*
 * This file is only used to mock this module because it's not available until after grunt build.
*/

"use strict";
 angular.module("mockConfig", [])

.constant("ENV", {
	"name": "server",
	"apiEndpoint": "http://localhost:19357/api/v1/",
	"issuesapiEndpoint": "http://centris.dev.nem.ru.is/issues/",
	"genesisEndpoint": "",
	"genesisMVC": "",
	"gradesEndpoint": "http://centris.dev.nem.ru.is/grades/",
	"searchEndpoint": "http://leit.dev.nem.ru.is:9200",
	"signalREndpoint": "http://centris.dev.nem.ru.is/slr/signalr",
	"staticFileEndpoint": "http://centris.dev.nem.ru.is/static/",
	"elasticSearchIndex": "centris"
});