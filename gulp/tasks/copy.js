"use strict";

const gulp   = require("gulp");
const util   = require("gulp-util");
const bower  = require("main-bower-files");
const config = require("../config");
const assets = ["src/assets/styles/**/*.*", "!src/assets/styles/**/*.css"];

gulp.task("copy", () => {
	gulp.src(config.paths.fonts.concat(bower("**/fonts/*"))).pipe(gulp.dest(config.paths.target + "/fonts"));
	gulp.src(config.paths.audio).pipe(gulp.dest(config.paths.target + "/assets/audio"));
	gulp.src("src/common/assets/**/*.png").pipe(gulp.dest(config.paths.target + "/assets"));

	// Required due to centris-person-image. TODO: find a common default avatar and put it in src/common/assets!
	gulp.src("src/assets/styles/img/avatar.png").pipe(gulp.dest(config.paths.target + "/assets/img"));
	gulp.src(assets).pipe(gulp.dest(config.paths.target + "/styles"));
    
    // Copy the assignment annotator files
    // tmp/**/*.* was not enough to get all the subdirectories, so that's why we have the 3 extra lines. 
    
    gulp.src("pdf/**/*.*") 
        .pipe(gulp.dest(config.paths.target + "/pdf"));

    // Copy the ace editor folder
    gulp.src("ace/**/*.*") 
        .pipe(gulp.dest(config.paths.target + "/ace"));
    //gulp.src("tmp/web/images/*.*")
    //    .pipe(gulp.dest(config.paths.target + "/pdf/web/images"));
    //gulp.src("tmp/external/**/*.*")
    //    .pipe(gulp.dest(config.paths.target + "/pdf/external"));
    //gulp.src("tmp/node_modules/**/*.*")
    //    .pipe(gulp.dest(config.paths.target + "/pdf/node_modules")); 
});
gulp.task("copyPdf", () => {
	// Copy the assignment annotator files
    // tmp/**/*.* was not enough to get all the subdirectories, so that's why we have the 3 extra lines. 
    
    gulp.src("pdf/**/*.*") 
        .pipe(gulp.dest(config.paths.target + "/pdf"));
});

gulp.task("copyAce", () => {
    // Copy the ace editor folder
    gulp.src("ace/**/*.*") 
        .pipe(gulp.dest(config.paths.target + "/ace"));
});