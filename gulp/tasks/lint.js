"use strict";

const gulp   = require("gulp");
const jshint = require("gulp-jshint");
const jscs   = require('gulp-jscs');
const config = require("../config");

gulp.task("jshint", () => {
	return gulp.src(config.paths.lint)
		.pipe(jshint(".jshintrc"))
		.pipe(jshint.reporter("jshint-stylish"))
		.pipe(jshint.reporter("fail"))
		.pipe(jscs())
		.pipe(jscs.reporter())
		.pipe(jscs.reporter('fail'))
});
