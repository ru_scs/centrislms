"use strict";

const args         = require("yargs").argv;
const isProduction = args.production;
const noSync       = args.nosync;

module.exports = {
	port: 9000,

	env: {
		global: {
			name:               "server",
			apiEndpoint:        "http://centris.dev.nem.ru.is/api/api/v1/",
			issuesapiEndpoint:  "http://centris.dev.nem.ru.is/issues/",
			genesisEndpoint:    "http://localhost:4803/api/",
			genesisMVC:         "http://localhost:4803/",
			gradesEndpoint:     "http://centris.dev.nem.ru.is/grades/",
			searchEndpoint:     "http://leit.dev.nem.ru.is:9200/",
			signalREndpoint:    "http://centris.dev.nem.ru.is/slr/signalr/",
			staticFileEndpoint: "http://centris.dev.nem.ru.is/static/",
			profileRequests:    "http://centris.dev.nem.ru.is/static/personrequests/",
			elasticSearchIndex: "centris",
			mooshakApi:         "http://188.166.148.75:7810/"
		},
		local:  {
			name:               "local",
			apiEndpoint:        "http://localhost:19357/api/v1/",
			issuesapiEndpoint:  "http://localhost:3000/issues/",
			genesisEndpoint:    "http://localhost:4803/api/",
			genesisMVC:         "http://localhost:4803/",
			gradesEndpoint:     "http://localhost:3000/",
			searchEndpoint:     "http://leit.dev.nem.ru.is:9200/",
			signalREndpoint:    "http://localhost:20764/signalr/",
			staticFileEndpoint: "http://centris.dev.nem.ru.is/static/",
			profileRequests:    "http://localhost:9001/static/personrequests/",
			elasticSearchIndex: "centris",
			mooshakApi:         "http://188.166.148.75:7810/"
		}
	},

	cache: {
		root:   "",
		module: "centrislms"
	},

	prod: isProduction,
	noSync: noSync,

	paths: {
		target:  isProduction ? "dist" : ".tmp",
		scripts: [
			"src/**/*.js",
			"!src/vendor/**/*.*",
			"!src/assets/**/*.*",
			// Exclude all testing files from what will be loaded
			// in our index.html:
			"!src/**/*.spec.js",
			"!src/**/*-spec.js",
			"!src/**/*.mock.js"
		],
		lint:    [
			"src/**/*.js",
			"!src/vendor/**/*.*",
			"!src/assets/**/*.*",
			// Will not be a part of the bundle but we need correct coding rules 
			"ace/src/**/*.js"
		],
		tests:   [
			"src/**/*-spec.js"
		],
		styles:  [
			"src/app.less",
			"src/common/**/*.less",
			"src/modules/**/*.less",
			"!src/modules/**/_*.less"
		],
		fonts:   [
			"src/vendor/font-awesome/fonts/**/*.*"
		],
		audio:   [
			"src/assets/audio/**/*.*"
		],
		html:    "src/modules/**/*.html",
		common:  "src/common/**/*.html"
	}
};
