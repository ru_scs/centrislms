Centris LMS
========================

This repository holds the LMS (Learning Management System) HTML client for Centris.

## Quick Start

Ensure Node.js is installed. We assume the following node tools are installed globally:

* npm
```sh
$ npm -g install babel-cli
$ npm -g install karma-cli
$ npm -g install gulp
$ npm -g install bower
```

* yarn
```sh
$ yarn global add babel-cli
$ yarn global add karma-cli
$ yarn global add gulp
$ yarn global add bower
```
  [Yarn Documentation](https://yarnpkg.com/en/docs/)

  [Yarn vs Npm Commands](https://yarnpkg.com/en/docs/migrating-from-npm#toc-cli-commands-comparison)


 Then you should clone the repository into a folder, but beware that the folder path should NOT contain any non-ASCII characters (such as Icelandic characters). Do the following:

```sh
$ git clone git@bitbucket.org:ru_scs/centrislms.git
$ cd centrislms
$ npm install
$ bower install
$ git submodule update --init
$ gulp
```

If you're going to install a bower package, please do:
```sh
$ bower install package_name --save-dev
```

Note that when executing gulp with no parameters, the project will connect to the global API instance. If you've got a local instance running (i.e. you have Visual Studio installed and have started debugging there), you can connect to that by using the command:

```sh
$ gulp --local
```

This project uses the browsersync extension to ensure changes to the code are reflected to the browser instantly. However, if you need to open two different browser windows (i.e. one Chrome and one Firefox), with different users in each browser and different pages open in each one, browsersync can get in the way. You can disable it using the --nosync option:

```sh
$ gulp --nosync
```

These options can be combined as appropriate.

## Gulp
The gulp file has several tasks for you to use.
Most of the time however you will simply be using:
```sh
$ gulp
```
This will compile the less files and create a webserver which listens on port 9000 for your viewing pleasure. To view the index site, simply go to http://localhost:9000/#/.
It also has livereload so you won't have to refresh constantly to see your changes.

## Tests
To execute tests, run the following command:
```sh
$ karma start
```
In order to see coverage simply hop over to http://localhost:9000/coverage which will serve up the coverage information.

## Building
In order to build a production, the following command should be used:
```sh
$ gulp index --production
```

## Heads up !

If you're on Windows, you might get a error that says something like:

```sh
$ Error: EPERM, rename...
```

This is a bug in Bower and a temporary fix is just to install older version of Bower (1.2.6):

```sh
$ npm install -g bower@1.2.6
```
Some Mac users have experienced errors regarding file limits. This is because mac by default only support 256 open files.
To remedy this do:
```sh
$ ulimit -n 1024
```
This increases the limit to 1024

## Install tips

If you get an authentication error while you try to update the submodule with *git submodule update --init*
be sure you have configured a SSH key for your bitbucket account detailed instructions on how to perform this can be found [here](https://confluence.atlassian.com/display/BITBUCKET/How+to+install+a+public+key+on+your+Bitbucket+account;jsessionid=C1D87086AF77E7888F04C218EE38EFEC.node2)